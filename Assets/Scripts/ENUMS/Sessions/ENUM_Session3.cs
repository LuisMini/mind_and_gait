﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using UnityEngine;

public enum ENUM_Session3
{
    [Description(/*"Start Screen"*/)]
    StartScreen,
    [Description("Cartão de Orientação")]
    OrientationCard,
    [Description("Em que dia é a festa do Bailarico?")]
    OrientationCard_Date,
    [Description("Onde é a festa do Bailarico?")]
    OrientationCard_Location,
    [Description("Qual é a morada para resposta ao convite?")]
    OrientationCard_Adress,
    [Description("A que horas começa a festa do bailarico?")]
    OrientationCard_HourBegin,
    [Description("A que horas acaba a festa do Bailarico?")]
    OrientationCard_HourEnd,
    [Description("O que tenho que levar?")]
    OrientationCard_QueLevar,
    [Description("Cartão de Orientação")]
    OrientationCard_Complete,
    [Description("Encontre os pares e some as quantias")]
    Activity_MemoryGame,
    [Description("Ordene as quantias de forma decrescente")]
    Activity_CorrectOrder,
    [Description("Escolher um orçamento para a festa")]
    Activity_Orcamento,
    [Description("Complete o provérbio com as imagens")]
    Saying,
    [Description("A sessão para mim foi...")]
    Review_Difficulty,
    [Description("Gostou da sessão?")]
    Review_Like,
    [Description("Lista de acontecimentos")]
    Summary,
    [Description("Nesta sessão trabalhou os domínios neurocognitivos:")]
    Domains
};

