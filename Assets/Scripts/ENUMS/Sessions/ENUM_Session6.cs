﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using UnityEngine;

public enum ENUM_Session6
{
    [Description(/*"Start Screen"*/)]
    StartScreen,
    [Description("Cartão de Orientação")]
    OrientationCard,
    [Description("Em que dia é a festa do Bailarico?")]
    OrientationCard_Date,
    [Description("Onde é a festa do Bailarico")]
    OrientationCard_Location,
    [Description("Qual é a morada para resposta ao convite?")]
    OrientationCard_Adress,
    [Description("A que horas começa a festa do Bailarico?")]
    OrientationCard_HourBegin,
    [Description("A que horas acaba a festa do Bailarico?")]
    OrientationCard_HourEnd,
    [Description("O que tenho que levar?")]
    OrientationCard_QueLevar,
    [Description("Qual é o meu orçamento?")]
    OrientationCard_Orcamento,
    [Description("Qual é a minha sobremesa?")]
    OrientationCard_Dessert,
    [Description("Cartão de Orientação")]
    OrientationCard_Complete,
    [Description("Recebeu uma mensagem no seu telefone")]
    Activity_PhoneMessage,
    [Description("Qual a cor do chapéu que tem de personalizar?")]
    Activity_HatColor,
    [Description("Escolha um emblema para personalizar o chapéu")]
    Activity_ChooseEmblem,
    [Description("Recorde a figura anterior e contorne o que está em falta")]
    Activity_CreateEmblem,
    [Description("Personalizar o emblema para o chapéu")]
    Activity_CustomizeEmblem,
    [Description("Complete o provérbio")]
    Saying,
    [Description("A sessão para mim foi...")]
    Review_Difficulty,
    [Description("Gostou da sessão?")]
    Review_Like,
    [Description("Lista de acontecimentos")]
    Summary,
    [Description("Nesta sessão trabalhou os domínios neurocognitivos:")]
    Domains
};

