﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using UnityEngine;

public enum ENUM_Session2
{
    [Description(/*"Start Screen"*/)]
    StartScreen,
    [Description("Cartão de Orientação")]
    OrientationCard,
    [Description("Em que dia é a festa do Bailarico?")]
    OrientationCard_Date,
    [Description("Onde é a festa do Bailarico?")]
    OrientationCard_Location,
    [Description("Qual é a morada para resposta ao convite?")]
    OrientationCard_Adress,
    [Description("A que horas começa a festa do Bailarico?")]
    OrientationCard_HourBegin,
    [Description("A que horas acaba a festa do Bailarico?")]
    OrientationCard_HourEnd,
    [Description("Cartão de Orientação")]
    OrientationCard_Complete,
    [Description("Meios de transporte...")]
    Activity_PickCorrect1,
    [Description("Meios de transporte...")]
    Activity_PickCorrect2,
    [Description("Meios de transporte...")]
    Activity_PickCorrect3,
    [Description("Meios de transporte...")]
    Activity_PickCorrect4,
    [Description("Meios de transporte...")]
    Activity_PickCorrect5,
    [Description("Meios de transporte...")]
    Activity_PickCorrect6,
    [Description("Meios de transporte de 2 rodas")]
    Activity_PickCorrectFinal,
    [Description("Qual o caminho mais curto para enviar a carta?")]
    Activity_SendLetter,
    [Description("Escolha o provérbio")]
    Saying,
    [Description("A sessão para mim foi...")]
    Review_Difficulty,
    [Description("Gostou da sessão?")]
    Review_Like,
    [Description("Lista de acontecimentos")]
    Summary,
    [Description("Nesta sessão trabalhou os domínios neurocognitivos:")]
    Domains
};

