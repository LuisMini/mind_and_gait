﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using UnityEngine;

public enum ENUM_Session8
{
    [Description(/*"Start Screen"*/)]
    StartScreen,
    [Description("Cartão de Orientação")]
    OrientationCard,
    [Description("Em que dia é a festa do Bailarico?")]
    OrientationCard_Date,
    [Description("Onde é a festa do Bailarico")]
    OrientationCard_Location,
    [Description("Qual é a morada para resposta ao convite?")]
    OrientationCard_Adress,
    [Description("A que horas começa a festa do Bailarico?")]
    OrientationCard_HourBegin,
    [Description("A que horas acaba a festa do Bailarico?")]
    OrientationCard_HourEnd,
    [Description("O que tenho que levar?")]
    OrientationCard_QueLevar,
    [Description("Qual é o meu Orçamento?")]
    OrientationCard_Orcamento,
    [Description("Qual é a minha sobremesa?")]
    OrientationCard_Dessert,
    [Description("Qual é o emblema que criei?")]
    OrientationCard_Emblem,
    [Description("Cartão de Orientação")]
    OrientationCard_Complete,
    [Description("Preparar-se para a festa do Bailarico")]
    Activity_Priorities,
    [Description("Escolher as peças de roupa")]
    Activity_Clothing,
    [Description("Ficou de levar 3 coisas para a festa do Bailarico, quais são?")]
    Activity_OQueLevar,
    [Description("Recorde onde é a festa do Bailarico")]
    Activity_Location,
    [Description("Percorra o percurso até ao local do evento")]
    Activity_Trajectory,
    [Description("")]
    Activity_Arrived,
    [Description("Complete o provérbio")]
    Saying,
    [Description("A sessão para mim foi...")]
    Review_Difficulty,
    [Description("Gostou da sessão?")]
    Review_Like,
    [Description("Lista de acontecimentos")]
    Summary,
    [Description("Nesta sessão trabalhou os domínios neurocognitivos:")]
    Domains
};

