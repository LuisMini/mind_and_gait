﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using UnityEngine;

public enum ENUM_Session4
{
    [Description(/*"Start Screen"*/)]
    StartScreen,
    [Description("Cartão de Orientação")]
    OrientationCard,
    [Description("Em que dia é a festa do Bailarico?")]
    OrientationCard_Date,
    [Description("Onde é a festa do Bailarico")]
    OrientationCard_Location,
    [Description("Qual é a morada para resposta ao convite?")]
    OrientationCard_Adress,
    [Description("A que horas começa a festa do Bailarico?")]
    OrientationCard_HourBegin,
    [Description("A que horas acaba a festa do Bailarico?")]
    OrientationCard_HourEnd,
    [Description("O que tenho que levar?")]
    OrientationCard_QueLevar,
    [Description("Qual é o meu orçamento?")]
    OrientationCard_Orcamento,
    [Description("Cartão de Orientação")]
    OrientationCard_Complete,
    [Description("Associar imagens")]
    Activity_Associar,
    [Description("Selecione as imagens que não são sobremesas")]
    Activity_ExcluirNaoSobremesa,
    [Description("Escolher sobremesa")]
    Activity_SelectDessert,    
    [Description("Utensílios de cozinha")]
    Activity_KitchenUtensils,
    [Description("Ordene as letras para formar a palavra em falta")]
    Saying,
    [Description("A sessão para mim foi...")]
    Review_Difficulty,
    [Description("Gostou da sessão?")]
    Review_Like,
    [Description("Lista de acontecimentos")]
    Summary,
    [Description("Nesta sessão trabalhou os domínios neurocognitivos:")]
    Domains
};

