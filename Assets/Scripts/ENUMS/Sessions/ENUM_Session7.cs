﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using UnityEngine;

public enum ENUM_Session7
{
    [Description(/*"Start Screen"*/)]
    StartScreen,
    [Description("Cartão de Orientação")]
    OrientationCard,
    [Description("Em que dia é a festa do Bailarico?")]
    OrientationCard_Date,
    [Description("Onde é a festa do Bailarico")]
    OrientationCard_Location,
    [Description("Qual é a morada para resposta ao convite?")]
    OrientationCard_Adress,
    [Description("A que horas começa a festa do Bailarico?")]
    OrientationCard_HourBegin,
    [Description("A que horas acaba a festa do Bailarico?")]
    OrientationCard_HourEnd,
    [Description("O que tenho que levar?")]
    OrientationCard_QueLevar,
    [Description("Qual é o meu orçamento?")]
    OrientationCard_Orcamento,
    [Description("Qual é a minha sobremesa?")]
    OrientationCard_Dessert,
    [Description("Qual é o emblema que criei?")]
    OrientationCard_Emblem,
    [Description("Cartão de Orientação")]
    OrientationCard_Complete,
    [Description("Escolha 3 músicas populares")]
    Activity_SelectSongs,
    [Description("Complete o refrão da música")]
    Activity_CompleteMusic1,
    [Description("Complete o refrão da música")]
    Activity_CompleteMusic2,
    [Description("Complete o refrão da música")]
    Activity_CompleteMusic3,
    [Description("Associe uma emoção á música \n \"Alecrim\"")]
    Activity_EmotionAlecrim,
    [Description("Associe uma emoção á música \n \"Indo Eu\"")]
    Activity_EmotionIndoEu,
    [Description("Complete o provérbio")]
    Saying,
    [Description("A sessão para mim foi...")]
    Review_Difficulty,
    [Description("Gostou da sessão?")]
    Review_Like,
    [Description("Lista de acontecimentos")]
    Summary,
    [Description("Nesta sessão trabalhou os domínios neurocognitivos:")]
    Domains
};

