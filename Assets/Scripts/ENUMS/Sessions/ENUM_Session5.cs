﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using UnityEngine;

public enum ENUM_Session5
{
    [Description(/*"Start Screen"*/)]
    StartScreen,
    [Description("Cartão de Orientação")]
    OrientationCard,
    [Description("Em que dia é a festa do Bailarico?")]
    OrientationCard_Date,
    [Description("Onde é a festa do Bailarico")]
    OrientationCard_Location,
    [Description("Qual é a morada para resposta ao convite?")]
    OrientationCard_Adress,
    [Description("A que horas começa a festa do Bailarico?")]
    OrientationCard_HourBegin,
    [Description("A que horas acaba a festa do Bailarico?")]
    OrientationCard_HourEnd,
    [Description("O que tenho que levar?")]
    OrientationCard_QueLevar,
    [Description("Qual é o meu orçamento?")]
    OrientationCard_Orcamento,
    [Description("Qual é a minha sobremesa?")]
    OrientationCard_Dessert,
    [Description("Cartão de Orientação")]
    OrientationCard_Complete,
    [Description("Selecione os ingredientes que existem na dispensa")]
    Activity_ListaIngredientes,
    [Description("Selecione os ingredientes que tem de comprar")]
    Activity_Folheto,
    [Description("Ordene as quantias de modo decrescente e calcule o total")]
    Activity_OrdenarQuantias,
    [Description("Calcule o valor restante do seu orçamento")]
    Activity_UpdateBudget,
    [Description("Arraste os utensilios para a posição correta")]
    Activity_ReceitaInterativaUtensils,
    [Description("Arraste os ingredientes para a posição correta")]
    Activity_ReceitaInterativaIngredients,
    [Description("Complete o provérbio")]
    Saying,
    [Description("A sessão para mim foi...")]
    Review_Difficulty,
    [Description("Gostou da sessão?")]
    Review_Like,
    [Description("Lista de acontecimentos")]
    Summary,
    [Description("Nesta sessão trabalhou os domínios neurocognitivos:")]
    Domains
};

