﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using UnityEngine;

public enum ENUM_Session1
{
    [Description(/*"Start Screen"*/)]
    StartScreen,
    [Description("Recebeu um convite")]
    Activity_Convite,
    [Description(/*"QuestLog"*/)]
    QuestLog,
    [Description("Em que dia é a festa do Bailarico?")]
    Activity_Calendar,
    [Description("A que horas começa a festa do Bailarico?")]
    Activity_Hours,
    [Description("A que horas acaba a festa do Bailarico?")]
    Activity_Clock,
    [Description("Escolha o que precisa para escrever e enviar uma carta")]
    Activity_Carta,
    [Description("Escrever a resposta ao convite")]
    Activity_RespostaSentences,
    [Description("Resposta ao convite")]
    Activity_Resposta,
    [Description("Cartão de Orientação")]
    OrientationCard,
    [Description("Complete o provérbio")]
    Saying,
    [Description("A sessão para mim foi...")]
    Review_Difficulty,
    [Description("Gostou da sessão?")]
    Review_Like,
    [Description("Lista de acontecimentos")]
    Summary,
    [Description("Nesta sessão trabalhou os domínios neurocognitivos:")]
    Domains
};

