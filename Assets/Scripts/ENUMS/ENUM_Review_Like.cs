﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using UnityEngine;

[System.Serializable]
public enum ENUM_Review_Like
{
    [Description("Não Gostei Nada")]
    NãoGosteiNada,
    [Description("Não Gostei")]
    NãoGostei,
    [Description("Gostei")]
    Gostei,
    [Description("Gostei Muito")]
    GosteiMuito,
}
