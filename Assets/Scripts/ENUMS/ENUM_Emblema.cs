﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using UnityEngine;

[System.Serializable]
public enum ENUM_Emblema
{
    [Description("Emblema1")]
    Emblem_1,
    [Description("Emblema2")]
    Emblem_2,
    [Description("Emblema3")]
    Emblem_3,
    [Description("Emblema4")]
    Emblem_4,
};

