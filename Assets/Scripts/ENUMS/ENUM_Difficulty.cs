﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using UnityEngine;

[System.Serializable]
public enum ENUM_Difficulty
{
    [Description("Alta")]
    Alta,
    [Description("Baixa")]
    Baixa,
};

