﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using UnityEngine;

[System.Serializable]
public enum ENUM_Patterns
{
    [Description("Pattern1")]
    Pattern_1,
    [Description("Pattern2")]
    Pattern_2,
    [Description("Pattern3")]
    Pattern_3,
    [Description("Pattern4")]
    Pattern_4,
    [Description("Pattern5")]
    Pattern_5,
    [Description("Pattern6")]
    Pattern_6,
    [Description("Pattern7")]
    Pattern_7,
    [Description("Pattern8")]
    Pattern_8,
};

