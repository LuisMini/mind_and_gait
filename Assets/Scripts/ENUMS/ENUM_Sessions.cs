﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using UnityEngine;

[System.Serializable]
public enum ENUM_Sessions
{
    [Description("Estou convidado!")]
    Session1,
    [Description("Deslocações e meios de transporte")]
    Session2,
    [Description("Fazer contas à vida")]
    Session3,
    [Description("À procura do doce")]
    Session4,
    [Description("Sobremesa interativa")]
    Session5,
    [Description("Chapéu, só o meu!")]
    Session6,
    [Description("Discos pedidos")]
    Session7,
    [Description("Bailarico aqui vou eu!")]
    Session8,
    [Description("Sessão de encerramento: O Baile")]
    Session9,
};

