﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using UnityEngine;

[System.Serializable]
public enum Sexo
{
    [Description("Masculino")]
    male,
    [Description("Feminino")]
    female,
};

