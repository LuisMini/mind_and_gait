﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using UnityEngine;

[System.Serializable]
public enum ENUM_Musics
{
    [Description("A Caminho de Viseu")]
    ACaminhoDeViseu,
    [Description("Alecrim")]
    Alecrim,
    [Description("A Machadinha")]
    AMachadinha,
    [Description("Ó rosa, arredonda a saia")]
    ORosaArredondaASaia,
    [Description("Oliveira da Serra")]
    OliveiraDaSerra,
    [Description("Lá vai o comboio")]
    LaVaiOComboio,
    [Description("Menina estás à janela")]
    MeninaEstasAJanela,
};