﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using UnityEngine;

[System.Serializable]
public enum ENUM_Review_Difficulty
{
    [Description("Muito Difícil")]
    MuitoDifícil,
    [Description("Difícil")]
    Difícil,
    [Description("Fácil")]
    Fácil,
    [Description("Muito Fácil")]
    MuitoFácil,
}

