﻿using System.Collections;
using System.Collections.Generic;
using System.Timers;
using System.Globalization;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using System.Text;

public class Manager : MonoBehaviour
{
    [System.Serializable]
    public class Session1
    {
        public GameObject activity_Convite;
        public GameObject activity_Calendar;
        public GameObject activity_Hours;
        public GameObject activity_Clock;
        public GameObject activity_Carta;
        public GameObject activity_RespostaSentences;
        public GameObject activity_Resposta;
    }
    [System.Serializable]
    public class Session2
    {
        public GameObject orientation_Date;
        public GameObject orientation_Location;
        public GameObject orientation_Adress;
        public GameObject orientation_HourBegin;
        public GameObject orientation_HourEnd;
        public GameObject PickCorrect_1;
        public GameObject PickCorrect_2;
        public GameObject PickCorrect_3;
        public GameObject PickCorrect_4;
        public GameObject PickCorrect_5;
        public GameObject PickCorrect_6;
        public GameObject PickCorrect_Final;
        public GameObject SendLetter;
    }
    [System.Serializable]
    public class Session3
    {
        public GameObject orientation_Date;
        public GameObject orientation_Location;
        public GameObject orientation_Adress;
        public GameObject orientation_HourBegin;
        public GameObject orientation_HourEnd;
        public GameObject orientation_QueLevar;
        public GameObject Activity_MemoryGame;
        public GameObject Activity_CorrectOrder;
        public GameObject Activity_Orcamento;
    }
    [System.Serializable]
    public class Session4
    {
        public GameObject orientation_Date;
        public GameObject orientation_Location;
        public GameObject orientation_Adress;
        public GameObject orientation_HourBegin;
        public GameObject orientation_HourEnd;
        public GameObject orientation_QueLevar;
        public GameObject orientation_Orcamento;
        public GameObject activity_Associar;
        public GameObject activity_ExcluirSobremesa;
        public GameObject activity_EscolherReceita;
        public GameObject activity_KitchenUtensils;
    }
    [System.Serializable]
    public class Session5
    {
        public GameObject orientation_Date;
        public GameObject orientation_Location;
        public GameObject orientation_Adress;
        public GameObject orientation_HourBegin;
        public GameObject orientation_HourEnd;
        public GameObject orientation_QueLevar;
        public GameObject orientation_Orcamento;
        public GameObject orientation_Dessert;
        public GameObject activity_IngredientList;
        public GameObject activity_Folheto;
        public GameObject activity_ValuesDragCount;
        public GameObject activity_UpdateBudget;
        public GameObject activity_ReceitaInterativa_Utensils;
        public GameObject activity_ReceitaInterativa_Ingredients;
    }
    [System.Serializable]
    public class Session6
    {
        public GameObject orientation_Date;
        public GameObject orientation_Location;
        public GameObject orientation_Adress;
        public GameObject orientation_HourBegin;
        public GameObject orientation_HourEnd;
        public GameObject orientation_QueLevar;
        public GameObject orientation_Orcamento;
        public GameObject orientation_Dessert;
        public GameObject activity_PhoneMessage;
        public GameObject activity_HatColor;
        public GameObject activity_ChooseEmblem;
        public GameObject activity_CreateEmblem;
        public GameObject activity_CustomizeEmblem;
    }
    [System.Serializable]
    public class Session7
    {
        public GameObject orientation_Date;
        public GameObject orientation_Location;
        public GameObject orientation_Adress;
        public GameObject orientation_HourBegin;
        public GameObject orientation_HourEnd;
        public GameObject orientation_QueLevar;
        public GameObject orientation_Orcamento;
        public GameObject orientation_Dessert;
        public GameObject orientation_emblem;
        public GameObject activity_selectSong;
        public GameObject activity_completeSong1;
        public GameObject activity_completeSong2;
        public GameObject activity_completeSong3;
        public GameObject activity_emotion_alecrim;
        public GameObject activity_emotion_indoEu;
    }
    [System.Serializable]
    public class Session8
    {
        public GameObject orientation_Date;
        public GameObject orientation_Location;
        public GameObject orientation_Adress;
        public GameObject orientation_HourBegin;
        public GameObject orientation_HourEnd;
        public GameObject orientation_QueLevar;
        public GameObject orientation_Orcamento;
        public GameObject orientation_Dessert;
        public GameObject orientation_Emblem;
        public GameObject activity_priorities;
        public GameObject activity_clothing;
        public GameObject activity_oQueLevar;
        public GameObject activity_location;
        public GameObject activity_trajectory;
        public GameObject activity_arrived;
    }
    [System.Serializable]
    public class Session9
    {
        public GameObject baile;
    }

    public Session1 session1;
    public Session2 session2;
    public Session3 session3;
    public Session4 session4;
    public Session5 session5;
    public Session6 session6;
    public Session7 session7;
    public Session8 session8;
    public Session9 session9;

    [Header("Current Session Data")]
    [SerializeField]
    public ENUM_Sessions currentSession;
    public ENUM_Session1 session1_activity;
    public ENUM_Session2 session2_activity;
    public ENUM_Session3 session3_activity;
    public ENUM_Session4 session4_activity;
    public ENUM_Session5 session5_activity;
    public ENUM_Session6 session6_activity;
    public ENUM_Session7 session7_activity;
    public ENUM_Session8 session8_activity;
    public ENUM_Session9 session9_activity;

    [Header("Profile Stats")]
    [NonSerialized]
    public System.Diagnostics.Stopwatch stopwatch;
    public int numInvitationOpen;

    [Header("Components")]
    #region
    private QuestLog questLogScript;
    [HideInInspector]
    public Menu menuScript;
    [SerializeField]
    private Game_Base currentGame;
    private Text activityTitle;
    private GameObject footer;
    private Button button_Ajuda;
    private Button button_Finished;
    private Button button_Retry;
    private SoundManager soundManager;
    #endregion
    [Header("Common Screens")]
    #region
    public GameObject panel_StartScreen;
    public GameObject panel_Orientation;
    public GameObject panel_QuestLog;
    public GameObject panel_Saying;
    public GameObject panel_Review_Difficulty;
    public GameObject panel_Review_Like;
    public GameObject panel_Summary;
    public GameObject panel_Domains;

    public Game_Base CurrentGame
    {
        get
        {
            return currentGame;
        }

        set
        {
            currentGame = value;
        }
    }

    public Button Button_Finished
    {
        get
        {
            return button_Finished;
        }

        set
        {
            button_Finished = value;
        }
    }
    #endregion

    public GameObject activePanel;
    private bool goingBack;

    // Use this for initialization
    void Start()
    {
        //set aos textos do start screen, dá display do nome e do ID da session
        panel_StartScreen.GetComponentsInChildren<Text>()[0].text = EnumsHelperExtension.ToDescription(currentSession);
        panel_StartScreen.GetComponentsInChildren<Text>()[1].text = "Sessão Nº " + ((int)currentSession + 1);
        //initialization of soundManager variable
        soundManager = (SoundManager)FindObjectOfType(typeof(SoundManager));
        //initializaçao do title e do footer
        activityTitle = GameObject.FindGameObjectWithTag("ActivityTitle").GetComponent<Text>();
        footer = GameObject.FindGameObjectWithTag("Footer");

        //initialização dos butões de dificuldade, terminar e tentar novamente
        button_Ajuda = GameObject.FindGameObjectWithTag("Button_Apoio").GetComponent<Button>();
        button_Finished = GameObject.FindGameObjectWithTag("Button_Finished").GetComponent<Button>();
        button_Retry = GameObject.FindGameObjectWithTag("Button_Retry").GetComponent<Button>();
        button_Retry.onClick.AddListener(ResetCurrentActivity);
        button_Ajuda.onClick.AddListener(LowerDifficulty);

        //inicialização do questLog script
        questLogScript = GetComponent<QuestLog>();

        //inicio do temporizador, data e hora do menu        
        stopwatch = System.Diagnostics.Stopwatch.StartNew();

        panel_StartScreen.SetActive(true);
        activePanel = panel_StartScreen;

        UpdateButtons();
    }

    public void LowerDifficulty()
    {
        if (currentGame != null)
        {
            if (currentGame.canChangeDifficulty == true)
            {
                currentGame.LowerDifficulty();
            }
            UpdateButtons();
        }
    }

    public void ChangeActivityTitle(bool visible, string newActivityTitle = "")
    {
        if (activityTitle)
        {
            if (visible)
            {
                activityTitle.text = newActivityTitle;
                return;
            }
            else
            {
                activityTitle.text = "";
            }
        }
    }

    /// <summary>
    /// Function for the finished button, calls the current game's CheckAnswers() function to validate given answers
    /// </summary>
    public void ButtonFinished()
    {
        currentGame.CheckAnswers();
    }

    /// <summary>
    /// Turns off the previous panel and turns on the new panel to activate.
    /// </summary>
    public void SwitchPanel(GameObject panelToActivate)
    {
        soundManager.StopPlayingAll();

        if (activePanel)
        {
            activePanel.SetActive(false);
        }

        panelToActivate.SetActive(true);

        activePanel = panelToActivate;
    }

    /// <summary>
    /// Used to return to the previous panel, changes the sessions' enum value
    /// </summary>
    public void PreviousPanel()
    {
        goingBack = true;
        switch (currentSession)
        {
            case ENUM_Sessions.Session1:
                {
                    if ((int)session1_activity >= 1) //if enum index is valid
                    {
                        int currentActivity = (int)session1_activity;
                        session1_activity = (ENUM_Session1)currentActivity - 1;
                        NextActivity();
                        goingBack = false;
                    }
                    break;
                }
            case ENUM_Sessions.Session2:
                {
                    if ((int)session2_activity >= 1) //if enum index is valid
                    {
                        int currentActivity = (int)session2_activity;
                        session2_activity = (ENUM_Session2)currentActivity - 1;
                        NextActivity();
                    }
                    break;
                }
            case ENUM_Sessions.Session3:
                {
                    if ((int)session3_activity >= 1) //if enum index is valid
                    {
                        int currentActivity = (int)session3_activity;
                        session3_activity = (ENUM_Session3)currentActivity - 1;
                        NextActivity();
                    }
                    break;
                }
            case ENUM_Sessions.Session4:
                {
                    if ((int)session4_activity >= 1) //if enum index is valid
                    {
                        int currentActivity = (int)session4_activity;
                        session4_activity = (ENUM_Session4)currentActivity - 1;
                        NextActivity();
                    }
                    break;
                }
            case ENUM_Sessions.Session5:
                {
                    if ((int)session5_activity >= 1) //if enum index is valid
                    {
                        int currentActivity = (int)session5_activity;
                        session5_activity = (ENUM_Session5)currentActivity - 1;
                        NextActivity();
                    }
                    break;
                }
            case ENUM_Sessions.Session6:
                {
                    if ((int)session6_activity >= 1) //if enum index is valid
                    {
                        int currentActivity = (int)session6_activity;
                        session6_activity = (ENUM_Session6)currentActivity - 1;
                        NextActivity();
                    }
                    break;
                }
            case ENUM_Sessions.Session7:
                {
                    if ((int)session7_activity >= 1) //if enum index is valid
                    {
                        int currentActivity = (int)session7_activity;
                        session7_activity = (ENUM_Session7)currentActivity - 1;
                        NextActivity();
                    }
                    break;
                }
            case ENUM_Sessions.Session8:
                {
                    if ((int)session8_activity >= 1) //if enum index is valid
                    {
                        int currentActivity = (int)session8_activity;
                        session8_activity = (ENUM_Session8)currentActivity - 1;
                        NextActivity();
                    }
                    break;
                }
            case ENUM_Sessions.Session9:
                {
                    if ((int)session9_activity >= 1) //if enum index is valid
                    {
                        int currentActivity = (int)session9_activity;
                        session9_activity = (ENUM_Session9)currentActivity - 1;
                        NextActivity();
                    }
                    break;
                }
        }
    }

    /// <summary>
    /// Used to go to the next panel, changes the sessions' enum value
    /// </summary>
    public void NextPanel()
    {
        switch (currentSession)
        {
            case ENUM_Sessions.Session1:
                {
                    int checkValid = (int)session1_activity;
                    checkValid += 1;
                    if (Enum.IsDefined(typeof(ENUM_Session1), checkValid)) //if enum index is valid
                    {
                        session1_activity = (ENUM_Session1)checkValid;
                        NextActivity();
                    }
                    break;
                }
            case ENUM_Sessions.Session2:
                {
                    int checkValid = (int)session2_activity;
                    checkValid += 1;
                    if (Enum.IsDefined(typeof(ENUM_Session2), checkValid)) //if enum index is valid
                    {
                        session2_activity = (ENUM_Session2)checkValid;
                        NextActivity();
                    }
                    break;
                }
            case ENUM_Sessions.Session3:
                {
                    int checkValid = (int)session3_activity;
                    checkValid += 1;
                    if (Enum.IsDefined(typeof(ENUM_Session3), checkValid)) //if enum index is valid
                    {
                        session3_activity = (ENUM_Session3)checkValid;
                        NextActivity();
                    }
                    break;
                }
            case ENUM_Sessions.Session4:
                {
                    int checkValid = (int)session4_activity;
                    checkValid += 1;
                    if (Enum.IsDefined(typeof(ENUM_Session4), checkValid)) //if enum index is valid
                    {
                        session4_activity = (ENUM_Session4)checkValid;
                        NextActivity();
                    }
                    break;
                }
            case ENUM_Sessions.Session5:
                {
                    int checkValid = (int)session5_activity;
                    checkValid += 1;
                    if (Enum.IsDefined(typeof(ENUM_Session5), checkValid)) //if enum index is valid
                    {
                        session5_activity = (ENUM_Session5)checkValid;
                        NextActivity();
                    }
                    break;
                }
            case ENUM_Sessions.Session6:
                {
                    int checkValid = (int)session6_activity;
                    checkValid += 1;
                    if (Enum.IsDefined(typeof(ENUM_Session6), checkValid)) //if enum index is valid
                    {
                        session6_activity = (ENUM_Session6)checkValid;
                        NextActivity();
                    }
                    break;
                }
            case ENUM_Sessions.Session7:
                {
                    int checkValid = (int)session7_activity;
                    checkValid += 1;
                    if (Enum.IsDefined(typeof(ENUM_Session7), checkValid)) //if enum index is valid
                    {
                        session7_activity = (ENUM_Session7)checkValid;
                        NextActivity();
                    }
                    break;
                }
            case ENUM_Sessions.Session8:
                {
                    int checkValid = (int)session8_activity;
                    checkValid += 1;
                    if (Enum.IsDefined(typeof(ENUM_Session8), checkValid)) //if enum index is valid
                    {
                        session8_activity = (ENUM_Session8)checkValid;
                        NextActivity();
                    }
                    break;
                }
            case ENUM_Sessions.Session9:
                {
                    int checkValid = (int)session9_activity;
                    checkValid += 1;
                    if (Enum.IsDefined(typeof(ENUM_Session9), checkValid)) //if enum index is valid
                    {
                        session9_activity = (ENUM_Session9)checkValid;
                        NextActivity();
                    }
                    break;
                }
        }
    }

    /// <summary>
    /// For debug purposes only: Skips the currently active activity.
    /// </summary>
    public void SkipActivity()
    {
        NextPanel();
    }

    public void ResetCurrentActivity()
    {
        currentGame.RestartGame();
    }

    private void NextActivity()
    {
        switch (currentSession)
        {
            case ENUM_Sessions.Session1:
                {
                    ChangeActivityTitle(true, EnumsHelperExtension.ToDescription(session1_activity));
                    switch (session1_activity)
                    {
                        case ENUM_Session1.StartScreen:
                            {
                                SwitchPanel(panel_StartScreen);
                                currentGame = null;
                                break;
                            }
                        case ENUM_Session1.Activity_Convite:
                            {
                                SwitchPanel(session1.activity_Convite);
                                currentGame = session1.activity_Convite.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session1.QuestLog:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session1.activity_invitation_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session1.activity_invitation_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session1.activity_invitation_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session1.activity_invitation_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    ProfileManager.selectedProfile.session1.activity_invitation_difficulty = gameScript.Difficulty.ToDescription();
                                }
                                SwitchPanel(panel_QuestLog);
                                currentGame = null;
                                break;
                            }
                        case ENUM_Session1.Activity_Calendar:
                            {
                                SwitchPanel(session1.activity_Calendar);
                                questLogScript.questLogButton.SetActive(true);
                                currentGame = session1.activity_Calendar.GetComponent<Game_Base>();
                                break;
                            }
                        case ENUM_Session1.Activity_Hours:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session1.activity_party_day_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session1.activity_party_day_difficulty = gameScript.Difficulty.ToDescription();
                                    ProfileManager.selectedProfile.session1.activity_party_day_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session1.activity_party_day_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session1.activity_party_day_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                }
                                SwitchPanel(session1.activity_Hours);
                                currentGame = session1.activity_Hours.GetComponent<Game_Base>();
                                break;
                            }
                        case ENUM_Session1.Activity_Clock:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session1.activity_party_hour_start_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session1.activity_party_hour_start_difficulty = gameScript.Difficulty.ToDescription();
                                    ProfileManager.selectedProfile.session1.activity_party_hour_start_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session1.activity_party_hour_start_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session1.activity_party_hour_start_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                }
                                SwitchPanel(session1.activity_Clock);
                                currentGame = session1.activity_Clock.GetComponent<Game_Base>();
                                break;
                            }
                        case ENUM_Session1.Activity_Carta:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_Clock gamescript = currentGame as Game_Clock;
                                    ProfileManager.selectedProfile.session1.activity_party_hour_end_correct_answers = gamescript.correctAnswers;
                                    ProfileManager.selectedProfile.session1.activity_party_hour_end_wrong_answers = gamescript.wrongAnswers;
                                    ProfileManager.selectedProfile.session1.activity_party_hour_end_chosen_hour = gamescript.CurrentHour;
                                    ProfileManager.selectedProfile.session1.activity_party_hour_end_chosen_minute = gamescript.CurrentMinutes;
                                }
                                SwitchPanel(session1.activity_Carta);
                                currentGame = session1.activity_Carta.GetComponent<Game_Base>();
                                break;
                            }
                        case ENUM_Session1.Activity_RespostaSentences:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session1.activity_write_letter_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session1.activity_write_letter_difficulty = gameScript.Difficulty.ToDescription();
                                    ProfileManager.selectedProfile.session1.activity_write_letter_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session1.activity_write_letter_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session1.activity_write_letter_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    ProfileManager.selectedProfile.session1.activity_write_letter_descriptions = gameScript.chosenAnswersDescription;
                                }
                                SwitchPanel(session1.activity_RespostaSentences);
                                currentGame = session1.activity_RespostaSentences.GetComponent<Game_Base>();
                                break;
                            }
                        case ENUM_Session1.Activity_Resposta:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_RespostaSentences gameScript = currentGame as Game_RespostaSentences;
                                    ProfileManager.selectedProfile.session1.activity_write_answer_invitation_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session1.activity_write_answer_invitation_correct_answers = gameScript.correctAnswers;
                                }
                                SwitchPanel(session1.activity_Resposta);
                                currentGame = session1.activity_Resposta.GetComponent<Game_Base>();
                                break;
                            }
                        case ENUM_Session1.OrientationCard:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_Resposta gameScript = currentGame as Game_Resposta;
                                    ProfileManager.selectedProfile.session1.activity_answer_invitation_name = gameScript.inputFields[0].text;
                                    ProfileManager.selectedProfile.session1.activity_answer_invitation_city = gameScript.inputFields[(gameScript.inputFields.Length) - 1].text;
                                    for (int i = 1; i < gameScript.inputFields.Length - 1; ++i)
                                    {
                                        ProfileManager.selectedProfile.session1.activity_answer_invitation_address += (gameScript.inputFields[i].text + " ");
                                    }
                                }
                                SwitchPanel(panel_Orientation);
                                currentGame = null;
                                break;
                            }
                        case ENUM_Session1.Saying:
                            {
                                SwitchPanel(panel_Saying);
                                questLogScript.questLogButton.SetActive(false);
                                currentGame = panel_Saying.GetComponent<Game_Base>();
                                break;
                            }
                        case ENUM_Session1.Review_Difficulty:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Proverbio_T1 gameScript = currentGame as Proverbio_T1;
                                    ProfileManager.selectedProfile.session1.proverb_difficulty = gameScript.Difficulty.ToDescription();
                                    if (gameScript.writtenAnswer == "")
                                    {
                                        ProfileManager.selectedProfile.session1.proverb_written_answer = "Skipped";
                                    }
                                    else
                                    {
                                        ProfileManager.selectedProfile.session1.proverb_written_answer = gameScript.writtenAnswer;
                                    }
                                }
                                SwitchPanel(panel_Review_Difficulty);
                                currentGame = panel_Review_Difficulty.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session1.Review_Like:
                            {
                                SwitchPanel(panel_Review_Like);
                                currentGame = panel_Review_Like.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session1.Summary:
                            {
                                SwitchPanel(panel_Summary);
                                currentGame = panel_Summary.GetComponentInChildren<Game_Base>();
                                //parar stopwatch
                                stopwatch.Stop();
                                //guardar info no profile
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    DateTime sessionDateTime = DateTime.Now;
                                    ProfileManager.selectedProfile.session1.session_date = sessionDateTime.ToString("dd-MM-yyyy");
                                    DateTime timer = new DateTime(stopwatch.ElapsedTicks);
                                    ProfileManager.selectedProfile.session1.session_duration = string.Format("{0:00}:{1:00}:{2:00}", timer.Hour, timer.Minute, timer.Second);
                                    ProfileManager.selectedProfile.session1.session_invitation_tries = numInvitationOpen;
                                }
                                else
                                {
                                    Debug.LogWarning("ProfileManager.selectedProfile is not valid");
                                }
                                break;
                            }
                        case ENUM_Session1.Domains:
                            {
                                SwitchPanel(panel_Domains);
                                footer.SetActive(false);
                                currentGame = null;
                                break;
                            }
                    }
                    break;
                }
            case ENUM_Sessions.Session2:
                {
                    ChangeActivityTitle(true, EnumsHelperExtension.ToDescription(session2_activity));
                    switch (session2_activity)
                    {
                        case ENUM_Session2.StartScreen:
                            {
                                SwitchPanel(panel_StartScreen);
                                currentGame = null;
                                break;
                            }
                        case ENUM_Session2.OrientationCard:
                            {
                                SwitchPanel(panel_Orientation);
                                currentGame = null;
                                break;
                            }
                        case ENUM_Session2.OrientationCard_Date:
                            {
                                SwitchPanel(session2.orientation_Date);
                                questLogScript.questLogButton.SetActive(true);
                                currentGame = session2.orientation_Date.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session2.OrientationCard_Location:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session2.orientation_party_day_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session2.orientation_party_day_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session2.orientation_party_day_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session2.orientation_party_day_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    ProfileManager.selectedProfile.session2.orientation_party_day_difficulty = gameScript.Difficulty.ToDescription();
                                }
                                SwitchPanel(session2.orientation_Location);
                                currentGame = session2.orientation_Location.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session2.OrientationCard_Adress:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session2.orientation_party_place_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session2.orientation_party_place_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session2.orientation_party_place_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session2.orientation_party_place_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    ProfileManager.selectedProfile.session2.orientation_party_place_answers_descriptions = gameScript.chosenAnswersDescription;
                                }
                                SwitchPanel(session2.orientation_Adress);
                                currentGame = session2.orientation_Adress.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session2.OrientationCard_HourBegin:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session2.orientation_invitation_address_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session2.orientation_invitation_address_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session2.orientation_invitation_address_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session2.orientation_invitation_address_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    ProfileManager.selectedProfile.session2.orientation_invitation_address_difficulty = gameScript.Difficulty.ToDescription();
                                }
                                SwitchPanel(session2.orientation_HourBegin);
                                currentGame = session2.orientation_HourBegin.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session2.OrientationCard_HourEnd:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session2.orientation_party_hour_start_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session2.orientation_party_hour_start_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session2.orientation_party_hour_start_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session2.orientation_party_hour_start_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    ProfileManager.selectedProfile.session2.orientation_party_hour_start_difficulty = gameScript.Difficulty.ToDescription();
                                }
                                SwitchPanel(session2.orientation_HourEnd);
                                currentGame = session2.orientation_HourEnd.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session2.OrientationCard_Complete:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session2.orientation_party_hour_end_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session2.orientation_party_hour_end_wrong_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session2.orientation_party_hour_end_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session2.orientation_party_hour_end_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    ProfileManager.selectedProfile.session2.orientation_party_hour_end_difficulty = gameScript.Difficulty.ToDescription();
                                }
                                panel_Orientation.GetComponentInChildren<OrientationPanel>().completed = true;
                                SwitchPanel(panel_Orientation);
                                currentGame = null;
                                break;
                            }
                        case ENUM_Session2.Activity_PickCorrect1:
                            {
                                SwitchPanel(session2.PickCorrect_1);
                                currentGame = session2.PickCorrect_1.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session2.Activity_PickCorrect2:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session2.activity_car_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session2.activity_car_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session2.activity_car_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session2.activity_car_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    ProfileManager.selectedProfile.session2.activity_car_difficulty = gameScript.Difficulty.ToDescription();
                                    ProfileManager.selectedProfile.session2.activity_car_answer_description = gameScript.chosenAnswersDescription;
                                }
                                SwitchPanel(session2.PickCorrect_2);
                                currentGame = session2.PickCorrect_2.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session2.Activity_PickCorrect3:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session2.activity_bike_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session2.activity_bike_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session2.activity_bike_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session2.activity_bike_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    ProfileManager.selectedProfile.session2.activity_bike_difficulty = gameScript.Difficulty.ToDescription();
                                    ProfileManager.selectedProfile.session2.activity_bike_answer_description = gameScript.chosenAnswersDescription;
                                }
                                SwitchPanel(session2.PickCorrect_3);
                                currentGame = session2.PickCorrect_3.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session2.Activity_PickCorrect4:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session2.activity_bicycle_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session2.activity_bicycle_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session2.activity_bicycle_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session2.activity_bicycle_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    ProfileManager.selectedProfile.session2.activity_bicycle_difficulty = gameScript.Difficulty.ToDescription();
                                    ProfileManager.selectedProfile.session2.activity_bicycle_answer_description = gameScript.chosenAnswersDescription;
                                }
                                SwitchPanel(session2.PickCorrect_4);
                                currentGame = session2.PickCorrect_4.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session2.Activity_PickCorrect5:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session2.activity_train_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session2.activity_train_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session2.activity_train_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session2.activity_train_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    ProfileManager.selectedProfile.session2.activity_train_difficulty = gameScript.Difficulty.ToDescription();
                                    ProfileManager.selectedProfile.session2.activity_train_answer_description = gameScript.chosenAnswersDescription;
                                }
                                SwitchPanel(session2.PickCorrect_5);
                                currentGame = session2.PickCorrect_5.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session2.Activity_PickCorrect6:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session2.activity_airplane_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session2.activity_airplane_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session2.activity_airplane_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session2.activity_airplane_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    ProfileManager.selectedProfile.session2.activity_airplane_difficulty = gameScript.Difficulty.ToDescription();
                                    ProfileManager.selectedProfile.session2.activity_airplane_answer_description = gameScript.chosenAnswersDescription;
                                }
                                SwitchPanel(session2.PickCorrect_6);
                                currentGame = session2.PickCorrect_6.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session2.Activity_PickCorrectFinal:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session2.activity_boat_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session2.activity_boat_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session2.activity_boat_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session2.activity_boat_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    ProfileManager.selectedProfile.session2.activity_boat_difficulty = gameScript.Difficulty.ToDescription();
                                    ProfileManager.selectedProfile.session2.activity_boat_answer_description = gameScript.chosenAnswersDescription;
                                }
                                SwitchPanel(session2.PickCorrect_Final);
                                currentGame = session2.PickCorrect_Final.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session2.Activity_SendLetter:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session2.activity_two_wheels_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session2.activity_two_wheels_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session2.activity_two_wheels_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session2.activity_two_wheels_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    ProfileManager.selectedProfile.session2.activity_two_wheels_difficulty = gameScript.Difficulty.ToDescription();
                                    ProfileManager.selectedProfile.session2.activity_two_wheels_answer_description = gameScript.chosenAnswersDescription;
                                }
                                SwitchPanel(session2.SendLetter);
                                currentGame = session2.SendLetter.GetComponent<Game_Base>();
                                break;
                            }
                        case ENUM_Session2.Saying:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_Map gameScript = currentGame as Game_Map;
                                    ProfileManager.selectedProfile.session2.activity_map_difficulty = gameScript.Difficulty.ToDescription();
                                    ProfileManager.selectedProfile.session2.activity_map_wrong_answers = gameScript.wrongAnswers;
                                }
                                SwitchPanel(panel_Saying);
                                questLogScript.questLogButton.SetActive(false);
                                currentGame = panel_Saying.GetComponent<Game_Base>();
                                break;
                            }
                        case ENUM_Session2.Review_Difficulty:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Proverbio_T2 gameScript = currentGame as Proverbio_T2;
                                    ProfileManager.selectedProfile.session2.proverb_correct_answers = gameScript.gamePickCorrect.correctAnswers;
                                    ProfileManager.selectedProfile.session2.proverb_wrong_answers = gameScript.gamePickCorrect.wrongAnswers;
                                    ProfileManager.selectedProfile.session2.proverb_correct_answers_not_selected = gameScript.gamePickCorrect.unselectedCorrectAnswers;
                                    if (gameScript.writtenAnswer == "")
                                    {
                                        ProfileManager.selectedProfile.session2.proverb_written_answer = "Skipped";
                                    }
                                    else
                                    {
                                        ProfileManager.selectedProfile.session2.proverb_written_answer = gameScript.writtenAnswer;
                                    }
                                }
                                SwitchPanel(panel_Review_Difficulty);
                                currentGame = panel_Review_Difficulty.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session2.Review_Like:
                            {
                                SwitchPanel(panel_Review_Like);
                                currentGame = panel_Review_Like.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session2.Summary:
                            {
                                SwitchPanel(panel_Summary);
                                currentGame = panel_Summary.GetComponentInChildren<Game_Base>();
                                ///parar stopwatch
                                stopwatch.Stop();
                                ///guardar info no profile
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    DateTime sessionDateTime = DateTime.Now;
                                    ProfileManager.selectedProfile.session2.session_date = sessionDateTime.ToString("dd-MM-yyyy");
                                    DateTime timer = new DateTime(stopwatch.ElapsedTicks);
                                    ProfileManager.selectedProfile.session2.session_duration = string.Format("{0:00}:{1:00}:{2:00}", timer.Hour, timer.Minute, timer.Second);
                                    ProfileManager.selectedProfile.session2.session_invitation_tries = numInvitationOpen;
                                }
                                else
                                {
                                    Debug.LogWarning("ProfileManager.selectedProfile is not valid");
                                }
                                break;
                            }
                        case ENUM_Session2.Domains:
                            {
                                SwitchPanel(panel_Domains);
                                footer.SetActive(false);
                                currentGame = null;
                                break;
                            }
                    }
                    break;
                }
            case ENUM_Sessions.Session3:
                {
                    ChangeActivityTitle(true, EnumsHelperExtension.ToDescription(session3_activity));
                    switch (session3_activity)
                    {
                        case ENUM_Session3.StartScreen:
                            {
                                SwitchPanel(panel_StartScreen);
                                currentGame = null;
                                break;
                            }
                        case ENUM_Session3.OrientationCard:
                            {
                                SwitchPanel(panel_Orientation);
                                currentGame = null;
                                break;
                            }
                        case ENUM_Session3.OrientationCard_Date:
                            {
                                SwitchPanel(session3.orientation_Date);
                                questLogScript.questLogButton.SetActive(true);
                                currentGame = session3.orientation_Date.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session3.OrientationCard_Location:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session3.orientation_party_day_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session3.orientation_party_day_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session3.orientation_party_day_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session3.orientation_party_day_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    ProfileManager.selectedProfile.session3.orientation_party_day_difficulty = gameScript.Difficulty.ToDescription();
                                }
                                SwitchPanel(session3.orientation_Location);
                                currentGame = session3.orientation_Location.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session3.OrientationCard_Adress:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session3.orientation_party_place_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session3.orientation_party_place_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session3.orientation_party_place_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session3.orientation_party_place_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    ProfileManager.selectedProfile.session3.orientation_party_place_descriptions = gameScript.chosenAnswersDescription;
                                }
                                SwitchPanel(session3.orientation_Adress);
                                currentGame = session3.orientation_Adress.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session3.OrientationCard_HourBegin:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session3.orientation_invitation_address_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session3.orientation_invitation_address_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session3.orientation_invitation_address_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session3.orientation_invitation_address_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    ProfileManager.selectedProfile.session3.orientation_invitation_address_difficulty = gameScript.Difficulty.ToDescription();
                                }
                                SwitchPanel(session3.orientation_HourBegin);
                                currentGame = session3.orientation_HourBegin.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session3.OrientationCard_HourEnd:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session3.orientation_party_hour_start_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session3.orientation_party_hour_start_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session3.orientation_party_hour_start_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session3.orientation_party_hour_start_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    ProfileManager.selectedProfile.session3.orientation_party_hour_start_difficulty = gameScript.Difficulty.ToDescription();
                                }
                                SwitchPanel(session3.orientation_HourEnd);
                                currentGame = session3.orientation_HourEnd.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session3.OrientationCard_QueLevar:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session3.orientation_party_hour_end_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session3.orientation_party_hour_end_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session3.orientation_party_hour_end_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session3.orientation_party_hour_end_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    ProfileManager.selectedProfile.session3.orientation_party_hour_end_difficulty = gameScript.Difficulty.ToDescription();
                                }
                                SwitchPanel(session3.orientation_QueLevar);
                                currentGame = session3.orientation_QueLevar.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session3.OrientationCard_Complete:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session3.orientation_what_to_take_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session3.orientation_what_to_take_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session3.orientation_what_to_take_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session3.orientation_what_to_take_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    //ProfileManager.selectedProfile.session3.orientation_WhatToTake_Difficulty = gameScript.Difficulty;
                                }
                                panel_Orientation.GetComponentInChildren<OrientationPanel>().completed = true; //last orientation activity therefore sets the panel to completed = true
                                SwitchPanel(panel_Orientation);
                                currentGame = null;
                                break;
                            }
                        case ENUM_Session3.Activity_MemoryGame:
                            {
                                SwitchPanel(session3.Activity_MemoryGame);
                                currentGame = session3.Activity_MemoryGame.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session3.Activity_CorrectOrder:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null)
                                {
                                    JogoMemoria gameScript = currentGame as JogoMemoria;
                                    ProfileManager.selectedProfile.session3.activity_memory_game_pairs_turned = gameScript.totalPairsFlipped;
                                    ProfileManager.selectedProfile.session3.activity_memory_game_pairs_correct = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session3.activity_memory_game_pairs_wrong = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session3.activity_memory_game_difficulty = gameScript.Difficulty.ToDescription();
                                    if (gameScript.soma4euros.inputField.text == "" || gameScript.soma10euros.inputField.text == "" || gameScript.soma20euros.inputField.text == "")
                                    {
                                        ProfileManager.selectedProfile.session3.activity_memory_game_2_2 = "Skipped";
                                        ProfileManager.selectedProfile.session3.activity_memory_game_5_5 = "Skipped";
                                        ProfileManager.selectedProfile.session3.activity_memory_game_10_10 = "Skipped";
                                    }
                                    else
                                    {
                                        ProfileManager.selectedProfile.session3.activity_memory_game_2_2 = gameScript.soma4euros.inputField.text;
                                        ProfileManager.selectedProfile.session3.activity_memory_game_5_5 = gameScript.soma10euros.inputField.text;
                                        ProfileManager.selectedProfile.session3.activity_memory_game_10_10 = gameScript.soma20euros.inputField.text;
                                    }
                                }
                                SwitchPanel(session3.Activity_CorrectOrder);
                                currentGame = session3.Activity_CorrectOrder.GetComponentInChildren<Game_Base>() as Game_Order;
                                break;
                            }
                        case ENUM_Session3.Activity_Orcamento:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null)
                                {
                                    Game_Order gameScript = currentGame as Game_Order;
                                    DateTime activityOrderTimer = DateTime.Now + gameScript.stopwatch.Elapsed;
                                    ProfileManager.selectedProfile.session3.activity_order_time = string.Format("{0}:{1}", activityOrderTimer.Minute, activityOrderTimer.Second);
                                }
                                SwitchPanel(session3.Activity_Orcamento);
                                currentGame = session3.Activity_Orcamento.GetComponentInChildren<EscolhaOrcamento>();
                                break;
                            }
                        case ENUM_Session3.Saying:
                            {
                                if (ProfileManager.selectedProfile != null)
                                {
                                    EscolhaOrcamento gameScript = currentGame as EscolhaOrcamento;
                                    ProfileManager.selectedProfile.session3.budget_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session3.budget_chosen = ProfileManager.selectedProfile.budget_chosen.ToString();
                                    if (gameScript.writtenAnswer == "")
                                    {
                                        ProfileManager.selectedProfile.session3.budget_writen_answer = "Skipped";
                                    }
                                    else
                                    {
                                        ProfileManager.selectedProfile.session3.budget_writen_answer = gameScript.writtenAnswer;
                                    }
                                    ProfileManager.selectedProfile.session3.budget_difficulty = gameScript.Difficulty.ToDescription();
                                }
                                SwitchPanel(panel_Saying);
                                questLogScript.questLogButton.SetActive(false);
                                currentGame = panel_Saying.GetComponent<Game_Base>();
                                break;
                            }
                        case ENUM_Session3.Review_Difficulty:
                            {
                                if (ProfileManager.selectedProfile != null)
                                {
                                    Proverbio_T2 gameScript = currentGame as Proverbio_T2;
                                    ProfileManager.selectedProfile.session3.proverb_correct_answers = gameScript.gamePickCorrect.correctAnswers;
                                    ProfileManager.selectedProfile.session3.proverb_wrong_answers = gameScript.gamePickCorrect.wrongAnswers;
                                    ProfileManager.selectedProfile.session3.proverb_correct_answers_not_selected = gameScript.gamePickCorrect.unselectedCorrectAnswers;
                                    ProfileManager.selectedProfile.session3.proverb_answers_description = gameScript.gamePickCorrect.chosenAnswersDescription;
                                    if (gameScript.writtenAnswer == "")
                                    {
                                        ProfileManager.selectedProfile.session3.proverb_written_answer = "Skipped";
                                    }
                                    else
                                    {
                                        ProfileManager.selectedProfile.session3.proverb_written_answer = gameScript.writtenAnswer;
                                    }
                                }
                                SwitchPanel(panel_Review_Difficulty);
                                currentGame = panel_Review_Difficulty.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session3.Review_Like:
                            {
                                SwitchPanel(panel_Review_Like);
                                currentGame = panel_Review_Like.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session3.Summary:
                            {
                                SwitchPanel(panel_Summary);
                                currentGame = panel_Summary.GetComponentInChildren<Game_Base>();
                                ///parar stopwatch
                                stopwatch.Stop();
                                ///guardar info no profile
                                if (ProfileManager.selectedProfile != null)
                                {
                                    DateTime sessionDateTime = DateTime.Now;
                                    ProfileManager.selectedProfile.session3.session_date = sessionDateTime.ToString("dd-MM-yyyy");
                                    DateTime timer = new DateTime(stopwatch.ElapsedTicks);
                                    ProfileManager.selectedProfile.session3.session_duration = string.Format("{0:00}:{1:00}:{2:00}", timer.Hour, timer.Minute, timer.Second);
                                    ProfileManager.selectedProfile.session3.session_invitation_tries = numInvitationOpen;
                                }
                                else
                                {
                                    Debug.LogWarning("ProfileManager.selectedProfile is not valid");
                                }
                                break;
                            }
                        case ENUM_Session3.Domains:
                            {
                                SwitchPanel(panel_Domains);
                                footer.SetActive(false);
                                currentGame = null;
                                break;
                            }
                    }
                    break;
                }
            case ENUM_Sessions.Session4:
                {
                    ChangeActivityTitle(true, EnumsHelperExtension.ToDescription(session4_activity));
                    switch (session4_activity)
                    {
                        case ENUM_Session4.StartScreen:
                            {
                                SwitchPanel(panel_StartScreen);
                                currentGame = null;
                                break;
                            }
                        case ENUM_Session4.OrientationCard:
                            {
                                SwitchPanel(panel_Orientation);
                                currentGame = null;
                                break;
                            }
                        case ENUM_Session4.OrientationCard_Date:
                            {
                                SwitchPanel(session4.orientation_Date);
                                questLogScript.questLogButton.SetActive(true);
                                currentGame = session4.orientation_Date.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session4.OrientationCard_Location:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session4.orientation_party_day_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session4.orientation_party_day_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session4.orientation_party_day_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session4.orientation_party_day_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    ProfileManager.selectedProfile.session4.orientation_party_day_difficulty = gameScript.Difficulty.ToDescription();
                                }
                                SwitchPanel(session4.orientation_Location);
                                currentGame = session4.orientation_Location.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session4.OrientationCard_Adress:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session4.orientation_party_place_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session4.orientation_party_place_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session4.orientation_party_place_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session4.orientation_party_place_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    ProfileManager.selectedProfile.session4.orientation_party_place_descriptions = gameScript.chosenAnswersDescription;
                                }
                                SwitchPanel(session4.orientation_Adress);
                                currentGame = session4.orientation_Adress.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session4.OrientationCard_HourBegin:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session4.orientation_invitation_address_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session4.orientation_invitation_address_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session4.orientation_invitation_address_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session4.orientation_invitation_address_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    ProfileManager.selectedProfile.session4.orientation_invitation_address_difficulty = gameScript.Difficulty.ToDescription();
                                }
                                SwitchPanel(session4.orientation_HourBegin);
                                currentGame = session4.orientation_HourBegin.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session4.OrientationCard_HourEnd:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session4.orientation_party_hour_start_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session4.orientation_party_hour_start_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session4.orientation_party_hour_start_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session4.orientation_party_hour_start_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    ProfileManager.selectedProfile.session4.orientation_party_hour_start_difficulty = gameScript.Difficulty.ToDescription();
                                }
                                SwitchPanel(session4.orientation_HourEnd);
                                currentGame = session4.orientation_HourEnd.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session4.OrientationCard_QueLevar:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session4.orientation_party_hour_end_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session4.orientation_party_hour_end_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session4.orientation_party_hour_end_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session4.orientation_party_hour_end_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    ProfileManager.selectedProfile.session4.orientation_party_hour_end_difficulty = gameScript.Difficulty.ToDescription();
                                }
                                SwitchPanel(session4.orientation_QueLevar);
                                currentGame = session4.orientation_QueLevar.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session4.OrientationCard_Orcamento:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session4.orientation_what_to_take_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session4.orientation_what_to_take_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session4.orientation_what_to_take_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session4.orientation_what_to_take_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                }
                                SwitchPanel(session4.orientation_Orcamento);
                                currentGame = session4.orientation_Orcamento.GetComponentInChildren<Game_PickCorrect>();
                                break;
                            }
                        case ENUM_Session4.OrientationCard_Complete:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session4.orientation_budget_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session4.orientation_budget_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session4.orientation_budget_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session4.orientation_budget_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    ProfileManager.selectedProfile.session4.orientation_budget_difficulty = gameScript.Difficulty.ToDescription();
                                }
                                panel_Orientation.GetComponentInChildren<OrientationPanel>().completed = true; //last orientation activity therefore sets the panel to completed = true
                                SwitchPanel(panel_Orientation);
                                currentGame = null;
                                break;
                            }
                        case ENUM_Session4.Activity_Associar:
                            {
                                SwitchPanel(session4.activity_Associar);
                                currentGame = session4.activity_Associar.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session4.Activity_ExcluirNaoSobremesa:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_Associar gameScript = currentGame as Game_Associar;
                                    ProfileManager.selectedProfile.session4.activity_associate_images_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session4.activity_associate_images_difficulty = gameScript.Difficulty.ToDescription();
                                }
                                SwitchPanel(session4.activity_ExcluirSobremesa);
                                currentGame = session4.activity_ExcluirSobremesa.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session4.Activity_SelectDessert:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                    ProfileManager.selectedProfile.session4.activity_select_not_dessert_correct_answers = gameScript.correctAnswers;
                                    ProfileManager.selectedProfile.session4.activity_select_not_dessert_wrong_answers = gameScript.wrongAnswers;
                                    ProfileManager.selectedProfile.session4.activity_select_not_dessert_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    ProfileManager.selectedProfile.session4.activity_select_not_dessert_answers_descriptions = gameScript.chosenAnswersDescription;
                                }
                                SwitchPanel(session4.activity_EscolherReceita);
                                currentGame = session4.activity_EscolherReceita.GetComponentInChildren<Game_EscolhaSobremesa>();
                                break;
                            }
                        case ENUM_Session4.Activity_KitchenUtensils:
                            {
                                //guardar qual a sobremesa escolhida
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_EscolhaSobremesa gameScript = currentGame as Game_EscolhaSobremesa;
                                    ProfileManager.selectedProfile.dessert = gameScript.sobremesa;
                                    ProfileManager.selectedProfile.session4.activity_select_dessert_chosen_dessert = gameScript.sobremesa.ToDescription();
                                    ProfileManager.selectedProfile.session4.activity_select_dessert_difficulty = gameScript.Difficulty.ToDescription();
                                }
                                SwitchPanel(session4.activity_KitchenUtensils);
                                currentGame = session4.activity_KitchenUtensils.GetComponentInChildren<Game_KitchenUtensils>();
                                break;
                            }
                        case ENUM_Session4.Saying:
                            {
                                //guardar resultados do jogo anterior
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Game_KitchenUtensils gameScript = currentGame as Game_KitchenUtensils;
                                    ProfileManager.selectedProfile.session4.activity_kitchen_utensils_tries = gameScript.retrysUsed;
                                    ProfileManager.selectedProfile.session4.activity_kitchen_utensils_correct_answers = gameScript.currentMode.correctAnswers;
                                    ProfileManager.selectedProfile.session4.activity_kitchen_utensils_wrong_answers = gameScript.currentMode.wrongAnswers;
                                    ProfileManager.selectedProfile.session4.activity_kitchen_utensils_correct_answers_not_selected = gameScript.currentMode.unselectedCorrectAnswers;
                                    ProfileManager.selectedProfile.session4.activity_kitchen_utensils_difficulty = gameScript.Difficulty.ToDescription();
                                    ProfileManager.selectedProfile.session4.activity_kitchen_utensils_answers_descriptions = gameScript.currentMode.chosenAnswersDescription;
                                }
                                SwitchPanel(panel_Saying);
                                questLogScript.questLogButton.SetActive(false);
                                currentGame = panel_Saying.GetComponent<Game_Base>();
                                break;
                            }
                        case ENUM_Session4.Review_Difficulty:
                            {
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    Proverbio_T3_Order gameScript = currentGame as Proverbio_T3_Order;
                                    ProfileManager.selectedProfile.session4.proverb_difficulty = gameScript.Difficulty.ToDescription();
                                    DateTime timer = new DateTime(gameScript.gameOrder.stopwatch.ElapsedTicks);
                                    ProfileManager.selectedProfile.session4.proverb_time = string.Format("{0:00}:{1:00}", timer.Minute, timer.Second);
                                    if (gameScript.writtenAnswer == "")
                                    {
                                        ProfileManager.selectedProfile.session4.proverb_written_answer = "Skipped";
                                    }
                                    else
                                    {
                                        ProfileManager.selectedProfile.session4.proverb_written_answer = gameScript.writtenAnswer;
                                    }
                                }
                                SwitchPanel(panel_Review_Difficulty);
                                currentGame = panel_Review_Difficulty.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session4.Review_Like:
                            {
                                SwitchPanel(panel_Review_Like);
                                currentGame = panel_Review_Like.GetComponentInChildren<Game_Base>();
                                break;
                            }
                        case ENUM_Session4.Summary:
                            {
                                SwitchPanel(panel_Summary);
                                currentGame = panel_Summary.GetComponentInChildren<Game_Base>();
                                ///parar stopwatch
                                stopwatch.Stop();
                                ///guardar info no profile
                                if (ProfileManager.selectedProfile != null && !goingBack)
                                {
                                    DateTime sessionDateTime = DateTime.Now;
                                    ProfileManager.selectedProfile.session4.session_date = sessionDateTime.ToString("dd-MM-yyyy");
                                    DateTime timer = new DateTime(stopwatch.ElapsedTicks);
                                    ProfileManager.selectedProfile.session4.session_duration = string.Format("{0:00}:{1:00}:{2:00}", timer.Hour, timer.Minute, timer.Second);
                                    ProfileManager.selectedProfile.session4.session_invitation_tries = numInvitationOpen;
                                }
                                else
                                {
                                    Debug.LogWarning("ProfileManager.selectedProfile is not valid");
                                }
                                break;
                            }
                        case ENUM_Session4.Domains:
                            {
                                SwitchPanel(panel_Domains);
                                footer.SetActive(false);
                                currentGame = null;
                                break;
                            }
                    }
                    break;
                }
            case ENUM_Sessions.Session5:
                {
                    {
                        ChangeActivityTitle(true, EnumsHelperExtension.ToDescription(session5_activity));
                        switch (session5_activity)
                        {
                            case ENUM_Session5.StartScreen:
                                {
                                    SwitchPanel(panel_StartScreen);
                                    currentGame = null;
                                    break;
                                }
                            case ENUM_Session5.OrientationCard:
                                {
                                    SwitchPanel(panel_Orientation);
                                    currentGame = null;
                                    break;
                                }
                            case ENUM_Session5.OrientationCard_Date:
                                {
                                    SwitchPanel(session5.orientation_Date);
                                    questLogScript.questLogButton.SetActive(true);
                                    currentGame = session5.orientation_Date.GetComponentInChildren<Game_Base>();
                                    break;
                                }
                            case ENUM_Session5.OrientationCard_Location:
                                {
                                    //guardar resultados do jogo anterior
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                        ProfileManager.selectedProfile.session5.orientation_party_day_tries = gameScript.retrysUsed;
                                        ProfileManager.selectedProfile.session5.orientation_party_day_correct_answers = gameScript.correctAnswers;
                                        ProfileManager.selectedProfile.session5.orientation_party_day_wrong_answers = gameScript.wrongAnswers;
                                        ProfileManager.selectedProfile.session5.orientation_party_day_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                        ProfileManager.selectedProfile.session5.orientation_party_day_difficulty = gameScript.Difficulty.ToDescription();
                                    }
                                    SwitchPanel(session5.orientation_Location);
                                    currentGame = session5.orientation_Location.GetComponentInChildren<Game_Base>();
                                    break;
                                }
                            case ENUM_Session5.OrientationCard_Adress:
                                {
                                    //guardar resultados do jogo anterior
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                        ProfileManager.selectedProfile.session5.orientation_party_place_tries = gameScript.retrysUsed;
                                        ProfileManager.selectedProfile.session5.orientation_party_place_correct_answers = gameScript.correctAnswers;
                                        ProfileManager.selectedProfile.session5.orientation_party_place_wrong_answers = gameScript.wrongAnswers;
                                        ProfileManager.selectedProfile.session5.orientation_party_place_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                        ProfileManager.selectedProfile.session5.orientation_party_place_descriptions = gameScript.chosenAnswersDescription;
                                    }
                                    SwitchPanel(session5.orientation_Adress);
                                    currentGame = session5.orientation_Adress.GetComponentInChildren<Game_Base>();
                                    break;
                                }
                            case ENUM_Session5.OrientationCard_HourBegin:
                                {
                                    //guardar resultados do jogo anterior
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                        ProfileManager.selectedProfile.session5.orientation_invitation_address_tries = gameScript.retrysUsed;
                                        ProfileManager.selectedProfile.session5.orientation_invitation_address_correct_answers = gameScript.correctAnswers;
                                        ProfileManager.selectedProfile.session5.orientation_invitation_address_wrong_answers = gameScript.wrongAnswers;
                                        ProfileManager.selectedProfile.session5.orientation_invitation_address_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                        ProfileManager.selectedProfile.session5.orientation_invitation_address_difficulty = gameScript.Difficulty.ToDescription();
                                    }
                                    SwitchPanel(session5.orientation_HourBegin);
                                    currentGame = session5.orientation_HourBegin.GetComponentInChildren<Game_Base>();
                                    break;
                                }
                            case ENUM_Session5.OrientationCard_HourEnd:
                                {
                                    //guardar resultados do jogo anterior
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                        ProfileManager.selectedProfile.session5.orientation_party_hour_start_tries = gameScript.retrysUsed;
                                        ProfileManager.selectedProfile.session5.orientation_party_hour_start_correct_answers = gameScript.correctAnswers;
                                        ProfileManager.selectedProfile.session5.orientation_party_hour_start_wrong_answers = gameScript.wrongAnswers;
                                        ProfileManager.selectedProfile.session5.orientation_party_hour_start_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                        ProfileManager.selectedProfile.session5.orientation_party_hour_start_difficulty = gameScript.Difficulty.ToDescription();
                                    }
                                    SwitchPanel(session5.orientation_HourEnd);
                                    currentGame = session5.orientation_HourEnd.GetComponentInChildren<Game_Base>();
                                    break;
                                }
                            case ENUM_Session5.OrientationCard_QueLevar:
                                {
                                    //guardar resultados do jogo anterior
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                        ProfileManager.selectedProfile.session5.orientation_party_hour_end_tries = gameScript.retrysUsed;
                                        ProfileManager.selectedProfile.session5.orientation_party_hour_end_correct_answers = gameScript.correctAnswers;
                                        ProfileManager.selectedProfile.session5.orientation_party_hour_end_wrong_answers = gameScript.wrongAnswers;
                                        ProfileManager.selectedProfile.session5.orientation_party_hour_end_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                        ProfileManager.selectedProfile.session5.orientation_party_hour_end_difficulty = gameScript.Difficulty.ToDescription();
                                    }
                                    SwitchPanel(session5.orientation_QueLevar);
                                    currentGame = session5.orientation_QueLevar.GetComponentInChildren<Game_Base>();
                                    break;
                                }
                            case ENUM_Session5.OrientationCard_Orcamento:
                                {
                                    //guardar resultados do jogo anterior
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                        ProfileManager.selectedProfile.session5.orientation_what_to_take_tries = gameScript.retrysUsed;
                                        ProfileManager.selectedProfile.session5.orientation_what_to_take_correct_answers = gameScript.correctAnswers;
                                        ProfileManager.selectedProfile.session5.orientation_what_to_take_wrong_answers = gameScript.wrongAnswers;
                                        ProfileManager.selectedProfile.session5.orientation_what_to_take_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    }
                                    SwitchPanel(session5.orientation_Orcamento);
                                    currentGame = session5.orientation_Orcamento.GetComponentInChildren<Game_PickCorrect>();
                                    break;
                                }
                            case ENUM_Session5.OrientationCard_Dessert:
                                {
                                    //guardar resultados do jogo anterior
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                        ProfileManager.selectedProfile.session5.orientation_budget_tries = gameScript.retrysUsed;
                                        ProfileManager.selectedProfile.session5.orientation_budget_correct_answers = gameScript.correctAnswers;
                                        ProfileManager.selectedProfile.session5.orientation_budget_wrong_answers = gameScript.wrongAnswers;
                                        ProfileManager.selectedProfile.session5.orientation_budget_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                        ProfileManager.selectedProfile.session5.orientation_budget_difficulty = gameScript.Difficulty.ToDescription();
                                    }
                                    SwitchPanel(session5.orientation_Dessert);
                                    currentGame = session5.orientation_Dessert.GetComponentInChildren<Game_PickCorrect>();
                                    break;
                                }
                            case ENUM_Session5.OrientationCard_Complete:
                                {
                                    //guardar resultados do jogo anterior
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                        if (gameScript.correctAnswers == 1)
                                        {
                                            ProfileManager.selectedProfile.session5.orientation_dessert_correct = "Opção Correta : Selecionou " + ProfileManager.selectedProfile.dessert.ToDescription();
                                        }
                                        else
                                        {
                                            ENUM_Sobremesa sobremesaEscolhida = ENUM_Sobremesa.ArrozDoce;
                                            if (ProfileManager.selectedProfile.dessert == ENUM_Sobremesa.ArrozDoce)
                                            {
                                                sobremesaEscolhida = ENUM_Sobremesa.BoloDeLaranja;
                                            }
                                            ProfileManager.selectedProfile.session5.orientation_dessert_correct = "Opção Errada : Selecionou " + sobremesaEscolhida.ToDescription();
                                        }
                                    }
                                    panel_Orientation.GetComponentInChildren<OrientationPanel>().completed = true; //last orientation activity therefore sets the panel to completed = true
                                    SwitchPanel(panel_Orientation);
                                    currentGame = null;
                                    break;
                                }
                            case ENUM_Session5.Activity_ListaIngredientes:
                                {
                                    SwitchPanel(session5.activity_IngredientList);
                                    currentGame = session5.activity_IngredientList.GetComponentInChildren<ListaIngredientes>();
                                    break;
                                }
                            case ENUM_Session5.Activity_Folheto:
                                {
                                    //guardar resultados do jogo anterior
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        ListaIngredientes gameScript = currentGame as ListaIngredientes;
                                        Game_PickCorrect gamePickCorrectScript = gameScript.currentGame;
                                        ProfileManager.selectedProfile.session5.activity_ingredients_correct_answers = gamePickCorrectScript.correctAnswers;
                                        ProfileManager.selectedProfile.session5.activity_ingredients_wrong_answers = gamePickCorrectScript.wrongAnswers;
                                    }
                                    SwitchPanel(session5.activity_Folheto);
                                    currentGame = session5.activity_Folheto.GetComponentInChildren<Game_PickCorrect>(true);
                                    questLogScript.orcamento.SetActive(true);
                                    break;
                                }
                            case ENUM_Session5.Activity_OrdenarQuantias:
                                {
                                    //guardar resultados do jogo anterior
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                        ProfileManager.selectedProfile.session5.activity_shopping_correct_answers = gameScript.correctAnswers;
                                        ProfileManager.selectedProfile.session5.activity_shopping_wrong_answers = gameScript.wrongAnswers;
                                    }
                                    SwitchPanel(session5.activity_ValuesDragCount);
                                    CurrentGame = session5.activity_ValuesDragCount.GetComponentInChildren<Game_Base>();
                                    break;
                                }
                            case ENUM_Session5.Activity_UpdateBudget:
                                {
                                    //guardar resultados do jogo anterior
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        OrdenarQuantias gameScript = currentGame as OrdenarQuantias;
                                        ProfileManager.selectedProfile.session5.activity_order_sum_correct_answers = gameScript.correctAnswers;
                                        ProfileManager.selectedProfile.session5.activity_order_sum_wrong_answers = gameScript.wrongAnswers;
                                        ProfileManager.selectedProfile.session5.activity_order_sum_written_answer = gameScript.writtenAnswer;
                                    }
                                    SwitchPanel(session5.activity_UpdateBudget);
                                    CurrentGame = session5.activity_UpdateBudget.GetComponentInChildren<Game_Base>();
                                    break;
                                }
                            case ENUM_Session5.Activity_ReceitaInterativaUtensils:
                                {
                                    //guardar resultados do jogo anterior
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        NovoOrçamento gameScript = currentGame as NovoOrçamento;
                                        ProfileManager.selectedProfile.session5.activity_budget_remaining_written_answer = gameScript.writtenAnswer;
                                    }
                                    SwitchPanel(session5.activity_ReceitaInterativa_Utensils);
                                    CurrentGame = session5.activity_ReceitaInterativa_Utensils.GetComponentInChildren<Game_Base>();
                                    break;
                                }
                            case ENUM_Session5.Activity_ReceitaInterativaIngredients:
                                {
                                    //guardar resultados do jogo anterior
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        ReceitaInterativa gameScript = currentGame as ReceitaInterativa;
                                        ProfileManager.selectedProfile.session5.activity_recipe_utensils_correct_answers = gameScript.correctAnswers;
                                        DateTime timer = new DateTime(gameScript.stopwatch.ElapsedTicks);
                                        ProfileManager.selectedProfile.session5.activity_recipe_utensils_time = string.Format("{0:00}:{1:00}", timer.Minute, timer.Second);
                                        ProfileManager.selectedProfile.session5.activity_recipe_utensils_wrong_answers = gameScript.wrongAnswers;
                                    }
                                    SwitchPanel(session5.activity_ReceitaInterativa_Ingredients);
                                    CurrentGame = session5.activity_ReceitaInterativa_Ingredients.GetComponentInChildren<Game_Base>();
                                    break;
                                }
                            case ENUM_Session5.Saying:
                                {
                                    //guardar resultados do jogo anterior
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        ReceitaInterativa gameScript = currentGame as ReceitaInterativa;
                                        ProfileManager.selectedProfile.session5.activity_recipe_ingredients_correct_answers = gameScript.correctAnswers;
                                        DateTime timer = new DateTime(gameScript.stopwatch.ElapsedTicks);
                                        ProfileManager.selectedProfile.session5.activity_recipe_ingredients_time = string.Format("{0:00}:{1:00}", timer.Minute, timer.Second);
                                        ProfileManager.selectedProfile.session5.activity_recipe_ingredients_wrong_answers = gameScript.wrongAnswers;
                                    }
                                    SwitchPanel(panel_Saying);
                                    questLogScript.questLogButton.SetActive(false);
                                    questLogScript.orcamento.SetActive(false);
                                    currentGame = panel_Saying.GetComponent<Game_Base>();
                                    break;
                                }
                            case ENUM_Session5.Review_Difficulty:
                                {
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        Proverbio_T1 gameScript = currentGame as Proverbio_T1;
                                        ProfileManager.selectedProfile.session5.proverb_written_answer = gameScript.writtenAnswer;
                                    }
                                    SwitchPanel(panel_Review_Difficulty);
                                    currentGame = panel_Review_Difficulty.GetComponentInChildren<Game_Base>();
                                    break;
                                }
                            case ENUM_Session5.Review_Like:
                                {
                                    SwitchPanel(panel_Review_Like);
                                    currentGame = panel_Review_Like.GetComponentInChildren<Game_Base>();
                                    break;
                                }
                            case ENUM_Session5.Summary:
                                {
                                    SwitchPanel(panel_Summary);
                                    currentGame = panel_Summary.GetComponentInChildren<Game_Base>();
                                    ///parar stopwatch
                                    stopwatch.Stop();
                                    ///guardar info no profile
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        DateTime sessionDateTime = DateTime.Now;
                                        ProfileManager.selectedProfile.session5.session_date = sessionDateTime.ToString("dd-MM-yyyy");
                                        DateTime timer = new DateTime(stopwatch.ElapsedTicks);
                                        ProfileManager.selectedProfile.session5.session_duration = string.Format("{0:00}:{1:00}:{2:00}", timer.Hour, timer.Minute, timer.Second);
                                        ProfileManager.selectedProfile.session5.session_invitation_tries = numInvitationOpen;
                                    }
                                    else
                                    {
                                        Debug.LogWarning("ProfileManager.selectedProfile is not valid");
                                    }
                                    break;
                                }
                            case ENUM_Session5.Domains:
                                {
                                    SwitchPanel(panel_Domains);
                                    footer.SetActive(false);
                                    currentGame = null;
                                    break;
                                }
                        }
                        break;
                    }
                }
            case ENUM_Sessions.Session6:
                {
                    {
                        ChangeActivityTitle(true, EnumsHelperExtension.ToDescription(session6_activity));
                        switch (session6_activity)
                        {
                            case ENUM_Session6.StartScreen:
                                {
                                    SwitchPanel(panel_StartScreen);
                                    currentGame = null;
                                    break;
                                }
                            case ENUM_Session6.OrientationCard:
                                {
                                    SwitchPanel(panel_Orientation);
                                    currentGame = null;
                                    break;
                                }
                            case ENUM_Session6.OrientationCard_Date:
                                {
                                    SwitchPanel(session6.orientation_Date);
                                    questLogScript.questLogButton.SetActive(true);
                                    currentGame = session6.orientation_Date.GetComponentInChildren<Game_Base>();
                                    break;
                                }
                            case ENUM_Session6.OrientationCard_Location:
                                {
                                    //guardar resultados do jogo anterior
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                        ProfileManager.selectedProfile.session6.orientation_party_day_tries = gameScript.retrysUsed;
                                        ProfileManager.selectedProfile.session6.orientation_party_day_correct_answers = gameScript.correctAnswers;
                                        ProfileManager.selectedProfile.session6.orientation_party_day_wrong_answers = gameScript.wrongAnswers;
                                        ProfileManager.selectedProfile.session6.orientation_party_day_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                        ProfileManager.selectedProfile.session6.orientation_party_day_difficulty = gameScript.Difficulty.ToDescription();
                                    }
                                    SwitchPanel(session6.orientation_Location);
                                    currentGame = session6.orientation_Location.GetComponentInChildren<Game_Base>();
                                    break;
                                }
                            case ENUM_Session6.OrientationCard_Adress:
                                {
                                    //guardar resultados do jogo anterior
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                        ProfileManager.selectedProfile.session6.orientation_party_place_tries = gameScript.retrysUsed;
                                        ProfileManager.selectedProfile.session6.orientation_party_place_correct_answers = gameScript.correctAnswers;
                                        ProfileManager.selectedProfile.session6.orientation_party_place_wrong_answers = gameScript.wrongAnswers;
                                        ProfileManager.selectedProfile.session6.orientation_party_place_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                        ProfileManager.selectedProfile.session6.orientation_party_place_descriptions = gameScript.chosenAnswersDescription;
                                    }
                                    SwitchPanel(session6.orientation_Adress);
                                    currentGame = session6.orientation_Adress.GetComponentInChildren<Game_Base>();
                                    break;
                                }
                            case ENUM_Session6.OrientationCard_HourBegin:
                                {
                                    //guardar resultados do jogo anterior
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                        ProfileManager.selectedProfile.session6.orientation_invitation_address_tries = gameScript.retrysUsed;
                                        ProfileManager.selectedProfile.session6.orientation_invitation_address_correct_answers = gameScript.correctAnswers;
                                        ProfileManager.selectedProfile.session6.orientation_invitation_address_wrong_answers = gameScript.wrongAnswers;
                                        ProfileManager.selectedProfile.session6.orientation_invitation_address_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                        ProfileManager.selectedProfile.session6.orientation_invitation_address_difficulty = gameScript.Difficulty.ToDescription();
                                    }
                                    SwitchPanel(session6.orientation_HourBegin);
                                    currentGame = session6.orientation_HourBegin.GetComponentInChildren<Game_Base>();
                                    break;
                                }
                            case ENUM_Session6.OrientationCard_HourEnd:
                                {
                                    //guardar resultados do jogo anterior
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                        ProfileManager.selectedProfile.session6.orientation_party_hour_start_tries = gameScript.retrysUsed;
                                        ProfileManager.selectedProfile.session6.orientation_party_hour_start_correct_answers = gameScript.correctAnswers;
                                        ProfileManager.selectedProfile.session6.orientation_party_hour_start_wrong_answers = gameScript.wrongAnswers;
                                        ProfileManager.selectedProfile.session6.orientation_party_hour_start_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                        ProfileManager.selectedProfile.session6.orientation_party_hour_start_difficulty = gameScript.Difficulty.ToDescription();
                                    }
                                    SwitchPanel(session6.orientation_HourEnd);
                                    currentGame = session6.orientation_HourEnd.GetComponentInChildren<Game_Base>();
                                    break;
                                }
                            case ENUM_Session6.OrientationCard_QueLevar:
                                {
                                    //guardar resultados do jogo anterior
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                        ProfileManager.selectedProfile.session6.orientation_party_hour_end_tries = gameScript.retrysUsed;
                                        ProfileManager.selectedProfile.session6.orientation_party_hour_end_correct_answers = gameScript.correctAnswers;
                                        ProfileManager.selectedProfile.session6.orientation_party_hour_end_wrong_answers = gameScript.wrongAnswers;
                                        ProfileManager.selectedProfile.session6.orientation_party_hour_end_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                        ProfileManager.selectedProfile.session6.orientation_party_hour_end_difficulty = gameScript.Difficulty.ToDescription();
                                    }
                                    SwitchPanel(session6.orientation_QueLevar);
                                    currentGame = session6.orientation_QueLevar.GetComponentInChildren<Game_Base>();
                                    break;
                                }
                            case ENUM_Session6.OrientationCard_Orcamento:
                                {
                                    //guardar resultados do jogo anterior
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                        ProfileManager.selectedProfile.session6.orientation_what_to_take_tries = gameScript.retrysUsed;
                                        ProfileManager.selectedProfile.session6.orientation_what_to_take_correct_answers = gameScript.correctAnswers;
                                        ProfileManager.selectedProfile.session6.orientation_what_to_take_wrong_answers = gameScript.wrongAnswers;
                                        ProfileManager.selectedProfile.session6.orientation_what_to_take_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                    }
                                    SwitchPanel(session6.orientation_Orcamento);
                                    currentGame = session6.orientation_Orcamento.GetComponentInChildren<Game_PickCorrect>();
                                    break;
                                }
                            case ENUM_Session6.OrientationCard_Dessert:
                                {
                                    //guardar resultados do jogo anterior
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                        ProfileManager.selectedProfile.session6.orientation_budget_tries = gameScript.retrysUsed;
                                        ProfileManager.selectedProfile.session6.orientation_budget_correct_answers = gameScript.correctAnswers;
                                        ProfileManager.selectedProfile.session6.orientation_budget_wrong_answers = gameScript.wrongAnswers;
                                        ProfileManager.selectedProfile.session6.orientation_budget_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                        ProfileManager.selectedProfile.session6.orientation_budget_difficulty = gameScript.Difficulty.ToDescription();
                                    }
                                    SwitchPanel(session6.orientation_Dessert);
                                    currentGame = session6.orientation_Dessert.GetComponentInChildren<Game_PickCorrect>();
                                    break;
                                }
                            case ENUM_Session6.OrientationCard_Complete:
                                {
                                    //guardar resultados do jogo anterior
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                        if (gameScript.correctAnswers == 1)
                                        {
                                            ProfileManager.selectedProfile.session6.orientation_dessert_correct = "Opção Correta : Selecionou " + ProfileManager.selectedProfile.dessert.ToDescription();
                                        }
                                        else
                                        {
                                            ENUM_Sobremesa sobremesaEscolhida = ENUM_Sobremesa.ArrozDoce;
                                            if (ProfileManager.selectedProfile.dessert == ENUM_Sobremesa.ArrozDoce)
                                            {
                                                sobremesaEscolhida = ENUM_Sobremesa.BoloDeLaranja;
                                            }
                                            ProfileManager.selectedProfile.session6.orientation_dessert_correct = "Opção Errada : Selecionou " + sobremesaEscolhida.ToDescription();
                                        }
                                    }
                                    panel_Orientation.GetComponentInChildren<OrientationPanel>().completed = true; //last orientation activity therefore sets the panel to completed = true
                                    SwitchPanel(panel_Orientation);
                                    currentGame = null;
                                    break;
                                }
                            case ENUM_Session6.Activity_PhoneMessage:
                                {
                                    SwitchPanel(session6.activity_PhoneMessage);
                                    currentGame = session6.activity_PhoneMessage.GetComponentInChildren<Game_Telephone>();
                                    break;
                                }
                            case ENUM_Session6.Activity_HatColor:
                                {
                                    //guardar resultados do jogo anterior
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        Game_Telephone gameScript = currentGame as Game_Telephone;
                                        ProfileManager.selectedProfile.session6.activity_phone_wrong_answers = gameScript.wrongAnswers;
                                        ProfileManager.selectedProfile.session6.activity_phone_deleted_message = gameScript.chosenAnswersDescription;
                                    }
                                    SwitchPanel(session6.activity_HatColor);
                                    currentGame = session6.activity_HatColor.GetComponentInChildren<Game_Base>();
                                    break;
                                }
                            case ENUM_Session6.Activity_ChooseEmblem:
                                {
                                    //guardar resultados do jogo anterior
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                        ProfileManager.selectedProfile.session6.activity_hat_color_correct_answers = gameScript.correctAnswers;
                                        ProfileManager.selectedProfile.session6.activity_hat_color_wrong_answers = gameScript.wrongAnswers;
                                    }
                                    SwitchPanel(session6.activity_ChooseEmblem);
                                    CurrentGame = session6.activity_ChooseEmblem.GetComponentInChildren<Game_ChooseEmblem>();
                                    break;
                                }
                            case ENUM_Session6.Activity_CreateEmblem:
                                {
                                    //guardar resultados do jogo anterior
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        Game_ChooseEmblem gameScript = currentGame as Game_ChooseEmblem;
                                        ProfileManager.selectedProfile.chosenEmblem = gameScript.chosenEmblem;
                                        ProfileManager.selectedProfile.session6.activity_choose_emblem_choice = gameScript.chosenEmblem.ToDescription();
                                    }
                                    SwitchPanel(session6.activity_CreateEmblem);
                                    CurrentGame = session6.activity_CreateEmblem.GetComponentInChildren<Game_Base>();
                                    break;
                                }
                            case ENUM_Session6.Activity_CustomizeEmblem:
                                {
                                    //Guardar resultados do jogo anterior
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        Game_ShapeManager gameScript = currentGame as Game_ShapeManager;
                                        DateTime activityCreateEmblemTimer = DateTime.Now + gameScript.stopwatch.Elapsed;
                                        ProfileManager.selectedProfile.session6.activity_create_emblem_time = string.Format("{0}:{1}", activityCreateEmblemTimer.Minute, activityCreateEmblemTimer.Second);
                                    }
                                    SwitchPanel(session6.activity_CustomizeEmblem);
                                    CurrentGame = session6.activity_CustomizeEmblem.GetComponentInChildren<Game_Base>();
                                    break;
                                }
                            case ENUM_Session6.Saying:
                                {
                                    //guardar resultados do jogo anterior
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        Game_CustomizeEmblem gameScript = currentGame as Game_CustomizeEmblem;
                                        ProfileManager.selectedProfile.session6.activity_personalize_emblem_wrong_answers = gameScript.wrongAnswers;
                                        DateTime activityCustomizeTimer = DateTime.Now + gameScript.stopwatch.Elapsed;
                                        ProfileManager.selectedProfile.session6.activity_personalize_emblem_time = string.Format("{0}:{1}", activityCustomizeTimer.Minute, activityCustomizeTimer.Second);

                                        ProfileManager.selectedProfile.emblemPatterns = new List<int>();
                                        for (int i = 0; i < gameScript.activeEmblem.emblemPieces.Count; ++i)
                                        {
                                            EmblemPiece piece = gameScript.activeEmblem.emblemPieces[i];
                                            ProfileManager.selectedProfile.emblemPatterns.Add((int)piece.enumCurrentPattern);
                                        }
                                    }
                                    SwitchPanel(panel_Saying);
                                    questLogScript.questLogButton.SetActive(false);
                                    currentGame = panel_Saying.GetComponent<Game_Base>();
                                    break;
                                }
                            case ENUM_Session6.Review_Difficulty:
                                {
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        Proverbio_T2 gameScript = currentGame as Proverbio_T2;
                                        ProfileManager.selectedProfile.session6.proverb_correct_answers = gameScript.correctAnswers;
                                        ProfileManager.selectedProfile.session6.proverb_wrong_answers = gameScript.wrongAnswers;
                                        ProfileManager.selectedProfile.session6.proverb_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                        ProfileManager.selectedProfile.session6.proverb_written_answer = gameScript.writtenAnswer;
                                    }
                                    SwitchPanel(panel_Review_Difficulty);
                                    currentGame = panel_Review_Difficulty.GetComponentInChildren<Game_Base>();
                                    break;
                                }
                            case ENUM_Session6.Review_Like:
                                {
                                    SwitchPanel(panel_Review_Like);
                                    currentGame = panel_Review_Like.GetComponentInChildren<Game_Base>();
                                    break;
                                }
                            case ENUM_Session6.Summary:
                                {
                                    SwitchPanel(panel_Summary);
                                    currentGame = panel_Summary.GetComponentInChildren<Game_Base>();
                                    //parar stopwatch
                                    stopwatch.Stop();
                                    //guardar info no profile
                                    if (ProfileManager.selectedProfile != null && !goingBack)
                                    {
                                        DateTime sessionDateTime = DateTime.Now;
                                        ProfileManager.selectedProfile.session6.session_date = sessionDateTime.ToString("dd-MM-yyyy");
                                        DateTime timer = new DateTime(stopwatch.ElapsedTicks);
                                        ProfileManager.selectedProfile.session6.session_duration = string.Format("{0:00}:{1:00}:{2:00}", timer.Hour, timer.Minute, timer.Second);
                                        ProfileManager.selectedProfile.session6.session_invitation_tries = numInvitationOpen;
                                    }
                                    else
                                    {
                                        Debug.LogWarning("ProfileManager.selectedProfile is not valid");
                                    }
                                    break;
                                }
                            case ENUM_Session6.Domains:
                                {
                                    SwitchPanel(panel_Domains);
                                    footer.SetActive(false);
                                    currentGame = null;
                                    break;
                                }
                        }
                        break;
                    }
                }
            case ENUM_Sessions.Session7:
                {
                    {
                        {
                            ChangeActivityTitle(true, EnumsHelperExtension.ToDescription(session7_activity));
                            switch (session7_activity)
                            {
                                case ENUM_Session7.StartScreen:
                                    {
                                        SwitchPanel(panel_StartScreen);
                                        currentGame = null;
                                        break;
                                    }
                                case ENUM_Session7.OrientationCard:
                                    {
                                        SwitchPanel(panel_Orientation);
                                        currentGame = null;
                                        break;
                                    }
                                case ENUM_Session7.OrientationCard_Date:
                                    {
                                        SwitchPanel(session7.orientation_Date);
                                        questLogScript.questLogButton.SetActive(true);
                                        currentGame = session7.orientation_Date.GetComponentInChildren<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session7.OrientationCard_Location:
                                    {
                                        //guardar resultados do jogo anterior
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                            ProfileManager.selectedProfile.session7.orientation_party_day_tries = gameScript.retrysUsed;
                                            ProfileManager.selectedProfile.session7.orientation_party_day_correct_answers = gameScript.correctAnswers;
                                            ProfileManager.selectedProfile.session7.orientation_party_day_wrong_answers = gameScript.wrongAnswers;
                                            ProfileManager.selectedProfile.session7.orientation_party_day_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                            ProfileManager.selectedProfile.session7.orientation_party_day_difficulty = gameScript.Difficulty.ToDescription();
                                        }
                                        SwitchPanel(session7.orientation_Location);
                                        currentGame = session7.orientation_Location.GetComponentInChildren<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session7.OrientationCard_Adress:
                                    {
                                        //guardar resultados do jogo anterior
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                            ProfileManager.selectedProfile.session7.orientation_party_place_tries = gameScript.retrysUsed;
                                            ProfileManager.selectedProfile.session7.orientation_party_place_correct_answers = gameScript.correctAnswers;
                                            ProfileManager.selectedProfile.session7.orientation_party_place_wrong_answers = gameScript.wrongAnswers;
                                            ProfileManager.selectedProfile.session7.orientation_party_place_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                            ProfileManager.selectedProfile.session7.orientation_party_place_descriptions = gameScript.chosenAnswersDescription;
                                        }
                                        SwitchPanel(session7.orientation_Adress);
                                        currentGame = session7.orientation_Adress.GetComponentInChildren<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session7.OrientationCard_HourBegin:
                                    {
                                        //guardar resultados do jogo anterior
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                            ProfileManager.selectedProfile.session7.orientation_invitation_address_tries = gameScript.retrysUsed;
                                            ProfileManager.selectedProfile.session7.orientation_invitation_address_correct_answers = gameScript.correctAnswers;
                                            ProfileManager.selectedProfile.session7.orientation_invitation_address_wrong_answers = gameScript.wrongAnswers;
                                            ProfileManager.selectedProfile.session7.orientation_invitation_address_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                            ProfileManager.selectedProfile.session7.orientation_invitation_address_difficulty = gameScript.Difficulty.ToDescription();
                                        }
                                        SwitchPanel(session7.orientation_HourBegin);
                                        currentGame = session7.orientation_HourBegin.GetComponentInChildren<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session7.OrientationCard_HourEnd:
                                    {
                                        //guardar resultados do jogo anterior
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                            ProfileManager.selectedProfile.session7.orientation_party_hour_start_tries = gameScript.retrysUsed;
                                            ProfileManager.selectedProfile.session7.orientation_party_hour_start_correct_answers = gameScript.correctAnswers;
                                            ProfileManager.selectedProfile.session7.orientation_party_hour_start_wrong_answers = gameScript.wrongAnswers;
                                            ProfileManager.selectedProfile.session7.orientation_party_hour_start_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                            ProfileManager.selectedProfile.session7.orientation_party_hour_start_difficulty = gameScript.Difficulty.ToDescription();
                                        }
                                        SwitchPanel(session7.orientation_HourEnd);
                                        currentGame = session7.orientation_HourEnd.GetComponentInChildren<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session7.OrientationCard_QueLevar:
                                    {
                                        //guardar resultados do jogo anterior
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                            ProfileManager.selectedProfile.session7.orientation_party_hour_end_tries = gameScript.retrysUsed;
                                            ProfileManager.selectedProfile.session7.orientation_party_hour_end_correct_answers = gameScript.correctAnswers;
                                            ProfileManager.selectedProfile.session7.orientation_party_hour_end_wrong_answers = gameScript.wrongAnswers;
                                            ProfileManager.selectedProfile.session7.orientation_party_hour_end_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                            ProfileManager.selectedProfile.session7.orientation_party_hour_end_difficulty = gameScript.Difficulty.ToDescription();
                                        }
                                        SwitchPanel(session7.orientation_QueLevar);
                                        currentGame = session7.orientation_QueLevar.GetComponentInChildren<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session7.OrientationCard_Orcamento:
                                    {
                                        //guardar resultados do jogo anterior
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                            ProfileManager.selectedProfile.session7.orientation_what_to_take_tries = gameScript.retrysUsed;
                                            ProfileManager.selectedProfile.session7.orientation_what_to_take_correct_answers = gameScript.correctAnswers;
                                            ProfileManager.selectedProfile.session7.orientation_what_to_take_wrong_answers = gameScript.wrongAnswers;
                                            ProfileManager.selectedProfile.session7.orientation_what_to_take_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                        }
                                        SwitchPanel(session7.orientation_Orcamento);
                                        currentGame = session7.orientation_Orcamento.GetComponentInChildren<Game_PickCorrect>();
                                        break;
                                    }
                                case ENUM_Session7.OrientationCard_Dessert:
                                    {
                                        //guardar resultados do jogo anterior
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                            ProfileManager.selectedProfile.session7.orientation_budget_tries = gameScript.retrysUsed;
                                            ProfileManager.selectedProfile.session7.orientation_budget_correct_answers = gameScript.correctAnswers;
                                            ProfileManager.selectedProfile.session7.orientation_budget_wrong_answers = gameScript.wrongAnswers;
                                            ProfileManager.selectedProfile.session7.orientation_budget_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                            ProfileManager.selectedProfile.session7.orientation_budget_difficulty = gameScript.Difficulty.ToDescription();
                                        }
                                        SwitchPanel(session7.orientation_Dessert);
                                        currentGame = session7.orientation_Dessert.GetComponentInChildren<Game_PickCorrect>();
                                        break;
                                    }
                                case ENUM_Session7.OrientationCard_Emblem:
                                    {
                                        //guardar resultados do jogo anterior
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                            if (gameScript.correctAnswers == 1)
                                            {
                                                ProfileManager.selectedProfile.session7.orientation_dessert_correct = "Opção Correta : Selecionou " + ProfileManager.selectedProfile.dessert.ToDescription();
                                            }
                                            else
                                            {
                                                ENUM_Sobremesa sobremesaEscolhida = ENUM_Sobremesa.ArrozDoce;
                                                if (ProfileManager.selectedProfile.dessert == ENUM_Sobremesa.ArrozDoce)
                                                {
                                                    sobremesaEscolhida = ENUM_Sobremesa.BoloDeLaranja;
                                                }
                                                ProfileManager.selectedProfile.session7.orientation_dessert_correct = "Opção Errada : Selecionou " + sobremesaEscolhida.ToDescription();
                                            }
                                        }
                                        SwitchPanel(session7.orientation_emblem);
                                        currentGame = session7.orientation_emblem.GetComponentInChildren<Game_PickCorrect>();
                                        break;
                                    }
                                case ENUM_Session7.OrientationCard_Complete:
                                    {
                                        //guardar resultados do jogo anterior
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                            ProfileManager.selectedProfile.session7.orientation_hat_tries = gameScript.retrysUsed;
                                            ProfileManager.selectedProfile.session7.orientation_hat_correct_answers = gameScript.correctAnswers;
                                            ProfileManager.selectedProfile.session7.orientation_hat_wrong_answers = gameScript.wrongAnswers;
                                            ProfileManager.selectedProfile.session7.orientation_hat_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                            ProfileManager.selectedProfile.session7.orientation_hat_difficulty = gameScript.Difficulty.ToDescription();
                                        }
                                        panel_Orientation.GetComponentInChildren<OrientationPanel>().completed = true; //last orientation activity therefore sets the panel to completed = true
                                        SwitchPanel(panel_Orientation);
                                        currentGame = null;
                                        break;
                                    }
                                case ENUM_Session7.Activity_SelectSongs:
                                    {
                                        SwitchPanel(session7.activity_selectSong);
                                        currentGame = session7.activity_selectSong.GetComponentInChildren<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session7.Activity_CompleteMusic1:
                                    {
                                        //guardar resultados do jogo anterior
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            PickSong gameScript = currentGame as PickSong;
                                            ProfileManager.selectedProfile.session7.activity_choose_music_1 = gameScript.selectedSongs[0].ToDescription();
                                            ProfileManager.selectedProfile.session7.activity_choose_music_2 = gameScript.selectedSongs[1].ToDescription();
                                            ProfileManager.selectedProfile.session7.activity_choose_music_3 = gameScript.selectedSongs[2].ToDescription();
                                        }
                                        SwitchPanel(session7.activity_completeSong1);
                                        currentGame = session7.activity_completeSong1.GetComponentInChildren<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session7.Activity_CompleteMusic2:
                                    {
                                        //guardar resultados do jogo anterior
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            CompleteSongs gameScript = currentGame as CompleteSongs;
                                            ProfileManager.selectedProfile.session7.activity_complete_music_1_first_word = gameScript.currentSong.inputFields[0].text;
                                            ProfileManager.selectedProfile.session7.activity_complete_music_1_second_word = gameScript.currentSong.inputFields[1].text;
                                            ProfileManager.selectedProfile.session7.activity_complete_music_1_third_word = gameScript.currentSong.inputFields[2].text;
                                            ProfileManager.selectedProfile.session7.activity_complete_music_1_fourth_word = gameScript.currentSong.inputFields[3].text;
                                            ProfileManager.selectedProfile.session7.activity_complete_music_1_fifth_word = gameScript.currentSong.inputFields[4].text;
                                            ProfileManager.selectedProfile.session7.activity_complete_music_1_difficulty = gameScript.currentSong.Difficulty.ToDescription();
                                        }
                                        SwitchPanel(session7.activity_completeSong2);
                                        currentGame = session7.activity_completeSong2.GetComponentInChildren<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session7.Activity_CompleteMusic3:
                                    {
                                        //guardar resultados do jogo anterior
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            CompleteSongs gameScript = currentGame as CompleteSongs;
                                            ProfileManager.selectedProfile.session7.activity_complete_music_2_first_word = gameScript.currentSong.inputFields[0].text;
                                            ProfileManager.selectedProfile.session7.activity_complete_music_2_second_word = gameScript.currentSong.inputFields[1].text;
                                            ProfileManager.selectedProfile.session7.activity_complete_music_2_third_word = gameScript.currentSong.inputFields[2].text;
                                            ProfileManager.selectedProfile.session7.activity_complete_music_2_fourth_word = gameScript.currentSong.inputFields[3].text;
                                            ProfileManager.selectedProfile.session7.activity_complete_music_2_fifth_word = gameScript.currentSong.inputFields[4].text;
                                            ProfileManager.selectedProfile.session7.activity_complete_music_2_difficulty = gameScript.currentSong.Difficulty.ToDescription();
                                        }
                                        SwitchPanel(session7.activity_completeSong3);
                                        currentGame = session7.activity_completeSong3.GetComponentInChildren<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session7.Activity_EmotionAlecrim:
                                    {
                                        //guardar resultados do jogo anterior
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            CompleteSongs gameScript = currentGame as CompleteSongs;
                                            ProfileManager.selectedProfile.session7.activity_complete_music_3_first_word = gameScript.currentSong.inputFields[0].text;
                                            ProfileManager.selectedProfile.session7.activity_complete_music_3_second_word = gameScript.currentSong.inputFields[1].text;
                                            ProfileManager.selectedProfile.session7.activity_complete_music_3_third_word = gameScript.currentSong.inputFields[2].text;
                                            ProfileManager.selectedProfile.session7.activity_complete_music_3_fourth_word = gameScript.currentSong.inputFields[3].text;
                                            ProfileManager.selectedProfile.session7.activity_complete_music_3_fifth_word = gameScript.currentSong.inputFields[4].text;
                                            ProfileManager.selectedProfile.session7.activity_complete_music_3_difficulty = gameScript.currentSong.Difficulty.ToDescription();
                                        }
                                        SwitchPanel(session7.activity_emotion_alecrim);
                                        currentGame = session7.activity_emotion_alecrim.GetComponentInChildren<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session7.Activity_EmotionIndoEu:
                                    {
                                        //guardar resultados do jogo anterior
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            ChooseEmotion gameScript = currentGame as ChooseEmotion;
                                            ProfileManager.selectedProfile.session7.activity_emotion_alecrim = gameScript.selectedEmotion.ToDescription();
                                        }
                                        SwitchPanel(session7.activity_emotion_indoEu);
                                        currentGame = session7.activity_emotion_indoEu.GetComponentInChildren<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session7.Saying:
                                    {
                                        //guardar resultados do jogo anterior
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            ChooseEmotion gameScript = currentGame as ChooseEmotion;
                                            ProfileManager.selectedProfile.session7.activity_emotion_indoeu = gameScript.selectedEmotion.ToDescription();
                                        }
                                        SwitchPanel(panel_Saying);
                                        questLogScript.questLogButton.SetActive(false);
                                        currentGame = panel_Saying.GetComponent<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session7.Review_Difficulty:
                                    {
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            Proverbio_T1 gameScript = currentGame as Proverbio_T1;
                                            ProfileManager.selectedProfile.session7.proverb_written_answer = gameScript.writtenAnswer;
                                        }
                                        SwitchPanel(panel_Review_Difficulty);
                                        currentGame = panel_Review_Difficulty.GetComponentInChildren<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session7.Review_Like:
                                    {
                                        SwitchPanel(panel_Review_Like);
                                        currentGame = panel_Review_Like.GetComponentInChildren<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session7.Summary:
                                    {
                                        SwitchPanel(panel_Summary);
                                        currentGame = panel_Summary.GetComponentInChildren<Game_Base>();
                                        stopwatch.Stop(); //parar stopwatch
                                        ///guardar info no profile
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            DateTime sessionDateTime = DateTime.Now;
                                            ProfileManager.selectedProfile.session7.session_date = sessionDateTime.ToString("dd-MM-yyyy");
                                            DateTime timer = new DateTime(stopwatch.ElapsedTicks);
                                            ProfileManager.selectedProfile.session7.session_duration = string.Format("{0:00}:{1:00}:{2:00}", timer.Hour, timer.Minute, timer.Second);
                                            ProfileManager.selectedProfile.session7.session_invitation_tries = numInvitationOpen;
                                        }
                                        else
                                        {
                                            Debug.LogWarning("ProfileManager.selectedProfile is not valid");
                                        }
                                        break;
                                    }
                                case ENUM_Session7.Domains:
                                    {
                                        SwitchPanel(panel_Domains);
                                        footer.SetActive(false);
                                        currentGame = null;
                                        break;
                                    }
                            }
                            break;
                        }
                    }
                }
            case ENUM_Sessions.Session8:
                {
                    {
                        {
                            ChangeActivityTitle(true, EnumsHelperExtension.ToDescription(session8_activity));
                            switch (session8_activity)
                            {
                                case ENUM_Session8.StartScreen:
                                    {
                                        SwitchPanel(panel_StartScreen);
                                        currentGame = null;
                                        break;
                                    }
                                case ENUM_Session8.OrientationCard:
                                    {
                                        SwitchPanel(panel_Orientation);
                                        currentGame = null;
                                        break;
                                    }
                                case ENUM_Session8.OrientationCard_Date:
                                    {
                                        SwitchPanel(session8.orientation_Date);
                                        questLogScript.questLogButton.SetActive(true);
                                        currentGame = session8.orientation_Date.GetComponentInChildren<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session8.OrientationCard_Location:
                                    {
                                        //guardar resultados do jogo anterior
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                            ProfileManager.selectedProfile.session8.orientation_party_day_tries = gameScript.retrysUsed;
                                            ProfileManager.selectedProfile.session8.orientation_party_day_correct_answers = gameScript.correctAnswers;
                                            ProfileManager.selectedProfile.session8.orientation_party_day_wrong_answers = gameScript.wrongAnswers;
                                            ProfileManager.selectedProfile.session8.orientation_party_day_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                            ProfileManager.selectedProfile.session8.orientation_party_day_difficulty = gameScript.Difficulty.ToDescription();
                                        }
                                        SwitchPanel(session8.orientation_Location);
                                        currentGame = session8.orientation_Location.GetComponentInChildren<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session8.OrientationCard_Adress:
                                    {
                                        //guardar resultados do jogo anterior
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                            ProfileManager.selectedProfile.session8.orientation_party_place_tries = gameScript.retrysUsed;
                                            ProfileManager.selectedProfile.session8.orientation_party_place_correct_answers = gameScript.correctAnswers;
                                            ProfileManager.selectedProfile.session8.orientation_party_place_wrong_answers = gameScript.wrongAnswers;
                                            ProfileManager.selectedProfile.session8.orientation_party_place_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                            ProfileManager.selectedProfile.session8.orientation_party_place_descriptions = gameScript.chosenAnswersDescription;
                                        }
                                        SwitchPanel(session8.orientation_Adress);
                                        currentGame = session8.orientation_Adress.GetComponentInChildren<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session8.OrientationCard_HourBegin:
                                    {
                                        //guardar resultados do jogo anterior
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                            ProfileManager.selectedProfile.session8.orientation_invitation_address_tries = gameScript.retrysUsed;
                                            ProfileManager.selectedProfile.session8.orientation_invitation_address_correct_answers = gameScript.correctAnswers;
                                            ProfileManager.selectedProfile.session8.orientation_invitation_address_wrong_answers = gameScript.wrongAnswers;
                                            ProfileManager.selectedProfile.session8.orientation_invitation_address_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                            ProfileManager.selectedProfile.session8.orientation_invitation_address_difficulty = gameScript.Difficulty.ToDescription();
                                        }
                                        SwitchPanel(session8.orientation_HourBegin);
                                        currentGame = session8.orientation_HourBegin.GetComponentInChildren<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session8.OrientationCard_HourEnd:
                                    {
                                        //guardar resultados do jogo anterior
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                            ProfileManager.selectedProfile.session8.orientation_party_hour_start_tries = gameScript.retrysUsed;
                                            ProfileManager.selectedProfile.session8.orientation_party_hour_start_correct_answers = gameScript.correctAnswers;
                                            ProfileManager.selectedProfile.session8.orientation_party_hour_start_wrong_answers = gameScript.wrongAnswers;
                                            ProfileManager.selectedProfile.session8.orientation_party_hour_start_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                            ProfileManager.selectedProfile.session8.orientation_party_hour_start_difficulty = gameScript.Difficulty.ToDescription();
                                        }
                                        SwitchPanel(session8.orientation_HourEnd);
                                        currentGame = session8.orientation_HourEnd.GetComponentInChildren<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session8.OrientationCard_QueLevar:
                                    {
                                        //guardar resultados do jogo anterior
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                            ProfileManager.selectedProfile.session8.orientation_party_hour_end_tries = gameScript.retrysUsed;
                                            ProfileManager.selectedProfile.session8.orientation_party_hour_end_correct_answers = gameScript.correctAnswers;
                                            ProfileManager.selectedProfile.session8.orientation_party_hour_end_wrong_answers = gameScript.wrongAnswers;
                                            ProfileManager.selectedProfile.session8.orientation_party_hour_end_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                            ProfileManager.selectedProfile.session8.orientation_party_hour_end_difficulty = gameScript.Difficulty.ToDescription();
                                        }
                                        SwitchPanel(session8.orientation_QueLevar);
                                        currentGame = session8.orientation_QueLevar.GetComponentInChildren<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session8.OrientationCard_Orcamento:
                                    {
                                        //guardar resultados do jogo anterior
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                            ProfileManager.selectedProfile.session8.orientation_what_to_take_tries = gameScript.retrysUsed;
                                            ProfileManager.selectedProfile.session8.orientation_what_to_take_correct_answers = gameScript.correctAnswers;
                                            ProfileManager.selectedProfile.session8.orientation_what_to_take_wrong_answers = gameScript.wrongAnswers;
                                            ProfileManager.selectedProfile.session8.orientation_what_to_take_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                        }
                                        SwitchPanel(session8.orientation_Orcamento);
                                        currentGame = session8.orientation_Orcamento.GetComponentInChildren<Game_PickCorrect>();
                                        break;
                                    }
                                case ENUM_Session8.OrientationCard_Dessert:
                                    {
                                        //guardar resultados do jogo anterior
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                            ProfileManager.selectedProfile.session8.orientation_budget_tries = gameScript.retrysUsed;
                                            ProfileManager.selectedProfile.session8.orientation_budget_correct_answers = gameScript.correctAnswers;
                                            ProfileManager.selectedProfile.session8.orientation_budget_wrong_answers = gameScript.wrongAnswers;
                                            ProfileManager.selectedProfile.session8.orientation_budget_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                            ProfileManager.selectedProfile.session8.orientation_budget_difficulty = gameScript.Difficulty.ToDescription();
                                        }
                                        SwitchPanel(session8.orientation_Dessert);
                                        currentGame = session8.orientation_Dessert.GetComponentInChildren<Game_PickCorrect>();
                                        break;
                                    }
                                case ENUM_Session8.OrientationCard_Emblem:
                                    {
                                        //guardar resultados do jogo anterior
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                            if (gameScript.correctAnswers == 1)
                                            {
                                                ProfileManager.selectedProfile.session8.orientation_dessert_correct = "Opção Correta : Selecionou " + ProfileManager.selectedProfile.dessert.ToDescription();
                                            }
                                            else
                                            {
                                                ENUM_Sobremesa sobremesaEscolhida = ENUM_Sobremesa.ArrozDoce;
                                                if (ProfileManager.selectedProfile.dessert == ENUM_Sobremesa.ArrozDoce)
                                                {
                                                    sobremesaEscolhida = ENUM_Sobremesa.BoloDeLaranja;
                                                }
                                                ProfileManager.selectedProfile.session8.orientation_dessert_correct = "Opção Errada : Selecionou " + sobremesaEscolhida.ToDescription();
                                            }
                                        }
                                        SwitchPanel(session8.orientation_Emblem);
                                        currentGame = session8.orientation_Emblem.GetComponentInChildren<Game_PickCorrect>();
                                        break;
                                    }
                                case ENUM_Session8.OrientationCard_Complete:
                                    {
                                        //guardar resultados do jogo anterior
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                            ProfileManager.selectedProfile.session8.orientation_hat_tries = gameScript.retrysUsed;
                                            ProfileManager.selectedProfile.session8.orientation_hat_correct_answers = gameScript.correctAnswers;
                                            ProfileManager.selectedProfile.session8.orientation_hat_wrong_answers = gameScript.wrongAnswers;
                                            ProfileManager.selectedProfile.session8.orientation_hat_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                            ProfileManager.selectedProfile.session8.orientation_hat_difficulty = gameScript.Difficulty.ToDescription();
                                        }
                                        panel_Orientation.GetComponentInChildren<OrientationPanel>().completed = true; //last orientation activity therefore sets the panel to completed = true
                                        SwitchPanel(panel_Orientation);
                                        currentGame = null;
                                        break;
                                    }
                                case ENUM_Session8.Activity_Priorities:
                                    {
                                        SwitchPanel(session8.activity_priorities);
                                        currentGame = session8.activity_priorities.GetComponentInChildren<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session8.Activity_Clothing:
                                    {
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            GamePriorities gameScript = currentGame as GamePriorities;
                                            ProfileManager.selectedProfile.session8.activity_preparation_order = gameScript.chosenOrder;
                                        }
                                        SwitchPanel(session8.activity_clothing);
                                        currentGame = session8.activity_clothing.GetComponentInChildren<Game_ChooseClothes>();
                                        break;
                                    }
                                case ENUM_Session8.Activity_OQueLevar:
                                    {
                                        //guardar resultados do jogo anterior
                                        if (ProfileManager.selectedProfile != null && !goingBack) //guardar resultados do jogo 
                                        {
                                            Game_ChooseClothes gameScript = currentGame as Game_ChooseClothes;
                                            string chosenAnswers = "";
                                            chosenAnswers = String.Join(", ", gameScript.selectedClothes.ToArray());
                                            ProfileManager.selectedProfile.session8.activity_clothes_answers = chosenAnswers;
                                        }
                                        SwitchPanel(session8.activity_oQueLevar);
                                        currentGame = session8.activity_oQueLevar.GetComponentInChildren<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session8.Activity_Location:
                                    {
                                        //guardar resultados do jogo anterior
                                        if (ProfileManager.selectedProfile != null && !goingBack) 
                                        {
                                            Game_OQueLevar gameScript = currentGame as Game_OQueLevar;
                                            ProfileManager.selectedProfile.session8.activity_toTake_first_object = gameScript.inputfield1.text;
                                            ProfileManager.selectedProfile.session8.activity_toTake_second_object = gameScript.inputfield2.text;
                                            ProfileManager.selectedProfile.session8.activity_toTake_third_object = gameScript.inputfield3.text;
                                        }
                                        SwitchPanel(session8.activity_location);
                                        currentGame = session8.activity_location.GetComponentInChildren<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session8.Activity_Trajectory:
                                    {
                                        if (ProfileManager.selectedProfile != null && !goingBack) //guardar resultados do jogo anterior
                                        {
                                            Game_PickCorrect gameScript = currentGame as Game_PickCorrect;
                                            ProfileManager.selectedProfile.session8.activity_party_place_tries = gameScript.retrysUsed;
                                            ProfileManager.selectedProfile.session8.activity_party_place_correct_answers = gameScript.correctAnswers;
                                            ProfileManager.selectedProfile.session8.activity_party_place_wrong_answers = gameScript.wrongAnswers;
                                            ProfileManager.selectedProfile.session8.activity_party_place_correct_answers_not_selected = gameScript.unselectedCorrectAnswers;
                                            ProfileManager.selectedProfile.session8.activity_party_place_answers_descriptions = gameScript.chosenAnswersDescription;
                                        }
                                        SwitchPanel(session8.activity_trajectory);
                                        currentGame = session8.activity_trajectory.GetComponentInChildren<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session8.Activity_Arrived:
                                    {
                                        SwitchPanel(session8.activity_arrived);
                                        currentGame = session8.activity_arrived.GetComponentInChildren<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session8.Saying:
                                    {
                                        SwitchPanel(panel_Saying);
                                        questLogScript.questLogButton.SetActive(false);
                                        currentGame = panel_Saying.GetComponent<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session8.Review_Difficulty:
                                    {
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            Proverbio_T1 gameScript = currentGame as Proverbio_T1;
                                            ProfileManager.selectedProfile.session8.proverb_written_answer = gameScript.writtenAnswer;
                                        }
                                        SwitchPanel(panel_Review_Difficulty);
                                        currentGame = panel_Review_Difficulty.GetComponentInChildren<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session8.Review_Like:
                                    {
                                        SwitchPanel(panel_Review_Like);
                                        currentGame = panel_Review_Like.GetComponentInChildren<Game_Base>();
                                        break;
                                    }
                                case ENUM_Session8.Summary:
                                    {
                                        SwitchPanel(panel_Summary);
                                        currentGame = panel_Summary.GetComponentInChildren<Game_Base>();
                                        stopwatch.Stop(); //parar stopwatch
                                        ///guardar info no profile
                                        if (ProfileManager.selectedProfile != null && !goingBack)
                                        {
                                            DateTime sessionDateTime = DateTime.Now;
                                            ProfileManager.selectedProfile.session8.session_date = sessionDateTime.ToString("dd-MM-yyyy");
                                            DateTime timer = new DateTime(stopwatch.ElapsedTicks);
                                            ProfileManager.selectedProfile.session8.session_duration = string.Format("{0:00}:{1:00}:{2:00}", timer.Hour, timer.Minute, timer.Second);
                                            ProfileManager.selectedProfile.session8.session_invitation_tries = numInvitationOpen;
                                        }
                                        else
                                        {
                                            Debug.LogWarning("ProfileManager.selectedProfile is not valid");
                                        }
                                        break;
                                    }
                                case ENUM_Session8.Domains:
                                    {
                                        SwitchPanel(panel_Domains);
                                        footer.SetActive(false);
                                        currentGame = null;
                                        break;
                                    }
                            }
                            break;
                        }
                    }
                }
            case ENUM_Sessions.Session9:
                {
                    {
                        {
                            ChangeActivityTitle(true, EnumsHelperExtension.ToDescription(session9_activity));
                            switch (session9_activity)
                            {
                                case ENUM_Session9.StartScreen:
                                    {
                                        SwitchPanel(panel_StartScreen);
                                        currentGame = null;
                                        break;
                                    }
                                case ENUM_Session9.Baile:
                                    {
                                        SwitchPanel(session9.baile);
                                        footer.SetActive(false);
                                        currentGame = null;

                                        if (ProfileManager.selectedProfile != null)
                                        {
                                            if (ProfileManager.selectedProfile.completedSessions <= (int)currentSession)
                                            {
                                                ProfileManager.selectedProfile.completedSessions += 1;
                                                ProfileManager.selectedProfile.lastSession = currentSession;
                                            }
                                            ProfileManager.selectedProfile.totalDuration = ProfileManager.selectedProfile.totalDuration + stopwatch.Elapsed;
                                            ProfileManager.Save();

                                            StartCoroutine(ProfileManager.APIUpdatePatientProfile());
                                        }
                                        break;
                                    }
                            }
                            break;
                        }
                    }
                }
        }
        UpdateButtons();
    }

    public void UpdateButtons()
    {
        //Debug.Log("Manager - Update Buttons");
        /* Update butão Terminar / Seguinte */
        #region
        if (currentGame && !currentGame.GameCompleted && currentGame.hasChosenAnswer)
        {
            //Debug.Log("Manager - UpdateButtons - Terminei - true");
            button_Finished.gameObject.SetActive(true);
            button_Finished.onClick.RemoveAllListeners();
            button_Finished.onClick.AddListener(ButtonFinished);
            button_Finished.GetComponentInChildren<Text>().text = "Terminei";
        }
        else if (currentGame && !currentGame.GameCompleted && !currentGame.hasChosenAnswer)
        {
            //Debug.Log("Manager - UpdateButtons - Terminei/Seguinte - false");
            button_Finished.gameObject.SetActive(false);
        }
        else
        {
            //Debug.Log("Manager - UpdateButtons - Seguinte - true");
            button_Finished.gameObject.SetActive(true);
            button_Finished.onClick.RemoveAllListeners();
            button_Finished.onClick.AddListener(NextPanel);
            if (panel_StartScreen.activeInHierarchy)
            {
                button_Finished.GetComponentInChildren<Text>().text = "Iniciar";
            }
            else
            {
                button_Finished.GetComponentInChildren<Text>().text = "Seguinte";
            }
        }
        #endregion

        /* Update butão de ajuda */
        #region
        if (!currentGame || currentGame && currentGame.canChangeDifficulty == false || currentGame && currentGame.firstTry == true || currentGame && currentGame.canRetry == true || currentGame && currentGame.Difficulty == ENUM_Difficulty.Baixa || currentGame && currentGame.GameCompleted)
        {
            button_Ajuda.gameObject.SetActive(false);
            //Debug.Log("Manager - UpdateButtons - Ajuda - false");
            if (currentGame)
            {
                //Debug.Log("Manager - UpdateButtons - Ajuda - false | currentGame: " + currentGame.name + " | Conditions: canChangeDifficulty: " + currentGame.canChangeDifficulty + " & firstTry: " + currentGame.firstTry + " & canRetry: " + currentGame.canRetry + " & Difficulty: " + currentGame.Difficulty.ToString());
            }
        }
        else if (currentGame && currentGame.canChangeDifficulty && !currentGame.firstTry && currentGame.Difficulty == ENUM_Difficulty.Alta)
        {
            button_Ajuda.gameObject.SetActive(true);
            //Debug.Log("Manager - UpdateButtons - Ajuda - true");
        }
        #endregion

        /* Update butão de Retry */
        #region
        if (!currentGame || currentGame && !currentGame.canRetry)
        {
            //Debug.Log("Manager - UpdateButtons - Retry - false");
            button_Retry.gameObject.SetActive(false);
        }
        else if (currentGame && currentGame.retrysUsed < currentGame.numRetrys && currentGame.canRetry)
        {
            //Debug.Log("Manager - UpdateButtons - Retry - true");
            button_Retry.gameObject.SetActive(true);
            button_Retry.onClick.RemoveAllListeners();
            button_Retry.onClick.AddListener(ResetCurrentActivity);
            button_Retry.GetComponentInChildren<Text>().text = "Tentar de novo";
        }
        #endregion
    }
}
