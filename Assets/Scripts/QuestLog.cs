﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Globalization;

public class QuestLog : MonoBehaviour {
    //[HideInInspector]
    public GameObject questLogButton;
    [HideInInspector]
    public GameObject orcamento;
    private GameObject panel_QuestLog;
    private Text questLogText;
    private Manager manager;
    private SoundManager soundManager;

    [Header("Text")]
    private string textBeforeDate;
    private string textAfterDate;
    private string textGender;
    private string textPartyDate;

    [Header("Lerp Properties")]
    private bool lerp_Open;
    private bool lerp_Close;
    private Vector2 posOpen;
    private Vector2 posClosed;
    private Vector2 scaleOpen = new Vector2(1, 1);
    private Vector2 scaleClosed = new Vector2(0, 0);
    private float currentLerpTime = 0; //current lerp time that changes according to += Time.DeltaTime
    private float timePercent; //value between 0 and 1 for the lerp
    public float lerpTime = 0.25f; //time it takes for the lerp to complete

    // Use this for initialization
    void Start () {
        manager = GameObject.FindGameObjectWithTag("Manager").GetComponent<Manager>();
        soundManager = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();

        if (manager.panel_QuestLog)
        {
            panel_QuestLog = manager.panel_QuestLog;
            questLogText = panel_QuestLog.GetComponentInChildren<Text>();

            CultureInfo culture = new CultureInfo(0x0816);
            if (ProfileManager.selectedProfile != null)
            {
                textPartyDate = "<b> dia " + ProfileManager.selectedProfile.dateParty.Day.ToString() + " de " + ProfileManager.selectedProfile.dateParty.ToString("MMMM", culture) + " de " + ProfileManager.selectedProfile.dateParty.ToString("yyyy") + "</b>. ";
                switch (ProfileManager.selectedProfile.sex)
                {
                    case Sexo.female:
                        textGender = "a";
                        break;
                    case Sexo.male:
                        textGender = "o";
                        break;
                }
            }
            else
            {
                textPartyDate = "<b> ERROR: No Profile Selected. </b>";
                textGender = "o";
            }

            textBeforeDate = "Olá Amig" + textGender + "!\n\nGostaria de " + textGender + " convidar para participar na <b>Festa do Bailarico</b> que irá decorrer no";
            textAfterDate = "Começa às <b>19:15 horas</b> e termina às <b>22:30 horas </b>.\nDecorrerá um jantar no <b>Salão de Festas</b> pelas <b>20:00 horas</b> e terá um custo de <b>5€</b>. " +
                "Pedimos que traga uma <b>sobremesa</b>.\n\n" + "Contamos consigo!\nPor favor confirme a sua presença através de uma carta.\nEnvie-nos a sua resposta para <b>Maria, Rua da Alegria Nº 17, Coimbra</b>.\n\nAté Lá!";
            questLogText.text = textBeforeDate + textPartyDate + textAfterDate;

            questLogButton = GameObject.Find("QuestLog_Button");

            if (questLogButton)
            {
                questLogButton.SetActive(false);
            }
            orcamento = GameObject.FindGameObjectWithTag("Header_Orçamento");
            if (orcamento)
            {
                orcamento.SetActive(false);
                if (ProfileManager.selectedProfile != null)
                {
                    orcamento.GetComponentInChildren<Text>().text = ProfileManager.selectedProfile.budget_updated.ToString() + "€";
                }
                else
                {
                    orcamento.GetComponentInChildren<Text>().text = "17€";
                }
            }

            posOpen = panel_QuestLog.transform.position;
            posClosed = questLogButton.transform.position;
        }
    }

    private void Update()
    {
        if (lerp_Open)
        {
            panel_QuestLog.transform.position = posClosed;

            currentLerpTime += Time.deltaTime;
            if (currentLerpTime >= lerpTime)
            {
                currentLerpTime = lerpTime;
            }
            timePercent = currentLerpTime / lerpTime;

            panel_QuestLog.transform.position = Vector2.Lerp(posClosed, posOpen, timePercent);
            panel_QuestLog.transform.localScale = Vector2.Lerp(scaleClosed, scaleOpen, timePercent);

            if ((Vector2)panel_QuestLog.transform.position == posOpen) //se a posição que chegou é a posição de quando está aberto
            {
                lerp_Open = false;
                currentLerpTime = 0;
                questLogButton.GetComponent<Toggle>().interactable = true;
            }
        }
        if (lerp_Close)
        {
            panel_QuestLog.transform.position = posOpen;

            currentLerpTime += Time.deltaTime;
            if (currentLerpTime >= lerpTime)
            {
                currentLerpTime = lerpTime;
            }
            timePercent = currentLerpTime / lerpTime;

            panel_QuestLog.transform.position = Vector2.Lerp(posOpen, posClosed, timePercent);
            panel_QuestLog.transform.localScale = Vector2.Lerp(scaleOpen, scaleClosed, timePercent);

            if ((Vector2)panel_QuestLog.transform.position == posClosed) //se a posição que chegou é a posição de quando está fechado
            {
                lerp_Close = false;
                currentLerpTime = 0;
                panel_QuestLog.SetActive(false);
                questLogButton.GetComponent<Toggle>().interactable = true;
            }
        }
    }

    public void Toggle_Message()
    {
        if (panel_QuestLog.activeInHierarchy == false)
        {
            panel_QuestLog.SetActive(true);
            lerp_Open = true;
            questLogButton.GetComponent<Toggle>().interactable = false;

            manager.numInvitationOpen++;

            if (manager.CurrentGame)
            {
                manager.CurrentGame.numOpenedInvitation++;
            }
        }
        else
        {
            lerp_Close = true;
            questLogButton.GetComponent<Toggle>().interactable = false;
            soundManager.StopPlayingAll();
        }
    }
}
