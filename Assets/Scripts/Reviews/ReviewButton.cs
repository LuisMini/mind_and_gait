﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.EventSystems;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class ReviewButton : MonoBehaviour {

    [Header("Components")]
    public Image image;
    public Text textComp;
    public ReviewScreen reviewScreen;

    [Header("Values")]
    private string text;
    public Sprite defaultImage;
    public Sprite selectedImage;
    [SerializeField]
    private int scoreValue;

    // Use this for initialization
    void Start()
    {
        reviewScreen = gameObject.GetComponentInParent<ReviewScreen>();
    }

    public void Initialize(Sprite defaultImage, Sprite selectedImage, string text, int scoreValue, ReviewScreen reviewScript)
    {
        this.text = text;
        this.defaultImage = defaultImage;
        this.selectedImage = selectedImage;
        this.scoreValue = scoreValue;
        this.reviewScreen = reviewScript;

        if (textComp && image)
        {
            textComp.text = this.text;
            image.sprite = defaultImage;
        }
    }

    private void OnMouseUpAsButton()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            image.sprite = selectedImage;
            reviewScreen.SelectButton(this, scoreValue);
        }
    }

    public void ResetButton ()
    {
        image.sprite = defaultImage;
    }
}
