﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class ReviewScreen : Game_Base {

    [System.Serializable]
    public class ReviewButtonProperties
    {
        public Sprite defaultImage;
        public Sprite selectedImage;
    }

    //[Header("Review Type")]
    [SerializeField]
    private ENUM_ReviewType reviewType;
    [SerializeField]
    private ENUM_Review_Difficulty selected_Difficulty;
    [SerializeField]
    private ENUM_Review_Like selected_Like;
    [SerializeField]
    private ENUM_Sessions currentSession;

    //[Header("Components")]
    public GameObject ruler;
    public GameObject prefab_Tab;
    public List<ReviewButtonProperties> buttonsProperties;
    public List<GameObject> rulerTabs;
    public List<ReviewButton> buttons;

    [Header("Values")]
    private Vector2 rulerSize; //private auxiliary value for the ruler's size
    private int tabAmmount; //private auxiliary value for the ammount of tabs in the ruler (can be made public and dynamic in the future)
    private Vector2 tabSize; //saves the size of the tabs
    private float totalTabSize; //private auxiliary value for calculations
    private float spacing; //the spacing between the tabs

    public ENUM_ReviewType ReviewType
    {
        get
        {
            return reviewType;
        }

        set
        {
            reviewType = value;
        }
    }

    // Use this for initialization
    new void Start () {
        base.Start();
        canChangeDifficulty = false;
        currentSession = manager.currentSession;           
    }

    private void OnEnable()
    {
        canChangeDifficulty = false;
    }

    private void OnValidate()
    {
        //Debug.Log("Review Screen Script - OnValidate");
    }

    public void UpdateLayout ()
    {
        if (ruler && prefab_Tab)
        {            
            foreach (GameObject tab in rulerTabs)
            {
                DestroyImmediate(tab);
            }

            rulerTabs.Clear();
            buttons.Clear();

            //verifica qual o review screen, e configura conforme
            switch (ReviewType)
            #region
            {
                case ENUM_ReviewType.Difficulty:
                    {
                        tabAmmount = EnumsHelperExtension.EnumSize(selected_Difficulty);
                        if (tabAmmount != buttonsProperties.Count)
                        {
                            buttonsProperties.Clear();
                            for (int i = 0; i < tabAmmount; ++i)
                            {
                                buttonsProperties.Add(null);
                            }
                        }
                        break;
                    }
                case ENUM_ReviewType.Like:
                    {
                        tabAmmount = EnumsHelperExtension.EnumSize(selected_Like);
                        if (tabAmmount != buttonsProperties.Count)
                        {
                            buttonsProperties.Clear();
                            for (int i = 0; i < tabAmmount; ++i)
                            {
                                buttonsProperties.Add(null);
                            }
                        }
                        break;
                    }
            }
            #endregion

            //instanciar e initializar os atributos dos butões
            for (int i = 0; i < buttonsProperties.Count; ++i)
            #region
            {
                switch (ReviewType)
                {
                    case ENUM_ReviewType.Difficulty:
                        {
                            var item = buttonsProperties[i];
                            GameObject rulerTab = Instantiate(prefab_Tab, transform.position, transform.rotation, transform); //instantiates the prefab as child of this component's transform
                            rulerTab.GetComponentInChildren<ReviewButton>().Initialize(item.defaultImage, item.selectedImage, EnumsHelperExtension.ToDescription((ENUM_Review_Difficulty)i), (i + 1), this);
                            buttons.Add(rulerTab.GetComponentInChildren<ReviewButton>());
                            rulerTabs.Add(rulerTab);
                            break;
                        }
                    case ENUM_ReviewType.Like:
                        {
                            var item = buttonsProperties[i];
                            GameObject rulerTab = Instantiate(prefab_Tab, transform.position, transform.rotation, transform); //instantiates the prefab as child of this component's transform
                            rulerTab.GetComponentInChildren<ReviewButton>().Initialize(item.defaultImage, item.selectedImage, EnumsHelperExtension.ToDescription((ENUM_Review_Like)i), (i + 1), this);
                            buttons.Add(rulerTab.GetComponentInChildren<ReviewButton>());
                            rulerTabs.Add(rulerTab);
                            break;
                        }
                }
            }
            #endregion

            rulerSize = ruler.GetComponent<RectTransform>().rect.size;

            totalTabSize = 0 * tabAmmount;

            spacing = (rulerSize.x - totalTabSize) / (tabAmmount - 1);

            float posX = 0;
            for (int t = 0; t < rulerTabs.Count; ++t)
            {
                RectTransform tabRectTransform = rulerTabs[t].GetComponent<RectTransform>();                
                                
                tabRectTransform.anchorMax = new Vector2(0, 0.5f);
                tabRectTransform.anchorMin = new Vector2(0, 0.5f);
                tabRectTransform.pivot = new Vector2(0, 0.5f);
                tabRectTransform.anchoredPosition = new Vector2(posX, 0);

                posX += spacing + 0;
            }
        }
        else
        {
            Debug.LogWarning("Error Caught - Null Reference Exception");
        }
    }

    public void ClearLayout ()
    {
        int children = transform.childCount;
        for (int i = children-1; i >= 0; --i)
        {
            DestroyImmediate(transform.GetChild(i).gameObject);
        }
    }

    //activates the button that was pressed by the user and deactivates all the other buttons
    public void SelectButton (ReviewButton selectedButton, int scoreValue)
    {
        if(ReviewType == ENUM_ReviewType.Difficulty)
        {
            selected_Difficulty = (ENUM_Review_Difficulty)scoreValue-1;
        }
        else
        {
            selected_Like = (ENUM_Review_Like)scoreValue-1;
        }

        //desactivar o resto dos butões
        foreach (Transform child in transform)
        {
            ReviewButton button = child.GetComponentInChildren<ReviewButton>();
            if (selectedButton != button)
            {
                button.ResetButton();
            }
        }
        base.CheckVictory();

        //guardar resultado no currently selected profile
        if (ProfileManager.selectedProfile != null)
        {
            switch (ReviewType)
            {
            #region
                case ENUM_ReviewType.Difficulty:
                    #region
                    if (selected_Difficulty.ToDescription() == "")
                    {
                        Debug.Log("Teste Review Screen lalalalalalal");
                        selected_Difficulty = ENUM_Review_Difficulty.MuitoFácil;
                    }
                    {
                        switch (currentSession)
                        {
                            case ENUM_Sessions.Session1:
                                {                                    
                                    ProfileManager.selectedProfile.session1.opinion_difficulty = selected_Difficulty.ToDescription();
                                    break;
                                }
                            case ENUM_Sessions.Session2:
                                {
                                    Debug.Log(ProfileManager.selectedProfile.session2.opinion_difficulty + "    " + selected_Difficulty);
                                    ProfileManager.selectedProfile.session2.opinion_difficulty = selected_Difficulty.ToDescription();
                                    Debug.Log(ProfileManager.selectedProfile.session2.opinion_difficulty + "    " + selected_Difficulty);
                                    break;
                                }
                            case ENUM_Sessions.Session3:
                                {
                                    ProfileManager.selectedProfile.session3.opinion_difficulty = selected_Difficulty.ToDescription(); ;
                                    break;
                                }
                            case ENUM_Sessions.Session4:
                                {
                                    ProfileManager.selectedProfile.session4.opinion_difficulty = selected_Difficulty.ToDescription(); ;
                                    break;
                                }
                            case ENUM_Sessions.Session5:
                                {
                                    ProfileManager.selectedProfile.session5.opinion_difficulty = selected_Difficulty.ToDescription(); ;
                                    break;
                                }
                            case ENUM_Sessions.Session6:
                                {
                                    ProfileManager.selectedProfile.session6.opinion_difficulty = selected_Difficulty.ToDescription(); ;
                                    break;
                                }
                            case ENUM_Sessions.Session7:
                                {
                                    ProfileManager.selectedProfile.session7.opinion_difficulty = selected_Difficulty.ToDescription(); ;
                                    break;
                                }
                            case ENUM_Sessions.Session8:
                                {
                                    ProfileManager.selectedProfile.session8.opinion_difficulty = selected_Difficulty.ToDescription();
                                    break;
                                }
                        }
                        break;
                    }
                #endregion
                case ENUM_ReviewType.Like:
                    #region
                    {
                        if (selected_Like.ToDescription() == "")
                        {
                            selected_Like = ENUM_Review_Like.GosteiMuito;
                        }
                        switch (currentSession)
                        {
                            case ENUM_Sessions.Session1:
                                {
                                    ProfileManager.selectedProfile.session1.opinion_session = selected_Like.ToDescription();
                                    break;
                                }
                            case ENUM_Sessions.Session2:
                                {
                                    ProfileManager.selectedProfile.session2.opinion_session = selected_Like.ToDescription();
                                    break;
                                }
                            case ENUM_Sessions.Session3:
                                {
                                    ProfileManager.selectedProfile.session3.opinion_session = selected_Like.ToDescription();
                                    break;
                                }
                            case ENUM_Sessions.Session4:
                                {
                                    ProfileManager.selectedProfile.session4.opinion_session = selected_Like.ToDescription();
                                    break;
                                }
                            case ENUM_Sessions.Session5:
                                {
                                    ProfileManager.selectedProfile.session5.opinion_session = selected_Like.ToDescription();
                                    break;
                                }
                            case ENUM_Sessions.Session6:
                                {
                                    ProfileManager.selectedProfile.session6.opinion_session = selected_Like.ToDescription();
                                    break;
                                }
                            case ENUM_Sessions.Session7:
                                {
                                    ProfileManager.selectedProfile.session7.opinion_session = selected_Like.ToDescription();
                                    break;
                                }
                            case ENUM_Sessions.Session8:
                                {
                                    ProfileManager.selectedProfile.session8.opinion_session = selected_Like.ToDescription();
                                    break;
                                }
                        }
                        break;
                    }
                    #endregion
            }
            #endregion
        }
    }    
}

public enum ENUM_ReviewType
{
    [Description("Dificuldade")]
    Difficulty,
    [Description("Gosto")]
    Like,
};

#if UNITY_EDITOR
[CustomEditor(typeof(ReviewScreen))]
public class ReviewScreenEditor : Editor
{
    public override void OnInspectorGUI()
    {
        ReviewScreen script = target as ReviewScreen;

        serializedObject.Update();

        EditorGUILayout.LabelField("Components", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("ruler"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("prefab_Tab"), true);
        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Button Properties", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("buttonsProperties"), true);
        EditorGUILayout.Space();

        //EditorGUILayout.Space();
        //EditorGUILayout.PropertyField(serializedObject.FindProperty("buttons"), true);
        //EditorGUILayout.PropertyField(serializedObject.FindProperty("rulerTabs"), true);
        //EditorGUILayout.Space();

        EditorGUILayout.LabelField("Review settings", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("reviewType"), true);        
        EditorGUILayout.PropertyField(serializedObject.FindProperty("currentSession"), true);
        if (script.ReviewType == ENUM_ReviewType.Difficulty)
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty("selected_Difficulty"), true);
        }
        else
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty("selected_Like"), true);
        }
        EditorGUILayout.Space();

        if (GUILayout.Button("Update"))
        {
            script.UpdateLayout();
        }
        if (GUILayout.Button("Delete Children"))
        {
            script.ClearLayout();
        }

        serializedObject.ApplyModifiedProperties();
    }
}
#endif