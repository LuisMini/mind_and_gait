﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;

public class PopUpDataFesta : MonoBehaviour
{
    public Dropdown input_festa_day;
    public Dropdown input_festa_month;
    public Dropdown input_festa_year;

    [Header("Properties")]
    private int currentYear;
    public int yearRange;
    public List<Dropdown.OptionData> listYears;
    public int lastDay;
    public List<Dropdown.OptionData> listDays;

    private void OnEnable()
    {
        StartCoroutine(ClearFields());
    }

    // Start is called before the first frame update
    void Start()
    {
        currentYear = DateTime.Now.Year;

        for (int i = 0; i < yearRange; ++i)
        {
            Dropdown.OptionData yearOption = new Dropdown.OptionData(currentYear.ToString());
            listYears.Add(yearOption);
            ++currentYear;
        }
        for (int i = 1; i <= lastDay; ++i)
        {
            Dropdown.OptionData dayOption = new Dropdown.OptionData(i.ToString());
            listDays.Add(dayOption);
        }

        UpdateInputs();
    }

    private void UpdateInputs ()
    {
        input_festa_year.ClearOptions();
        input_festa_year.AddOptions(listYears);
        input_festa_day.ClearOptions();
        input_festa_day.AddOptions(listDays);
    }

    private IEnumerator ClearFields()
    {
        yield return new WaitForEndOfFrame();
        input_festa_year.value = 0;
        input_festa_month.value = 0;
        input_festa_day.value = 0;
    }

    public void SaveDate() 
    {
        DateTime dataFesta = DateTime.Now;
        if (input_festa_year.value != 0)
        {
            int ano = Int32.Parse(input_festa_year.captionText.text);
            dataFesta = new DateTime(ano, input_festa_month.value + 1, input_festa_day.value + 1);
        }

        ProfileManager.selectedProfile.dateParty = dataFesta;
        ProfileManager.Save();

        transform.gameObject.SetActive(false);
    }
}
