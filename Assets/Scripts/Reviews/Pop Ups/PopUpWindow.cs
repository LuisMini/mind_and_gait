﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpWindow : MonoBehaviour {

    private Text popUpText;
    private Button button;

    private MonoBehaviour script;
    private string method;

    public void Initialize(string errorText, MonoBehaviour script = null, string method = "PopUp", int fontSize= 40)
    {        
        popUpText = transform.GetComponentInChildren<Text>();
        popUpText.text = errorText;
        popUpText.fontSize = fontSize;
        button = transform.GetComponentInChildren<Button>();
        button.onClick.AddListener(TaskOnClick);
        if (script != null)
        {
            this.script = script;
            this.method = method;
        }
    }

    private void TaskOnClick ()
    {
        if (script)
        {
            script.Invoke(method, 0f);
        }
        Destroy(transform.gameObject);
    }
}
