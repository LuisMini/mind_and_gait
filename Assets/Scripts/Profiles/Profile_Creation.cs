﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Networking;
using System.Text;
using SimpleJSON;

public class Profile_Creation : MonoBehaviour
{
    [HideInInspector]
    Profile newProfile = new Profile();
    private WindowManager windowManager;

    [Header("Components")]
    public GameObject deficesSensoriaisResposta_Container;
    public List<GameObject> comorbilidadesList = new List<GameObject>();

    [Header("Prefabs")]
    public GameObject popUpWindow;

    [Header("Input Elements")]
    public InputField input_name;
    public Dropdown input_birthday_year;
    public Dropdown input_birthday_month;
    public Dropdown input_birthday_day;
    public Dropdown input_sex;
    public Image photo;
    private Sprite defaultPhoto;
    private byte[] photoBytes;
    public Slider slider_escolaridade;
    public Text text_escolaridade;
    public Toggle toggle_DeficesSensoriais;
    private InputField input_DeficesSensoriais;
    public InputField input_Medicacao;
    public Button createButton;
    public InputField comorbilidadesOutra;

    [Header("Input Validation")]
    public int name_MaxCharacters;
    public int deficesSensoriais_maxCharacters;
    public int comorbilidades_maxCharacters;
    public int medicacao_maxCharacters;
    private bool yearValueChanged;

    [Header("Properties")]
    private int currentYear;
    public int yearRange;
    public List<Dropdown.OptionData> listYears;
    public int lastDay;
    public List<Dropdown.OptionData> listDays;

    private void OnEnable()
    {
        StartCoroutine(ClearFields());
    }

    // Use this for initialization
    void Start()
    {
        windowManager = GameObject.FindGameObjectWithTag("Manager").GetComponent<WindowManager>();
        defaultPhoto = photo.sprite;
        currentYear = DateTime.Now.Year;

        input_DeficesSensoriais = deficesSensoriaisResposta_Container.GetComponentInChildren<InputField>();

        createButton.onClick.AddListener(Button_Create);
        TouchScreenKeyboard.hideInput = true;

        for (int i = 0; i < yearRange; ++i)
        {
            Dropdown.OptionData yearOption = new Dropdown.OptionData(currentYear.ToString());
            listYears.Add(yearOption);
            --currentYear;
        }
        for (int i = 1; i <= lastDay; ++i)
        {
            Dropdown.OptionData dayOption = new Dropdown.OptionData(i.ToString());
            listDays.Add(dayOption);
        }

        UpdateOptions();
    }

    private void UpdateOptions()
    {
        input_birthday_year.ClearOptions();
        input_birthday_year.AddOptions(listYears);
        input_birthday_day.ClearOptions();
        input_birthday_day.AddOptions(listDays);

        input_name.characterLimit = name_MaxCharacters;
        input_DeficesSensoriais.characterLimit = deficesSensoriais_maxCharacters;
        input_Medicacao.characterLimit = medicacao_maxCharacters;
        comorbilidadesOutra.characterLimit = comorbilidades_maxCharacters;
    }

    private IEnumerator ClearFields()
    {
        yield return new WaitForEndOfFrame();
        input_name.text = "";
        photo.sprite = defaultPhoto;
        slider_escolaridade.value = 0;
        input_birthday_year.value = 0;
        input_birthday_month.value = 0;
        input_birthday_day.value = 0;
        input_sex.value = 0;
        toggle_DeficesSensoriais.isOn = false;
        input_DeficesSensoriais.text = "";
        deficesSensoriaisResposta_Container.SetActive(false);
        foreach (var item in comorbilidadesList)
        {
            item.GetComponentInChildren<Toggle>().isOn = false;
        }
        comorbilidadesOutra.text = "";
        input_Medicacao.text = "";
    }

    //get picture data from webCamManager, draws it to the picture slot and saves it on a variable
    public void SetPhoto(Texture2D photo)
    {
        this.photo.sprite = Sprite.Create(photo, new Rect(0, 0, photo.width, photo.height), new Vector2(0.5f, 0.5f));
        photoBytes = photo.EncodeToPNG();
    }

    /*Valida e instancia PopUp windows com erros*/
    private bool ValidateInputs()
    {
        if (input_name.text.Trim().Length <= 0)
        {
            PopUpWindow popUp = Instantiate(popUpWindow, transform.position, transform.rotation, GameObject.Find("Canvas").transform).GetComponent<PopUpWindow>();
            popUp.Initialize("Erro: Nenhum nome inserido.");
            return false;
        }
        else if (!yearValueChanged)
        {
            PopUpWindow popUp = Instantiate(popUpWindow, transform.position, transform.rotation, GameObject.Find("Canvas").transform).GetComponent<PopUpWindow>();
            popUp.Initialize("Erro: Data de Nascimento não escolhida.");
            return false;
        }
        return true;
    }

    private IEnumerator CoroutineCreateProfile()
    {
        CreateProfileLocal();
        yield return new WaitForEndOfFrame();
    }

    private void CreateProfileLocal()
    {
        if (ValidateInputs())
        {
            //set profile Name
            newProfile.name = input_name.text.Trim();

            //set profile picture
            if (photoBytes != null && photoBytes.Length > 0)
            {
                newProfile.photo = new byte[photoBytes.Length];
                newProfile.photo = photoBytes;
            }

            //calcular e fazer set á data de nascimento e idade
            DateTime birthdate = DateTime.Now;
            DateTime today = DateTime.Now;
            int ano = Int32.Parse(input_birthday_year.captionText.text);
            birthdate = new DateTime(ano, input_birthday_month.value + 1, input_birthday_day.value + 1);

            //idade
            int age = today.Year - birthdate.Year;
            if (birthdate > today.AddYears(-age))
            {
                age--;
            }
            newProfile.birthdate = birthdate;
            newProfile.age = age;

            //set profile sex
            newProfile.sex = (Sexo)input_sex.value;

            //set profile Anos de Escolaridade
            if ((int)slider_escolaridade.value == 13)
            {
                newProfile.anosEscolaridade = "+12";
            }
            else
            {
                newProfile.anosEscolaridade = slider_escolaridade.value.ToString();
            }

            //data de criação do profile
            newProfile.creationDate = DateTime.Now;

            //defices sensoriais se o butão estiver ON
            if (toggle_DeficesSensoriais.isOn)
            {
                newProfile.deficesSensoriais = input_DeficesSensoriais.text.Trim();
            }

            //comorbilidades
            newProfile.comorbilidades.Clear();
            foreach (var item in comorbilidadesList)
            {
                if (item.GetComponentInChildren<Toggle>().isOn)
                {
                    Profile.Comorbilidades com = new Profile.Comorbilidades();
                    com.name = item.GetComponentInChildren<Text>().text.Replace(':', ' ').Trim();
                    com.value = item.GetComponentInChildren<Toggle>().isOn;

                    newProfile.comorbilidades.Add(com);
                }
                Debug.Log(newProfile.comorbilidades.Count);
            }
            newProfile.comorbilidadesOutra = comorbilidadesOutra.text.Trim();

            //medicação
            newProfile.medicacao = input_Medicacao.text.Trim();

            //sessões
            newProfile.session1 = new Session1Data();
            newProfile.session2 = new Session2Data();
            newProfile.session3 = new Session3Data();
            newProfile.session4 = new Session4Data();
            newProfile.session5 = new Session5Data();
            newProfile.session6 = new Session6Data();
            newProfile.session7 = new Session7Data();
            newProfile.session8 = new Session8Data();

            //data da festa temporária
            newProfile.dateParty = newProfile.creationDate;

            ProfileManager.profileList.Add(newProfile);
            ProfileManager.Save();

            if (Application.internetReachability == NetworkReachability.NotReachable) //não está conectado à internet
            {
                PopUpWindow popUp = Instantiate(popUpWindow, transform.position, transform.rotation, GameObject.Find("Canvas").transform).GetComponent<PopUpWindow>();
                string errorString = "Erro: Não está conectado à internet, perfil do utente irá ser guardado apenas localmente.";
                popUp.Initialize(errorString, this, "SucessfullPatientCreation");
            }
            else //está conectado à internet
            {
                StartCoroutine(APICreatePatient());
            }
        }
    }

    private IEnumerator APICreatePatient()
    {
        Debug.Log("CRIAR UTENTE POST");
        string url = "https://mind-gait.esenfc.pt/api/patients/create";

        string json_EncodedPhoto;
        string json_Comorbilidades = "";
        string json_DeficesSensoriais;
        string json_Medication;

        string json_birth_date = (newProfile.birthdate.Year + "-" + newProfile.birthdate.Month + "-" + newProfile.birthdate.Day);
        string json_lastAcess = (newProfile.lastAcess.Year + "-" + newProfile.lastAcess.Month + "-" + newProfile.lastAcess.Day);
        string json_totalDuration = string.Format("{0:00}:{1:00}:{2:00}", newProfile.totalDuration.Hours, newProfile.totalDuration.Minutes, newProfile.totalDuration.Seconds);
        string json_partyDate = (newProfile.dateParty.Year + "-" + newProfile.dateParty.Month + "-" + newProfile.dateParty.Day);
        string json_emblemPatterns;

        //validate values
        #region
        //validate comorbilidades
        foreach (var item in newProfile.comorbilidades)
        {
            if (item.value)
            {
                if (json_Comorbilidades == "")
                {
                    json_Comorbilidades = item.name;
                }
                else
                {
                    json_Comorbilidades = json_Comorbilidades + ", " + item.name;
                }
            }
        }

        if (newProfile.comorbilidadesOutra != "")
        {
            if (json_Comorbilidades != "")
            {
                json_Comorbilidades = json_Comorbilidades + "; " + newProfile.comorbilidadesOutra;
            }
            else
            {
                json_Comorbilidades = newProfile.comorbilidadesOutra;
            }
        }

        if (json_Comorbilidades == "")
        {
            json_Comorbilidades = "Sem Informação";
        }

        //validate defices sensoriais
        if (newProfile.deficesSensoriais != "" && toggle_DeficesSensoriais.isOn)
        {
            json_DeficesSensoriais = newProfile.deficesSensoriais;
        }
        else
        {
            json_DeficesSensoriais = "Sem Informação";
        }

        //validate medication
        if (newProfile.medicacao != "")
        {
            json_Medication = newProfile.medicacao;
        }
        else
        {
            json_Medication = "Sem Informação";
        }

        //validate emblem patterns
        if (newProfile.emblemPatterns.Count > 0)
        {
            List<string> tempEmblemPatterns = newProfile.emblemPatterns.ConvertAll<string>(delegate (int i) { return i.ToString(); }); //converter a lista de ints para uma lista de strings temporaria
            json_emblemPatterns = string.Join(", ", tempEmblemPatterns.ToArray());
        }
        else
        {
            json_emblemPatterns = "Sem Informação";
        }

        //validate photo
        if (photoBytes != null && photoBytes.Length > 0) //photoBytes.Length > 0
        {
            json_EncodedPhoto = Convert.ToBase64String(photoBytes);
        }
        else
        {
            json_EncodedPhoto = null;
        }

        string id = null;
        #endregion

        string jsonString = 
            "{\"name\":\"" + newProfile.name + "\", " +
            "\"id\": \"" + id + "\", " +
            "\"photo\": \"" + json_EncodedPhoto + "\", " +
            "\"birth_date\": \"" + json_birth_date + "\", " +
            "\"age\": \"" + newProfile.age.ToString() + "\", " +
            "\"gender\": \"" + newProfile.sex.ToString() + "\", " +
            "\"school_years\": \"" + newProfile.anosEscolaridade + "\", " +
            "\"sensory_deficits_list\": \"" + json_DeficesSensoriais + "\", " +
            "\"comorbidities_list\": \"" + json_Comorbilidades + "\", " +
            "\"medication_list\": \"" + json_Medication + "\", " +
            "\"last_session\": \"" + (int)newProfile.lastSession + "\", " +
            "\"last_access\": \"" + json_lastAcess + "\", " +
            "\"total_time_on_program\": \"" + json_totalDuration + "\", " +
            "\"completed_session\": \"" + newProfile.completedSessions + "\", " +
            "\"budget_chosen\": \"" + newProfile.budget_chosen.ToString() + "\", " +
            "\"budget_updated\": \"" + newProfile.budget_updated.ToString() + "\", " +
            "\"dessert\": \"" + (int)newProfile.dessert + "\", " +
            "\"party_date\": \"" + json_partyDate + "\", " +
            "\"chosen_emblem\": \"" + (int)newProfile.chosenEmblem + "\", " +
            "\"emblem_patterns\": \"" + json_emblemPatterns + "\"}";

        using (UnityWebRequest www = new UnityWebRequest(url, "POST"))
        {
            Debug.Log(jsonString);
            byte[] bodyRaw = Encoding.UTF8.GetBytes(jsonString);
            www.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
            www.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
            www.SetRequestHeader("Authorization", "Bearer " + ProfileManager.acessToken);
            www.SetRequestHeader("Accept", "application/json");
            www.SetRequestHeader("Content-Type", "application/json");
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                PopUpWindow popUp = Instantiate(popUpWindow, transform.position, transform.rotation, GameObject.Find("Canvas").transform).GetComponent<PopUpWindow>();
                popUp.Initialize("Erro: " + www.error);
            }
            else
            {
                Debug.Log("UTENTE Form Upload Complete:" + www.downloadHandler.text);
                JSONNode svResponseData = JSON.Parse(www.downloadHandler.text);
                string patientID = svResponseData["data"]["id"].Value;
                newProfile.patientID = patientID;
                SucessfullPatientCreation();
            }
        }
    }

    private void SucessfullPatientCreation()
    {
        transform.gameObject.SetActive(false);
        windowManager.panel_SelectProfile.SetActive(true);
    }

    private void Button_Create()
    {
        StartCoroutine(CoroutineCreateProfile());
    }

    public void SliderChangeValue()
    {
        if ((int)slider_escolaridade.value == 13)
        {
            text_escolaridade.text = "+12";
        }
        else
        {
            text_escolaridade.text = slider_escolaridade.value.ToString();
        }
    }

    public void YearChangeValue()
    {
        yearValueChanged = true;
    }
}
