﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TagManager : MonoBehaviour {

    [Header("Properties")]
    [SerializeField]
    private List<string> addedTags = new List<string>();
    [Header("Prefabs")]
    public GameObject tagSlot;
    [Header("Scene Components")]
    public GameObject tagContainer;
    public InputField tagInputField;
    [SerializeField]
    private FlowingGridLayout flowingLayout;

    public List<string> AddedTags
    {
        get
        {
            return addedTags;
        }

        set
        {
            addedTags = value;
        }
    }

    public FlowingGridLayout Layout
    {
        get
        {
            return flowingLayout;
        }

        set
        {
            flowingLayout = value;
        }
    }

    private void Start()
    {
        flowingLayout = tagContainer.GetComponent<FlowingGridLayout>();
    }

    private void OnEnable()
    {
        flowingLayout = tagContainer.GetComponent<FlowingGridLayout>();
    }
    
    //instancia o objecto da tag com o container como parent
    public void AddTag (string tag = "")
    {
        string tagToAdd = "";
        if (tag != "")
        {
            tagToAdd = tag.Trim();
            addedTags.Add(tagToAdd);
        }
        else if (tagInputField.text.Trim() != "")
        {
            tagToAdd = tagInputField.text.Trim();
            addedTags.Add(tagToAdd);
            tagInputField.text = null;
        }

        if (tagToAdd != "")
        {
            GameObject tagObject = Instantiate(tagSlot, new Vector3(0, 0, 0), Quaternion.identity, tagContainer.transform);
            tagObject.GetComponent<Tag>().Initialize(tagToAdd, this);
            if (tag == "")
            {
                StartCoroutine(CallUpdateGrid());
            }
        }
    }

    public void RemoveTag (string strToRemove)
    {
        addedTags.Remove(strToRemove);        
        StartCoroutine(CallUpdateGrid());
    }

    public void CallUpdateGridIEnum()
    {
        StartCoroutine(CallUpdateGrid());
    }

    private IEnumerator CallUpdateGrid ()
    {
        Debug.Log("Got to Coroutine: Delete Tag");
        yield return new WaitForEndOfFrame();
        Debug.Log("Waited for end frame on Coroutine: Delete Tag");
        if (flowingLayout != null)
        {
            flowingLayout.UpdateGrid();
        }
        if (transform.parent.GetComponent<VerticalLayoutGroup>().isActiveAndEnabled)
        {
            StartCoroutine(UpdateParentLayot());
        }
    }

    private IEnumerator UpdateParentLayot ()
    {
        yield return new WaitForEndOfFrame();
        LayoutRebuilder.ForceRebuildLayoutImmediate(transform.parent.GetComponent<RectTransform>());
    }
}
