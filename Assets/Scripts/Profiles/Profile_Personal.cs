﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;
using System.Threading;
using System.Globalization;
using UnityEngine.Networking;
using System.Text;
using SimpleJSON;

public class Profile_Personal : MonoBehaviour
{
    public Profile personalProfile;

    [Header("Tabs Container Properties")]
    public GameObject tabsContainer;
    public GameObject dadosUtilizador;
    public GameObject dadosApp;
    private Vector2 targetPos;
    private Vector2 originalPos;

    [Header("Lerp Properties")]
    private bool lerp_SwipeLeft;
    private bool lerp_SwipeRight;
    private bool posOriginal = true;
    private float currentLerpTime = 0; //current lerp time that changes according to += Time.DeltaTime
    private float timePercent; //value between 0 and 1 for the lerp
    public float lerpTime = 1; //time it takes for the lerp to complete

    [Header("Components")]
    public Button arrowTabs;
    public Button buttonPlay;
    public Image profilePhoto;
    public Text profileName;
    public Text profileAge;
    public Text profileEscolaridade;
    public Text text_Defices;
    public Text text_Medicacao;
    public Text LastSession;
    public Text LastAcessDate;
    public Text ProgramDuration;
    public GameObject grid_comorbilidades;

    [Header("Data Festa Bailarico")]
    public Dropdown input_DayFesta;
    public Dropdown input_MonthFesta;
    public Dropdown input_YearFesta;
    public int yearRange;
    [HideInInspector]
    public List<Dropdown.OptionData> listYears;
    public int lastDay;
    [HideInInspector]
    public List<Dropdown.OptionData> listDays;

    [Header("Prefabs")]
    public GameObject tagDisplay;
    public Sprite defaultProfilePhoto;

    // Use this for initialization
    void Start()
    {
        float objectWidth = tabsContainer.GetComponent<RectTransform>().rect.width;
        dadosApp.transform.localPosition = new Vector3(objectWidth, dadosApp.transform.localPosition.y, dadosApp.transform.localPosition.z);
        targetPos = new Vector2(-objectWidth, tabsContainer.transform.localPosition.y);
        originalPos = tabsContainer.transform.localPosition;
        arrowTabs.onClick.AddListener(SwipeLeft);

        //criar as listas de options para meter nos dropdowns
        int currentYear = DateTime.Now.Year;
        for (int i = 0; i < yearRange; ++i)
        {
            Dropdown.OptionData yearOption = new Dropdown.OptionData(currentYear.ToString());
            listYears.Add(yearOption);
            ++currentYear;
        }
        for (int i = 1; i <= lastDay; ++i)
        {
            Dropdown.OptionData dayOption = new Dropdown.OptionData(i.ToString());
            listDays.Add(dayOption);
        }

        UpdateInputs();
    }

    private void Update()
    {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            Vector2 touchDelta;
            touchDelta = Input.GetTouch(0).deltaPosition;
            if (touchDelta.x > 10) //This was a flick to the left with magnitude of 5 or more
            {
                if (!lerp_SwipeRight && !lerp_SwipeLeft && !posOriginal)
                {
                    lerp_SwipeRight = true;
                }
            }
            if (touchDelta.x < -10) //This was a flick to the right with magnitude of 5 or more
            {
                if (!lerp_SwipeLeft && !lerp_SwipeRight && posOriginal)
                {
                    lerp_SwipeLeft = true;
                }
            }
        }

        if (lerp_SwipeLeft)
        {
            currentLerpTime += Time.deltaTime;
            if (currentLerpTime >= lerpTime)
            {
                currentLerpTime = lerpTime;
            }
            //Debug.Log("Update - Lerp");
            timePercent = currentLerpTime / lerpTime;
            tabsContainer.transform.localPosition = Vector2.Lerp(originalPos, targetPos, timePercent);

            if ((Vector2)tabsContainer.transform.localPosition == targetPos) //se a posição que chegou for a target pos
            {
                //Debug.Log("Lerp Swipe Left = FALSE");
                lerp_SwipeLeft = false;
                currentLerpTime = 0;
                posOriginal = false;
                UpdateTabArrow();
            }
        }

        if (lerp_SwipeRight)
        {
            currentLerpTime += Time.deltaTime;
            if (currentLerpTime >= lerpTime)
            {
                currentLerpTime = lerpTime;
            }
            //Debug.Log("Update - Lerp");
            timePercent = currentLerpTime / lerpTime;
            tabsContainer.transform.localPosition = Vector2.Lerp(targetPos, originalPos, timePercent);

            if ((Vector2)tabsContainer.transform.localPosition == originalPos) //se a posição que chegou voltou á original
            {
                //Debug.Log("Lerp Swipe Right = False");
                lerp_SwipeRight = false;
                currentLerpTime = 0;
                posOriginal = true;
                UpdateTabArrow();
            }
        }
    }

    private void OnEnable()
    {
        StartCoroutine(ClearFields());

        personalProfile = ProfileManager.selectedProfile;

        tabsContainer.transform.localPosition = originalPos;
        posOriginal = true;
        lerp_SwipeLeft = false;
        lerp_SwipeRight = false;
        UpdateTabArrow();

        dadosUtilizador.SetActive(true);
        dadosApp.SetActive(true);

        ProfileManager.selectedProfile.lastAcess = DateTime.Now; //atualizar a data de ultimo acesso
        CultureInfo originalCulture = Thread.CurrentThread.CurrentCulture; //alterar a current culture para pt-PT, isto é para alterar o formato da data quando for apresentada
        Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-PT");  //^

        profileName.text = personalProfile.name;
        profileAge.text = personalProfile.age.ToString() + " Anos";
        profileEscolaridade.text = "Escolaridade: " + personalProfile.anosEscolaridade;
        text_Defices.text = personalProfile.deficesSensoriais;
        text_Medicacao.text = personalProfile.medicacao;
        FillComorbilidades();

        if (personalProfile.photo != null && personalProfile.photo.Length > 0)
        {
            Texture2D picture = new Texture2D(0, 0);
            picture.LoadImage(personalProfile.photo);
            picture.Apply();
            profilePhoto.sprite = Sprite.Create(picture, new Rect(0, 0, picture.width, picture.height), new Vector2(0.5f, 0.5f));
            Debug.Log("PICTURE WIDTH: " + picture.width + " | " + "PICTURE HEIGHT: " + picture.height);
        }

        input_DayFesta.onValueChanged.AddListener(delegate { ConfirmDate(); });
        input_MonthFesta.onValueChanged.AddListener(delegate { ConfirmDate(); });
        input_YearFesta.onValueChanged.AddListener(delegate { ConfirmDate(); });
        StartCoroutine(GetProfileDate());

        //ver se alguma sessão foi completa, e guardar a ultima que foi completa
        bool anySessionCompleted = false;
        if (ProfileManager.selectedProfile.completedSessions > 0)
        {
            anySessionCompleted = true;
            int sessionNumber = (int)personalProfile.lastSession + 1;
            LastSession.text = "Sessão Nº" + sessionNumber.ToString();
        }
        if (anySessionCompleted == false)
        {
            LastSession.text = "Nenhuma";
        }

        LastAcessDate.text = personalProfile.lastAcess.ToShortDateString();
        ProgramDuration.text = string.Format("{0:00}:{1:00}:{2:00}", personalProfile.totalDuration.Hours, personalProfile.totalDuration.Minutes, personalProfile.totalDuration.Seconds);

        StartCoroutine(ProfileManager.APIUpdatePatientProfile()); //para atualizar last acess no servidor
    }

    private void UpdateInputs()
    {
        input_YearFesta.ClearOptions();
        input_YearFesta.AddOptions(listYears);
        input_DayFesta.ClearOptions();
        input_DayFesta.AddOptions(listDays);
    }

    private IEnumerator ClearFields()
    {
        input_YearFesta.value = 0;
        input_MonthFesta.value = 0;
        input_DayFesta.value = 0;
        profilePhoto.sprite = defaultProfilePhoto;
        yield return new WaitForEndOfFrame();
    }

    private IEnumerator GetProfileDate()
    {
        yield return new WaitForEndOfFrame();

        if (personalProfile.dateParty != personalProfile.creationDate)
        {
            buttonPlay.interactable = true;

            Debug.Log("Ano: " + input_YearFesta.options.Count.ToString() + " | Selected Profile: " + ProfileManager.selectedProfile.dateParty.Year.ToString());
            for (int i = 0; i < input_YearFesta.options.Count; ++i)
            {
                if (input_YearFesta.options[i].text == (personalProfile.dateParty.Year).ToString())
                {
                    input_YearFesta.value = i;
                    break;
                }
            }
            Debug.Log("Mês: " + input_MonthFesta.options.Count.ToString() + " | Selected Profile: " + ProfileManager.selectedProfile.dateParty.Month.ToString());
            for (int i = 0; i < input_MonthFesta.options.Count; ++i)
            {
                if (i == personalProfile.dateParty.Month - 1)
                {
                    input_MonthFesta.value = i;
                    break;
                }
            }
            Debug.Log("Dia: " + input_DayFesta.options.Count.ToString() + " | Selected Profile: " + ProfileManager.selectedProfile.dateParty.Day.ToString());
            for (int i = 0; i < input_DayFesta.options.Count; ++i)
            {
                if (input_DayFesta.options[i].text == (personalProfile.dateParty.Day).ToString())
                {
                    input_DayFesta.value = i;
                    break;
                }
            }
        }
        else
        {
            input_YearFesta.value = 0;
            input_MonthFesta.value = 0;
            input_DayFesta.value = 0;
            buttonPlay.interactable = false;
        }
    }

    /// <summary>
    /// Populates the text field for the Comorbilidades
    /// </summary>
    private void FillComorbilidades()
    {
        if (grid_comorbilidades.transform.childCount > 0)
        {
            for (int i = 0; i < grid_comorbilidades.transform.childCount; ++i)
            {
                Destroy(grid_comorbilidades.transform.GetChild(i).gameObject);
            }
        }

        for (int i = 0; i < personalProfile.comorbilidades.Count; ++i)
        {
            if (personalProfile.comorbilidades[i].value == true)
            {
                GameObject newItem = Instantiate(tagDisplay, grid_comorbilidades.transform);
                newItem.GetComponent<Text>().text = " - " + personalProfile.comorbilidades[i].name;
            }
        }
        if (personalProfile.comorbilidadesOutra != "")
        {
            GameObject lastItem = Instantiate(tagDisplay, grid_comorbilidades.transform);
            lastItem.GetComponent<Text>().text = " - " + personalProfile.comorbilidadesOutra;
        }
    }

    private void FillGrid(List<string> list, GameObject grid)
    {
        if (grid.transform.childCount > 0)
        {
            for (int i = 0; i < grid.transform.childCount; ++i)
            {
                Destroy(grid.transform.GetChild(i).gameObject);
            }
        }

        for (int i = 0; i < list.Count; ++i)
        {
            GameObject newItem = Instantiate(tagDisplay, grid.transform);
            newItem.GetComponent<Text>().text = " - " + list[i];
        }
    }

    public void SwipeLeft()
    {
        if (!lerp_SwipeLeft && !lerp_SwipeRight && posOriginal)
        {
            lerp_SwipeLeft = true;
            arrowTabs.onClick.AddListener(SwipeRight);
        }
    }

    public void SwipeRight()
    {
        if (!lerp_SwipeRight && !lerp_SwipeLeft && !posOriginal)
        {
            lerp_SwipeRight = true;
            arrowTabs.onClick.AddListener(SwipeLeft);
        }
    }

    private void UpdateTabArrow()
    {
        float rotation;
        if (posOriginal)
        {
            rotation = 180;
        }
        else
        {
            rotation = 0;
        }
        arrowTabs.gameObject.transform.GetChild(0).transform.localRotation = new Quaternion(0, 0, rotation, 0);
    }

    private void ConfirmDate()
    {
        bool dateValid;
        DateTime dateTimeNow = new DateTime();
        dateTimeNow = DateTime.Now;
        DateTime dataFesta = new DateTime();

        int year = Int32.Parse(input_YearFesta.captionText.text);
        Debug.Log(year);
        dataFesta = new DateTime(year, input_MonthFesta.value + 1, input_DayFesta.value + 1);

        int result = DateTime.Compare(dataFesta, dateTimeNow);
        //earlier date than
        if (result < 0)
        {
            dateValid = false;
        }
        //same date as
        else if (result == 0)
        {
            dateValid = false;
        }
        //later than, its valid
        else
        {
            dateValid = true;
        }

        if (dateValid)
        {
            ProfileManager.selectedProfile.dateParty = dataFesta;
            buttonPlay.interactable = true;
            Debug.Log("Data é valida! | " + ProfileManager.selectedProfile.dateParty.ToLongDateString());
            ProfileManager.Save();
            StartCoroutine(ProfileManager.APIUpdatePatientProfile());
        }
        else
        {
            Debug.Log("Data NÃO é valida! | " + dataFesta.ToLongDateString());
            buttonPlay.interactable = false;
        }
    }
}
