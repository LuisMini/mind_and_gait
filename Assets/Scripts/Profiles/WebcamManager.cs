﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WebcamManager : MonoBehaviour
{
    private bool camAvailable;
#pragma warning disable CS0108 // Member hides inherited member; missing new keyword
    private WebCamTexture camera;
#pragma warning restore CS0108 // Member hides inherited member; missing new keyword
    private WebCamDevice[] devices;
    private int cameraID;
    public Texture2D photo;

    public RawImage background;
    public AspectRatioFitter fit;
    public GameObject cameraUI;

    [Header("Pop-Up")]
    public GameObject PopUpPrefab;

    void Start()
    {
        devices = WebCamTexture.devices;

        if (devices.Length == 0)
        {
            camAvailable = false;
            return;
        }

        for (int i = 0; i < devices.Length; ++i)
        {
            if (!devices[i].isFrontFacing)
            {
                camera = new WebCamTexture(devices[i].name, Screen.width, Screen.height);
                cameraID = i;
            }
        }

        if (camera == null)
        {
            camAvailable = false;
            return;
        }

        camAvailable = true;
    }

    private void Update()
    {
        if (!camAvailable)
        {
            CloseCamera();
            return;
        }
        float ratio = (float)camera.width / (float)camera.height;
        fit.aspectRatio = ratio;

        if (devices[cameraID].isFrontFacing)
        {
            float scaleY = camera.videoVerticallyMirrored ? -1f : 1f;
            background.rectTransform.localScale = new Vector3(-1f, scaleY, 1f);
        }
        else
        {
            float scaleY = camera.videoVerticallyMirrored ? -1f : 1f;
            background.rectTransform.localScale = new Vector3(1f, scaleY, 1f);
        }

        if (devices[cameraID].isFrontFacing)
        {
            int orient = camera.videoRotationAngle;
            background.rectTransform.localEulerAngles = new Vector3(0, 0, orient);
        }
        else
        {
            int orient = -camera.videoRotationAngle;
            background.rectTransform.localEulerAngles = new Vector3(0, 0, orient);
        }
    }

    public void SwitchCamera()
    {
        if (devices.Length == 0)
        {
            camAvailable = false;
            return;
        }
        else
        {
            cameraID += 1;
            if (cameraID >= devices.Length)
            {
                cameraID = 0;
            }
            camAvailable = true;
            camera = new WebCamTexture(devices[cameraID].name, Screen.width, Screen.height);
            camera.Play();
            background.texture = camera;
        }
    }

    public void TakePicture()
    {
        StartCoroutine(TakePhoto());
    }

    IEnumerator TakePhoto()  // Start this Coroutine on some button click
    {
        if (!camAvailable)
        {
            GameObject parent = null;
            if (GameObject.FindGameObjectWithTag("Manager").GetComponent<WindowManager>())
            {
                parent = GameObject.FindGameObjectWithTag("Manager").GetComponent<WindowManager>().panel_CreateProfile;
            }
            PopUpWindow popUp = Instantiate(PopUpPrefab, parent.transform.position, transform.rotation, GameObject.Find("Canvas").transform).GetComponent<PopUpWindow>();
            popUp.Initialize("Erro: Nenhuma camera disponivel.", this, "CloseCamera");
        }
        else
        {

            print("Take Photo");
            yield return new WaitForEndOfFrame();

            photo = new Texture2D(camera.width, camera.height);
            photo.SetPixels(camera.GetPixels());
            photo.Apply();

            if (GetComponentInParent<Profile_Creation>())
            {
                GetComponentInParent<Profile_Creation>().SetPhoto(photo);
            }
            else if (GetComponentInParent<Profile_Edit>())
            {
                GetComponentInParent<Profile_Edit>().SetPhoto(photo);
            }
            //Encode to a PNG
            //byte[] bytes = photo.EncodeToPNG();

            ////Write out the PNG. Of course you have to substitute your_path for something sensible
            //File.WriteAllBytes(your_path + "photo.png", bytes);

            CloseCamera();
        }
    }

    private void OnMouseDown()
    {
        Debug.Log("On Mouse Down");
        if (!camAvailable)
        {
            GameObject parent = null;
            if (GameObject.FindGameObjectWithTag("Manager").GetComponent<WindowManager>())
            {
                parent = GameObject.FindGameObjectWithTag("Manager").GetComponent<WindowManager>().panel_CreateProfile;
            }
            PopUpWindow popUp = Instantiate(PopUpPrefab, parent.transform.position, transform.rotation, GameObject.Find("Canvas").transform).GetComponent<PopUpWindow>();
            popUp.Initialize("Erro: Nenhuma camera disponivel.", this, "CloseCamera");
            return;
        }
        background.gameObject.SetActive(true);
        camera.Play();
        background.texture = camera;
        cameraUI.SetActive(true);
    }

    public void CloseCamera()
    {
        background.gameObject.SetActive(false);
        cameraUI.SetActive(false);
    }
}