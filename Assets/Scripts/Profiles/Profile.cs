﻿using System;
using System.Collections.Generic;
using UnityEngine;

//[System.Serializable]
[Serializable()]
public class Profile {
    [Header("Personal Info")]
    public string name; //nome do utilizador
    public byte[] photo;
    public DateTime birthdate; //data de nascimento do utilizador
    public int age; //idade do utilizador
    public Sexo sex; //sexo do utilizador
    public string anosEscolaridade; //ano de escolaridade do utilizador
    public string deficesSensoriais; //defices sensoriais do utilizador resposta aberta
    public string medicacao; //medicação a ser tomada pelo utilizador
    public List<Comorbilidades> comorbilidades = new List<Comorbilidades>(); //lista de comorbilidades (nome + valor) do utilizador
    public string comorbilidadesOutra; //outras comorbilidades resposta aberta
    public string patientID;

    [Header("App Data")]
    public ENUM_Sessions lastSession; //ultima sessão a ser jogada
    public DateTime creationDate; //data de criação do profile    
    public DateTime lastAcess; //data do ultimo login
    public TimeSpan totalDuration; //duração total do programa 
    public int completedSessions; //quantas sessões ja foram completas
    public float budget_chosen;
    public float budget_updated;
    public ENUM_Sobremesa dessert;
    public DateTime dateParty;
    public ENUM_Emblema chosenEmblem;
    public List<int> emblemPatterns = new List<int>(); //lista para guardar os padrões que foram utilizados
    public Session1Data session1;
    public Session2Data session2;
    public Session3Data session3;
    public Session4Data session4;
    public Session5Data session5;
    public Session6Data session6;
    public Session7Data session7;
    public Session8Data session8;

    [Serializable]
    public struct Comorbilidades
    {
        public string name;
        public bool value;
    }

    public void ResetProfile ()
    {
        name = null;
        birthdate = DateTime.Now;
        age = 0;
        sex = 0;
        anosEscolaridade = null;
    }
}

[System.Serializable]
public class Session1Data
{
    public int session_invitation_tries;
    public string session_date;
    public string session_duration;
    //--
    public int activity_invitation_tries;
    public int activity_invitation_correct_answers;
    public int activity_invitation_wrong_answers;
    public int activity_invitation_correct_answers_not_selected;
    public string activity_invitation_difficulty;
    //--
    public int activity_party_day_tries;
    public int activity_party_day_correct_answers;
    public int activity_party_day_wrong_answers;
    public int activity_party_day_correct_answers_not_selected;
    public string activity_party_day_difficulty;
    //--
    public int activity_party_hour_start_tries;
    public int activity_party_hour_start_correct_answers;
    public int activity_party_hour_start_wrong_answers;
    public int activity_party_hour_start_correct_answers_not_selected;
    public string activity_party_hour_start_difficulty;
    //--
    public int activity_party_hour_end_correct_answers;
    public int activity_party_hour_end_wrong_answers;
    public int activity_party_hour_end_chosen_hour;
    public int activity_party_hour_end_chosen_minute;
    //--
    public int activity_write_letter_tries;
    public int activity_write_letter_correct_answers;
    public int activity_write_letter_wrong_answers;
    public int activity_write_letter_correct_answers_not_selected;
    public string activity_write_letter_difficulty;
    public string activity_write_letter_descriptions;
    //--
    public int activity_write_answer_invitation_correct_answers;
    public int activity_write_answer_invitation_wrong_answers;
    //--
    public string activity_answer_invitation_name;
    public string activity_answer_invitation_address;
    public string activity_answer_invitation_city;
    //--
    public string proverb_difficulty;
    public string proverb_written_answer;
    //--
    public string opinion_difficulty;
    public string opinion_session;
    //--
    public string patient_id;
}
[System.Serializable]
public class Session2Data
{
    public int session_invitation_tries;
    public string session_date;
    public string session_duration;
    //--
    public int orientation_party_day_tries;
    public int orientation_party_day_correct_answers;
    public int orientation_party_day_wrong_answers;
    public int orientation_party_day_correct_answers_not_selected;
    public string orientation_party_day_difficulty;
    //--
    public int orientation_party_place_tries;
    public int orientation_party_place_correct_answers;
    public int orientation_party_place_wrong_answers;
    public int orientation_party_place_correct_answers_not_selected;
    public string orientation_party_place_answers_descriptions;
    //--
    public int orientation_invitation_address_tries;
    public int orientation_invitation_address_correct_answers;
    public int orientation_invitation_address_wrong_answers;
    public int orientation_invitation_address_correct_answers_not_selected;
    public string orientation_invitation_address_difficulty;
    //--
    public int orientation_party_hour_start_tries;
    public int orientation_party_hour_start_correct_answers;
    public int orientation_party_hour_start_wrong_answers;
    public int orientation_party_hour_start_correct_answers_not_selected;
    public string orientation_party_hour_start_difficulty;
    //--
    public int orientation_party_hour_end_tries;
    public int orientation_party_hour_end_correct_answers;
    public int orientation_party_hour_end_wrong_answers;
    public int orientation_party_hour_end_correct_answers_not_selected;
    public string orientation_party_hour_end_difficulty;
    //--
    public int activity_car_tries;
    public int activity_car_correct_answers;
    public int activity_car_wrong_answers;
    public int activity_car_correct_answers_not_selected;
    public string activity_car_difficulty;
    public string activity_car_answer_description;
    //--
    public int activity_bike_tries;
    public int activity_bike_correct_answers;
    public int activity_bike_wrong_answers;
    public int activity_bike_correct_answers_not_selected;
    public string activity_bike_difficulty;
    public string activity_bike_answer_description;
    //--
    public int activity_bicycle_tries;
    public int activity_bicycle_correct_answers;
    public int activity_bicycle_wrong_answers;
    public int activity_bicycle_correct_answers_not_selected;
    public string activity_bicycle_difficulty;
    public string activity_bicycle_answer_description;
    //--
    public int activity_train_tries;
    public int activity_train_correct_answers;
    public int activity_train_wrong_answers;
    public int activity_train_correct_answers_not_selected;
    public string activity_train_difficulty;
    public string activity_train_answer_description;
    //--
    public int activity_airplane_tries;
    public int activity_airplane_correct_answers;
    public int activity_airplane_wrong_answers;
    public int activity_airplane_correct_answers_not_selected;
    public string activity_airplane_difficulty;
    public string activity_airplane_answer_description;
    //--
    public int activity_boat_tries;
    public int activity_boat_correct_answers;
    public int activity_boat_wrong_answers;
    public int activity_boat_correct_answers_not_selected;
    public string activity_boat_difficulty;
    public string activity_boat_answer_description;
    //--
    public int activity_two_wheels_tries;
    public int activity_two_wheels_correct_answers;
    public int activity_two_wheels_wrong_answers;
    public int activity_two_wheels_correct_answers_not_selected;
    public string activity_two_wheels_difficulty;
    public string activity_two_wheels_answer_description;
    //--
    public int activity_map_wrong_answers;
    public string activity_map_difficulty;
    //--
    public int proverb_correct_answers;
    public int proverb_wrong_answers;
    public int proverb_correct_answers_not_selected;
    public string proverb_written_answer;
    //--
    public string opinion_difficulty;
    public string opinion_session;
    //--
    public string patient_id;
}
[System.Serializable]
public class Session3Data
{
    public int session_invitation_tries;
    public string session_date;
    public string session_duration;
    //--
    public int orientation_party_day_tries;
    public int orientation_party_day_correct_answers;
    public int orientation_party_day_wrong_answers;
    public int orientation_party_day_correct_answers_not_selected;
    public string orientation_party_day_difficulty;
    //--
    public int orientation_party_place_tries;
    public int orientation_party_place_correct_answers;
    public int orientation_party_place_wrong_answers;
    public int orientation_party_place_correct_answers_not_selected;
    public string orientation_party_place_descriptions;
    //--
    public int orientation_invitation_address_tries;
    public int orientation_invitation_address_correct_answers;
    public int orientation_invitation_address_wrong_answers;
    public int orientation_invitation_address_correct_answers_not_selected;
    public string orientation_invitation_address_difficulty;
    //--
    public int orientation_party_hour_start_tries;
    public int orientation_party_hour_start_correct_answers;
    public int orientation_party_hour_start_wrong_answers;
    public int orientation_party_hour_start_correct_answers_not_selected;
    public string orientation_party_hour_start_difficulty;
    //--
    public int orientation_party_hour_end_tries;
    public int orientation_party_hour_end_correct_answers;
    public int orientation_party_hour_end_wrong_answers;
    public int orientation_party_hour_end_correct_answers_not_selected;
    public string orientation_party_hour_end_difficulty;
    //--
    public int orientation_what_to_take_tries;
    public int orientation_what_to_take_correct_answers;
    public int orientation_what_to_take_wrong_answers;
    public int orientation_what_to_take_correct_answers_not_selected;
   //--
    public int activity_memory_game_pairs_turned;
    public int activity_memory_game_pairs_correct;
    public int activity_memory_game_pairs_wrong;
    public string activity_memory_game_difficulty;
    public string activity_memory_game_2_2;
    public string activity_memory_game_5_5;
    public string activity_memory_game_10_10;
    //--
    public string activity_order_time;
    //--
    public int budget_wrong_answers;
    public string budget_chosen;
    public string budget_writen_answer;
    public string budget_difficulty;
    //--
    public int proverb_correct_answers;
    public int proverb_wrong_answers;
    public int proverb_correct_answers_not_selected;
    public string proverb_written_answer;
    public string proverb_answers_description;
    //--
    public string opinion_difficulty;
    public string opinion_session;
    //--
    public string patient_id;
}
[System.Serializable]
public class Session4Data
{
    public int session_invitation_tries;
    public string session_date;
    public string session_duration;
    //--
    public int orientation_party_day_tries;
    public int orientation_party_day_correct_answers;
    public int orientation_party_day_wrong_answers;
    public int orientation_party_day_correct_answers_not_selected;
    public string orientation_party_day_difficulty;
    //--
    public int orientation_party_place_tries;
    public int orientation_party_place_correct_answers;
    public int orientation_party_place_wrong_answers;
    public int orientation_party_place_correct_answers_not_selected;
    public string orientation_party_place_descriptions;
    //--
    public int orientation_invitation_address_tries;
    public int orientation_invitation_address_correct_answers;
    public int orientation_invitation_address_wrong_answers;
    public int orientation_invitation_address_correct_answers_not_selected;
    public string orientation_invitation_address_difficulty;
    //--
    public int orientation_party_hour_start_tries;
    public int orientation_party_hour_start_correct_answers;
    public int orientation_party_hour_start_wrong_answers;
    public int orientation_party_hour_start_correct_answers_not_selected;
    public string orientation_party_hour_start_difficulty;
    //--
    public int orientation_party_hour_end_tries;
    public int orientation_party_hour_end_correct_answers;
    public int orientation_party_hour_end_wrong_answers;
    public int orientation_party_hour_end_correct_answers_not_selected;
    public string orientation_party_hour_end_difficulty;
    //--
    public int orientation_what_to_take_tries;
    public int orientation_what_to_take_correct_answers;
    public int orientation_what_to_take_wrong_answers;
    public int orientation_what_to_take_correct_answers_not_selected;
    //--
    public int orientation_budget_tries;
    public int orientation_budget_correct_answers;
    public int orientation_budget_wrong_answers;
    public int orientation_budget_correct_answers_not_selected;
    public string orientation_budget_difficulty;
    //--
    public int activity_associate_images_wrong_answers;
    public string activity_associate_images_difficulty;
    //--
    public int activity_select_not_dessert_correct_answers;
    public int activity_select_not_dessert_wrong_answers;
    public int activity_select_not_dessert_correct_answers_not_selected;
    public string activity_select_not_dessert_answers_descriptions;
    //--
    public string activity_select_dessert_chosen_dessert;
    public string activity_select_dessert_difficulty;
    //--
    public int activity_kitchen_utensils_tries;
    public int activity_kitchen_utensils_correct_answers;
    public int activity_kitchen_utensils_wrong_answers;
    public int activity_kitchen_utensils_correct_answers_not_selected;
    public string activity_kitchen_utensils_difficulty;
    public string activity_kitchen_utensils_answers_descriptions;
    //--
    public string proverb_difficulty;
    public string proverb_time;
    public string proverb_written_answer;
    //--
    public string opinion_difficulty;
    public string opinion_session;
    //--
    public string patient_id;
}
[System.Serializable]
public class Session5Data
{
    public int session_invitation_tries;
    public string session_date;
    public string session_duration;
    //--
    public int orientation_party_day_tries;
    public int orientation_party_day_correct_answers;
    public int orientation_party_day_wrong_answers;
    public int orientation_party_day_correct_answers_not_selected;
    public string orientation_party_day_difficulty;
    //--
    public int orientation_party_place_tries;
    public int orientation_party_place_correct_answers;
    public int orientation_party_place_wrong_answers;
    public int orientation_party_place_correct_answers_not_selected;
    public string orientation_party_place_descriptions;
    //--
    public int orientation_invitation_address_tries;
    public int orientation_invitation_address_correct_answers;
    public int orientation_invitation_address_wrong_answers;
    public int orientation_invitation_address_correct_answers_not_selected;
    public string orientation_invitation_address_difficulty;
    //--
    public int orientation_party_hour_start_tries;
    public int orientation_party_hour_start_correct_answers;
    public int orientation_party_hour_start_wrong_answers;
    public int orientation_party_hour_start_correct_answers_not_selected;
    public string orientation_party_hour_start_difficulty;
    //--
    public int orientation_party_hour_end_tries;
    public int orientation_party_hour_end_correct_answers;
    public int orientation_party_hour_end_wrong_answers;
    public int orientation_party_hour_end_correct_answers_not_selected;
    public string orientation_party_hour_end_difficulty;
    //--
    public int orientation_what_to_take_tries;
    public int orientation_what_to_take_correct_answers;
    public int orientation_what_to_take_wrong_answers;
    public int orientation_what_to_take_correct_answers_not_selected;
    //--
    public int orientation_budget_tries;
    public int orientation_budget_correct_answers;
    public int orientation_budget_wrong_answers;
    public int orientation_budget_correct_answers_not_selected;
    public string orientation_budget_difficulty;
    //--
    public string orientation_dessert_correct;
    //--
    public int activity_ingredients_correct_answers;
    public int activity_ingredients_wrong_answers;
    //--
    public int activity_shopping_correct_answers;
    public int activity_shopping_wrong_answers;
    //--
    public int activity_order_sum_correct_answers;
    public int activity_order_sum_wrong_answers;
    public string activity_order_sum_written_answer;
    //--
    public string activity_budget_remaining_written_answer;
    //--
    public int activity_recipe_utensils_correct_answers;
    public int activity_recipe_utensils_wrong_answers;
    public string activity_recipe_utensils_time;
    //--
    public int activity_recipe_ingredients_correct_answers;
    public int activity_recipe_ingredients_wrong_answers;
    public string activity_recipe_ingredients_time;
    //--
    public string proverb_written_answer;
    //--
    public string opinion_difficulty;
    public string opinion_session;
    //--
    public string patient_id;
}
[System.Serializable]
public class Session6Data
{
    public int session_invitation_tries;
    public string session_date;
    public string session_duration;
    //--
    public int orientation_party_day_tries;
    public int orientation_party_day_correct_answers;
    public int orientation_party_day_wrong_answers;
    public int orientation_party_day_correct_answers_not_selected;
    public string orientation_party_day_difficulty;
    //--
    public int orientation_party_place_tries;
    public int orientation_party_place_correct_answers;
    public int orientation_party_place_wrong_answers;
    public int orientation_party_place_correct_answers_not_selected;
    public string orientation_party_place_descriptions;
    //--
    public int orientation_invitation_address_tries;
    public int orientation_invitation_address_correct_answers;
    public int orientation_invitation_address_wrong_answers;
    public int orientation_invitation_address_correct_answers_not_selected;
    public string orientation_invitation_address_difficulty;
    //--
    public int orientation_party_hour_start_tries;
    public int orientation_party_hour_start_correct_answers;
    public int orientation_party_hour_start_wrong_answers;
    public int orientation_party_hour_start_correct_answers_not_selected;
    public string orientation_party_hour_start_difficulty;
    //--
    public int orientation_party_hour_end_tries;
    public int orientation_party_hour_end_correct_answers;
    public int orientation_party_hour_end_wrong_answers;
    public int orientation_party_hour_end_correct_answers_not_selected;
    public string orientation_party_hour_end_difficulty;
    //--
    public int orientation_what_to_take_tries;
    public int orientation_what_to_take_correct_answers;
    public int orientation_what_to_take_wrong_answers;
    public int orientation_what_to_take_correct_answers_not_selected;
    //--
    public int orientation_budget_tries;
    public int orientation_budget_correct_answers;
    public int orientation_budget_wrong_answers;
    public int orientation_budget_correct_answers_not_selected;
    public string orientation_budget_difficulty;
    //--
    public string orientation_dessert_correct;
    //--
    public int activity_phone_wrong_answers;
    public string activity_phone_deleted_message;
    //--
    public int activity_hat_color_correct_answers;
    public int activity_hat_color_wrong_answers;
    //--
    public string activity_choose_emblem_choice;
    //--
    public string activity_create_emblem_time;
    //---
    public int activity_personalize_emblem_wrong_answers;
    public string activity_personalize_emblem_time;
    //--
    public int proverb_correct_answers;
    public int proverb_wrong_answers;
    public int proverb_correct_answers_not_selected;
    public string proverb_written_answer;
    //--
    public string opinion_difficulty;
    public string opinion_session;
    //--
    public string patient_id;
}
[System.Serializable]
public class Session7Data
{
    public int session_invitation_tries;
    public string session_date;
    public string session_duration;
    //--
    public int orientation_party_day_tries;
    public int orientation_party_day_correct_answers;
    public int orientation_party_day_wrong_answers;
    public int orientation_party_day_correct_answers_not_selected;
    public string orientation_party_day_difficulty;
    //--
    public int orientation_party_place_tries;
    public int orientation_party_place_correct_answers;
    public int orientation_party_place_wrong_answers;
    public int orientation_party_place_correct_answers_not_selected;
    public string orientation_party_place_descriptions;
    //--
    public int orientation_invitation_address_tries;
    public int orientation_invitation_address_correct_answers;
    public int orientation_invitation_address_wrong_answers;
    public int orientation_invitation_address_correct_answers_not_selected;
    public string orientation_invitation_address_difficulty;
    //--
    public int orientation_party_hour_start_tries;
    public int orientation_party_hour_start_correct_answers;
    public int orientation_party_hour_start_wrong_answers;
    public int orientation_party_hour_start_correct_answers_not_selected;
    public string orientation_party_hour_start_difficulty;
    //--
    public int orientation_party_hour_end_tries;
    public int orientation_party_hour_end_correct_answers;
    public int orientation_party_hour_end_wrong_answers;
    public int orientation_party_hour_end_correct_answers_not_selected;
    public string orientation_party_hour_end_difficulty;
    //--
    public int orientation_what_to_take_tries;
    public int orientation_what_to_take_correct_answers;
    public int orientation_what_to_take_wrong_answers;
    public int orientation_what_to_take_correct_answers_not_selected;
    //--
    public int orientation_budget_tries;
    public int orientation_budget_correct_answers;
    public int orientation_budget_wrong_answers;
    public int orientation_budget_correct_answers_not_selected;
    public string orientation_budget_difficulty;
    //--
    public string orientation_dessert_correct;
    //--
    public int orientation_hat_tries;
    public int orientation_hat_correct_answers;
    public int orientation_hat_wrong_answers;
    public int orientation_hat_correct_answers_not_selected;
    public string orientation_hat_difficulty;
    //--
    public string activity_choose_music_1;
    public string activity_choose_music_2;
    public string activity_choose_music_3;
    //--
    public string activity_complete_music_1_first_word;
    public string activity_complete_music_1_second_word;
    public string activity_complete_music_1_third_word;
    public string activity_complete_music_1_fourth_word;
    public string activity_complete_music_1_fifth_word;
    public string activity_complete_music_1_difficulty;
    //--
    public string activity_complete_music_2_first_word;
    public string activity_complete_music_2_second_word;
    public string activity_complete_music_2_third_word;
    public string activity_complete_music_2_fourth_word;
    public string activity_complete_music_2_fifth_word;
    public string activity_complete_music_2_difficulty;
    //--
    public string activity_complete_music_3_first_word;
    public string activity_complete_music_3_second_word;
    public string activity_complete_music_3_third_word;
    public string activity_complete_music_3_fourth_word;
    public string activity_complete_music_3_fifth_word;
    public string activity_complete_music_3_difficulty;
    //--
    public string activity_emotion_indoeu;
    //--
    public string activity_emotion_alecrim;
    //--
    public string proverb_written_answer;
    //--
    public string opinion_difficulty;
    public string opinion_session;
    //--
    public string patient_id;
}
[System.Serializable]
public class Session8Data
{
    public int session_invitation_tries;
    public string session_date;
    public string session_duration;
    //--
    public int orientation_party_day_tries;
    public int orientation_party_day_correct_answers;
    public int orientation_party_day_wrong_answers;
    public int orientation_party_day_correct_answers_not_selected;
    public string orientation_party_day_difficulty;
    //--
    public int orientation_party_place_tries;
    public int orientation_party_place_correct_answers;
    public int orientation_party_place_wrong_answers;
    public int orientation_party_place_correct_answers_not_selected;
    public string orientation_party_place_descriptions;
    //--
    public int orientation_invitation_address_tries;
    public int orientation_invitation_address_correct_answers;
    public int orientation_invitation_address_wrong_answers;
    public int orientation_invitation_address_correct_answers_not_selected;
    public string orientation_invitation_address_difficulty;
    //--
    public int orientation_party_hour_start_tries;
    public int orientation_party_hour_start_correct_answers;
    public int orientation_party_hour_start_wrong_answers;
    public int orientation_party_hour_start_correct_answers_not_selected;
    public string orientation_party_hour_start_difficulty;
    //--
    public int orientation_party_hour_end_tries;
    public int orientation_party_hour_end_correct_answers;
    public int orientation_party_hour_end_wrong_answers;
    public int orientation_party_hour_end_correct_answers_not_selected;
    public string orientation_party_hour_end_difficulty;
    //--
    public int orientation_what_to_take_tries;
    public int orientation_what_to_take_correct_answers;
    public int orientation_what_to_take_wrong_answers;
    public int orientation_what_to_take_correct_answers_not_selected;
    //--
    public int orientation_budget_tries;
    public int orientation_budget_correct_answers;
    public int orientation_budget_wrong_answers;
    public int orientation_budget_correct_answers_not_selected;
    public string orientation_budget_difficulty;
    //--
    public string orientation_dessert_correct;
    //--
    public int orientation_hat_tries;
    public int orientation_hat_correct_answers;
    public int orientation_hat_wrong_answers;
    public int orientation_hat_correct_answers_not_selected;
    public string orientation_hat_difficulty;
    //--
    public string activity_preparation_order;
    //--
    public string activity_clothes_answers;
    //--
    public string activity_toTake_first_object;
    public string activity_toTake_second_object;
    public string activity_toTake_third_object;
    //--
    public int activity_party_place_tries;
    public int activity_party_place_correct_answers;
    public int activity_party_place_wrong_answers;
    public int activity_party_place_correct_answers_not_selected;
    public string activity_party_place_answers_descriptions;
    //--
    public string proverb_written_answer;
    //--
    public string opinion_difficulty;
    public string opinion_session;
    //--
    public string patient_id;
}