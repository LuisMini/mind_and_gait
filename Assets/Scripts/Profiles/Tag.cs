﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.Serialization;

[System.Serializable]
public class Tag : MonoBehaviour {

    private string strTag;
    private TagManager tagManager;
    private Button deleteButton;
    private Text textComp;
    
    public void Initialize(string tag, TagManager tagManager)
    {
        deleteButton = GetComponentInChildren<Button>();
        deleteButton.onClick.AddListener(DeleteTag);
        textComp = GetComponentInChildren<Text>();

        strTag = tag;
        this.tagManager = tagManager;
        textComp.text = strTag;
    } 
	
	public void DeleteTag ()
    {
        Destroy(transform.gameObject);
        tagManager.RemoveTag(strTag);
    }
}
