﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Profile_List : MonoBehaviour {

    [Header("Scene Elements")]
    public GameObject Grid;
    public GameObject deleteWindow;
    [Header("Prefabs")]
    public GameObject profileSlot;
    public GameObject prefab_popUpWindow;
    [Header("Debug")]
    public List<GameObject> activeSlots = new List<GameObject>();

    public List<Profile> profiles = new List<Profile>();

    private WindowManager windowManager;

    EventSystem eventdata;

    private void OnEnable()
    {
        Debug.Log("Profile_List -- OnENABLE");        
        ClearGrid();
    }

    // Use this for initialization
    void Start () {
        Debug.Log("Profile_List -- START");
        profiles = ProfileManager.profileList;
        windowManager = GameObject.FindGameObjectWithTag("Manager").GetComponent<WindowManager>();
    }

    public void ClearGrid ()
    {
        Debug.Log("Clear Grid");
        for (int i = 0; i <= Grid.transform.childCount-1; ++i)
        {
            activeSlots.Remove(Grid.transform.GetChild(i).gameObject);
            Destroy(Grid.transform.GetChild(i).gameObject);
        }
        if(CheckValidProfileExists())
        {
            Invoke("PopulateGrid", 0);
        }
        else
        {
            PopUpWindow popUp = Instantiate(prefab_popUpWindow, transform.position, transform.rotation, GameObject.Find("Canvas").transform).GetComponent<PopUpWindow>();
            popUp.Initialize("Nenhum perfil detectado, criar novo?", this, "PopUp");
        }
    }

    //creates the list of selectable profiles
    public void PopulateGrid ()
    {
        Debug.Log("Populate Grid");
        
        if (Grid.transform.childCount == 0)
        {
            Debug.Log("Grid Instance");
            foreach (Profile profile in ProfileManager.profileList)
            {
                //instanciar o slot
                GameObject slot = Instantiate(profileSlot, transform.position, transform.rotation, Grid.transform);
                activeSlots.Add(slot);
                //atribuir profile ao slot
                Profile_Slot slotScript = slot.GetComponentInChildren<Profile_Slot>();
                slotScript.profile = profile;
            }
        }
        
    }

    //checks if at least one profile exists
    public bool CheckValidProfileExists()
    {
        Debug.Log("Check Valid Profile Exists");
        if (ProfileManager.profileList.Count <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public void PopUp ()
    {
        windowManager.panel_SelectProfile.SetActive(false);
        windowManager.panel_CreateProfile.SetActive(true);
    }
}
