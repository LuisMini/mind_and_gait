﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Networking;
using System.Text;
using SimpleJSON;

public class Profile_Edit : MonoBehaviour {

    [Header("Components")]
    public GameObject deficesSensoriaisResposta_Container;
    public List<GameObject> comorbilidadesList = new List<GameObject>();

    [Header("Prefabs")]
    public GameObject popUpWindow;

    [Header("Input Elements")]
    public InputField input_name;
    public Dropdown input_birthday_year;
    public Dropdown input_birthday_month;
    public Dropdown input_birthday_day;
    public Dropdown input_sex;
    public Slider slider_escolaridade;
    public Text text_escolaridade;
    public Toggle toggle_DeficesSensoriais;
    private InputField input_DeficesSensoriais;
    public InputField input_Medicacao;
    public Button editButton;
    public InputField comorbilidadesOutra;
    [Header("Photo")]
    public Image profilePhotoComp;
    private byte[] photoBytes;
    private Sprite defaultPhoto;

    [Header("Input Validation")]
    public int name_MaxCharacters;
    public int deficesSensoriais_maxCharacters;
    public int comorbilidades_maxCharacters;
    public int medicacao_maxCharacters;

    [Header("Properties")]
    private int currentYear;
    public int yearRange;
    public List<Dropdown.OptionData> listYears;
    public int lastDay;
    public List<Dropdown.OptionData> listDays;

    private static readonly string endpointEdit = "https://mind-gait.esenfc.pt/api/patients/create";

    // Use this for initialization
    void Start () {
        currentYear = DateTime.Now.Year;
        defaultPhoto = profilePhotoComp.sprite;

        input_DeficesSensoriais = deficesSensoriaisResposta_Container.GetComponentInChildren<InputField>();

        editButton.onClick.AddListener(Button_FinishEdit);
        TouchScreenKeyboard.hideInput = true;

        for (int i = 0; i < yearRange; ++i)
        {          
            Dropdown.OptionData yearOption = new Dropdown.OptionData(currentYear.ToString());
            listYears.Add(yearOption);
            --currentYear;            
        }
        for (int i = 1; i <= lastDay; ++i)
        {
            Dropdown.OptionData dayOption = new Dropdown.OptionData(i.ToString());
            listDays.Add(dayOption);            
        }

        UpdateOptions();
    }

    private void OnEnable()
    {
        StartCoroutine(ClearFields());
        StartCoroutine(LoadFields());
    }

    private IEnumerator ClearFields()
    {
        yield return new WaitForEndOfFrame();
        input_birthday_year.value = 0;
        input_birthday_month.value = 0;
        input_birthday_day.value = 0;
        input_Medicacao.text = "";
        profilePhotoComp.sprite = defaultPhoto;
    }

    public void UpdateOptions ()
    {
        input_birthday_year.ClearOptions();
        input_birthday_year.AddOptions(listYears);
        input_birthday_day.ClearOptions();
        input_birthday_day.AddOptions(listDays);

        input_name.characterLimit = name_MaxCharacters;
        input_DeficesSensoriais.characterLimit = deficesSensoriais_maxCharacters;
        input_Medicacao.characterLimit = medicacao_maxCharacters;
        comorbilidadesOutra.characterLimit = comorbilidades_maxCharacters;
    }

    public void SliderChangeValue()
    {
        if ((int)slider_escolaridade.value == 13)
        {
            text_escolaridade.text = "+12";
        }
        else
        {
            text_escolaridade.text = slider_escolaridade.value.ToString();
        }
    }

    //get picture data from webCamManager, draws it to the picture slot and saves it on a variable
    public void SetPhoto(Texture2D photo)
    {
        this.profilePhotoComp.sprite = Sprite.Create(photo, new Rect(0, 0, photo.width, photo.height), new Vector2(0.5f, 0.5f));
        photoBytes = photo.EncodeToPNG();
    }

    //preenche os inputs da janela com os valores que estão no profile
    public IEnumerator LoadFields ()
    {
        yield return new WaitForEndOfFrame();
        //load name
        input_name.text = ProfileManager.profileToEdit.name;

        //load photo
        if (ProfileManager.profileToEdit.photo != null && ProfileManager.profileToEdit.photo.Length > 0)
        {
            Texture2D picture = new Texture2D(0, 0);
            picture.LoadImage(ProfileManager.profileToEdit.photo);
            picture.Apply();
            profilePhotoComp.sprite = Sprite.Create(picture, new Rect(0, 0, picture.width, picture.height), new Vector2(0.5f, 0.5f));
            photoBytes = picture.EncodeToPNG();
            Debug.Log("PICTURE WIDTH: " + picture.width + " | " + "PICTURE HEIGHT: " + picture.height);
        }

        //load anos de escolaridade
        if (ProfileManager.profileToEdit.anosEscolaridade == "+12")
        {
            slider_escolaridade.value = 13;
        }
        else
        {
            slider_escolaridade.value = Int32.Parse(ProfileManager.profileToEdit.anosEscolaridade); 
        }

        //load birthdate
        for (int i = 0; i < input_birthday_year.options.Count; ++i)
        {
            if (input_birthday_year.options[i].text == (ProfileManager.profileToEdit.birthdate.Year).ToString())
            {
                input_birthday_year.value = i;
                break;
            }
        }
        for (int i = 0; i < input_birthday_month.options.Count; ++i)
        {
            if (i == ProfileManager.profileToEdit.birthdate.Month - 1)
            {
                input_birthday_month.value = i;
                break;
            }
        }
        for (int i = 0; i < input_birthday_day.options.Count; ++i)
        {
            if (input_birthday_day.options[i].text == (ProfileManager.profileToEdit.birthdate.Day).ToString())
            {
                input_birthday_day.value = i;
                break;
            }
        }
        
        //load gender
        switch (ProfileManager.profileToEdit.sex)
        {
            case Sexo.male:
                {
                    input_sex.value = 0;
                    break;
                }
            case Sexo.female:
                {
                    input_sex.value = 1;
                    break;
                }
        }
        
        //load defices sensoriais
        if (!String.IsNullOrEmpty(ProfileManager.profileToEdit.deficesSensoriais))
        {
            //Debug.Log("Profile To Edit - Defices Sensoriais: " + ProfileManager.profileToEdit.deficesSensoriais + "| Lenght: " + ProfileManager.profileToEdit.deficesSensoriais.Length);
            toggle_DeficesSensoriais.isOn = true;
            deficesSensoriaisResposta_Container.SetActive(true);
            input_DeficesSensoriais.text = ProfileManager.profileToEdit.deficesSensoriais;
        }
        else
        {
            toggle_DeficesSensoriais.isOn = false;
            deficesSensoriaisResposta_Container.SetActive(false);
            input_DeficesSensoriais.text = "";
        }

        //load comorbilidades
        for (int i = 0; i < ProfileManager.profileToEdit.comorbilidades.Count; ++i)
        {
            for (int j = 0; j < comorbilidadesList.Count; ++j)
            {
                if (ProfileManager.profileToEdit.comorbilidades[i].name == comorbilidadesList[j].GetComponentInChildren<Text>().text.Replace(':', ' ').Trim())
                {
                    comorbilidadesList[j].GetComponentInChildren<Toggle>().isOn = true;
                }
            }
        }
        comorbilidadesOutra.text = ProfileManager.profileToEdit.comorbilidadesOutra;
        
        //Load medication
        input_Medicacao.text = ProfileManager.profileToEdit.medicacao;
    }

    /*Valida e instancia PopUp windows com erros*/
    private bool ValidateInputs ()
    {        
        if (input_name.text.Trim().Length <= 0)
        {
            PopUpWindow popUp = Instantiate(popUpWindow, transform.position, transform.rotation, GameObject.Find("Canvas").transform).GetComponent<PopUpWindow>();
            popUp.Initialize("Erro: Nenhum nome inserido.");
            return false;
        }
        return true;
    }

    private void Button_FinishEdit()
    {
        StartCoroutine(CoroutineEditProfileLocal());
    }

    private IEnumerator CoroutineEditProfileLocal()
    {
        EditProfileLocal();
        yield return new WaitForEndOfFrame();
        //StartCoroutine(APIPatchPatient()); //nao sei se chamo isto aqui ou dentro do EditProfileLocal
    }

    /*Alterar localmente o profile*/
    public void EditProfileLocal ()
    {        
        if (ValidateInputs())
        {
            //set profile Name
            ProfileManager.profileToEdit.name = input_name.text.Trim();

            //set profile picture
            if (photoBytes != null && photoBytes.Length > 0)
            {
                ProfileManager.profileToEdit.photo = new byte[photoBytes.Length];
                ProfileManager.profileToEdit.photo = photoBytes;
            }

            //calcular e fazer set á data de nascimento e idade
            DateTime birthdate = DateTime.Now;
            DateTime today = DateTime.Now;
            int ano = Int32.Parse(input_birthday_year.captionText.text);
            birthdate = new DateTime(ano, input_birthday_month.value + 1, input_birthday_day.value + 1);
            //idade
            int age = today.Year - birthdate.Year;
            if (birthdate > today.AddYears(-age))
            {
                age--;
            }
            ProfileManager.profileToEdit.birthdate = birthdate;
            ProfileManager.profileToEdit.age = age;

            //set profile sex
            ProfileManager.profileToEdit.sex = (Sexo)input_sex.value;

            //set profile Anos de Escolaridade
            if ((int)slider_escolaridade.value == 13)
            {
                ProfileManager.profileToEdit.anosEscolaridade = "+12";
            }
            else
            {
                ProfileManager.profileToEdit.anosEscolaridade = slider_escolaridade.value.ToString();
            }

            ProfileManager.profileToEdit.deficesSensoriais = input_DeficesSensoriais.text.Trim();

            //set profile to edit's comorbilidades list
            ProfileManager.profileToEdit.comorbilidades.Clear();
            foreach (var item in comorbilidadesList)
            {
                if (item.GetComponentInChildren<Toggle>().isOn)
                {
                    Profile.Comorbilidades com = new Profile.Comorbilidades();
                    com.name = item.GetComponentInChildren<Text>().text.Replace(':', ' ').Trim();
                    com.value = item.GetComponentInChildren<Toggle>().isOn;

                    ProfileManager.profileToEdit.comorbilidades.Add(com);
                }
            }
            ProfileManager.profileToEdit.comorbilidadesOutra = comorbilidadesOutra.text.Trim();            

            //set profile to edit's medication list
            ProfileManager.profileToEdit.medicacao = input_Medicacao.text;

            ProfileManager.Save();
            StartCoroutine(APIPatchPatient());
        }
    }

    /*Alterar profile no servidor*/
    private IEnumerator APIPatchPatient()
    {
        Debug.Log("EDIT UTENTE PATCH");

        string json_EncodedPhoto;
        string json_Comorbilidades = "";
        string json_DeficesSensoriais;
        string json_Medication;

        string json_birth_date = (ProfileManager.profileToEdit.birthdate.Year + "-" + ProfileManager.profileToEdit.birthdate.Month + "-" + ProfileManager.profileToEdit.birthdate.Day);
        string json_lastAcess = (ProfileManager.profileToEdit.lastAcess.Year + "-" + ProfileManager.profileToEdit.lastAcess.Month + "-" + ProfileManager.profileToEdit.lastAcess.Day);
        string json_totalDuration = string.Format("{0:00}:{1:00}:{2:00}", ProfileManager.profileToEdit.totalDuration.Hours, ProfileManager.profileToEdit.totalDuration.Minutes, ProfileManager.profileToEdit.totalDuration.Seconds);
        string json_partyDate = (ProfileManager.profileToEdit.dateParty.Year + "-" + ProfileManager.profileToEdit.dateParty.Month + "-" + ProfileManager.profileToEdit.dateParty.Day);
        string json_emblemPatterns;

        //validate values
        #region
        //validate comorbilidades
        foreach (var item in ProfileManager.profileToEdit.comorbilidades)
        {
            if (item.value)
            {
                if (json_Comorbilidades == "")
                {
                    json_Comorbilidades = item.name;
                }
                else
                {
                    json_Comorbilidades = json_Comorbilidades + ", " + item.name;
                }
            }
        }

        if (ProfileManager.profileToEdit.comorbilidadesOutra != "")
        {
            if (json_Comorbilidades != "")
            {
                json_Comorbilidades = json_Comorbilidades + "; " + ProfileManager.profileToEdit.comorbilidadesOutra;
            }
            else
            {
                json_Comorbilidades = ProfileManager.profileToEdit.comorbilidadesOutra;
            }
        }

        if (json_Comorbilidades == "")
        {
            json_Comorbilidades = "Sem Informação";
        }

        //validate defices sensoriais
        if (ProfileManager.profileToEdit.deficesSensoriais != "" && toggle_DeficesSensoriais.isOn)
        {
            json_DeficesSensoriais = ProfileManager.profileToEdit.deficesSensoriais;
        }
        else
        {
            json_DeficesSensoriais = "Sem Informação";
        }

        //validate medication
        if (ProfileManager.profileToEdit.medicacao != "")
        {
            json_Medication = ProfileManager.profileToEdit.medicacao;
        }
        else
        {
            json_Medication = "Sem Informação";
        }

        //validate emblem patterns
        if (ProfileManager.profileToEdit.emblemPatterns.Count > 0)
        {
            List<string> tempEmblemPatterns = ProfileManager.profileToEdit.emblemPatterns.ConvertAll<string>(delegate (int i) { return i.ToString(); }); //converter a lista de ints para uma lista de strings temporaria
            json_emblemPatterns = string.Join(", ", tempEmblemPatterns.ToArray());
        }
        else
        {
            json_emblemPatterns = "Sem Informação";
        }

        //validate photo
        if (photoBytes != null && photoBytes.Length > 0) //photoBytes.Length > 0
        {
            json_EncodedPhoto = Convert.ToBase64String(photoBytes);
        }
        else
        {
            json_EncodedPhoto = null;
        }
        #endregion

        string jsonString = 
            "{\"name\": \"" + ProfileManager.profileToEdit.name + "\", " +
            "\"id\": \"" + ProfileManager.profileToEdit.patientID + "\", " +
            "\"photo\": \"" + json_EncodedPhoto + "\", " +
            "\"birth_date\": \"" + json_birth_date + "\", " +
            "\"age\": \"" + ProfileManager.profileToEdit.age + "\", " +
            "\"gender\": \"" + ProfileManager.profileToEdit.sex.ToString() + "\", " +
            "\"school_years\": \"" + ProfileManager.profileToEdit.anosEscolaridade + "\", " +
            "\"sensory_deficits_list\": \"" + json_DeficesSensoriais + "\", " +
            "\"comorbidities_list\": \"" + json_Comorbilidades + "\", " +
            "\"medication_list\": \"" + json_Medication + "\", " +
            "\"last_session\": \"" + (int)ProfileManager.profileToEdit.lastSession + "\", " +
            "\"last_access\": \"" + json_lastAcess + "\", " +
            "\"total_time_on_program\": \"" + json_totalDuration + "\", " +
            "\"completed_session\": \"" + ProfileManager.profileToEdit.completedSessions + "\", " +
            "\"budget_chosen\": \"" + ProfileManager.profileToEdit.budget_chosen.ToString() + "\", " +
            "\"budget_updated\": \"" + ProfileManager.profileToEdit.budget_updated.ToString() + "\", " +
            "\"dessert\": \"" + (int)ProfileManager.profileToEdit.dessert + "\", " +
            "\"party_date\": \"" + json_partyDate + "\", " +
            "\"chosen_emblem\": \"" + (int)ProfileManager.profileToEdit.chosenEmblem + "\", " +
            "\"emblem_patterns\": \"" + json_emblemPatterns + "\"}";

        Debug.Log(jsonString);

        using (UnityWebRequest www = new UnityWebRequest(endpointEdit, "POST"))
        {
            byte[] bodyRaw = Encoding.UTF8.GetBytes(jsonString);
            www.uploadHandler = new UploadHandlerRaw(bodyRaw);
            www.downloadHandler = new DownloadHandlerBuffer();
            www.SetRequestHeader("Authorization", "Bearer " + ProfileManager.acessToken);
            www.SetRequestHeader("Accept", "application/json");
            www.SetRequestHeader("Content-Type", "application/json");
            yield return www.SendWebRequest();

            if (Application.internetReachability == NetworkReachability.NotReachable) //não está conectado à internet
            {
                PopUpWindow popUp = Instantiate(popUpWindow, transform.position, transform.rotation, GameObject.Find("Canvas").transform).GetComponent<PopUpWindow>();
                string errorString = "Erro: Não está conectado à internet, perfil do utente irá ser alterado apenas localmente.";
                popUp.Initialize(errorString, this, "SucessfullPatientEdit");
            }
            else //está conectado à internet
            {
                if (www.isNetworkError || www.isHttpError)
                {
                    PopUpWindow popUp = Instantiate(popUpWindow, transform.position, transform.rotation, GameObject.Find("Canvas").transform).GetComponent<PopUpWindow>();
                    string errorString = "Erro: " + www.error + "\n" + "Perfil do utente irá ser alterado apenas localmente.";
                    popUp.Initialize(errorString, this, "SucessfullPatientEdit");
                }
                else
                {
                    Debug.Log("UTENTE Form Upload Complete:" + www.downloadHandler.text);
                    SucessfullPatientEdit();
                }
            }
        }
    }

    private void SucessfullPatientEdit ()
    {
        transform.gameObject.SetActive(false);
        GameObject.FindGameObjectWithTag("Manager").GetComponent<WindowManager>().panel_SelectProfile.SetActive(true);
    }

}
