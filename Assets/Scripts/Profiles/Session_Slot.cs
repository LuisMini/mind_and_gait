﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class Session_Slot : MonoBehaviour {

    [Header("Session Info")]
    [SerializeField]
    private int sessionNumber;
    [SerializeField]
    private string sessionDescription;
    [SerializeField]
    private bool sessionCompleted;
    private bool sessionLocked;

    [Header("Components")]
    public Text text_Number;
    public Text text_Description;
    public Image sessionStatus;
    public Image background;
    public Color backgroundColor;

    [Header("Sprites")]
    public Sprite completed;
    public Sprite locked;
    
	// Use this for initialization
	void Start () {
        text_Number.text = ("Sessão Nº" + sessionNumber);
        text_Description.text = sessionDescription;
        if (sessionCompleted || sessionLocked)
        {
            sessionStatus.enabled = true;
            if (sessionCompleted)
            {
                sessionStatus.sprite = completed;
            }
            else
            {
                sessionStatus.sprite = locked;
            }
        }
        else
        {
            sessionStatus.enabled = false;
        }
	}

    public void Initialize (int number, string description, bool completed, bool locked)
    {
        sessionNumber = number;
        sessionDescription = description;
        this.sessionCompleted = completed;
        this.sessionLocked = locked;
    }

    private void OnMouseUpAsButton()
    {
        if (!EventSystem.current.IsPointerOverGameObject()) // if no UI elements are getting the hit/hover
        {
            if (sessionLocked == false)
            {
                if (Application.CanStreamedLevelBeLoaded("Session_" + sessionNumber))
                {
                    background.color = backgroundColor;
                    SceneManager.LoadScene("Session_" + sessionNumber);
                }
                else
                {
                    Debug.Log("Error: Scene does not Exist");
                }
            }
        }
        else
        {
            Debug.Log("Pointer over Gameobject");
        }
    }
}
