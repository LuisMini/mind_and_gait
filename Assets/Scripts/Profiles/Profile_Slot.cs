﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class Profile_Slot : MonoBehaviour {

    public Profile profile;

    [Header("Scene Elements")]
    public GameObject deleteWindow;
    private WindowManager windowManager;
    [Header("Components")]
    public Text name_Field;
    public Text age_Field;
    public Image Image_Field;
    public Button selectButton;
    public Button editButton;

    [Header("Misc")]
    public int profileID;

    [Header("Touch Interaction")]
    private float scrollbarPrevValue;
    private Profile_List profileList;
    private Scrollbar scrollbar;
    private GameObject list;


    // Use this for initialization
    void Start () {
        SetProperties();
        
        deleteWindow = transform.GetComponentInParent<Profile_List>().deleteWindow;

        selectButton.onClick.AddListener(SelectProfile);
        editButton.onClick.AddListener(OnClickEdit);

        profileList = GetComponentInParent<Profile_List>();
        scrollbar = profileList.transform.GetComponentInChildren<Scrollbar>();
        list = profileList.transform.GetComponentInChildren<ScrollRect>().gameObject;

        windowManager = GameObject.FindGameObjectWithTag("Manager").GetComponent<WindowManager>();
    }
	
    public void SetProperties ()
    {
        name_Field.text = profile.name;
        age_Field.text = ("Idade: " + profile.age.ToString());
        if (profile.photo != null && profile.photo.Length > 0)
        {
            Texture2D picture = new Texture2D(0,0);
            picture.LoadImage(profile.photo);
            picture.Apply();
            Image_Field.sprite = Sprite.Create(picture, new Rect(0, 0, picture.width, picture.height), new Vector2(0.5f, 0.5f));
            Debug.Log("PICTURE WIDTH: " + picture.width + " | " + "PICTURE HEIGHT: " + picture.height);
        }
    }

    private void OnClickDelete ()
    {
        deleteWindow.SetActive(true);
        deleteWindow.GetComponent<Profile_Delete>().Initialize(profile);
    }

    private void OnClickEdit ()
    {
        ProfileManager.profileToEdit = profile;
        WindowManager windowManager = GameObject.FindGameObjectWithTag("Manager").GetComponent<WindowManager>();
        windowManager.panel_EditProfile.SetActive(true);
        windowManager.panel_SelectProfile.SetActive(false);
    }

    private void OnMouseDown()
    {
        scrollbarPrevValue = scrollbar.value;
    }
    
    private void OnMouseUpAsButton()
    {
        if (EventSystem.current.currentSelectedGameObject == null && scrollbarPrevValue == scrollbar.value) //se não estiver outro objecto selected e a scrollbar não tiver sido dragged
        {
            if(windowManager.raycastResults[0].gameObject.name == list.gameObject.name) //se o primeiro resultado do raycast é igual a list que contem o scrollRect
            {
                ProfileManager.selectedProfile = profile;
                GameObject.FindGameObjectWithTag("Manager").GetComponent<WindowManager>().panel_PersonalProfile.SetActive(true);
                GameObject.FindGameObjectWithTag("Manager").GetComponent<WindowManager>().panel_SelectProfile.SetActive(false);
            }

        }        
    }

    private void SelectProfile ()
    {
        ProfileManager.selectedProfile = profile;
        GameObject.FindGameObjectWithTag("Manager").GetComponent<WindowManager>().panel_PersonalProfile.SetActive(true);
        GameObject.FindGameObjectWithTag("Manager").GetComponent<WindowManager>().panel_SelectProfile.SetActive(false);
    }

}
