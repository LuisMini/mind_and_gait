﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;
using System;
using UnityEngine.Networking;
using SimpleJSON;
using System.Linq;
using System.Text;

public static class ProfileManager
{

    /*Profiles*/
    public static Profile selectedProfile;
    public static Profile profileToEdit;
    public static List<Profile> profileList = new List<Profile>();

    public static string acessToken;

    public static bool returnToSessionSelection;

    /*Endpoints*/
    private static readonly string endpointGet = "http://mind-gait.esenfc.pt/api/patients";
    private static readonly string endpointUpdate = "https://mind-gait.esenfc.pt/api/patients/create";

    /*File Storage*/
    [SerializeField]
    private static readonly string fileName = "profiles.dat";
    [SerializeField]
    private static readonly string filePath = (Application.persistentDataPath + Path.DirectorySeparatorChar + fileName);

    public static void Save()
    {
        Debug.Log("PROFILE MANAGER - SAVE");

        BinaryFormatter bf = new BinaryFormatter();

        FileStream file = File.Open(filePath, FileMode.OpenOrCreate);

        bf.Serialize(file, ProfileManager.profileList);
        if (ProfileManager.acessToken != "")
        {
            bf.Serialize(file, ProfileManager.acessToken);
        }
        file.Close();

    }

    public static void Load()
    {
        Debug.Log("PROFILE MANAGER - LOAD" + "Persistent Data Path: " + Application.persistentDataPath);

        if (File.Exists(filePath))
        {
            try
            {
                BinaryFormatter bf = new BinaryFormatter();

                FileStream file = File.Open(filePath, FileMode.Open);

                profileList = (List<Profile>)bf.Deserialize(file);
                acessToken = (string)bf.Deserialize(file);
                file.Close();
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
        }
    }

    public static void ClearProfiles()
    {
        profileList.Clear();
    }

    /// <summary>
    /// Returns the list of patients of the currently logged in therapist
    /// </summary>
    /// <returns></returns>
    public static IEnumerator GetRequest()
    {
        profileList.Clear(); //wipe the current profile list

        using (UnityWebRequest webRequest = UnityWebRequest.Get(endpointGet))
        {
            webRequest.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
            webRequest.SetRequestHeader("Authorization", "Bearer " + ProfileManager.acessToken);
            webRequest.SetRequestHeader("Accept", "application/json");
            webRequest.SetRequestHeader("Content-Type", "application/json");
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError)
            {
                Debug.Log(": Error: " + webRequest.error);
            }
            else
            {
                Debug.Log("Patients Received: " + webRequest.downloadHandler.text);
            }

            //Debug.Log(JsonUtility.FromJson(webRequest.downloadHandler.text, String)); //todo usar isto para os resultados das sessoes

            JSONNode svResponseData = JSON.Parse(webRequest.downloadHandler.text);
            //Debug.Log(svResponseData["data"]); //works, printa tudo, pq "data" é o array com tudo
            //Debug.Log(svResponseData["data"][0]);
            //Debug.Log(svResponseData["data"][0][0]);
            //Debug.Log(svResponseData["data"][0][25].ToString());


            for (int i = 0; i < svResponseData["data"].Count; ++i)
            {
                Profile newProfile = new Profile();

                JSONObject patientData = (JSONObject)svResponseData["data"][i]; //i representa o numero do utente que tamos a iterar
                //Debug.Log(patientData);
                //Debug.Log("MAKÉLÉLELELELELELLELELELELE   " + patientData["second_sessions"].ToString());
                Debug.Log("LALALALALALALALALALLALALLAL   " + patientData["second_sessions"][0].ToString());
                //Debug.Log("LALALALALALALALALALLALALLAL   " + patientData["second_sessions"][0]["session_duration"]);

                newProfile.name = patientData["name"];

                if (patientData["photo"] != null)
                {
                    Debug.Log("PROFILE PHOTO JSON: " + patientData["photo"]); //json prints fine

                    byte[] test;
                    test = Convert.FromBase64String(patientData["photo"]);
                    Debug.Log("TEST LENGTH: " + test.Length);

                    newProfile.photo = test;
                    Debug.Log("PROFILE PHOTO: " + newProfile.photo.Length);
                }

                //validate birth date
                if (patientData["birth_date"] == null)
                {
                    newProfile.birthdate = new DateTime();
                }
                else
                {
                    List<string> tempListBirthdate = new List<string>(patientData["birth_date"].ToString().Replace('"', ' ').Trim().Split('-').ToList());
                    newProfile.birthdate = new DateTime(int.Parse(tempListBirthdate[0]), int.Parse(tempListBirthdate[1]), int.Parse(tempListBirthdate[2]));
                }

                newProfile.age = patientData["age"];
                newProfile.sex = (Sexo)System.Enum.Parse(typeof(Sexo), patientData["gender"]);
                newProfile.anosEscolaridade = patientData["school_years"];
                newProfile.deficesSensoriais = patientData["sensory_deficits_list"];
                newProfile.medicacao = patientData["medication_list"];

                //validate comorbidities
                //Debug.Log(patientData["comorbidities_list"]);
                if (patientData["comorbidities_list"] == null || patientData["comorbidities_list"] == "Sem informação")
                {
                    newProfile.comorbilidades.Clear();
                    newProfile.comorbilidadesOutra = string.Empty;
                }
                else
                {
                    List<string> tempComorbilidades = new List<string>(patientData["comorbidities_list"].ToString().Replace('"', ' ').Trim().Split(';').ToList());
                    newProfile.comorbilidadesOutra = tempComorbilidades.Last().Trim();
                    tempComorbilidades = tempComorbilidades[0].ToString().Trim().Split(',').ToList();

                    foreach (var item in tempComorbilidades)
                    {
                        Profile.Comorbilidades com = new Profile.Comorbilidades();
                        com.name = item.Replace(':',' ').Trim();
                        com.value = true;
                        newProfile.comorbilidades.Add(com);
                    }
                }

                newProfile.patientID = patientData["id"];
                newProfile.lastSession = (ENUM_Sessions)(int)patientData["last_session"];

                //validar creation date
                if (patientData["created_at"] == null)
                {
                    newProfile.creationDate = DateTime.Now;
                }
                else
                {
                    List<string> tempListCreationDate = new List<string>(patientData["created_at"].ToString().Replace('"', ' ').Trim().Split(' '));
                    tempListCreationDate = new List<string>(tempListCreationDate[0].Split('-').ToList());
                    newProfile.creationDate = new DateTime(int.Parse(tempListCreationDate[0]), int.Parse(tempListCreationDate[1]), int.Parse(tempListCreationDate[2]));
                }
                //Debug.Log("API Creation Date = " + patientData["created_at"]);
                //Debug.Log("Profile Creation Date = " + newProfile.creationDate.ToShortDateString());

                //validar last acess date
                if (patientData["last_acess"] == null)
                {
                    newProfile.lastAcess = newProfile.creationDate;
                }
                else
                {
                    List<string> tempListLastAcess = new List<string>(patientData["last_access"].ToString().Replace('"', ' ').Trim().Split('-').ToList());
                    newProfile.lastAcess = new DateTime(int.Parse(tempListLastAcess[0]), int.Parse(tempListLastAcess[1]), int.Parse(tempListLastAcess[2]));
                }
                //Debug.Log("API Last Acess = " + patientData["last_access"]);
                //Debug.Log("Profile Last Acess = " + newProfile.lastAcess.ToShortDateString());

                //validar party date
                if (patientData["party_date"] == null)
                {
                    newProfile.dateParty = newProfile.creationDate;
                }
                else
                {
                    List<string> tempListPartyDate = new List<string>(patientData["party_date"].ToString().Replace('"', ' ').Trim().Split('-').ToList());
                    newProfile.dateParty = new DateTime(int.Parse(tempListPartyDate[0]), int.Parse(tempListPartyDate[1]), int.Parse(tempListPartyDate[2]));
                }
                //Debug.Log("API Party Date = " + patientData["party_date"]);
                //Debug.Log("Profile Party Date = " + newProfile.dateParty.ToShortDateString());


                newProfile.totalDuration = TimeSpan.Parse(patientData["total_time_on_program"]);
                newProfile.completedSessions = patientData["completed_session"];
                newProfile.budget_chosen = (float)patientData["budget_chosen"];
                newProfile.budget_updated = (float)patientData["budget_updated"];

                if (patientData["dessert"] != null)
                {
                    newProfile.dessert = (ENUM_Sobremesa)System.Enum.Parse(typeof(ENUM_Sobremesa), patientData["dessert"]);
                }

                if (patientData["chosen_emblem"] != null)
                {
                    newProfile.chosenEmblem = (ENUM_Emblema)System.Enum.Parse(typeof(ENUM_Emblema), patientData["chosen_emblem"]);
                }

                try
                {
                    //string a = "1,2,3,4";
                    Debug.Log("EMBLEM PATTERNS OF PROFILE: " + newProfile.name + "  " + patientData["emblem_patterns"].ToString());
                    List<string> tempList = new List<string>(patientData["emblem_patterns"].ToString().Replace('"', ' ').Trim().Split(',').ToList());
                    for (int emblemID = 0; emblemID < tempList.Count; ++emblemID)
                    {
                        newProfile.emblemPatterns.Add(int.Parse(tempList[emblemID].Trim()));
                    }
                    Debug.Log(newProfile.emblemPatterns[0]);
                    Debug.Log(newProfile.emblemPatterns[1]);
                    Debug.Log(newProfile.emblemPatterns[2]);
                    Debug.Log(newProfile.emblemPatterns[3]);
                }
                catch (Exception)
                {

                }

                /*Get sessions results*/
                #region 
                newProfile.session1 = new Session1Data();
                try
                {
                    Session1Data myObject = JsonUtility.FromJson<Session1Data>(patientData["first_sessions"][0].ToString());
                    newProfile.session1 = myObject;
                }
                catch (ArgumentException)
                {
                    Debug.Log("Error Caught: Profile - " + newProfile.name + "Probably session 1 data is empty");
                }

                newProfile.session2 = new Session2Data();
                try
                {
                    Session2Data myObject = JsonUtility.FromJson<Session2Data>(patientData["second_sessions"][0].ToString());
                    Debug.Log("My Object patientID : " + myObject.patient_id);
                    newProfile.session2 = myObject;
                }
                catch (ArgumentException)
                {
                    Debug.Log("Error Caught: Profile - " + newProfile.name + "Probably session 2 data is empty");
                }

                newProfile.session3 = new Session3Data();
                try
                {
                    Session3Data myObject = JsonUtility.FromJson<Session3Data>(patientData["third_sessions"][0].ToString());
                    newProfile.session3 = myObject;
                }
                catch (ArgumentException)
                {
                    Debug.Log("Error Caught: Profile - " + newProfile.name + "Probably session 3 data is empty");
                }

                newProfile.session4 = new Session4Data();
                try
                {
                    Session4Data myObject = JsonUtility.FromJson<Session4Data>(patientData["fourth_sessions"][0].ToString());
                    newProfile.session4 = myObject;
                }
                catch (ArgumentException)
                {
                    Debug.Log("Error Caught: Profile - " + newProfile.name + "Probably session 4 data is empty");
                }

                newProfile.session5 = new Session5Data();
                try
                {
                    Session5Data myObject = JsonUtility.FromJson<Session5Data>(patientData["fifth_sessions"][0].ToString());
                    newProfile.session5 = myObject;
                }
                catch (ArgumentException)
                {
                    Debug.Log("Error Caught: Profile - " + newProfile.name + "Probably session 5 data is empty");
                }

                newProfile.session6 = new Session6Data();
                try
                {
                    Session6Data myObject = JsonUtility.FromJson<Session6Data>(patientData["sixth_sessions"][0].ToString());
                    newProfile.session6 = myObject;
                }
                catch (ArgumentException)
                {
                    Debug.Log("Error Caught: Profile - " + newProfile.name + "Probably session 6 data is empty");
                }

                newProfile.session7 = new Session7Data();
                try
                {
                    Session7Data myObject = JsonUtility.FromJson<Session7Data>(patientData["seventh_sessions"][0].ToString());
                    newProfile.session7 = myObject;
                }
                catch (ArgumentException)
                {
                    Debug.Log("Error Caught: Profile - " + newProfile.name + "Probably session 7 data is empty");
                }

                newProfile.session8 = new Session8Data();
                try
                {
                    Session8Data myObject = JsonUtility.FromJson<Session8Data>(patientData["eighth_sessions"][0].ToString());
                    newProfile.session8 = myObject;
                }
                catch (ArgumentException)
                {
                    Debug.Log("Error Caught: Profile - " + newProfile.name + "Probably session 8 data is empty");
                }
                #endregion
                profileList.Add(newProfile);
            }

            if (GameObject.FindGameObjectWithTag("Manager").GetComponent<WindowManager>()) //validar se WindowManager component existe
            {
                WindowManager windowManager = GameObject.FindGameObjectWithTag("Manager").GetComponent<WindowManager>();
                windowManager.panel_LoginScreen.SetActive(false);
                windowManager.panel_SelectProfile.SetActive(true);
            }
        }
    }

    /// <summary>
    /// Updates the current selected patient's profile on the server
    /// </summary>
    /// <returns></returns>
    public static IEnumerator APIUpdatePatientProfile()
    {
        Debug.Log("UPDATE UTENTE POST");

        string json_EncodedPhoto;
        string json_Comorbilidades = "";
        string json_DeficesSensoriais;
        string json_Medication;

        string json_birth_date = selectedProfile.birthdate.Year + "-" + selectedProfile.birthdate.Month + "-" + selectedProfile.birthdate.Day;
        string json_lastAcess = selectedProfile.lastAcess.Year + "-" + selectedProfile.lastAcess.Month + "-" + selectedProfile.lastAcess.Day;
        string json_totalDuration = string.Format("{0:00}:{1:00}:{2:00}", selectedProfile.totalDuration.Hours, selectedProfile.totalDuration.Minutes, selectedProfile.totalDuration.Seconds);
        string json_partyDate = selectedProfile.dateParty.Year + "-" + selectedProfile.dateParty.Month + "-" + selectedProfile.dateParty.Day;
        string json_emblemPatterns;

        //validate values
        #region
        //validate comorbilidades
        foreach (var item in selectedProfile.comorbilidades)
        {
            if (item.value)
            {
                if (json_Comorbilidades == "")
                {
                    json_Comorbilidades = item.name;
                }
                else
                {
                    json_Comorbilidades = json_Comorbilidades + ", " + item.name;
                }
            }
        }

        if (selectedProfile.comorbilidadesOutra != "")
        {
            if (json_Comorbilidades != "")
            {
                json_Comorbilidades = json_Comorbilidades + "; " + selectedProfile.comorbilidadesOutra;
            }
            else
            {
                json_Comorbilidades = selectedProfile.comorbilidadesOutra;
            }
        }

        if (json_Comorbilidades == "")
        {
            json_Comorbilidades = "Sem Informação";
        }

        //validate defices sensoriais
        if (selectedProfile.deficesSensoriais != "")
        {
            json_DeficesSensoriais = selectedProfile.deficesSensoriais;
        }
        else
        {
            json_DeficesSensoriais = "Sem Informação";
        }

        //validate medication
        if (selectedProfile.medicacao != "")
        {
            json_Medication = selectedProfile.medicacao;
        }
        else
        {
            json_Medication = "Sem Informação";
        }

        //validate emblem patterns
        if (selectedProfile.emblemPatterns.Count > 0)
        {
            List<string> tempEmblemPatterns = selectedProfile.emblemPatterns.ConvertAll<string>(delegate (int i) { return i.ToString(); }); //converter a lista de ints para uma lista de strings temporaria
            json_emblemPatterns = string.Join(", ", tempEmblemPatterns.ToArray());
        }
        else
        {
            json_emblemPatterns = "Sem Informação";
        }

        //validate photo
        if (selectedProfile.photo != null && selectedProfile.photo.Length > 0) //photoBytes.Length > 0
        {
            json_EncodedPhoto = Convert.ToBase64String(selectedProfile.photo);
        }
        else
        {
            json_EncodedPhoto = null;
        }
        #endregion

        string jsonString =
            "{\"name\": \"" + selectedProfile.name + "\", " +
            "\"id\": \"" + selectedProfile.patientID + "\", " +
            "\"photo\": \"" + json_EncodedPhoto + "\", " +
            "\"birth_date\": \"" + json_birth_date + "\", " +
            "\"age\": \"" + selectedProfile.age + "\", " +
            "\"gender\": \"" + selectedProfile.sex.ToString() + "\", " +
            "\"school_years\": \"" + selectedProfile.anosEscolaridade + "\", " +
            "\"sensory_deficits_list\": \"" + json_DeficesSensoriais + "\", " + 
            "\"comorbidities_list\": \"" + json_Comorbilidades + "\", " + 
            "\"medication_list\": \"" + json_Medication + "\", " + 
            "\"last_session\": \"" + ((int)selectedProfile.lastSession) + "\", " +
            "\"last_access\": \"" + json_lastAcess + "\", " +
            "\"total_time_on_program\": \"" + json_totalDuration + "\", " +
            "\"completed_session\": \"" + selectedProfile.completedSessions + "\", " +
            "\"budget_chosen\": \"" + selectedProfile.budget_chosen.ToString() + "\", " +
            "\"budget_updated\": \"" + selectedProfile.budget_updated.ToString() + "\", " +
            "\"dessert\": \"" + (int)selectedProfile.dessert + "\", " +
            "\"party_date\": \"" + json_partyDate + "\", " +
            "\"chosen_emblem\": \"" + (int)selectedProfile.chosenEmblem + "\", " +
            "\"emblem_patterns\": \"" + json_emblemPatterns + "\"}";

        Debug.Log(jsonString);

        using (UnityWebRequest www = new UnityWebRequest(endpointUpdate, "POST"))
        {
            byte[] bodyRaw = Encoding.UTF8.GetBytes(jsonString);
            www.uploadHandler = new UploadHandlerRaw(bodyRaw);
            www.downloadHandler = new DownloadHandlerBuffer();
            www.SetRequestHeader("Authorization", "Bearer " + ProfileManager.acessToken);
            www.SetRequestHeader("Accept", "application/json");
            www.SetRequestHeader("Content-Type", "application/json");
            yield return www.SendWebRequest();

            if (Application.internetReachability == NetworkReachability.NotReachable) //não está conectado à internet
            {
                //PopUpWindow popUp = Instantiate(popUpWindow, transform.position, transform.rotation, GameObject.Find("Canvas").transform).GetComponent<PopUpWindow>();
                string errorString = "Erro: Não está conectado à internet, perfil do utente irá ser atualizado apenas localmente.";
                //popUp.Initialize(errorString, this, "SucessfullPatientEdit");
                Debug.Log(errorString);
            }
            else //está conectado à internet
            {
                if (www.isNetworkError || www.isHttpError)
                {
                    //PopUpWindow popUp = Instantiate(popUpWindow, transform.position, transform.rotation, GameObject.Find("Canvas").transform).GetComponent<PopUpWindow>();
                    string errorString = "Erro: " + www.error + "\n" + "Perfil do utente irá ser atualizado apenas localmente.";
                    //popUp.Initialize(errorString, this, "SucessfullPatientEdit");
                    Debug.Log(errorString);
                }
                else
                {
                    Debug.Log("UTENTE atualizado com sucesso:" + www.downloadHandler.text);
                }
            }
        }
    }
}
