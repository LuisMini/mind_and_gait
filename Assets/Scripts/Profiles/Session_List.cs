﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Session_List : MonoBehaviour {

    [Header("Profile")]
    public Profile personalProfile;
    public List<GameObject> sessionSlots = new List<GameObject>();
    private int sessionCount;
    public GameObject grid;
    public GameObject prefab_SessionSlot;

    private void OnEnable()
    {
        Debug.Log("SESSION LIST -unity  ENABLED");
        personalProfile = ProfileManager.selectedProfile;
        sessionCount = EnumsHelperExtension.EnumSize(new ENUM_Sessions());
        Debug.Log(sessionCount);
        foreach (GameObject slot in sessionSlots)
        {
            Destroy(slot);
        }
        PopulateSessionList();
    }

    void PopulateSessionList ()
    {
        Debug.Log("Session List - Populate Session List");
        for (int i = 0; i < sessionCount; ++i)
        {
            Debug.Log("Loop: " + i);
            GameObject slot = Instantiate(prefab_SessionSlot, grid.transform) as GameObject;
            Session_Slot slotScript = slot.GetComponent<Session_Slot>();
            if (slotScript != null)
            {
                bool locked = true;
                if (i == 0 || personalProfile.completedSessions > i - 1)
                {
                    locked = false;
                }

                bool completed = false;
                if ((i+1) <= personalProfile.completedSessions)
                {
                    completed = true;
                }
                slotScript.Initialize(i + 1, EnumsHelperExtension.ToDescription((ENUM_Sessions)i), completed, locked);
            }
            sessionSlots.Add(slot);
        }
    }
}
