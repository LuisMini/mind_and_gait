﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class S5Results : SessionDataTab
{
    public Results5Fields orientation_date;
    public Results5Fields orientation_map;
    public Results5Fields orientation_adress;
    public Results5Fields orientation_hourBegin;
    public Results5Fields orientation_hourEnd;
    public Results5Fields orientation_WhatToTake;
    public Results5Fields orientation_Budget;
    public Text orientation_dessert_correct;
    public Results2Fields activity_ingredients;
    public Results2Fields activity_folheto;
    public Results3Fields activity_order_sum;
    public Text activity_budget_remaining;
    public Results3Fields activity_recipe_utensils;
    public Results3Fields activity_recipe_ingredients;
    public Text proverbio;

    protected override void UpdateResults()
    {
        sessionProperties.numConviteOpened.text = ProfileManager.selectedProfile.session5.session_invitation_tries.ToString();

        if (ProfileManager.selectedProfile.completedSessions >= 5)
        {
            sessionProperties.Date.text = "Data: " + ProfileManager.selectedProfile.session5.session_date;
            sessionProperties.Timer.text = "Duração: " + ProfileManager.selectedProfile.session5.session_duration;

            orientation_date.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session5.orientation_party_day_tries);
            orientation_date.CorrectAnswers.text = ProfileManager.selectedProfile.session5.orientation_party_day_correct_answers.ToString();
            orientation_date.WrongAnswers.text = ProfileManager.selectedProfile.session5.orientation_party_day_wrong_answers.ToString();
            orientation_date.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session5.orientation_party_day_correct_answers_not_selected.ToString();
            orientation_date.Difficulty.text = ProfileManager.selectedProfile.session5.orientation_party_day_difficulty;

            orientation_map.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session5.orientation_party_place_tries);
            orientation_map.CorrectAnswers.text = ProfileManager.selectedProfile.session5.orientation_party_place_correct_answers.ToString();
            orientation_map.WrongAnswers.text = ProfileManager.selectedProfile.session5.orientation_party_place_wrong_answers.ToString();
            orientation_map.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session5.orientation_party_place_correct_answers_not_selected.ToString();
            orientation_map.Difficulty.text = ProfileManager.selectedProfile.session5.orientation_party_place_descriptions;

            orientation_adress.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session5.orientation_invitation_address_tries);
            orientation_adress.CorrectAnswers.text = ProfileManager.selectedProfile.session5.orientation_invitation_address_correct_answers.ToString();
            orientation_adress.WrongAnswers.text = ProfileManager.selectedProfile.session5.orientation_invitation_address_wrong_answers.ToString();
            orientation_adress.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session5.orientation_invitation_address_correct_answers_not_selected.ToString();
            orientation_adress.Difficulty.text = ProfileManager.selectedProfile.session5.orientation_invitation_address_difficulty;

            orientation_hourBegin.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session5.orientation_party_hour_start_tries);
            orientation_hourBegin.CorrectAnswers.text = ProfileManager.selectedProfile.session5.orientation_party_hour_start_correct_answers.ToString();
            orientation_hourBegin.WrongAnswers.text = ProfileManager.selectedProfile.session5.orientation_party_hour_start_wrong_answers.ToString();
            orientation_hourBegin.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session5.orientation_party_hour_start_correct_answers_not_selected.ToString();
            orientation_hourBegin.Difficulty.text = ProfileManager.selectedProfile.session5.orientation_party_hour_start_difficulty;

            orientation_hourEnd.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session5.orientation_party_hour_end_tries);
            orientation_hourEnd.CorrectAnswers.text = ProfileManager.selectedProfile.session5.orientation_party_hour_end_correct_answers.ToString();
            orientation_hourEnd.WrongAnswers.text = ProfileManager.selectedProfile.session5.orientation_party_hour_end_wrong_answers.ToString();
            orientation_hourEnd.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session5.orientation_party_hour_end_correct_answers_not_selected.ToString();
            orientation_hourEnd.Difficulty.text = ProfileManager.selectedProfile.session5.orientation_party_hour_end_difficulty;

            orientation_WhatToTake.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session5.orientation_what_to_take_tries);
            orientation_WhatToTake.CorrectAnswers.text = ProfileManager.selectedProfile.session5.orientation_what_to_take_correct_answers.ToString();
            orientation_WhatToTake.WrongAnswers.text = ProfileManager.selectedProfile.session5.orientation_what_to_take_wrong_answers.ToString();
            orientation_WhatToTake.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session5.orientation_what_to_take_correct_answers_not_selected.ToString();

            orientation_Budget.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session5.orientation_budget_tries);
            orientation_Budget.CorrectAnswers.text = ProfileManager.selectedProfile.session5.orientation_budget_correct_answers.ToString();
            orientation_Budget.WrongAnswers.text = ProfileManager.selectedProfile.session5.orientation_budget_wrong_answers.ToString();
            orientation_Budget.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session5.orientation_budget_correct_answers_not_selected.ToString();
            orientation_Budget.Difficulty.text = ProfileManager.selectedProfile.session5.orientation_budget_difficulty;

            orientation_dessert_correct.text = ProfileManager.selectedProfile.session5.orientation_dessert_correct;
            
            activity_ingredients.field1.text = ProfileManager.selectedProfile.session5.activity_ingredients_correct_answers.ToString();
            activity_ingredients.field2.text = ProfileManager.selectedProfile.session5.activity_ingredients_wrong_answers.ToString();

            activity_folheto.field1.text = ProfileManager.selectedProfile.session5.activity_shopping_correct_answers.ToString();
            activity_folheto.field2.text = ProfileManager.selectedProfile.session5.activity_shopping_wrong_answers.ToString();

            activity_order_sum.field1.text = ProfileManager.selectedProfile.session5.activity_order_sum_correct_answers.ToString();
            activity_order_sum.field2.text = ProfileManager.selectedProfile.session5.activity_order_sum_wrong_answers.ToString();
            activity_order_sum.field3.text = ProfileManager.selectedProfile.session5.activity_order_sum_written_answer.ToString();

            activity_budget_remaining.text = ProfileManager.selectedProfile.session5.activity_budget_remaining_written_answer;

            activity_recipe_utensils.field1.text = ProfileManager.selectedProfile.session5.activity_recipe_utensils_correct_answers.ToString();
            activity_recipe_utensils.field2.text = ProfileManager.selectedProfile.session5.activity_recipe_utensils_wrong_answers.ToString();
            activity_recipe_utensils.field3.text = ProfileManager.selectedProfile.session5.activity_recipe_utensils_time.ToString();

            activity_recipe_ingredients.field1.text = ProfileManager.selectedProfile.session5.activity_recipe_ingredients_correct_answers.ToString();
            activity_recipe_ingredients.field2.text = ProfileManager.selectedProfile.session5.activity_recipe_ingredients_wrong_answers.ToString();
            activity_recipe_ingredients.field3.text = ProfileManager.selectedProfile.session5.activity_recipe_ingredients_time.ToString();

            proverbio.text = ProfileManager.selectedProfile.session5.proverb_written_answer;

            reviewLike.text = ProfileManager.selectedProfile.session5.opinion_session;
            reviewDifficulty.text = ProfileManager.selectedProfile.session5.opinion_difficulty;
        }
        else
        {
            sessionProperties.Timer.text = "";
            sessionProperties.Date.text = "Não foi concluida";
        }
    }
}
