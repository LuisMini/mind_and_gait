﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class S4Results : SessionDataTab
{
    [System.Serializable]
    public struct Proverbio
    {
        public Text difficulty;
        public Text timer;
        public Text writtenAnswer;
    }

    public Results5Fields orientation_date;
    public Results5Fields orientation_map;
    public Results5Fields orientation_adress;
    public Results5Fields orientation_hourBegin;
    public Results5Fields orientation_hourEnd;
    public Results5Fields orientation_WhatToTake;
    public Results5Fields orientation_Budget;
    public Text activity_AssociateImages_WrongAnswers;
    public Text activity_AssociateImages_Difficulty;
    public Results4Fields activity_NotDessert;
    public Text activity_ChooseDessert_ChosenDessert;
    public Text activity_ChooseDessert_Difficulty;
    public Results6Fields activity_KitchenUtensils;
    public Proverbio proverbio;

    protected override void UpdateResults()
    {
        sessionProperties.numConviteOpened.text = ProfileManager.selectedProfile.session4.session_invitation_tries.ToString();

        if (ProfileManager.selectedProfile.completedSessions >= 4)
        {
            sessionProperties.Date.text = "Data: " + ProfileManager.selectedProfile.session4.session_date;
            sessionProperties.Timer.text = "Duração: " + ProfileManager.selectedProfile.session4.session_duration;

            orientation_date.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session4.orientation_party_day_tries);
            orientation_date.CorrectAnswers.text = ProfileManager.selectedProfile.session4.orientation_party_day_correct_answers.ToString();
            orientation_date.WrongAnswers.text = ProfileManager.selectedProfile.session4.orientation_party_day_wrong_answers.ToString();
            orientation_date.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session4.orientation_party_day_correct_answers_not_selected.ToString();
            orientation_date.Difficulty.text = ProfileManager.selectedProfile.session4.orientation_party_day_difficulty;

            orientation_map.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session4.orientation_party_place_tries);
            orientation_map.CorrectAnswers.text = ProfileManager.selectedProfile.session4.orientation_party_place_correct_answers.ToString();
            orientation_map.WrongAnswers.text = ProfileManager.selectedProfile.session4.orientation_party_place_wrong_answers.ToString();
            orientation_map.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session4.orientation_party_place_correct_answers_not_selected.ToString();
            orientation_map.Difficulty.text = ProfileManager.selectedProfile.session4.orientation_party_place_descriptions;

            orientation_adress.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session4.orientation_invitation_address_tries);
            orientation_adress.CorrectAnswers.text = ProfileManager.selectedProfile.session4.orientation_invitation_address_correct_answers.ToString();
            orientation_adress.WrongAnswers.text = ProfileManager.selectedProfile.session4.orientation_invitation_address_wrong_answers.ToString();
            orientation_adress.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session4.orientation_invitation_address_correct_answers_not_selected.ToString();
            orientation_adress.Difficulty.text = ProfileManager.selectedProfile.session4.orientation_invitation_address_difficulty;

            orientation_hourBegin.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session4.orientation_party_hour_start_tries);
            orientation_hourBegin.CorrectAnswers.text = ProfileManager.selectedProfile.session4.orientation_party_hour_start_correct_answers.ToString();
            orientation_hourBegin.WrongAnswers.text = ProfileManager.selectedProfile.session4.orientation_party_hour_start_wrong_answers.ToString();
            orientation_hourBegin.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session4.orientation_party_hour_start_correct_answers_not_selected.ToString();
            orientation_hourBegin.Difficulty.text = ProfileManager.selectedProfile.session4.orientation_party_hour_start_difficulty;

            orientation_hourEnd.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session4.orientation_party_hour_end_tries);
            orientation_hourEnd.CorrectAnswers.text = ProfileManager.selectedProfile.session4.orientation_party_hour_end_correct_answers.ToString();
            orientation_hourEnd.WrongAnswers.text = ProfileManager.selectedProfile.session4.orientation_party_hour_end_wrong_answers.ToString();
            orientation_hourEnd.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session4.orientation_party_hour_end_correct_answers_not_selected.ToString();
            orientation_hourEnd.Difficulty.text = ProfileManager.selectedProfile.session4.orientation_party_hour_end_difficulty;

            orientation_WhatToTake.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session4.orientation_what_to_take_tries);
            orientation_WhatToTake.CorrectAnswers.text = ProfileManager.selectedProfile.session4.orientation_what_to_take_correct_answers.ToString();
            orientation_WhatToTake.WrongAnswers.text = ProfileManager.selectedProfile.session4.orientation_what_to_take_wrong_answers.ToString();
            orientation_WhatToTake.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session4.orientation_what_to_take_correct_answers_not_selected.ToString();

            orientation_Budget.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session4.orientation_budget_tries);
            orientation_Budget.CorrectAnswers.text = ProfileManager.selectedProfile.session4.orientation_budget_correct_answers.ToString();
            orientation_Budget.WrongAnswers.text = ProfileManager.selectedProfile.session4.orientation_budget_wrong_answers.ToString();
            orientation_Budget.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session4.orientation_budget_correct_answers_not_selected.ToString();
            orientation_Budget.Difficulty.text = ProfileManager.selectedProfile.session4.orientation_budget_difficulty;

            activity_AssociateImages_WrongAnswers.text = ProfileManager.selectedProfile.session4.activity_associate_images_wrong_answers.ToString();
            activity_AssociateImages_Difficulty.text = ProfileManager.selectedProfile.session4.activity_associate_images_difficulty;

            activity_NotDessert.field1.text = ProfileManager.selectedProfile.session4.activity_select_not_dessert_correct_answers.ToString();
            activity_NotDessert.field2.text = ProfileManager.selectedProfile.session4.activity_select_not_dessert_wrong_answers.ToString();
            activity_NotDessert.field3.text = ProfileManager.selectedProfile.session4.activity_select_not_dessert_correct_answers_not_selected.ToString();
            activity_NotDessert.field4.text = ProfileManager.selectedProfile.session4.activity_select_not_dessert_answers_descriptions;

            activity_ChooseDessert_ChosenDessert.text = ProfileManager.selectedProfile.dessert.ToDescription();

            activity_KitchenUtensils.field1.text = CalculateRetrys(ProfileManager.selectedProfile.session4.activity_kitchen_utensils_tries);
            activity_KitchenUtensils.field2.text = ProfileManager.selectedProfile.session4.activity_kitchen_utensils_correct_answers.ToString();
            activity_KitchenUtensils.field3.text = ProfileManager.selectedProfile.session4.activity_kitchen_utensils_wrong_answers.ToString();
            activity_KitchenUtensils.field4.text = ProfileManager.selectedProfile.session4.activity_kitchen_utensils_correct_answers.ToString();
            activity_KitchenUtensils.field5.text = ProfileManager.selectedProfile.session4.activity_kitchen_utensils_difficulty;
            activity_KitchenUtensils.field6.text = ProfileManager.selectedProfile.session4.activity_kitchen_utensils_answers_descriptions;

            proverbio.difficulty.text = ProfileManager.selectedProfile.session4.proverb_difficulty;
            proverbio.timer.text = ProfileManager.selectedProfile.session4.proverb_time;
            proverbio.writtenAnswer.text = ProfileManager.selectedProfile.session4.proverb_written_answer;

            reviewLike.text = ProfileManager.selectedProfile.session4.opinion_session;
            reviewDifficulty.text = ProfileManager.selectedProfile.session4.opinion_difficulty;
        }
        else
        {
            sessionProperties.Timer.text = "";
            sessionProperties.Date.text = "Não foi concluida";
        }
    }
}
