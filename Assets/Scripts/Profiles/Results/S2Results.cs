﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class S2Results : SessionDataTab
{
    [System.Serializable]
    public struct Proverbio
    {
        public Text correctAnswers;
        public Text wrongAnswers;
        public Text unselectedCorrectAnswers;
        public Text writtenAnswer;
    }

    public Results5Fields orientation_date;
    public Results5Fields orientation_map;
    public Results5Fields orientation_adress;
    public Results5Fields orientation_hourBegin;
    public Results5Fields orientation_hourEnd;
    public Results6Fields activity_vehicle1;
    public Results6Fields activity_vehicle2;
    public Results6Fields activity_vehicle3;
    public Results6Fields activity_vehicle4;
    public Results6Fields activity_vehicle5;
    public Results6Fields activity_vehicle6;
    public Results6Fields activity_vehicleFinal;
    public Text activity_map_wrongAnswers;
    public Text activity_map_difficulty;
    public Proverbio proverbio;

    protected override void UpdateResults()
    {
        sessionProperties.numConviteOpened.text = ProfileManager.selectedProfile.session2.session_invitation_tries.ToString();
        if (ProfileManager.selectedProfile.completedSessions >= 2)
        {
            sessionProperties.Date.text = "Data: " + ProfileManager.selectedProfile.session2.session_date;
            sessionProperties.Timer.text = "Duração: " + ProfileManager.selectedProfile.session2.session_duration;

            orientation_date.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session2.orientation_party_day_tries);
            orientation_date.CorrectAnswers.text = ProfileManager.selectedProfile.session2.orientation_party_day_correct_answers.ToString();
            orientation_date.WrongAnswers.text = ProfileManager.selectedProfile.session2.orientation_party_day_wrong_answers.ToString();
            orientation_date.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session2.orientation_party_day_correct_answers_not_selected.ToString();
            orientation_date.Difficulty.text = ProfileManager.selectedProfile.session2.orientation_party_day_difficulty;

            orientation_map.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session2.orientation_party_place_tries);
            orientation_map.CorrectAnswers.text = ProfileManager.selectedProfile.session2.orientation_party_place_correct_answers.ToString();
            orientation_map.WrongAnswers.text = ProfileManager.selectedProfile.session2.orientation_party_place_wrong_answers.ToString();
            orientation_map.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session2.orientation_party_place_correct_answers_not_selected.ToString();
            orientation_map.Difficulty.text = ProfileManager.selectedProfile.session2.orientation_party_place_answers_descriptions;

            orientation_adress.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session2.orientation_invitation_address_tries);
            orientation_adress.CorrectAnswers.text = ProfileManager.selectedProfile.session2.orientation_invitation_address_correct_answers.ToString();
            orientation_adress.WrongAnswers.text = ProfileManager.selectedProfile.session2.orientation_invitation_address_wrong_answers.ToString();
            orientation_adress.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session2.orientation_invitation_address_correct_answers_not_selected.ToString();
            orientation_adress.Difficulty.text = ProfileManager.selectedProfile.session2.orientation_invitation_address_difficulty;

            orientation_hourBegin.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session2.orientation_party_hour_start_tries);
            orientation_hourBegin.CorrectAnswers.text = ProfileManager.selectedProfile.session2.orientation_party_hour_start_correct_answers.ToString();
            orientation_hourBegin.WrongAnswers.text = ProfileManager.selectedProfile.session2.orientation_party_hour_start_wrong_answers.ToString();
            orientation_hourBegin.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session2.orientation_party_hour_start_correct_answers_not_selected.ToString();
            orientation_hourBegin.Difficulty.text = ProfileManager.selectedProfile.session2.orientation_party_hour_start_difficulty;

            orientation_hourEnd.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session2.orientation_party_hour_end_tries);
            orientation_hourEnd.CorrectAnswers.text = ProfileManager.selectedProfile.session2.orientation_party_hour_end_correct_answers.ToString();
            orientation_hourEnd.WrongAnswers.text = ProfileManager.selectedProfile.session2.orientation_party_hour_end_wrong_answers.ToString();
            orientation_hourEnd.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session2.orientation_party_hour_end_correct_answers_not_selected.ToString();
            orientation_hourEnd.Difficulty.text = ProfileManager.selectedProfile.session2.orientation_party_hour_end_difficulty;

            activity_vehicle1.field1.text = CalculateRetrys(ProfileManager.selectedProfile.session2.activity_car_tries);
            activity_vehicle1.field2.text = ProfileManager.selectedProfile.session2.activity_car_correct_answers.ToString();
            activity_vehicle1.field3.text = ProfileManager.selectedProfile.session2.activity_car_wrong_answers.ToString();
            activity_vehicle1.field4.text = ProfileManager.selectedProfile.session2.activity_car_correct_answers_not_selected.ToString();
            activity_vehicle1.field5.text = ProfileManager.selectedProfile.session2.activity_car_difficulty;
            activity_vehicle1.field6.text = ProfileManager.selectedProfile.session2.activity_car_answer_description;

            activity_vehicle2.field1.text = CalculateRetrys(ProfileManager.selectedProfile.session2.activity_bike_tries);
            activity_vehicle2.field2.text = ProfileManager.selectedProfile.session2.activity_bike_correct_answers.ToString();
            activity_vehicle2.field3.text = ProfileManager.selectedProfile.session2.activity_bike_wrong_answers.ToString();
            activity_vehicle2.field4.text = ProfileManager.selectedProfile.session2.activity_bike_correct_answers_not_selected.ToString();
            activity_vehicle2.field5.text = ProfileManager.selectedProfile.session2.activity_bike_difficulty;
            activity_vehicle2.field6.text = ProfileManager.selectedProfile.session2.activity_bike_answer_description;

            activity_vehicle3.field1.text = CalculateRetrys(ProfileManager.selectedProfile.session2.activity_bicycle_tries);
            activity_vehicle3.field2.text = ProfileManager.selectedProfile.session2.activity_bicycle_correct_answers.ToString();
            activity_vehicle3.field3.text = ProfileManager.selectedProfile.session2.activity_bicycle_wrong_answers.ToString();
            activity_vehicle3.field4.text = ProfileManager.selectedProfile.session2.activity_bicycle_correct_answers_not_selected.ToString();
            activity_vehicle3.field5.text = ProfileManager.selectedProfile.session2.activity_bicycle_difficulty;
            activity_vehicle3.field6.text = ProfileManager.selectedProfile.session2.activity_bicycle_answer_description;

            activity_vehicle4.field1.text = CalculateRetrys(ProfileManager.selectedProfile.session2.activity_train_tries);
            activity_vehicle4.field2.text = ProfileManager.selectedProfile.session2.activity_train_correct_answers.ToString();
            activity_vehicle4.field3.text = ProfileManager.selectedProfile.session2.activity_train_wrong_answers.ToString();
            activity_vehicle4.field4.text = ProfileManager.selectedProfile.session2.activity_train_correct_answers_not_selected.ToString();
            activity_vehicle4.field5.text = ProfileManager.selectedProfile.session2.activity_train_difficulty;
            activity_vehicle4.field6.text = ProfileManager.selectedProfile.session2.activity_train_answer_description;

            activity_vehicle5.field1.text = CalculateRetrys(ProfileManager.selectedProfile.session2.activity_airplane_tries);
            activity_vehicle5.field2.text = ProfileManager.selectedProfile.session2.activity_airplane_correct_answers.ToString();
            activity_vehicle5.field3.text = ProfileManager.selectedProfile.session2.activity_airplane_wrong_answers.ToString();
            activity_vehicle5.field4.text = ProfileManager.selectedProfile.session2.activity_airplane_correct_answers_not_selected.ToString();
            activity_vehicle5.field5.text = ProfileManager.selectedProfile.session2.activity_airplane_difficulty;
            activity_vehicle5.field6.text = ProfileManager.selectedProfile.session2.activity_airplane_answer_description;

            activity_vehicle6.field1.text = CalculateRetrys(ProfileManager.selectedProfile.session2.activity_boat_tries);
            activity_vehicle6.field2.text = ProfileManager.selectedProfile.session2.activity_boat_correct_answers.ToString();
            activity_vehicle6.field3.text = ProfileManager.selectedProfile.session2.activity_boat_wrong_answers.ToString();
            activity_vehicle6.field4.text = ProfileManager.selectedProfile.session2.activity_boat_correct_answers_not_selected.ToString();
            activity_vehicle6.field5.text = ProfileManager.selectedProfile.session2.activity_boat_difficulty;
            activity_vehicle6.field6.text = ProfileManager.selectedProfile.session2.activity_boat_answer_description;

            activity_vehicleFinal.field1.text = CalculateRetrys(ProfileManager.selectedProfile.session2.activity_two_wheels_tries);
            activity_vehicleFinal.field2.text = ProfileManager.selectedProfile.session2.activity_two_wheels_correct_answers.ToString();
            activity_vehicleFinal.field3.text = ProfileManager.selectedProfile.session2.activity_two_wheels_wrong_answers.ToString();
            activity_vehicleFinal.field4.text = ProfileManager.selectedProfile.session2.activity_two_wheels_correct_answers_not_selected.ToString();
            activity_vehicleFinal.field5.text = ProfileManager.selectedProfile.session2.activity_two_wheels_difficulty;
            activity_vehicleFinal.field6.text = ProfileManager.selectedProfile.session2.activity_two_wheels_answer_description;

            activity_map_wrongAnswers.text = ProfileManager.selectedProfile.session2.activity_map_wrong_answers.ToString();
            activity_map_difficulty.text = ProfileManager.selectedProfile.session2.activity_map_difficulty;

            proverbio.correctAnswers.text = ProfileManager.selectedProfile.session2.proverb_correct_answers.ToString();
            proverbio.wrongAnswers.text = ProfileManager.selectedProfile.session2.proverb_wrong_answers.ToString();
            proverbio.unselectedCorrectAnswers.text = ProfileManager.selectedProfile.session2.proverb_correct_answers_not_selected.ToString();
            proverbio.writtenAnswer.text = ProfileManager.selectedProfile.session2.proverb_written_answer;

            reviewLike.text = ProfileManager.selectedProfile.session2.opinion_session;
            reviewDifficulty.text = ProfileManager.selectedProfile.session2.opinion_difficulty;
        }
        else
        {
            sessionProperties.Timer.text = "";
            sessionProperties.Date.text = "Não foi concluida";
        }
    }

}
