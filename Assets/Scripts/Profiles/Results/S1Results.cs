﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class S1Results : SessionDataTab
{
    [System.Serializable]
    public struct Proverbio
    {
        public Text difficulty;
        public Text writtenAnswer;
    }
    [System.Serializable]
    public struct GameClock
    {
        public Text correctAnswers;
        public Text wrongAnswers;
        public Text hourAnswer;
        public Text minutesAnswer;
    }
    [System.Serializable]
    public struct EnvelopeAnswer
    {
        public Text correctAnswers;
        public Text wrongAnswers;
        public Text name;
        public Text adress;
        public Text city;
    }

    public Results5Fields convite;
    public Results5Fields calendars;
    public Results5Fields hoursBegin;
    public GameClock gameClock;
    public Results6Fields carta;
    public EnvelopeAnswer envelope;
    public Proverbio proverbio;

    protected override void UpdateResults()
    {
        sessionProperties.numConviteOpened.text = ProfileManager.selectedProfile.session1.session_invitation_tries.ToString();

        if (ProfileManager.selectedProfile.completedSessions >= 1)
        {
            sessionProperties.Date.text = "Data: " + ProfileManager.selectedProfile.session1.session_date;
            sessionProperties.Timer.text = "Duração: " + ProfileManager.selectedProfile.session1.session_duration;

            convite.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session1.activity_invitation_tries);
            convite.CorrectAnswers.text = ProfileManager.selectedProfile.session1.activity_invitation_correct_answers.ToString();
            convite.WrongAnswers.text = ProfileManager.selectedProfile.session1.activity_invitation_wrong_answers.ToString();
            convite.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session1.activity_invitation_correct_answers_not_selected.ToString();
            convite.Difficulty.text = ProfileManager.selectedProfile.session1.activity_invitation_difficulty;

            calendars.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session1.activity_party_day_tries);
            calendars.CorrectAnswers.text = ProfileManager.selectedProfile.session1.activity_party_day_correct_answers.ToString();
            calendars.WrongAnswers.text = ProfileManager.selectedProfile.session1.activity_party_day_wrong_answers.ToString();
            calendars.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session1.activity_party_day_correct_answers_not_selected.ToString();
            calendars.Difficulty.text = ProfileManager.selectedProfile.session1.activity_party_day_difficulty;

            hoursBegin.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session1.activity_party_hour_start_tries);
            hoursBegin.CorrectAnswers.text = ProfileManager.selectedProfile.session1.activity_party_hour_start_correct_answers.ToString();
            hoursBegin.WrongAnswers.text = ProfileManager.selectedProfile.session1.activity_party_hour_start_wrong_answers.ToString();
            hoursBegin.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session1.activity_party_hour_start_correct_answers_not_selected.ToString();
            hoursBegin.Difficulty.text = ProfileManager.selectedProfile.session1.activity_party_hour_start_difficulty;

            gameClock.correctAnswers.text = ProfileManager.selectedProfile.session1.activity_party_hour_end_correct_answers.ToString();
            gameClock.wrongAnswers.text = ProfileManager.selectedProfile.session1.activity_party_hour_end_wrong_answers.ToString();
            gameClock.hourAnswer.text = ProfileManager.selectedProfile.session1.activity_party_hour_end_chosen_hour.ToString();
            gameClock.minutesAnswer.text = ProfileManager.selectedProfile.session1.activity_party_hour_end_chosen_minute.ToString();

            carta.field1.text = CalculateRetrys(ProfileManager.selectedProfile.session1.activity_write_letter_tries);
            carta.field2.text = ProfileManager.selectedProfile.session1.activity_write_letter_correct_answers.ToString();
            carta.field3.text = ProfileManager.selectedProfile.session1.activity_write_letter_wrong_answers.ToString();
            carta.field4.text = ProfileManager.selectedProfile.session1.activity_write_letter_correct_answers_not_selected.ToString();
            carta.field5.text = ProfileManager.selectedProfile.session1.activity_write_letter_difficulty;
            carta.field6.text = ProfileManager.selectedProfile.session1.activity_write_letter_descriptions;

            envelope.correctAnswers.text = ProfileManager.selectedProfile.session1.activity_write_answer_invitation_correct_answers.ToString();
            envelope.wrongAnswers.text = ProfileManager.selectedProfile.session1.activity_write_answer_invitation_wrong_answers.ToString();

            envelope.name.text = ProfileManager.selectedProfile.session1.activity_answer_invitation_name;
            envelope.city.text = ProfileManager.selectedProfile.session1.activity_answer_invitation_city;
            envelope.adress.text = ProfileManager.selectedProfile.session1.activity_answer_invitation_address;

            proverbio.difficulty.text = ProfileManager.selectedProfile.session1.proverb_difficulty;
            proverbio.writtenAnswer.text = ProfileManager.selectedProfile.session1.proverb_written_answer;

            reviewLike.text = ProfileManager.selectedProfile.session1.opinion_difficulty;
            reviewDifficulty.text = ProfileManager.selectedProfile.session1.opinion_session;
        }
        else
        {
            sessionProperties.Timer.text = "";
            sessionProperties.Date.text = "Não foi concluida";
        }
    }
}
