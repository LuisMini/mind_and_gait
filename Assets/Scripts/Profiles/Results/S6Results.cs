﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class S6Results : SessionDataTab
{
    public Results5Fields orientation_date;
    public Results5Fields orientation_map;
    public Results5Fields orientation_adress;
    public Results5Fields orientation_hourBegin;
    public Results5Fields orientation_hourEnd;
    public Results5Fields orientation_WhatToTake;
    public Results5Fields orientation_Budget;
    public Text orientation_dessert;
    public Results2Fields activity_telephone;
    public Results2Fields activity_hatColor;
    public Text activity_escolherEmblema;
    public Text activity_criarEmblema;
    public Results2Fields activity_personalizarEmblema;
    public Results4Fields proverbio;

    protected override void UpdateResults()
    {
        sessionProperties.numConviteOpened.text = ProfileManager.selectedProfile.session6.session_invitation_tries.ToString();

        if (ProfileManager.selectedProfile.completedSessions >= 6)
        {
            sessionProperties.Date.text = "Data: " + ProfileManager.selectedProfile.session6.session_date;
            sessionProperties.Timer.text = "Duração: " + ProfileManager.selectedProfile.session6.session_duration;

            orientation_date.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session6.orientation_party_day_tries);
            orientation_date.CorrectAnswers.text = ProfileManager.selectedProfile.session6.orientation_party_day_correct_answers.ToString();
            orientation_date.WrongAnswers.text = ProfileManager.selectedProfile.session6.orientation_party_day_wrong_answers.ToString();
            orientation_date.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session6.orientation_party_day_correct_answers_not_selected.ToString();
            orientation_date.Difficulty.text = ProfileManager.selectedProfile.session6.orientation_party_day_difficulty;

            orientation_map.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session6.orientation_party_place_tries);
            orientation_map.CorrectAnswers.text = ProfileManager.selectedProfile.session6.orientation_party_place_correct_answers.ToString();
            orientation_map.WrongAnswers.text = ProfileManager.selectedProfile.session6.orientation_party_place_wrong_answers.ToString();
            orientation_map.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session6.orientation_party_place_correct_answers_not_selected.ToString();
            orientation_map.Difficulty.text = ProfileManager.selectedProfile.session6.orientation_party_place_descriptions;

            orientation_adress.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session6.orientation_invitation_address_tries);
            orientation_adress.CorrectAnswers.text = ProfileManager.selectedProfile.session6.orientation_invitation_address_correct_answers.ToString();
            orientation_adress.WrongAnswers.text = ProfileManager.selectedProfile.session6.orientation_invitation_address_wrong_answers.ToString();
            orientation_adress.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session6.orientation_invitation_address_correct_answers_not_selected.ToString();
            orientation_adress.Difficulty.text = ProfileManager.selectedProfile.session6.orientation_invitation_address_difficulty;

            orientation_hourBegin.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session6.orientation_party_hour_start_tries);
            orientation_hourBegin.CorrectAnswers.text = ProfileManager.selectedProfile.session6.orientation_party_hour_start_correct_answers.ToString();
            orientation_hourBegin.WrongAnswers.text = ProfileManager.selectedProfile.session6.orientation_party_hour_start_wrong_answers.ToString();
            orientation_hourBegin.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session6.orientation_party_hour_start_correct_answers_not_selected.ToString();
            orientation_hourBegin.Difficulty.text = ProfileManager.selectedProfile.session6.orientation_party_hour_start_difficulty;

            orientation_hourEnd.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session6.orientation_party_hour_end_tries);
            orientation_hourEnd.CorrectAnswers.text = ProfileManager.selectedProfile.session6.orientation_party_hour_end_correct_answers.ToString();
            orientation_hourEnd.WrongAnswers.text = ProfileManager.selectedProfile.session6.orientation_party_hour_end_wrong_answers.ToString();
            orientation_hourEnd.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session6.orientation_party_hour_end_correct_answers_not_selected.ToString();
            orientation_hourEnd.Difficulty.text = ProfileManager.selectedProfile.session6.orientation_party_hour_end_difficulty;

            orientation_WhatToTake.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session6.orientation_what_to_take_tries);
            orientation_WhatToTake.CorrectAnswers.text = ProfileManager.selectedProfile.session6.orientation_what_to_take_correct_answers.ToString();
            orientation_WhatToTake.WrongAnswers.text = ProfileManager.selectedProfile.session6.orientation_what_to_take_wrong_answers.ToString();
            orientation_WhatToTake.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session6.orientation_what_to_take_correct_answers_not_selected.ToString();

            orientation_Budget.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session6.orientation_budget_tries);
            orientation_Budget.CorrectAnswers.text = ProfileManager.selectedProfile.session6.orientation_budget_correct_answers.ToString();
            orientation_Budget.WrongAnswers.text = ProfileManager.selectedProfile.session6.orientation_budget_wrong_answers.ToString();
            orientation_Budget.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session6.orientation_budget_correct_answers_not_selected.ToString();
            orientation_Budget.Difficulty.text = ProfileManager.selectedProfile.session6.orientation_budget_difficulty;

            orientation_dessert.text = ProfileManager.selectedProfile.session6.orientation_dessert_correct;
            
            activity_telephone.field1.text = ProfileManager.selectedProfile.session6.activity_phone_wrong_answers.ToString();
            activity_telephone.field2.text = ProfileManager.selectedProfile.session6.activity_phone_deleted_message.ToString();

            activity_hatColor.field1.text = ProfileManager.selectedProfile.session6.activity_hat_color_correct_answers.ToString();
            activity_hatColor.field2.text = ProfileManager.selectedProfile.session6.activity_hat_color_wrong_answers.ToString();

            activity_escolherEmblema.text = ProfileManager.selectedProfile.chosenEmblem.ToDescription();

            activity_criarEmblema.text = ProfileManager.selectedProfile.session6.activity_create_emblem_time;

            activity_personalizarEmblema.field1.text = ProfileManager.selectedProfile.session6.activity_personalize_emblem_wrong_answers.ToString();
            activity_personalizarEmblema.field2.text = ProfileManager.selectedProfile.session6.activity_personalize_emblem_time.ToString();

            proverbio.field1.text = ProfileManager.selectedProfile.session6.proverb_correct_answers.ToString();
            proverbio.field2.text = ProfileManager.selectedProfile.session6.proverb_wrong_answers.ToString();
            proverbio.field3.text = ProfileManager.selectedProfile.session6.proverb_correct_answers_not_selected.ToString();
            proverbio.field4.text = ProfileManager.selectedProfile.session6.proverb_written_answer.ToString();

            reviewLike.text = ProfileManager.selectedProfile.session6.opinion_session;
            reviewDifficulty.text = ProfileManager.selectedProfile.session6.opinion_difficulty;
        }
        else
        {
            sessionProperties.Timer.text = "";
            sessionProperties.Date.text = "Não foi concluida";
        }
    }
}
