﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class SessionDataTab : MonoBehaviour {
    
    public GameObject dataContainer;
    private Button button;

    [Header("Session Data")]
    #region
    public SessionProperties sessionProperties;
    public Text reviewDifficulty;
    public Text reviewLike;
    #endregion

    [System.Serializable]
    public struct SessionProperties
    {
        public Text numConviteOpened;
        public Text Date;
        public Text Timer;
    }

    [System.Serializable]
    public struct Results2Fields
    {
        public Text field1;
        public Text field2;
    }

    [System.Serializable]
    public struct Results3Fields
    {
        public Text field1;
        public Text field2;
        public Text field3;
    }

    [System.Serializable]
    public struct Results4Fields
    {
        public Text field1;
        public Text field2;
        public Text field3;
        public Text field4;
    }

    [System.Serializable]
    public struct Results5Fields
    {
        public Text Attempts;
        public Text CorrectAnswers;
        public Text WrongAnswers;
        public Text UnselectedCorrectAnswers;
        public Text Difficulty;
    }

    [System.Serializable]
    public struct Results6Fields
    {
        public Text field1;
        public Text field2;
        public Text field3;
        public Text field4;
        public Text field5;
        public Text field6;
    }

    private void Start()
    {
        dataContainer = transform.Find("Container").gameObject;
        button = gameObject.GetComponentInChildren<Button>();
        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(OpenContainer);
    }

    private void OnEnable()
    {
        UpdateResults();
    }

    protected virtual void UpdateResults() { }

    public void OpenContainer ()
    {
        Debug.Log("OpenContainer");
        if (dataContainer.activeInHierarchy)
        {
            dataContainer.SetActive(false);
        }
        else
        {
            dataContainer.SetActive(true);
        }
    }

    protected string CalculateRetrys (int retrysUsed)
    {
        if (retrysUsed == 0)
        {
            return "Não";
        }
        else if (retrysUsed == 1)
        {
            return "Sim, 1 vez";
        }
        else if (retrysUsed > 1)
        {
            return "Sim, " + retrysUsed + " vezes";
        }
        else
        {
            return "Erro";
        }
    }
}
