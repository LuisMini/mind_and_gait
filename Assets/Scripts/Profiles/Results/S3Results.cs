﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class S3Results : SessionDataTab
{
    [System.Serializable]
    public struct Proverbio
    {
        public Text correctAnswers;
        public Text wrongAnswers;
        public Text unselectedCorrectAnswers;
        public Text writtenAnswer;
        public Text description;
    }
    [System.Serializable]
    public struct ChooseBudget
    {
        public Text wrongAnswers;
        public Text chosenBudget;
        public Text writtenAnswer;
        public Text difficulty;
    }
    [System.Serializable]
    public struct MemoryGame
    {
        public Text totalPairsFlipped;
        public Text correctPairs;
        public Text wrongPairs;
        public Text difficulty;
        public Text writtenAnswer1;
        public Text writtenAnswer2;
        public Text writtenAnswer3;
    }

    public Results5Fields orientation_date;
    public Results5Fields orientation_map;
    public Results5Fields orientation_adress;
    public Results5Fields orientation_hourBegin;
    public Results5Fields orientation_hourEnd;
    public Results5Fields orientation_WhatToTake;
    public MemoryGame activity_MemoryGame;
    public Text activity_GameOrder_Time;
    public ChooseBudget activity_ChooseBudget;
    public Proverbio proverbio;

    protected override void UpdateResults()
    {
        sessionProperties.numConviteOpened.text = ProfileManager.selectedProfile.session3.session_invitation_tries.ToString();

        if (ProfileManager.selectedProfile.completedSessions >= 3)
        {
            sessionProperties.Date.text = "Data: " + ProfileManager.selectedProfile.session3.session_date;
            sessionProperties.Timer.text = "Duração: " + ProfileManager.selectedProfile.session3.session_duration;

            orientation_date.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session3.orientation_party_day_tries);
            orientation_date.CorrectAnswers.text = ProfileManager.selectedProfile.session3.orientation_party_day_correct_answers.ToString();
            orientation_date.WrongAnswers.text = ProfileManager.selectedProfile.session3.orientation_party_day_wrong_answers.ToString();
            orientation_date.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session3.orientation_party_day_correct_answers_not_selected.ToString();
            orientation_date.Difficulty.text = ProfileManager.selectedProfile.session3.orientation_party_day_difficulty;

            orientation_map.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session3.orientation_party_place_tries);
            orientation_map.CorrectAnswers.text = ProfileManager.selectedProfile.session3.orientation_party_place_correct_answers.ToString();
            orientation_map.WrongAnswers.text = ProfileManager.selectedProfile.session3.orientation_party_place_wrong_answers.ToString();
            orientation_map.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session3.orientation_party_place_correct_answers_not_selected.ToString();
            orientation_map.Difficulty.text = ProfileManager.selectedProfile.session3.orientation_party_place_descriptions;

            orientation_adress.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session3.orientation_invitation_address_tries);
            orientation_adress.CorrectAnswers.text = ProfileManager.selectedProfile.session3.orientation_invitation_address_correct_answers.ToString();
            orientation_adress.WrongAnswers.text = ProfileManager.selectedProfile.session3.orientation_invitation_address_wrong_answers.ToString();
            orientation_adress.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session3.orientation_invitation_address_correct_answers_not_selected.ToString();
            orientation_adress.Difficulty.text = ProfileManager.selectedProfile.session3.orientation_invitation_address_difficulty;

            orientation_hourBegin.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session3.orientation_party_hour_start_tries);
            orientation_hourBegin.CorrectAnswers.text = ProfileManager.selectedProfile.session3.orientation_party_hour_start_correct_answers.ToString();
            orientation_hourBegin.WrongAnswers.text = ProfileManager.selectedProfile.session3.orientation_party_hour_start_wrong_answers.ToString();
            orientation_hourBegin.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session3.orientation_party_hour_start_correct_answers_not_selected.ToString();
            orientation_hourBegin.Difficulty.text = ProfileManager.selectedProfile.session3.orientation_party_hour_start_difficulty;

            orientation_hourEnd.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session3.orientation_party_hour_end_tries);
            orientation_hourEnd.CorrectAnswers.text = ProfileManager.selectedProfile.session3.orientation_party_hour_end_correct_answers.ToString();
            orientation_hourEnd.WrongAnswers.text = ProfileManager.selectedProfile.session3.orientation_party_hour_end_wrong_answers.ToString();
            orientation_hourEnd.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session3.orientation_party_hour_end_correct_answers_not_selected.ToString();
            orientation_hourEnd.Difficulty.text = ProfileManager.selectedProfile.session3.orientation_party_hour_end_difficulty;

            orientation_WhatToTake.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session3.orientation_what_to_take_tries);
            orientation_WhatToTake.CorrectAnswers.text = ProfileManager.selectedProfile.session3.orientation_what_to_take_correct_answers.ToString();
            orientation_WhatToTake.WrongAnswers.text = ProfileManager.selectedProfile.session3.orientation_what_to_take_wrong_answers.ToString();
            orientation_WhatToTake.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session3.orientation_what_to_take_correct_answers_not_selected.ToString();

            activity_MemoryGame.totalPairsFlipped.text = ProfileManager.selectedProfile.session3.activity_memory_game_pairs_turned.ToString();
            activity_MemoryGame.correctPairs.text = ProfileManager.selectedProfile.session3.activity_memory_game_pairs_correct.ToString();
            activity_MemoryGame.wrongPairs.text = ProfileManager.selectedProfile.session3.activity_memory_game_pairs_wrong.ToString();
            activity_MemoryGame.difficulty.text = ProfileManager.selectedProfile.session3.activity_memory_game_difficulty;
            activity_MemoryGame.writtenAnswer1.text = ProfileManager.selectedProfile.session3.activity_memory_game_2_2;
            activity_MemoryGame.writtenAnswer2.text = ProfileManager.selectedProfile.session3.activity_memory_game_5_5;
            activity_MemoryGame.writtenAnswer3.text = ProfileManager.selectedProfile.session3.activity_memory_game_10_10;

            activity_GameOrder_Time.text = ProfileManager.selectedProfile.session3.activity_order_time;

            activity_ChooseBudget.wrongAnswers.text = ProfileManager.selectedProfile.session3.budget_wrong_answers.ToString();
            activity_ChooseBudget.chosenBudget.text = ProfileManager.selectedProfile.session3.budget_chosen + " €";
            activity_ChooseBudget.writtenAnswer.text = ProfileManager.selectedProfile.session3.budget_writen_answer;
            activity_ChooseBudget.difficulty.text = ProfileManager.selectedProfile.session3.budget_difficulty;

            proverbio.correctAnswers.text = ProfileManager.selectedProfile.session3.proverb_correct_answers.ToString();
            proverbio.wrongAnswers.text = ProfileManager.selectedProfile.session3.proverb_wrong_answers.ToString();
            proverbio.unselectedCorrectAnswers.text = ProfileManager.selectedProfile.session3.proverb_correct_answers_not_selected.ToString();
            proverbio.writtenAnswer.text = ProfileManager.selectedProfile.session3.proverb_written_answer;
            proverbio.description.text = ProfileManager.selectedProfile.session3.proverb_answers_description;

            reviewLike.text = ProfileManager.selectedProfile.session3.opinion_session;
            reviewDifficulty.text = ProfileManager.selectedProfile.session3.opinion_difficulty;
        }
        else
        {
            sessionProperties.Timer.text = "";
            sessionProperties.Date.text = "Não foi concluida";
        }
    }
}
