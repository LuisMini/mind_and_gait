﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class S7Results : SessionDataTab
{
    public Results5Fields orientation_date;
    public Results5Fields orientation_map;
    public Results5Fields orientation_adress;
    public Results5Fields orientation_hourBegin;
    public Results5Fields orientation_hourEnd;
    public Results5Fields orientation_WhatToTake;
    public Results5Fields orientation_Budget;
    public Text orientation_dessert;
    public Results5Fields orientation_Hat;
    public Results3Fields activity_selectSongs;
    public Results6Fields activity_music1;
    public Results6Fields activity_music2;
    public Results6Fields activity_music3;
    public Text activity_emotion_IndoEu;
    public Text activity_emotion_Alecrim;
    public Text proverbio;

    protected override void UpdateResults()
    {
        sessionProperties.numConviteOpened.text = ProfileManager.selectedProfile.session7.session_invitation_tries.ToString();

        if (ProfileManager.selectedProfile.completedSessions >= 7)
        {
            sessionProperties.Date.text = "Data: " + ProfileManager.selectedProfile.session7.session_date;
            sessionProperties.Timer.text = "Duração: " + ProfileManager.selectedProfile.session7.session_duration;

            orientation_date.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session7.orientation_party_day_tries);
            orientation_date.CorrectAnswers.text = ProfileManager.selectedProfile.session7.orientation_party_day_correct_answers.ToString();
            orientation_date.WrongAnswers.text = ProfileManager.selectedProfile.session7.orientation_party_day_wrong_answers.ToString();
            orientation_date.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session7.orientation_party_day_correct_answers_not_selected.ToString();
            orientation_date.Difficulty.text = ProfileManager.selectedProfile.session7.orientation_party_day_difficulty;

            orientation_map.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session7.orientation_party_place_tries);
            orientation_map.CorrectAnswers.text = ProfileManager.selectedProfile.session7.orientation_party_place_correct_answers.ToString();
            orientation_map.WrongAnswers.text = ProfileManager.selectedProfile.session7.orientation_party_place_wrong_answers.ToString();
            orientation_map.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session7.orientation_party_place_correct_answers_not_selected.ToString();
            orientation_map.Difficulty.text = ProfileManager.selectedProfile.session7.orientation_party_place_descriptions;

            orientation_adress.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session7.orientation_invitation_address_tries);
            orientation_adress.CorrectAnswers.text = ProfileManager.selectedProfile.session7.orientation_invitation_address_correct_answers.ToString();
            orientation_adress.WrongAnswers.text = ProfileManager.selectedProfile.session7.orientation_invitation_address_wrong_answers.ToString();
            orientation_adress.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session7.orientation_invitation_address_correct_answers_not_selected.ToString();
            orientation_adress.Difficulty.text = ProfileManager.selectedProfile.session7.orientation_invitation_address_difficulty;

            orientation_hourBegin.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session7.orientation_party_hour_start_tries);
            orientation_hourBegin.CorrectAnswers.text = ProfileManager.selectedProfile.session7.orientation_party_hour_start_correct_answers.ToString();
            orientation_hourBegin.WrongAnswers.text = ProfileManager.selectedProfile.session7.orientation_party_hour_start_wrong_answers.ToString();
            orientation_hourBegin.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session7.orientation_party_hour_start_correct_answers_not_selected.ToString();
            orientation_hourBegin.Difficulty.text = ProfileManager.selectedProfile.session7.orientation_party_hour_start_difficulty;

            orientation_hourEnd.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session7.orientation_party_hour_end_tries);
            orientation_hourEnd.CorrectAnswers.text = ProfileManager.selectedProfile.session7.orientation_party_hour_end_correct_answers.ToString();
            orientation_hourEnd.WrongAnswers.text = ProfileManager.selectedProfile.session7.orientation_party_hour_end_wrong_answers.ToString();
            orientation_hourEnd.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session7.orientation_party_hour_end_correct_answers_not_selected.ToString();
            orientation_hourEnd.Difficulty.text = ProfileManager.selectedProfile.session7.orientation_party_hour_end_difficulty;

            orientation_WhatToTake.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session7.orientation_what_to_take_tries);
            orientation_WhatToTake.CorrectAnswers.text = ProfileManager.selectedProfile.session7.orientation_what_to_take_correct_answers.ToString();
            orientation_WhatToTake.WrongAnswers.text = ProfileManager.selectedProfile.session7.orientation_what_to_take_wrong_answers.ToString();
            orientation_WhatToTake.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session7.orientation_what_to_take_correct_answers_not_selected.ToString();

            orientation_Budget.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session7.orientation_budget_tries);
            orientation_Budget.CorrectAnswers.text = ProfileManager.selectedProfile.session7.orientation_budget_correct_answers.ToString();
            orientation_Budget.WrongAnswers.text = ProfileManager.selectedProfile.session7.orientation_budget_wrong_answers.ToString();
            orientation_Budget.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session7.orientation_budget_correct_answers_not_selected.ToString();
            orientation_Budget.Difficulty.text = ProfileManager.selectedProfile.session7.orientation_budget_difficulty;

            orientation_dessert.text = ProfileManager.selectedProfile.session7.orientation_dessert_correct;

            orientation_Hat.Attempts.text = CalculateRetrys(ProfileManager.selectedProfile.session7.orientation_hat_tries);
            orientation_Hat.CorrectAnswers.text = ProfileManager.selectedProfile.session7.orientation_hat_correct_answers.ToString();
            orientation_Hat.WrongAnswers.text = ProfileManager.selectedProfile.session7.orientation_hat_wrong_answers.ToString();
            orientation_Hat.UnselectedCorrectAnswers.text = ProfileManager.selectedProfile.session7.orientation_hat_correct_answers_not_selected.ToString();
            orientation_Hat.Difficulty.text = ProfileManager.selectedProfile.session7.orientation_hat_difficulty;

            activity_selectSongs.field1.text = ProfileManager.selectedProfile.session7.activity_choose_music_1;
            activity_selectSongs.field2.text = ProfileManager.selectedProfile.session7.activity_choose_music_2;
            activity_selectSongs.field3.text = ProfileManager.selectedProfile.session7.activity_choose_music_3;

            activity_music1.field1.text = ProfileManager.selectedProfile.session7.activity_complete_music_1_first_word;
            activity_music1.field2.text = ProfileManager.selectedProfile.session7.activity_complete_music_1_second_word;
            activity_music1.field3.text = ProfileManager.selectedProfile.session7.activity_complete_music_1_third_word;
            activity_music1.field4.text = ProfileManager.selectedProfile.session7.activity_complete_music_1_fourth_word;
            activity_music1.field5.text = ProfileManager.selectedProfile.session7.activity_complete_music_1_fifth_word;
            activity_music1.field6.text = ProfileManager.selectedProfile.session7.activity_complete_music_1_difficulty;

            activity_music2.field1.text = ProfileManager.selectedProfile.session7.activity_complete_music_2_first_word;
            activity_music2.field2.text = ProfileManager.selectedProfile.session7.activity_complete_music_2_second_word;
            activity_music2.field3.text = ProfileManager.selectedProfile.session7.activity_complete_music_2_third_word;
            activity_music2.field4.text = ProfileManager.selectedProfile.session7.activity_complete_music_2_fourth_word;
            activity_music2.field5.text = ProfileManager.selectedProfile.session7.activity_complete_music_2_fifth_word;
            activity_music2.field6.text = ProfileManager.selectedProfile.session7.activity_complete_music_2_difficulty;

            activity_music3.field1.text = ProfileManager.selectedProfile.session7.activity_complete_music_3_first_word;
            activity_music3.field2.text = ProfileManager.selectedProfile.session7.activity_complete_music_3_second_word;
            activity_music3.field3.text = ProfileManager.selectedProfile.session7.activity_complete_music_3_third_word;
            activity_music3.field4.text = ProfileManager.selectedProfile.session7.activity_complete_music_3_fourth_word;
            activity_music3.field5.text = ProfileManager.selectedProfile.session7.activity_complete_music_3_fifth_word;
            activity_music3.field6.text = ProfileManager.selectedProfile.session7.activity_complete_music_3_difficulty;

            activity_emotion_IndoEu.text = ProfileManager.selectedProfile.session7.activity_emotion_indoeu;

            activity_emotion_Alecrim.text = ProfileManager.selectedProfile.session7.activity_emotion_alecrim;

            proverbio.text = ProfileManager.selectedProfile.session7.proverb_written_answer;

            reviewLike.text = ProfileManager.selectedProfile.session7.opinion_session;
            reviewDifficulty.text = ProfileManager.selectedProfile.session7.opinion_difficulty;
        }
        else
        {
            sessionProperties.Timer.text = "";
            sessionProperties.Date.text = "Não foi concluida";
        }
    }
}
