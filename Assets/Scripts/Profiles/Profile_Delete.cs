﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Profile_Delete : MonoBehaviour {

    [Header("Scene Elements")]
    public Profile_List profileList;
    [Header("Components")]
    public Button button_yes;
    public Button button_no;
    private Profile profileToDelete;

    public void Initialize(Profile profileToDelete)
    {
        this.profileToDelete = profileToDelete;
        button_yes.onClick.AddListener(DeleteProfile);
        button_no.onClick.AddListener(Cancel);
    }

    private void OnDisable()
    {
        button_yes.onClick.RemoveAllListeners();
        button_no.onClick.RemoveAllListeners();
    }

    private void DeleteProfile ()
    {
        Debug.Log("Delete Profile");
        ProfileManager.profileList.Remove(profileToDelete);
        ProfileManager.Save();
        profileToDelete = null;
        profileList.ClearGrid();
        transform.gameObject.SetActive(false);               
    }

    private void Cancel ()
    {
        profileToDelete = null;
        transform.gameObject.SetActive(false);
    }
}
