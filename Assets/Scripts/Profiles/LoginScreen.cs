﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Text;

public class LoginScreen : MonoBehaviour
{
    public InputField usernameInputfield;
    public InputField passwordInputfield;
    public GameObject panel_profileList;
    public Text errorMessage;
    private readonly string urlAuthentication = "https://mind-gait.esenfc.pt/oauth/token";
    private string username;
    private string password;

    public void ButtonLogin()
    {
        username = usernameInputfield.text.Trim();
        password = passwordInputfield.text.Trim();
        StartCoroutine("Authenticate");
    }

    IEnumerator Authenticate()
    {
        string JsonArraystring = "{\"username\":\"" + username + "\", \"password\": \"" + password + "\", \"grant_type\": \"password\", \"client_id\":2, \"client_secret\": \"qtI2vaiCm2O5qlqY0VG0Xk38hbkwbbHEXJkogV0E\"}";

        using (UnityWebRequest www = new UnityWebRequest(urlAuthentication, "POST"))
        {
            byte[] bodyRaw = Encoding.UTF8.GetBytes(JsonArraystring);
            www.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
            www.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
            www.SetRequestHeader("Content-Type", "application/json");
            www.SetRequestHeader("Accept", "application/json");
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                if (Application.internetReachability == NetworkReachability.NotReachable)
                {
                    if (errorMessage)
                    {
                        errorMessage.text = "Erro: Sem conexão à internet";
                    }
                }
                else if (Application.internetReachability != NetworkReachability.NotReachable)
                {
                    if (errorMessage)
                    {
                        errorMessage.text = "Erro: Utilizador ou palavra passe errados!";
                    }
                }
            }
            else
            {
                Debug.Log("Form upload complete: Sucessfull Login!\n" + www.downloadHandler.text);

                string svResponse = www.downloadHandler.text;

                if (!string.IsNullOrEmpty(svResponse))
                {
                    //ProfileManager.Load();
                    ProfileManager.acessToken = svResponse.Split(',')[2].Split(':')[1].Replace('"', ' ').Trim(); ;
                    Debug.Log(ProfileManager.acessToken);
                    //ProfileManager.Save();
                }

                StartCoroutine(ProfileManager.GetRequest());
            }
        }
    }
}
