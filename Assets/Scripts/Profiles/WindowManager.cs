﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class WindowManager : MonoBehaviour {

    [Header("Scene Elements - Panels")]
    public GameObject panel_LoginScreen;
    public GameObject panel_SelectProfile;
    public GameObject panel_CreateProfile;
    public GameObject panel_EditProfile;
    public GameObject panel_PersonalProfile;
    public GameObject panel_SelectSession;
    public GameObject panel_Results;
    [Header("Mouse Manager")]
    public List<RaycastResult> raycastResults;

    private void Awake()
    {
        if (ProfileManager.returnToSessionSelection == true)
        {
            panel_LoginScreen.SetActive(false);
            panel_SelectProfile.SetActive(false);
            panel_CreateProfile.SetActive(false);
            panel_PersonalProfile.SetActive(false);
            panel_SelectSession.SetActive(true);
            panel_Results.SetActive(false);
            ProfileManager.returnToSessionSelection = false;
        }
        else
        {
            panel_SelectProfile.SetActive(false);
            panel_CreateProfile.SetActive(false);
            panel_PersonalProfile.SetActive(false);
            panel_SelectSession.SetActive(false);
            panel_Results.SetActive(false);
            panel_LoginScreen.SetActive(true);
        }

}

    // Use this for initialization
    void Start () {
		
	}

    private void Update()
    {
        MouseManage();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (panel_CreateProfile.activeInHierarchy)
            {
                panel_SelectProfile.SetActive(true);
                panel_CreateProfile.SetActive(false);
            }
            if (panel_PersonalProfile.activeInHierarchy)
            {
                panel_SelectProfile.SetActive(true);
                panel_PersonalProfile.SetActive(false);
            }
        }
    }

    private void MouseManage ()
    {
        if (Input.GetMouseButton(0))
        {
            PointerEventData pointer = new PointerEventData(EventSystem.current);
            pointer.position = Input.mousePosition;

            raycastResults = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pointer, raycastResults);

            if (raycastResults.Count > 0)
            {
                for (int i = 0; i < raycastResults.Count; ++i)
                {
                    //Debug.Log(raycastResults[i].gameObject.name + i, raycastResults[i].gameObject);
                }
            }
        }
    }



}
