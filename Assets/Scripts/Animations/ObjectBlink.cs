﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ObjectBlink : MonoBehaviour
{
    public float speed = 0.01f;

    private Image[] imageArray;
    private Text[] textArray;

    private float alpha = 1;
    public float alphaToAdd = 0.04f;
    private float currentAlphaToAdd;

    public int maxLoops = 2;
    private int loopCount = -1;

    public bool animPlaying;

    // Start is called before the first frame update
    void Start()
    {
        InitializeComponentArrays();
        currentAlphaToAdd = alphaToAdd;
        //InvokeRepeating("AnimateObject", speed, speed);
        //Invoke("AnimateObject", 2);
    }

    public void AnimateObject ()
    {
        InvokeRepeating("Animation", speed, speed);
        animPlaying = true;
    }

    public void Animation()
    {
        Debug.Log("Animation - Blink");

        foreach (var item in imageArray)
        {
            item.color = new Color(item.color.r, item.color.g, item.color.b, alpha);
            Debug.Log(item.color.ToString());
        }
        foreach (var item in textArray)
        {
            item.color = new Color(item.color.r, item.color.g, item.color.b, alpha);
            Debug.Log(item.color.ToString());
        }

        if (alpha <= 0)
        {
            alpha = 0;
            Debug.Log("Alpha == 0");
            currentAlphaToAdd = alphaToAdd;
        }
        else if (alpha >= 1)
        {
            if (loopCount >= 0)
            {
                alpha = 1;
            }
            Debug.Log("Alpha == 1");
            currentAlphaToAdd = alphaToAdd * -1;
            ++loopCount;
            if (loopCount >= maxLoops)
            {
                alpha = 1;
                loopCount = -1;
                CancelInvoke("Animation");
                animPlaying = false;
                return;
            }
        }

        alpha += currentAlphaToAdd;
    }

    private void InitializeComponentArrays ()
    {
        imageArray = GetComponentsInChildren<Image>();
        textArray = GetComponentsInChildren<Text>();
    }
}
