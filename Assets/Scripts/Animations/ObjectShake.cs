﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectShake  : MonoBehaviour {

    private GameObject objectToShake;
    public bool isShaking;
    public float shakeDuration = 1f;
    public float shakeAmmount = 10f;
    public float decreaseFactor = 1f;

    private Vector3 originalPos;

    public void ShakeObject (GameObject objectToShake, float shakeDuration = 1, float shakeAmmount = 5, float decreaseFactor = 1)
    {
        this.objectToShake = objectToShake;
        this.shakeDuration = shakeDuration;
        this.shakeAmmount = shakeAmmount;
        this.decreaseFactor = decreaseFactor;
        this.originalPos = objectToShake.transform.localPosition;
        isShaking = true;
    }

    private void Update()
    {
        if (objectToShake)
        { 
            if (shakeDuration > 0)
            {
                objectToShake.transform.localPosition = (Vector2)originalPos + Random.insideUnitCircle * shakeAmmount;

                shakeDuration -= Time.deltaTime * decreaseFactor;
            }
            else
            {
                shakeDuration = 0;
                objectToShake.transform.localPosition = originalPos;
                isShaking = false;
                this.enabled = false;
            }
        }
    }
}
