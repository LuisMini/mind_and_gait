﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.EventSystems;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class EmoteIcons : MonoBehaviour
{
    [Header("Components")]
    public Image image;
    public Text textComp;
    public ChooseEmotion parentScript;

    [Header("Values")]
    public Color defaultImageColor = Color.white;
    public Color selectedImageColor = Color.cyan;
    [SerializeField]
    public ENUM_Emotions emotion;

    private void Start()
    {
        UpdateButton();
    }

    private void OnMouseUpAsButton()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            image.color = selectedImageColor;
            parentScript.SelectButton(this, emotion);
        }
    }

    public void ResetButton()
    {
        image.color = defaultImageColor;
    }

    public void UpdateButton ()
    {
        image.color = defaultImageColor;
        textComp.text = emotion.ToDescription();
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(EmoteIcons))]
public class EmoteIconEditor : Editor
{
    public override void OnInspectorGUI()
    {
        EmoteIcons script = target as EmoteIcons;

        serializedObject.Update();

        EditorGUILayout.LabelField("Button Components", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("image"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("textComp"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("parentScript"), true);

        EditorGUILayout.LabelField("Button Properties", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("emotion"), true);

        EditorGUILayout.Space();

        if (GUILayout.Button("Update"))
        {
            script.UpdateButton();
        }

        serializedObject.ApplyModifiedProperties();
    }
}
#endif