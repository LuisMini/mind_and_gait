﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickSong : Game_Base
{
    public Game_PickCorrect gamePickCorrect;
    public bool disableUpdate;
    public List<ENUM_Musics> selectedSongs = new List<ENUM_Musics>();

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        if (!disableUpdate)
        {
            int counter = 0;
            for (int i = 0; i < gamePickCorrect.ImagesList.Length; ++i)
            {
                if (gamePickCorrect.ImagesList[i].ImagePrefab.GetComponent<Choices>().Selected)
                {
                    ++counter;
                }
            }
            if (counter == 3)
            {
                hasChosenAnswer = true;
                manager.UpdateButtons();
            }
            else
            {
                hasChosenAnswer = false;
                manager.UpdateButtons();
            }
        }
    }

    public override void CheckAnswers()
    {
        selectedSongs.Clear();
        for (int i = 0; i < gamePickCorrect.ImagesList.Length; ++i)
        {
            if (gamePickCorrect.ImagesList[i].ImagePrefab.GetComponent<Choices>().Selected)
            {
                selectedSongs.Add((ENUM_Musics)i);
            }
        }
        base.CheckAnswers();
        base.CheckVictory();
        gamePickCorrect.CheckVictory();
        disableUpdate = true;
        manager.UpdateButtons();

    }
}
