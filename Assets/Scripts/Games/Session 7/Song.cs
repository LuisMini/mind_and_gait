﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.Serialization;
using System;

public class Song : Game_Base
{
    public InputField[] inputFields;// = new List<InputField>();
    public List<InputFieldOptions> inputFieldsToComplete = new List<InputFieldOptions>();

    [Serializable]
    public struct InputFieldOptions
    {
        public InputField inputField;
        public string value;
    }

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
        inputFields = GetComponentsInChildren<InputField>();
    }

    public override void LowerDifficulty()
    {
        foreach (var item in inputFieldsToComplete)
        {
            item.inputField.text = item.value;
            item.inputField.interactable = false;
        }
        base.LowerDifficulty();
    }

    public void ValidateInputFields()
    {
        bool allFieldsComplete = true;
        foreach (var item in inputFields)
        {
            if(item.text == "")
            {
                allFieldsComplete = false;
                break;
            }
        }
        if (allFieldsComplete)
        {
            hasChosenAnswer = true;
            manager.CurrentGame.hasChosenAnswer = true;
        }
        else
        {
            hasChosenAnswer = false;
            manager.CurrentGame.hasChosenAnswer = false;
        }
        manager.UpdateButtons();
    }
}
