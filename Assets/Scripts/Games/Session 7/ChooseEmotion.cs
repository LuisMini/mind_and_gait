﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using UnityEngine;
using System;

public class ChooseEmotion : Game_Base
{
    public ENUM_Emotions selectedEmotion;

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
    }

    public void SelectButton(EmoteIcons emotionButton, ENUM_Emotions emotion)
    {
        selectedEmotion = emotion;

        //desactivar o resto dos butões
        foreach (Transform child in transform)
        {
            EmoteIcons button = child.GetComponentInChildren<EmoteIcons>();
            if (button && selectedEmotion != button.emotion)
            {
                button.ResetButton();
            }
        }
        base.CheckVictory();
    }
}

public enum ENUM_Emotions
{
    [Description("Alegria")]
    Alegria,
    [Description("Tristeza")]
    Tristeza,
    [Description("Surpresa")]
    Surpresa,
    [Description("Aborrecimento")]
    Aborrecimento,
    [Description("Encanto")]
    Encanto,
    [Description("Fúria")]
    Fúria,
};
