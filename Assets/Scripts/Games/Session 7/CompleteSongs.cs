﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompleteSongs : Game_Base
{
    public PickSong pickSongActivity;
    public List<ENUM_Musics> selectedSongs = new List<ENUM_Musics>();
    public int songToOpen;
    public Song currentSong;
    public GameObject viseu;
    public GameObject alecrim;
    public GameObject machadinha;
    public GameObject comboio;
    public GameObject menina;
    public GameObject oliveira;
    public GameObject rosaSaia;

    private void Awake()
    {
        firstTry = false;
        canRetry = false;
    }

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
        selectedSongs = pickSongActivity.selectedSongs;
        OpenSong(selectedSongs[songToOpen]);
        manager.ChangeActivityTitle(true, manager.session7_activity.ToDescription() + "\n" + "\"" + selectedSongs[songToOpen].ToDescription() + "\"");
    }

    private void OpenSong (ENUM_Musics musics)
    {
        switch (musics)
        {
            case ENUM_Musics.ACaminhoDeViseu:
                {
                    viseu.SetActive(true);
                    currentSong = viseu.GetComponent<Song>();
                    return;
                }
            case ENUM_Musics.Alecrim:
                {
                    alecrim.SetActive(true);
                    currentSong = alecrim.GetComponent<Song>();
                    return;
                }
            case ENUM_Musics.AMachadinha:
                {
                    machadinha.SetActive(true);
                    currentSong = machadinha.GetComponent<Song>();
                    return;
                }
            case ENUM_Musics.LaVaiOComboio:
                {
                    comboio.SetActive(true);
                    currentSong = comboio.GetComponent<Song>();
                    return;
                }
            case ENUM_Musics.MeninaEstasAJanela:
                {
                    menina.SetActive(true);
                    currentSong = menina.GetComponent<Song>();
                    return;
                }
            case ENUM_Musics.OliveiraDaSerra:
                {
                    oliveira.SetActive(true);
                    currentSong = oliveira.GetComponent<Song>();
                    return;
                }
            case ENUM_Musics.ORosaArredondaASaia:
                {
                    rosaSaia.SetActive(true);
                    currentSong = rosaSaia.GetComponent<Song>();
                    return;
                }
        }
    }

    public override void LowerDifficulty()
    {
        base.LowerDifficulty();
        currentSong.LowerDifficulty();
    }

    public override void CheckAnswers()
    {
        foreach (var item in currentSong.inputFields)
        {
            item.interactable = false;
        }
        CheckVictory();
    }

    public override void CheckVictory()
    {
        base.CheckVictory();
    }
}
