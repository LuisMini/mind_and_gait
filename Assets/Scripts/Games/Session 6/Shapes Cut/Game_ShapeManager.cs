﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Game_ShapeManager : Game_Base
{
    public Game_ChooseEmblem chooseEmblemScript;
    [Header("Overlays")]
    public float overlayDelay = 20f;
    public List<ShapeManager> shapeManagers = new List<ShapeManager>();
    [Header("Emblems Containers")]
    public GameObject emblem1;
    public GameObject emblem2;
    public GameObject emblem3;
    public GameObject emblem4;

    [Header("Results")]
    private ENUM_Emblema chosenEmblem;
    private GameObject chosenEmblemGameobject;

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
        chosenEmblem = chooseEmblemScript.chosenEmblem;

        switch (chosenEmblem)
        {
            case ENUM_Emblema.Emblem_1:
                {
                    chosenEmblemGameobject = emblem1;
                    shapeManagers = new List<ShapeManager>(emblem1.GetComponentsInChildren<ShapeManager>());
                    break;
                }
            case ENUM_Emblema.Emblem_2:
                {
                    chosenEmblemGameobject = emblem2;
                    shapeManagers = new List<ShapeManager>(emblem2.GetComponentsInChildren<ShapeManager>());
                    break;
                }
            case ENUM_Emblema.Emblem_3:
                {
                    chosenEmblemGameobject = emblem3;
                    shapeManagers = new List<ShapeManager>(emblem3.GetComponentsInChildren<ShapeManager>());
                    break;
                }
            case ENUM_Emblema.Emblem_4:
                {
                    shapeManagers = new List<ShapeManager>(emblem4.GetComponentsInChildren<ShapeManager>());
                    chosenEmblemGameobject = emblem4;
                    break;
                }
        }
        chosenEmblemGameobject.SetActive(true);
        chosenEmblemGameobject.GetComponent<Image>().enabled = true;
        manager.ChangeActivityTitle(true, "Observe com atenção a figura");
        stopwatch = new System.Diagnostics.Stopwatch();

        foreach (var item in shapeManagers)
        {
            if (item.completed)
            {
                item.PopulateShape();
            }
        }

        Invoke("PopulateShape", overlayDelay);
    }

    public void PopulateShape ()
    {
        manager.ChangeActivityTitle(true, EnumsHelperExtension.ToDescription(manager.session6_activity));
        //chosenEmblemGameobject.GetComponent<Image>().enabled = false;
        stopwatch.Start();
        foreach (var item in shapeManagers)
        {
            if (item.enabled)
            {
                item.PopulateShape();
            }
            else
            {
                item.completed = true;
            }
        }
    }

    public void VerifiyShapes ()
    {
        int counter = 0;
        Debug.Log(counter);
        foreach (var shapeManager in shapeManagers)
        {
            if (shapeManager.completed)
            {
                ++counter;
                Debug.Log(counter);
            }
            else
            {
                return;
            }
            if (counter == shapeManagers.Count)
            {
                CheckVictory();
            }
        }
    }

    public override void CheckVictory()
    {
        //foreach (var item in shapeManagers)
        //{
        //    item.gameObject.SetActive(false);
        //}
        //chosenEmblemGameobject.GetComponent<Image>().enabled = true;
        hasChosenAnswer = true;
        stopwatch.Stop();
        base.CheckVictory();
    }
}
