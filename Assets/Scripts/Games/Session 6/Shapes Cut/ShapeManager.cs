﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BansheeGz.BGSpline.Components;
using BansheeGz.BGSpline.Curve;

public class ShapeManager : MonoBehaviour
{
    [Header("Game Manager")]
    public Game_ShapeManager gameManager;

    [Header("Curves")]
    public List<GameObject> curveGameobjects = new List<GameObject>();
    private BGCcMath math;

    [Header("Overlays")]
    public bool ignoreCorners;
    public GameObject prefabOverlay;
    public Vector3 prefabScale = new Vector3(1, 1, 1);
    public List<Image> overlays = new List<Image>();
    public int colliderScale = 1;

    [Header("Results")]
    public bool completed = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    public void PopulateShape()
    {
        foreach (var curveGameObject in curveGameobjects)
        {
            math = curveGameObject.GetComponent<BGCcMath>();

            var sections = math.Math.SectionInfos; //array com as sections todas
            var sectionsCount = sections.Count; //numero de sections

            //for loop para percorrer as sections
            for (int i = 0; i < sectionsCount; ++i)
            {
                var currentSection = sections[i]; //section actual
                //int currentSectionInt = i;

                var points = currentSection.Points; //section's array of points
                var pointsCount = points.Count; //numero de points da section actual

                int pointsToCreate;
                if (ignoreCorners)
                {
                    pointsToCreate = pointsCount - 1;
                }
                else
                {
                    pointsToCreate = pointsCount;
                }

                //for loop para percorrer os points
                for (int j = 1; j < pointsToCreate; ++j)
                {
                    BGCurveBaseMath.SectionPointInfo currentPoint = points[j];
                    Vector3 currentPointPos = currentPoint.Position;

                    Vector3 position = new Vector3(currentPointPos.x, currentPointPos.y, -1);
                    Quaternion rotation = new Quaternion(0, 0, 0, 0);

                    GameObject newOverlay = Instantiate(prefabOverlay, position, rotation, curveGameObject.transform);
                    newOverlay.transform.localScale = prefabScale;
                    newOverlay.GetComponent<ShapeCut>().shapeManager = this;
                    if (newOverlay.GetComponent<CircleCollider2D>() != null)
                    {
                        newOverlay.GetComponent<CircleCollider2D>().radius = newOverlay.GetComponent<CircleCollider2D>().radius * colliderScale;
                    }
                    else if (newOverlay.GetComponent<BoxCollider2D>() != null)
                    {
                        newOverlay.GetComponent<BoxCollider2D>().size = newOverlay.GetComponent<BoxCollider2D>().size * colliderScale;
                    }
                    overlays.Add(newOverlay.GetComponent<Image>());
                }
            }
        }
    }

    public void CheckOverlays ()
    {
        if (!completed)
        {
            foreach (var item in overlays)
            {
                if (item.enabled)
                {
                    //algum está enabled, faz return nao vale a pena ver o resto
                    completed = false;
                    return;
                }
            }
            completed = true;
        }
        gameManager.VerifiyShapes();
    }
}