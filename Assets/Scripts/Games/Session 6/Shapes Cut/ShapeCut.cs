﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShapeCut : MonoBehaviour
{
    public ShapeManager shapeManager;
    private bool canDelete;
    private Image image;

    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();
        if (shapeManager.completed)
        {
            canDelete = false;
        }
        else
        {
            canDelete = true;
        }
    }

    private void OnMouseOver()
    {
        Debug.Log("OnMouseOver");
        if (canDelete)
        {
            image.enabled = false;
            shapeManager.CheckOverlays();
        }
    }
}
