﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_CustomizeEmblem : Game_Base
{
    public Game_ChooseEmblem gameChooseEmblem;
    public Emblem emblem1;
    public Emblem emblem2;
    public Emblem emblem3;
    public Emblem emblem4;
    public Emblem activeEmblem;
    [Header("Patterns")]
    public GameObject listPatterns;
    public List<Drag> patterns = new List<Drag>();
    public bool patternBeingDragged = false;

    private void OnEnable()
    {
        firstTry = false;
        canRetry = false;
        gameCompleted = false;
    }

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
        stopwatch = new System.Diagnostics.Stopwatch();
        listPatterns.GetComponent<GridLayout>().enabled = false;
        patterns = new List<Drag>(listPatterns.GetComponentsInChildren<Drag>());

        switch (gameChooseEmblem.chosenEmblem)
        {
            case ENUM_Emblema.Emblem_1:
                {
                    emblem1.transform.gameObject.SetActive(true);
                    activeEmblem = emblem1;
                    break;
                }
            case ENUM_Emblema.Emblem_2:
                {
                    emblem2.transform.gameObject.SetActive(true);
                    activeEmblem = emblem2;
                    break;
                }
            case ENUM_Emblema.Emblem_3:
                {
                    emblem3.transform.gameObject.SetActive(true);
                    activeEmblem = emblem3;
                    break;
                }
            case ENUM_Emblema.Emblem_4:
                {
                    emblem4.transform.gameObject.SetActive(true);
                    activeEmblem = emblem4;
                    break;
                }
        }
    }

    // Update is called once per frame
    void Update()
    {
        patternBeingDragged = false;
        foreach (var item in patterns)
        {
            if (item.Dragging)
            {
                patternBeingDragged = true;
                break;
            }
        }
    }

    public override void TriggerAction(GameObject interactiveObject, string action)
    {
        if (action == "Pattern")
        {
            int counter = 0;
            foreach (var emblemPiece in activeEmblem.emblemPieces)
            {
                if (emblemPiece.image.sprite != emblemPiece.defaultPattern)
                {
                    ++counter;
                }
                else
                {
                    return;
                }
            }
            if (counter == activeEmblem.emblemPieces.Count)
            {
                hasChosenAnswer = true;
            }
            else
            {
                hasChosenAnswer = false;
            }
            manager.UpdateButtons();
        }
    }

    public override void CheckAnswers()
    {
        foreach (var dragScript in patterns)
        {
            dragScript.canDrag = false;
        }
        foreach (EmblemPiece item in activeEmblem.emblemPieces)
        {
            if (!item.correctAnswer)
            {
                ++wrongAnswers;
            }
        }
        if (wrongAnswers > 0)
        {
            canRetry = true;
        }
        stopwatch.Stop();
        base.CheckVictory();
    }

    public override void LowerDifficulty()
    {
        listPatterns.GetComponent<GridLayout>().enabled = false;

        if (difficulty == ENUM_Difficulty.Alta && gameCompleted == false)
        {
            List<GameObject> wrongAnswersList = new List<GameObject>();

            foreach (var dragScript in patterns)
            {
                if (!dragScript.correctAnswer)
                {
                    wrongAnswersList.Add(dragScript.gameObject);
                }
            }

            int ammountToDelete = 2;
            List<GameObject> objectsToDelete = new List<GameObject>();

            for (int i = 1; i <= ammountToDelete; i++)
            {
                int index = UnityEngine.Random.Range(0, wrongAnswersList.Count);
                objectsToDelete.Add(wrongAnswersList[index]);
                wrongAnswersList.RemoveAt(index);
            }

            foreach (GameObject element in objectsToDelete)
            {
                element.SetActive(false);
            }
            base.LowerDifficulty();
        }
    }

    public void ResetOtherPieces(EmblemPiece notToReset)
    {
        foreach (var item in activeEmblem.emblemPieces)
        {
            if (item != notToReset)
            {
                item.ResetImage();
            }
        }
    }

    public override void RestartGame()
    {
        base.RestartGame();
        foreach (var dragScript in patterns)
        {
            dragScript.canDrag = true;
        }
        foreach (EmblemPiece piece in activeEmblem.emblemPieces)
        {
            piece.RestartImage();
        }
        hasChosenAnswer = false;
        gameCompleted = false;
        firstTry = false;
        canRetry = false;
        difficulty = ENUM_Difficulty.Alta;
        manager.UpdateButtons();
    }
}