﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EmblemPiece : MonoBehaviour
{
    //[HideInInspector]
    public Image image;
    public Game_CustomizeEmblem gameManager;
    public Sprite defaultPattern;
    public Sprite currentPattern;
    [HideInInspector]
    public ENUM_Patterns enumCurrentPattern;
    [HideInInspector]
    public bool correctAnswer;

    // Start is called before the first frame update
    void Start()
    {
        if (!image)
        {
            image = transform.GetChild(0).GetComponentInChildren<Image>();
        }
        if (defaultPattern)
        {
            image.sprite = defaultPattern;
        }
        ImageType();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("On trigger enter2D");
        image.sprite = collision.gameObject.GetComponent<Image>().sprite;
        gameManager.ResetOtherPieces(this);
        ImageType();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Debug.Log("On trigger exit2D");
        if (!gameManager.patternBeingDragged)
        {
            Debug.Log("Held");
            currentPattern = collision.gameObject.GetComponent<Image>().sprite;
            image.sprite = currentPattern;
            ImageType();
            enumCurrentPattern = collision.gameObject.GetComponent<PatternInfo>().patternNumber;
            correctAnswer = collision.gameObject.GetComponent<PatternInfo>().correct;
            gameManager.TriggerAction(this.gameObject, "Pattern");
            //gameManager.CheckVictory();
        }
        else
        {
            Debug.Log("Not held");
            image.sprite = currentPattern;
            ImageType();
        }
    }

    public void ResetImage ()
    {
        image.sprite = currentPattern;
        ImageType();
    }

    public void RestartImage ()
    {
        image.sprite = defaultPattern;
        currentPattern = defaultPattern;
        correctAnswer = false;
        enumCurrentPattern = ENUM_Patterns.Pattern_1;
        ImageType();
    }

    public void ImageType()
    {
        if (image.sprite == defaultPattern)
        {
            image.type = Image.Type.Simple;
        }
        else
        {
            image.type = Image.Type.Tiled;
        }
    }
}
