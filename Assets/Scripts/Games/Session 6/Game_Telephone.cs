﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Game_Telephone : Game_Base
{
    public List<Button> keypad = new List<Button>();
    [Header("Instructions")]
    public GameObject instructions;
    public float timeInstructions;
    [Header("Message")]
    private bool canOpenMessage = true;
    public GameObject message;
    public GameObject redLight;
    public SoundButton buttonSound;
    [Header("Sounds")]
    public AudioClip messageDeleted;
    public AudioClip messageSaved;
    public AudioClip errorSound;
    //[Header("Results")]
    private bool button1Pressed = false;
    private bool button3Pressed = false;
    private bool deletedMessage = false;
    private int vezesClicou1;
    private int vezesClicou3;
    private int vezesClicou5;

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
        chosenAnswersDescription = "Não";
    }

    public void EnableInstructions ()
    {
        instructions.SetActive(true);
        Invoke("DisableInstructions", timeInstructions);
    }

    private void DisableInstructions ()
    {
        instructions.SetActive(false);
        foreach (Button key in keypad)
        {
            key.interactable = true;
        }
    }

    public void Button1Listen()
    {
        if (canOpenMessage)
        {
            message.SetActive(true);
            buttonSound.ButtonPress();
            ++vezesClicou1;
            button1Pressed = true;
            redLight.GetComponent<Image>().enabled = false;
            redLight.GetComponent<Animation>().enabled = false;
        }
        else //mensagem ja apagada
        {
            buttonSound.SoundManager.PlaySound(errorSound, buttonSound, 1);
            ++wrongAnswers;
        }
    }

    public void Button3Save()
    {
        if (button1Pressed && canOpenMessage)
        {
            ++vezesClicou3;
            buttonSound.SoundManager.PlaySound(messageSaved, buttonSound, 1);
            button3Pressed = true;
            base.CheckVictory();
        }
        else //mensagem ja foi apagada ou ainda nao a ouviu
        {
            buttonSound.SoundManager.PlaySound(errorSound, buttonSound, 1);
            ++wrongAnswers;
        }
    }

    public void Button5Delete()
    {
        if (button3Pressed && !deletedMessage)
        {
            ++vezesClicou5;
            deletedMessage = true;
            chosenAnswersDescription = "Sim";
            canOpenMessage = false;
            message.SetActive(false);
            buttonSound.SoundManager.PlaySound(messageDeleted, buttonSound, 1);
        }
        else //mensagem ja foi apagada, ou ainda não foi guardada
        {
            buttonSound.SoundManager.PlaySound(errorSound, buttonSound, 1);
            ++wrongAnswers;
        }
    }

    public void ButtonWrong()
    {
        buttonSound.SoundManager.PlaySound(errorSound, buttonSound, 1);
        ++wrongAnswers;
    }

    public void CloseMessage()
    {
        buttonSound.SoundManager.StopPlayingAll();
        message.gameObject.SetActive(false);
    }
}
