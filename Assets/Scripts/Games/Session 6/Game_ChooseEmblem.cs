﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_ChooseEmblem : Game_Base
{
    public Game_PickCorrect gamePickCorrect;
    public bool disableUpdate;
    public ENUM_Emblema chosenEmblem;
 
    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        if (!disableUpdate)
        {
            int counter = 0;
            for (int i = 0; i < gamePickCorrect.ImagesList.Length; ++i)
            {
                if (gamePickCorrect.ImagesList[i].ImagePrefab.GetComponent<Choices>().Selected)
                {
                    ++counter;
                }
            }
            if (counter == 1)
            {
                hasChosenAnswer = true;
                manager.UpdateButtons();
            }
            else
            {
                hasChosenAnswer = false;
                manager.UpdateButtons();
            }
        }
    }

    public override void CheckAnswers()
    {
        Choices selectedChoice = null;

        for (int i = 0; i < gamePickCorrect.ImagesList.Length; ++i) //for para percorrer as imagens
        {
            if (gamePickCorrect.ImagesList[i].ImagePrefab.GetComponent<Choices>().Selected) //se a imagem estiver selecionada
            {
                selectedChoice = gamePickCorrect.ImagesList[i].ImagePrefab.GetComponent<Choices>();
                gamePickCorrect.CheckVictory();
                break;
            }
        }

        for (int j = 0; j <= 3; ++j) //for para percorrer os valores do enum
        {
            if (((ENUM_Emblema)j).ToDescription() == selectedChoice.description) //se a descriçao da imagem selecionada for igual á entry do enum
            {
                chosenEmblem = (ENUM_Emblema)j;
            }
        }

        disableUpdate = true;
        Debug.Log("LALALALALa");
        CheckVictory();
    }
}
