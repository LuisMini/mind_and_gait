﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour {

    private float distance = 100;
    [SerializeField]
    private bool canRotate = false;
    private Camera cam;

    // Use this for initialization
    void Start () {
        cam = Camera.main;
	}
	
	// Update is called once per frame
	void Update () {
        RotateObject();
	}

    private void OnMouseDown()
    {
        Debug.Log("Mouse Down");
        canRotate = true;
    }

    private void OnMouseUp()
    {
        Debug.Log("Mouse Up");
        if (canRotate == true)
        {
            canRotate = false;
        }
    }

    void RotateObject ()
    {
        if (canRotate == true)
        {
            //int previousAngle = 0;
            Vector3 mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
            Vector3 mouseWorldPos = cam.ScreenToWorldPoint(mousePos); //target the object will look at (mouse world position)

            //Vector3 aimingDir = mouseWorldPos - transform.position;
            float angle = -Mathf.Atan2(mouseWorldPos.x - transform.position.x, mouseWorldPos.y - transform.position.y) * Mathf.Rad2Deg; ; //angle for the object to look at the target
            angle = Mathf.Round(angle / 30.0f) * 30.0f; //angle snap to 30ºs
            transform.rotation = Quaternion.Euler(0, 0, angle);
        }
    }
}
