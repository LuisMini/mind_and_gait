﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Game_Associar : Game_Base {

    [System.Serializable]
    public class Pairs
    {
        public Sprite Image;
        public bool difficultyDisable;
        public GameObject pair;
        [HideInInspector]
        public bool discovered;
    }

    public Pairs[] pairsList; //array that contains the images to be shown on screen and which are correct or incorrect
    public Pairs currentPair;
    public float timeImageChange = 2f;
    public int currentPairID;

    [Header("Components")]
    public Image imageCycle;

    // Use this for initialization
    new void Start () {
        base.Start();
        InvokeRepeating("ChangePair", timeImageChange, timeImageChange);
    }	

    private void ChangePair ()
    { 
        if (difficulty == ENUM_Difficulty.Alta) //dificuldade normal
        {        
            if (currentPairID == pairsList.Length - 1)
            {
                currentPairID = 0;
            }
            else
            {
                currentPairID += 1;
            }            
            currentPair = pairsList[currentPairID];
            imageCycle.sprite = currentPair.Image;
        }
        else //baixa dificuldade
        {
            if (currentPairID == pairsList.Length - 1)
            {
                currentPairID = 0;
            }
            else
            {
                currentPairID += 1;
            }

            if (pairsList[currentPairID].difficultyDisable)
            {
                ChangePair();
            }

            currentPair = pairsList[currentPairID];
            imageCycle.sprite = currentPair.Image;
        }
    }

    public void CheckPair (GameObject pair)
    {
        AssociarPar pairScript = pair.GetComponent<AssociarPar>();
        if (pair == currentPair.pair && pairScript.canClick) //par correto
        {
            Debug.Log("Game_Associar: Resposta Correta");
            pairsList[currentPairID].discovered = true;
            pairScript.selectionBorder.enabled = true;
            pairScript.canClick = false;
            CheckVictory();
        }
        else if (!pairScript.shakeScript.isShaking && pairScript.canClick) //par incorrecto
        {
            Debug.Log("Game_Associar: Resposta Errada");
            pairScript.shakeScript.enabled = true;
            pairScript.shakeScript.ShakeObject(pairScript.gameObject);
            ++wrongAnswers;
        }
    }

    public override void LowerDifficulty()
    {
        GridLayoutGroup gridComp = gameObject.GetComponentInChildren<GridLayoutGroup>();
        gridComp.enabled = false;

        foreach (var item in pairsList)
        {
            if (item.difficultyDisable) {
                item.pair.SetActive(false);
            }
        }
        base.LowerDifficulty();
    }

    public override void CheckVictory()
    {
        //ver quantas respostas existem
        int counterTotal = 0;
        foreach (var item in pairsList)
        {
            if (item.pair.activeInHierarchy)
            {
                ++counterTotal;
            }
        }
        //ver quantas respostas ja estao correctas
        int counterDiscovered = 0;
        foreach (var item in pairsList)
        {
            if (item.discovered && item.pair.activeInHierarchy)
            {
                ++counterDiscovered;
            }
        }
        //comparar as respostas possiveis com as correctas
        if (counterDiscovered == counterTotal)
        {
            Debug.Log("Game_Associar: Jogo completo!");
            base.CheckVictory();
            imageCycle.gameObject.SetActive(false);
        }        
    }
}
