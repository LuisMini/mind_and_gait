﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_KitchenUtensils : Game_Base {

    public Game_PickCorrect hardmode;
    public Game_PickCorrect easymode;
    [HideInInspector]
    public Game_PickCorrect currentMode;

    // Use this for initialization
    new void Start () {
        base.Start();
        hardmode.gameObject.SetActive(true);
        currentMode = hardmode;
        easymode.gameObject.SetActive(false);
    }

    private void Update()
    {
        hasChosenAnswer = currentMode.hasChosenAnswer;

        if (difficulty == ENUM_Difficulty.Baixa)
        {
            if (easymode.GameCompleted)
            {
                base.CheckVictory();
            }
        }
        else
        {
            if (hardmode.GameCompleted)
            {
                base.CheckVictory();
            }
        }
    }

    public override void RestartGame()
    {
        base.RestartGame();
        firstTry = false;
        canRetry = false;
        hasChosenAnswer = false;
        difficulty = ENUM_Difficulty.Alta;
        gameCompleted = false;
        currentMode.RestartGame();
    }

    public override void LowerDifficulty()
    {
        hardmode.gameObject.SetActive(false);
        easymode.gameObject.SetActive(true);
        currentMode = easymode;
        base.LowerDifficulty();
    }

    public override void CheckAnswers()
    {
        currentMode.CheckAnswers();
        Debug.Log(currentMode.wrongAnswers + "   " + currentMode.unselectedCorrectAnswers);
        if ((currentMode == hardmode && currentMode.wrongAnswers > 0) || (currentMode == hardmode && currentMode.unselectedCorrectAnswers > 0)) //se foram dadas respostas erradas, ou não selecionadas respostas correctas
        {
            canRetry = true; //dizer que pode tentar de novo
        }
        manager.UpdateButtons();
    }
}
