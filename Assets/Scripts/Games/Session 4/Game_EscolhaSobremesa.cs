﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_EscolhaSobremesa : Game_Base {

    [HideInInspector]
    public ENUM_Sobremesa sobremesa;
    [HideInInspector]
    public Sobremesa chosenDessert;
    public Sobremesa boloLaranja;
    public Sobremesa arrozDoce;

    // Use this for initialization
    new void Start () {
        base.Start();
        firstTry = false;
        manager.UpdateButtons();
	}	

    public void SelectDessert (Sobremesa dessert)
    {
        if (chosenDessert)
        {
            chosenDessert.selectionBorder.enabled = false;
        }
        chosenDessert = dessert;
        sobremesa = dessert.sobremesa;
        hasChosenAnswer = true;
        manager.UpdateButtons();
    }

    public override void LowerDifficulty()
    {
        arrozDoce.gameObject.SetActive(false);
        base.LowerDifficulty();
    }

    public override void CheckAnswers()
    {
        base.CheckVictory();
        manager.UpdateButtons();
    }
}
