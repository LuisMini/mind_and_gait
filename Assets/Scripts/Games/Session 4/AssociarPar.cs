﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class AssociarPar : MonoBehaviour {

    private Game_Associar gameAssociar;
    [HideInInspector]
    public Image selectionBorder;
    [HideInInspector]
    public bool canClick = true;
    [HideInInspector]
    public ObjectShake shakeScript;

	// Use this for initialization
	void Start () {
        gameAssociar = GetComponentInParent<Game_Associar>();
        selectionBorder = transform.gameObject.FindComponentInChildWithTag<Image>("SelectionBorder");
        shakeScript = GetComponent<ObjectShake>();
    }

    private void OnEnable()
    {
        gameAssociar = GetComponentInParent<Game_Associar>();
        selectionBorder = transform.gameObject.FindComponentInChildWithTag<Image>("SelectionBorder");
        shakeScript = GetComponent<ObjectShake>();
    }

    // Update is called once per frame
    void Update () {
		
	}

    private void OnMouseDown()
    {
        if (!EventSystem.current.IsPointerOverGameObject()) // if no UI elements are getting the hit/hover
        {
            if (canClick)
            {
                gameAssociar.CheckPair(this.gameObject);
            }
        }   
    }

}
