﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Sobremesa : MonoBehaviour {

    public Game_EscolhaSobremesa gameScript;
    [HideInInspector]
    public Image selectionBorder;
    public ENUM_Sobremesa sobremesa;

    // Use this for initialization
    void Start () {
        gameScript = GetComponentInParent<Game_EscolhaSobremesa>();
        selectionBorder = transform.gameObject.FindComponentInChildWithTag<Image>("SelectionBorder");
    }

    private void OnEnable()
    {
        gameScript = GetComponentInParent<Game_EscolhaSobremesa>();
        selectionBorder = transform.gameObject.FindComponentInChildWithTag<Image>("SelectionBorder");
    }

    private void OnMouseDown()
    {
        if (!EventSystem.current.IsPointerOverGameObject() && gameScript) // if no UI elements are getting the hit/hover
        {
            gameScript.SelectDessert(this);
            selectionBorder.enabled = true;
        }
    }
}
