﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Game_Base : MonoBehaviour {

    protected ENUM_Difficulty difficulty; //represents the current game's CURRENT difficulty level
    public bool canChangeDifficulty = true; //tells if the game's difficulty CAN BE lowered
    [HideInInspector]
    public bool hasChosenAnswer = false;
    [HideInInspector]
    public bool firstTry = true;
    public int numRetrys;
    [HideInInspector]
    public int retrysUsed;
    [HideInInspector]
    public bool canRetry;
    protected bool gameCompleted;
    protected Manager manager;

    [Header("Game Results")]
    [SerializeField]
    public System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
    [SerializeField]
    public int correctAnswers;
    [SerializeField]
    public int wrongAnswers;
    [SerializeField]
    public int unselectedCorrectAnswers;
    [SerializeField]
    public int numOpenedInvitation;
    [SerializeField]
    public string chosenAnswersDescription;

    public bool GameCompleted
    {
        get
        {
            return gameCompleted;
        }

        set
        {
            gameCompleted = value;
        }
    }
    public ENUM_Difficulty Difficulty
    {
        get
        {
            return difficulty;
        }

        set
        {
            difficulty = value;
        }
    }

    public void Start()
    {
        manager = GameObject.FindGameObjectWithTag("Manager").GetComponent<Manager>();
    }

    public virtual void RestartGame() 
    {
        ++retrysUsed;
        correctAnswers = 0;
        wrongAnswers = 0;
        unselectedCorrectAnswers = 0;
    }

    /// <summary>
    /// Called when the user presses the "Terminei" Button.
    /// For this button to appear the variable hasChosenAnswer must be set to true
    /// </summary>
    public virtual void CheckAnswers () {}

    /// <summary>
    /// Called when the game has been completed, 
    /// tells the game that it has been completed and updates the manager buttons
    /// </summary>
    public virtual void CheckVictory() {
        GameCompleted = true;
        if (stopwatch.IsRunning)
        {
            stopwatch.Stop();
        }
        manager.UpdateButtons();
    }
    public virtual void LowerDifficulty() {
        difficulty = ENUM_Difficulty.Baixa;
        manager.UpdateButtons();
    }
    public virtual void TriggerAction(GameObject interactiveObject, string action = "none") { }
}
