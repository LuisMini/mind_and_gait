﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class NovoOrçamento : Game_Base
{
    [Header("Conta")]
    public Text orçamento;
    public InputField inputField;
    public Image resultImg;
    public Sprite correctIMG;
    public Sprite incorrectIMG;
    [Header("Results")]
    public string writtenAnswer;

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
        if (ProfileManager.selectedProfile != null)
        {
            orçamento.text = ProfileManager.selectedProfile.budget_chosen.ToString();
        }
    }

    public void ValidateInputField ()
    {
        if (inputField.text.Trim() != "")
        {
            hasChosenAnswer = true;
        }
        else
        {
            hasChosenAnswer = false;
        }
        manager.UpdateButtons();
    }

    public override void CheckAnswers()
    {
        inputField.interactable = false;
        writtenAnswer = inputField.text;
        canChangeDifficulty = false;
        base.CheckVictory();
        GameObject orcamentoUI = GameObject.FindGameObjectWithTag("Header_Orçamento"); //para atualizar o orçamento no header / title

        if (ProfileManager.selectedProfile != null)
        {
            ProfileManager.selectedProfile.budget_updated = ProfileManager.selectedProfile.budget_chosen - 7.25f;

            if (inputField.text.Replace('€',' ').Trim() == ProfileManager.selectedProfile.budget_updated.ToString())
            {
                resultImg.sprite = correctIMG;
            }
            else
            {
                resultImg.sprite = incorrectIMG;
            }
            resultImg.enabled = true;

            if (orcamentoUI)
            {
                orcamentoUI.GetComponentInChildren<Text>().text = ProfileManager.selectedProfile.budget_updated.ToString() + "€";
            }
        }
        else
        {
            if (inputField.text.Replace('€', ' ').Trim() == "9,75")
            {
                resultImg.sprite = correctIMG;
            }
            else
            {
                resultImg.sprite = incorrectIMG;
            }
            resultImg.enabled = true;

            if (orcamentoUI)
            {
                orcamentoUI.GetComponentInChildren<Text>().text = "9.75€";
            }
        }

        
    }
}
