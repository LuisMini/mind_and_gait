﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ListaIngredientes : Game_Base
{
    public Text titleRecipe;
    public Game_PickCorrect gamePickCorrectBoloLaranja;
    public Game_PickCorrect gamePickCorrectArrozDoce;
    public Game_PickCorrect currentGame;
    private bool playingGamePickCorrect = true;
    public GameObject legenda;

    // Start is called before the first frame update
    new void Start()
    {
        gamePickCorrectBoloLaranja.gameObject.SetActive(false);
        gamePickCorrectArrozDoce.gameObject.SetActive(false);

        base.Start();
        if (ProfileManager.selectedProfile != null)
        {
            switch (ProfileManager.selectedProfile.dessert)
            {
                case ENUM_Sobremesa.ArrozDoce:
                    {
                        gamePickCorrectArrozDoce.gameObject.SetActive(true);
                        currentGame = gamePickCorrectArrozDoce;
                        return;
                    }
                case ENUM_Sobremesa.BoloDeLaranja:
                    {
                        gamePickCorrectBoloLaranja.gameObject.SetActive(true);
                        currentGame = gamePickCorrectBoloLaranja;
                        return;
                    }
            }
        }
        else
        {
            gamePickCorrectArrozDoce.gameObject.SetActive(true);
            currentGame = gamePickCorrectArrozDoce;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (playingGamePickCorrect)
        {
            hasChosenAnswer = currentGame.hasChosenAnswer;
            manager.UpdateButtons();
        }
    }

    public override void CheckAnswers()
    {
        legenda.SetActive(true);
        if (playingGamePickCorrect)
        {
            currentGame.CheckAnswers();

            foreach (var item in currentGame.ImagesList)
            {
                if (item.correct /* && item.ImagePrefab.GetComponent<Choices>().Selected */)
                {
                    //Debug.Log(item.ImagePrefab.name + "/Checkmark");
                    GameObject checkmark = GameObject.Find(item.ImagePrefab.name + "/Checkmark");
                    checkmark.GetComponent<Image>().enabled = true;
                }
                item.ImagePrefab.FindComponentInChildWithTag<Image>("SelectionBorder").enabled = false;
            }
            CheckVictory();
            playingGamePickCorrect = false;
        }
    }

    public override void CheckVictory()
    {
        base.CheckVictory();
    }

    public override void LowerDifficulty()
    {
        base.LowerDifficulty();
        //currentGame.LowerDifficulty();
    }
}
