﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;

public class ReceitaInterativa : Game_Base
{
    [Header("Containers")]
    public GameObject boloLaranja;
    public GameObject arrozDoce;

    [Header("Draggables")]
    public List<GameObject> draggables = new List<GameObject>();
    public int numEasyDraggables;

    [Header("Objectives")]
    public List<GameObject> objectives = new List<GameObject>();

    [Header("Game Results")]
    public int correctPosCounter;
    public int incorrectPosCounter;

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
        stopwatch = new System.Diagnostics.Stopwatch();
        if (ProfileManager.selectedProfile != null)
        {
            switch (ProfileManager.selectedProfile.dessert)
            {
                case ENUM_Sobremesa.BoloDeLaranja:
                    {
                        GameObject draggablesContainer = boloLaranja.transform.Find("Draggables").gameObject;
                        GameObject objectivesContainer = boloLaranja.transform.Find("Objectives").gameObject;

                        draggablesContainer.SetActive(true);
                        objectivesContainer.SetActive(true);

                        foreach (Transform child in draggablesContainer.transform)
                        {
                            draggables.Add(child.gameObject);
                        }
                        foreach (Transform child in objectivesContainer.transform)
                        {
                            objectives.Add(child.gameObject);
                        }
                        break;
                    }
                case ENUM_Sobremesa.ArrozDoce:
                    {
                        GameObject draggablesContainer = arrozDoce.transform.Find("Draggables").gameObject;
                        GameObject objectivesContainer = arrozDoce.transform.Find("Objectives").gameObject;

                        draggablesContainer.SetActive(true);
                        objectivesContainer.SetActive(true);

                        foreach (Transform child in draggablesContainer.transform)
                        {
                            draggables.Add(child.gameObject);
                        }
                        foreach (Transform child in objectivesContainer.transform)
                        {
                            objectives.Add(child.gameObject);
                        }
                        break;
                    }
            }
        }
        else
        {
            GameObject draggablesContainer = boloLaranja.transform.Find("Draggables").gameObject;
            GameObject objectivesContainer = boloLaranja.transform.Find("Objectives").gameObject;

            draggablesContainer.SetActive(true);
            objectivesContainer.SetActive(true);

            foreach (Transform child in draggablesContainer.transform)
            {
                draggables.Add(child.gameObject);
            }
            foreach (Transform child in objectivesContainer.transform)
            {
                objectives.Add(child.gameObject);
            }
        }
    }

    public override void TriggerAction(GameObject interactiveObject, string action)
    {
        if (action == "CheckSentences")
        {
            Debug.Log("TRIGGER ACTION - CHECKSENTENCES");
            correctPosCounter = 0;
            incorrectPosCounter = 0;
            foreach (GameObject sentence in draggables)
            {
                if (sentence.GetComponent<Drag>().IsInCorrectPosition)
                {
                    ++correctPosCounter;
                }
                else if (sentence.GetComponent<Drag>().isInIncorrectPosition)
                {
                    ++incorrectPosCounter;
                }

                if ((correctPosCounter + incorrectPosCounter) == draggables.Count)
                {
                    Debug.Log("Receita Interativa: Tudo numa Posição");
                    hasChosenAnswer = true;
                }
                else
                {
                    hasChosenAnswer = false;
                }
                manager.UpdateButtons();
            }
        }
    }

    public override void RestartGame()
    {
        base.RestartGame();
        Debug.Log("Restart Game");
        foreach (var item in draggables)
        {
            item.GetComponent<Drag>().ResetItem();
        }
        foreach (var slot in objectives)
        {
            slot.GetComponentInChildren<Image>().enabled = true;
        }
        correctPosCounter = 0;
        incorrectPosCounter = 0;
        firstTry = false;
        canRetry = false;
        hasChosenAnswer = false;
        difficulty = ENUM_Difficulty.Alta;
        gameCompleted = false;
        manager.UpdateButtons();
    }

    public override void LowerDifficulty()
    {
        base.LowerDifficulty();
        List<GameObject> easyDraggables = new List<GameObject>();
        int easyDraggablesAmmount = draggables.Count - numEasyDraggables;

        while (easyDraggables.Count < easyDraggablesAmmount)
        {
            int randomIndex = UnityEngine.Random.Range(0, draggables.Count);
            if (!easyDraggables.Contains(draggables[randomIndex]))
            {
                easyDraggables.Add(draggables[randomIndex]);
            }
        }

        foreach (var item in easyDraggables)
        {
            Drag dragComp = item.GetComponent<Drag>();
            item.transform.position = dragComp.correctObjective.transform.position;
            dragComp.canDrag = false;
        }
    }

    public override void CheckAnswers()
    {
        foreach (GameObject draggableObject in draggables)
        {
            draggableObject.GetComponent<Drag>().canDrag = false;
        }
        foreach (var slot in objectives)
        {
            slot.GetComponentInChildren<Image>().enabled = false;
        }
        correctAnswers = correctPosCounter;
        wrongAnswers = incorrectPosCounter;
        stopwatch.Stop();
        if (incorrectPosCounter > 0)
        {
            if (wrongAnswers > 0 || unselectedCorrectAnswers > 0) //se foram dadas respostas erradas, ou não selecionadas respostas correctas
            {
                canRetry = true; //dizer que pode tentar de novo
            }
        }
        base.CheckVictory();
        manager.UpdateButtons();
    }
}
