﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OrdenarQuantias : Game_Base
{

    [Header("Values")]
    public GameObject[] draggables;

    [Header("Conta")]
    public GameObject[] objectives;
    public InputField inputField;
    public string resultado;
    public Image resultImg;
    public Sprite correctIMG;
    public Sprite incorrectIMG;

    [Header("Game Results")]
    public string writtenAnswer;

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
        stopwatch = new System.Diagnostics.Stopwatch();
    }

    private void OnEnable()
    {
        inputField.interactable = false;
    }

    public override void TriggerAction(GameObject interactiveObject, string action)
    {
        if (action == "CheckSentences")
        {
            Debug.Log("TRIGGER ACTION - CHECKSENTENCES");
            int counter = 0;
            foreach (GameObject sentence in draggables)
            {
                if (sentence.GetComponent<Drag>().IsInCorrectPosition)
                {
                    ++counter;
                    Debug.Log(counter);
                    if (counter == 3) //se os tres slots estiverem ocupados o jogo pode terminar
                    {
                        Debug.Log("Counter == 3");
                        inputField.interactable = true;
                        foreach (GameObject draggableObject in draggables)
                        {
                            draggableObject.GetComponent<Drag>().canDrag = false;
                        }
                        foreach (var slot in objectives)
                        {
                            slot.GetComponentInChildren<Image>().enabled = false;
                        }
                    }
                    else
                    {
                        inputField.interactable = false;
                    }
                }
            }
        }
    }

    public void ValidateInputField()
    {
        if (inputField.text.Trim() != "")
        {
            hasChosenAnswer = true;
        }
        else
        {
            hasChosenAnswer = false;
        }
        manager.UpdateButtons();
    }

    public override void CheckAnswers()
    {
        inputField.interactable = false;
        writtenAnswer = inputField.text;

        if (inputField.text == resultado || inputField.text == resultado + "€")
        {
            resultImg.sprite = correctIMG;
        }
        else
        {
            resultImg.sprite = incorrectIMG;
        }
        resultImg.enabled = true;

        canChangeDifficulty = false;
        stopwatch.Stop();
        base.CheckVictory();
    }
}
