﻿using System.Collections;
using System.Collections.Generic;
using BansheeGz.BGSpline.Components;
using BansheeGz.BGSpline.Curve;
using UnityEngine.UI;
using UnityEngine;

public class Game_Map : Game_Base {

    [System.Serializable]
    public class Trajectories
    {
        public GameObject curveGameobject;
        public bool correct;
        public Color32 color;  
    }

    public List<Trajectories> trajectories = new List<Trajectories>();
    private Trajectories correctTrajectory;
    private GameObject correctPath_Gameobject;
    private BGCcCursorChangeLinear cursorLerp;
    private BGCcCursor cursor;

    [Header("Color Button")]
    private GameObject button;
    private Image buttonIMG;
    private int currentColorID;
    private ObjectShake shakeScript;
    public float colorChangeDelay = 3.5f;

    // Use this for initialization
    new void Start () {
        base.Start();

        foreach (var item in trajectories)
        {
            if (item.correct)
            {
                correctTrajectory = item;
                correctPath_Gameobject = item.curveGameobject;
                cursorLerp = correctPath_Gameobject.GetComponent<BGCcCursorChangeLinear>();
                cursor = correctPath_Gameobject.GetComponent<BGCcCursor>();
            }
        }
        if (correctPath_Gameobject == null)
        {
            Debug.LogWarning("Game_Map: No correct path found");
        }

        button = this.transform.GetComponentInChildren<Button>().transform.gameObject;
        buttonIMG = button.GetComponent<Image>();
        shakeScript = this.gameObject.AddComponent<ObjectShake>();
        InvokeRepeating("ColorChange", 0f, colorChangeDelay);
    }

    private void OnEnable()
    {
        firstTry = false;
        canRetry = false;
    }

    private void Update()
    {
        if (cursor.DistanceRatio == 1)
        {
            base.CheckVictory();
        }
    }

    private void ColorChange ()
    {
        if (difficulty == ENUM_Difficulty.Alta)
        {
            currentColorID += 1;
            if (currentColorID > trajectories.Count - 1)
            {
                currentColorID = 0;
            }
            buttonIMG.color = trajectories[currentColorID].color;
        }
        else
        {
            currentColorID += 1;
            if (currentColorID > trajectories.Count - (1+2) )
            {
                currentColorID = 0;
            }
            buttonIMG.color = trajectories[currentColorID].color;
        }
        
    }

    public override void LowerDifficulty()
    {
        base.LowerDifficulty();
        trajectories[2].curveGameobject.SetActive(false);
        trajectories[3].curveGameobject.SetActive(false);
    }

    public override void CheckVictory ()
    {
        if (buttonIMG.color == correctTrajectory.color)
        {
            canChangeDifficulty = false;  //não deixar baixar dificuldade depois de carregar no correcto      
            manager.UpdateButtons();
            foreach (var item in trajectories) //esconder outros paths
            {
                if (!item.correct)
                {
                    item.curveGameobject.SetActive(false);
                }
            }            
            cursorLerp.Speed = 1; //começar o trajecto
            CancelInvoke("ColorChange");
            button.GetComponentInChildren<Button>().interactable = false;
        }
        else
        {
            shakeScript.enabled = true;
            shakeScript.ShakeObject(button);
            ++wrongAnswers;
        }
    }
}
