﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Proverbio_T3_Order : Proverbio {

    public Game_Order gameOrder;
    private bool updateDisable;
    private bool playingGameOrder = true;
    [Header("Components")]
    public Text sayingTextComp;
    [Header("Saying Properties")]
    public string sayingIncomplete;
    public AudioClip s_sayingIncomplete;
    public string sayingComplete;
    public AudioClip s_sayingComplete;

    new void Start()
    {
        base.Start();
        firstTry = false; //para poder ativar o butao ajuda
        manager.UpdateButtons();
    }

	// Update is called once per frame
	void Update () {
        if (!updateDisable)
        {
            if (gameOrder.GameCompleted)
            {
                playingGameOrder = false;
                hasChosenAnswer = true; //para poder continuar para escrever o proverbio
                canChangeDifficulty = false; //para nao poder alterar a dificuldade depois de acabar de ordenar
                manager.UpdateButtons();
                updateDisable = true;
            }
        }
	}

    private new void OnEnable()
    {
        base.OnEnable();
        gameOrder = GetComponentInChildren<Game_Order>();
    }

    public override void LowerDifficulty()
    {
        gameOrder.LowerDifficulty();
        base.LowerDifficulty();
    }

    public override void CheckAnswers()
    {
        if (!playingGameOrder && !inputField.gameObject.activeInHierarchy)
        {
            gameOrder.gameObject.SetActive(false);
            inputField.gameObject.SetActive(true);
            hasChosenAnswer = false;
            GameCompleted = false;
            manager.UpdateButtons();
        }
        else
        {
            writtenAnswer = inputField.text;
            inputField.interactable = false;
            sayingTextComp.text = sayingComplete;
            soundButton.soundClip = s_sayingComplete;
            base.CheckVictory();
        }
    }

}
