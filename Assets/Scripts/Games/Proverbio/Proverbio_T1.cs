﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Proverbio_T1 : Proverbio
{
    public SayingProperties[] proverbio = new SayingProperties[3];
    private Text texto;
    public int proverbioID;

    // Use this for initialization
    new void Start()
    {
        base.Start();
        texto = this.GetComponentInChildren<Text>();
        DisplaySaying(0);
    }

    private new void OnEnable()
    {
        base.OnEnable();
    }

    private void OnDisable()
    {
        soundManager.StopPlayingAll();
    }

    public override void LowerDifficulty()
    {
        base.LowerDifficulty();
        DisplaySaying(1);
    }

    public void DisplaySaying(int ID)
    {
        proverbioID = ID;
        soundManager.StopPlayingAll();
        texto.text = proverbio[ID].sayingText;
        soundButton.soundClip = proverbio[ID].sayingSoundClip;
    }

    public override void CheckAnswers()
    {
        inputField.interactable = false;
        //guardar a resposta escrita
        writtenAnswer = inputField.text;
        //mostrar proverbio completo
        DisplaySaying(proverbio.Length - 1);
        //nao deixar baixar a dificuldade
        canChangeDifficulty = false;
        //meter game completed a true aka chamar check victory
        base.CheckVictory();
        }
    }
