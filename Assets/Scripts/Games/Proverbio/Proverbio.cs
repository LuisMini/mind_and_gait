﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class SayingProperties
{
    public string sayingText;
    public AudioClip sayingSoundClip;
}

public class Proverbio : Game_Base
{
    [Header("Components")]
    protected SoundManager soundManager;
    [SerializeField]
    protected SoundButton soundButton;
    public InputField inputField;
    [Header("Game Results")]
    public string writtenAnswer;

    // Use this for initialization
    new void Start()
    {
        base.Start();
    }

    protected void OnEnable()
    {
        //button = GetComponentInChildren<SoundButton>();
        soundManager = (SoundManager)FindObjectOfType(typeof(SoundManager));
    }

    private void OnDisable()
    {
        soundManager.StopPlayingAll();
    }

    public override void LowerDifficulty()
    {
        base.LowerDifficulty();
    }

    public override void CheckVictory()
    {
        base.CheckVictory();
    }

    /// <summary>
    /// Validates the input field and sets hasChosenAnswer to true so the user can complete this task
    /// </summary>
    public void ValidateInputField()
    {
        Debug.Log("Validate Input Field");
        if (inputField.text.Trim() != "")
        {
            Debug.Log("has Chosen Answer = true");
            hasChosenAnswer = true;
        }
        else
        {
            Debug.Log("has Chosen Answer = false");
            hasChosenAnswer = false;
        }
        manager.UpdateButtons();
    }
}
