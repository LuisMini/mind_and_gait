﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Proverbio_T2 : Proverbio {

    [Header("Proverbio T2")]
    public Game_PickCorrect gamePickCorrect;
    private bool playingGamePickCorrect = true;
    public GameObject choicesContainer;
    private bool updateActive = true;
    [Header("Components")]
    public Text sayingTextComp;
    [Header("Saying Properties")]
    public string sayingIncomplete;
    public AudioClip s_sayingIncomplete;
    public string sayingComplete;
    public AudioClip s_sayingComplete;
    public GameObject sayingIncompleteGO; //isto é preciso para o da Sessão 2, pq tem imagens ent temos de dar toggle ao gameobject
    public GameObject sayingCompleteGO; //isto é preciso para o da Sessão 2, pq tem imagens ent temos de dar toggle ao gameobject

    new void Start()
    {
        base.Start();

        if (soundButton)
        {
            soundButton.soundClip = s_sayingIncomplete;
        }
    }

    // Update is called once per frame
    void Update () {
        if (updateActive)
        {
            hasChosenAnswer = gamePickCorrect.hasChosenAnswer;
            manager.UpdateButtons();

            if (gamePickCorrect.GameCompleted)
            {
                playingGamePickCorrect = false;
                updateActive = false;

                manager.Button_Finished.GetComponentInChildren<Text>().text = "Seguinte";
            }
        }
	}

    private new void OnEnable()
    {
        base.OnEnable();
        gamePickCorrect = GetComponentInChildren<Game_PickCorrect>();
    }

    public override void CheckAnswers()
    { 
        if (playingGamePickCorrect)
        {
            gamePickCorrect.CheckAnswers();
        }
        else if (!playingGamePickCorrect && !inputField.gameObject.activeInHierarchy)
        {
            choicesContainer.SetActive(false);
            inputField.gameObject.SetActive(true);
            hasChosenAnswer = false;
            manager.UpdateButtons();
        }
        else
        {
            writtenAnswer = inputField.text;
            inputField.interactable = false;
            if (sayingTextComp)
            {
                sayingTextComp.text = sayingComplete;
                soundButton.soundClip = s_sayingComplete;
            }
            else
            {
                sayingIncompleteGO.SetActive(false);
                sayingCompleteGO.SetActive(true);
                soundButton.soundClip = s_sayingComplete;
            }

            base.CheckVictory();
        }
    }
}
