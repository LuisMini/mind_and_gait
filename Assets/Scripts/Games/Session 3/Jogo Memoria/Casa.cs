﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;
#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteInEditMode]
public class Casa : MonoBehaviour
{
    [SerializeField]
    public Sprite image;
    public Casa pair;
    private bool selected;
    private JogoMemoria jogoMemoria;
    [Header("Components")]
    public Image imageComp;
    public Animator animator;

    // Use this for initialization
    void Start()
    {
        UpdateImage();
    }

    private void OnValidate()
    {
        UpdateImage();
    }

    public void UpdateImage ()
    {
        imageComp.sprite = image;
        animator = transform.GetComponent<Animator>();
        jogoMemoria = GetComponentInParent<JogoMemoria>();
    }

    public void ResetPairs() //chamada quando a animação de esconder a carta acaba "HIDE"
    {
        Debug.Log("ESCONDE CARTA");
        selected = false;
        jogoMemoria.ResetEscolhas();
    } 

    public void CasaSelected () //chamada quando a animação de revelar a carta acaba "SHOW"
    {
        Debug.Log("CASA SELECIONADA");
        if (jogoMemoria.segundaEscolha == this)
        {
            if (jogoMemoria.primeiraEscolha && jogoMemoria.segundaEscolha)
            {
                ++jogoMemoria.totalPairsFlipped;

                if (jogoMemoria.primeiraEscolha.pair == jogoMemoria.segundaEscolha)
                {
                    Debug.Log("PAR ENCONTRADO YAAAAAY");
                    StartCoroutine(CorrectPair());
                    if (image == jogoMemoria.imagemDoisEuros)
                    {
                        jogoMemoria.conta4Euros.SetActive(true);
                    }
                    else if (image == jogoMemoria.imagemCincoEuros)
                    {
                        jogoMemoria.conta10Euros.SetActive(true);
                    }
                    else if (image == jogoMemoria.imagemDezEuros)
                    {
                        jogoMemoria.conta20Euros.SetActive(true);
                    }
                }
                else
                {
                    Debug.Log("Par nao foi encontrado");
                    StartCoroutine(WrongPair());
                }
            }
        }
    }

    private IEnumerator CorrectPair ()
    {
        yield return new WaitForSecondsRealtime(jogoMemoria.delayDesativarCarta);
        Debug.Log("DESATIVAR CARTA");
        GetComponentInParent<GridLayoutGroup>().enabled = false;
        jogoMemoria.ResetEscolhas();
        jogoMemoria.PairFound();
    }

    private IEnumerator WrongPair ()
    {
        yield return new WaitForSecondsRealtime(jogoMemoria.delayEsconderCarta);
        jogoMemoria.primeiraEscolha.animator.SetTrigger("Hide");
        jogoMemoria.segundaEscolha.animator.SetTrigger("Hide");
        jogoMemoria.wrongAnswers++;
    }

    private void OnMouseDown()
    {
        if (!EventSystem.current.IsPointerOverGameObject()) // if no UI elements are getting the hit/hover
        {
            //Debug.Log("WARNING: A UI ELEMENT IS IN THE WAY");        
            if (jogoMemoria.canClick && !selected)
            {
                if (jogoMemoria.primeiraEscolha == null) //ainda nao existe nenhuma carta escolhida
                {
                    Debug.Log("Nao existe escolha nenhuma ainda");
                    jogoMemoria.primeiraEscolha = this;
                    selected = true;
                    animator.SetTrigger("Show");                    
                }
                else if (jogoMemoria.segundaEscolha == null) //existe primeira carta escolhida mas nao existe segunda
                {
                    Debug.Log("Existe primeira escolha mas nao existe segunda");
                    jogoMemoria.segundaEscolha = this;
                    selected = true;
                    jogoMemoria.canClick = false;
                    animator.SetTrigger("Show");
                }
                else if (jogoMemoria.primeiraEscolha != null && jogoMemoria.segundaEscolha != null) //ja existem duas cartas escolhidas
                {
                    Debug.Log("WARNING: Ja existem duas escolhas");
                }
            }
        }
    }
}
