﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class JogoMemoria : Game_Base
{
    [System.Serializable]
    public class PairInfo
    {
        public Sprite Image;
        public bool difficulty_Disable;
    }
    [System.Serializable]
    public struct Conta
    {
        public InputField inputField;
        public Image resultIMG;
    }

    [Header("Game Results")]
    public int totalPairsFlipped;

    [Header("Game Properties")]
    public List<PairInfo> pares = new List<PairInfo>();
    public List<Casa> casas = new List<Casa>();
    public float delayDesativarCarta = 1;
    public float delayEsconderCarta = 0.25f;

    [Header("Game Helpers")]
    public Casa primeiraEscolha;
    public Casa segundaEscolha;
    public bool canClick = true;
    public int paresRemaining; //meter private

    [Header("Prefab")]
    public GameObject prefabCasa;

    [Header("Pares Euros")]
    public Sprite imagemDoisEuros;
    public Sprite imagemCincoEuros;
    public Sprite imagemDezEuros;
    public GameObject conta4Euros;
    public GameObject conta10Euros;
    public GameObject conta20Euros;

    [Header("Contas")]
    public Conta soma4euros;
    public Conta soma10euros;
    public Conta soma20euros;
    public Sprite correctIMG;
    public Sprite incorrectIMG;

    // Use this for initialization
    new void Start()
    {
        base.Start();
        paresRemaining = pares.Count;
    }

    public void UpdateSlots()
    {
        casas.Clear();
        foreach (Transform child in transform) //ver quantas casas estao instanciadas
        {
            casas.Add(child.GetComponent<Casa>());
        }

        if (casas.Count < pares.Count * 2) //se as casas nao estiverem todas instanciadas
        {
            int objectosParaInstanciar = ((pares.Count * 2) - casas.Count);
            for (int i = 0; i < objectosParaInstanciar; ++i)
            {
                //instanciar os que falta
                GameObject casa = Instantiate(prefabCasa, transform.position, new Quaternion(0, 180, 0, 0), transform); //instantiates the prefab as child of this component's transform
                casas.Add(casa.GetComponent<Casa>());
            }
        }
    }

    public void DeleteSlots()
    {
        int children = transform.childCount;
        for (int i = children - 1; i >= 0; --i)
        {
            DestroyImmediate(transform.GetChild(i).gameObject);
        }
        casas.Clear();
    }

    public void ShuffleSlots()
    {
        Debug.Log("Shuffle");
        foreach (var casa in casas) //clear the pairs that were already set
        {
            Debug.Log("foreach (var casa in casas) //clear the pairs that were already set");
            casa.pair = null;
        }

        List<Casa> casasSemPar = new List<Casa>(casas);

        foreach (var par in pares)
        {
            Debug.Log("1");
            Casa primeiroElemento = null;
            Casa segundoElemento = null;
            for (int c = 0; c < 2; ++c) //loop de 2 para criar o par
            {
                int randomIndex = Random.Range(0, casasSemPar.Count); //random index
                Debug.Log("2 - RNG:" + randomIndex);
                casasSemPar[randomIndex].image = par.Image; //set da imagem ao elemento do par
                casasSemPar[randomIndex].UpdateImage();

                if (c == 0)
                {
                    primeiroElemento = casasSemPar[randomIndex]; //dá set como sendo o primeiro elemento
                }
                else
                {
                    segundoElemento = casasSemPar[randomIndex]; //dá set como sendo o segundo elemento
                    primeiroElemento.pair = segundoElemento; //e guardam referencias um ao outro
                    segundoElemento.pair = primeiroElemento; // ^
                }

                casasSemPar.RemoveAt(randomIndex); //remover o elemento do par da lista de casas sem par
            }
        }
        Canvas.ForceUpdateCanvases();
    }

    public void ResetEscolhas()
    {
        primeiraEscolha = null;
        segundaEscolha = null;
        canClick = true;
    }

    public void PairFound()
    {
        --paresRemaining;
        canChangeDifficulty = false;
        ValidateInputFields();
        correctAnswers++;
    }

    public void ValidateInputFields()
    {
        //se todos os pares foram descobertos e todos os input fields preenchidos activa o butão terminei
        if (soma4euros.inputField.text != "" && soma10euros.inputField.text != "" && soma20euros.inputField.text != "" && paresRemaining <= 0)
        {
            //pode carregar no terminei
            hasChosenAnswer = true;
            manager.UpdateButtons();
        }
        else
        {
            //não pode carregar no terminei
            hasChosenAnswer = false;
            manager.UpdateButtons();
        }
    }

    public override void CheckAnswers()
    {
        soma4euros.inputField.interactable = false;
        soma4euros.inputField.GetComponent<Image>().enabled = false;
        soma10euros.inputField.interactable = false;
        soma10euros.inputField.GetComponent<Image>().enabled = false;
        soma20euros.inputField.interactable = false;
        soma20euros.inputField.GetComponent<Image>().enabled = false;

        /*Check 2+2 answer*/
        #region
        string answer = soma4euros.inputField.text;
        if (answer == "4" || answer == "4,0" || answer == "4,00" || answer == "4€" || answer == "4,0€" || answer == "4,00€")
        {
            soma4euros.resultIMG.sprite = correctIMG;
        }
        else
        {
            soma4euros.resultIMG.sprite = incorrectIMG;
        }
        soma4euros.resultIMG.enabled = true;
        #endregion

        /*Check 5+5 answer*/
        #region
        answer = soma10euros.inputField.text;
        if (answer == "10" || answer == "10,0" || answer == "10,00" || answer == "10€" || answer == "10,0€" || answer == "10,00€")
        {
            soma10euros.resultIMG.sprite = correctIMG;
        }
        else
        {
            soma10euros.resultIMG.sprite = incorrectIMG;
        }
        soma10euros.resultIMG.enabled = true;
        #endregion

        /*Check 10+10 answer*/
        #region
        answer = soma20euros.inputField.text;
        if (answer == "20" || answer == "20,0" || answer == "20,00" || answer == "20€" || answer == "20,0€" || answer == "20,00€")
        {
            soma20euros.resultIMG.sprite = correctIMG;
        }
        else
        {
            soma20euros.resultIMG.sprite = incorrectIMG;
        }
        soma20euros.resultIMG.enabled = true;
        #endregion
        base.CheckVictory();
    }

    public override void LowerDifficulty()
    {
        Debug.Log("JOGO MEMORIA - Lower Difficulty");
        GetComponent<GridLayoutGroup>().enabled = false;
        foreach (var item in pares)
        {
            Debug.Log("1");
            if (item.difficulty_Disable)
            {
                Debug.Log("2");
                foreach (var casa in casas)
                {
                    Debug.Log("3");
                    if (casa.image == item.Image) //se a imagem desta casa for igual á imagem do item que pode ser desativado
                    {
                        Debug.Log("4");
                        casa.pair.gameObject.SetActive(false);
                        casa.gameObject.SetActive(false);
                    }
                }
            }
        }
        paresRemaining -= 2;
        base.LowerDifficulty();
        if (paresRemaining <= 0)
        {
            ValidateInputFields();
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(JogoMemoria))]
public class JogoMemoriaEditor : Editor
{
    public override void OnInspectorGUI()
    {
        JogoMemoria script = target as JogoMemoria;

        serializedObject.Update();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("paresRemaining"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("wrongAnswers"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("canChangeDifficulty"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("firstTry"), true);
        EditorGUILayout.Space();

        EditorGUILayout.PropertyField(serializedObject.FindProperty("prefabCasa"), true);
        EditorGUILayout.Space();

        //EditorGUILayout.LabelField("Game Properties", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("pares"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("casas"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("delayDesativarCarta"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("delayEsconderCarta"), true);
        EditorGUILayout.Space();

        EditorGUILayout.PropertyField(serializedObject.FindProperty("imagemDoisEuros"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("conta4Euros"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("imagemCincoEuros"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("conta10Euros"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("imagemDezEuros"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("conta20Euros"), true);
        EditorGUILayout.Space();

        EditorGUILayout.PropertyField(serializedObject.FindProperty("soma4euros"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("soma10euros"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("soma20euros"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("correctIMG"), true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("incorrectIMG"), true);
        EditorGUILayout.Space();

        if (GUILayout.Button("Update Slots"))
        {
            script.UpdateSlots();
        }
        if (GUILayout.Button("Shuffle Slots"))
        {
            script.ShuffleSlots();
        }
        if (GUILayout.Button("Delete Children"))
        {
            script.DeleteSlots();
        }

        serializedObject.ApplyModifiedProperties();


    }
}

#endif