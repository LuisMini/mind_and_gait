﻿using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class EscolhaOrcamento : Game_Base {
    
    [System.Serializable]
    public class ImageProperties
    {
        public GameObject objectChoice;
        public bool correct;
        public bool difficulty_CanDisable;        
    }

    public ImageProperties[] ImagesList; //array that contains the images to be shown on screen and which are correct or incorrect

    public GameObject conta;
    public Text subtracao;
    [Header("Conta")]
    public InputField result;
    public GameObject orçamentoEscolhidoObjeto;
    public float orcamento;
    public string writtenAnswer;
    public Image resultImg;
    public Sprite correctIMG;
    public Sprite incorrectIMG;

    // Use this for initialization
    new void Start () {
        base.Start();
        firstTry = false;
        manager.UpdateButtons();
	}

    private void OnEnable()
    {
        foreach (var item in ImagesList)
        {
            item.objectChoice.GetComponentInChildren<Choices>().correct = item.correct;
        }
    }

    public void EscolherOrcamento (GameObject escolhaObjeto)
    {
        foreach (var item in ImagesList)
        {
            if (item.objectChoice != escolhaObjeto) //remove selection from the other possible choices
            {
                Choices choiceScript = item.objectChoice.GetComponentInChildren<Choices>();
                choiceScript.Selected = false;
                choiceScript.SelectionBorder.enabled = false;
            }
        }

        orçamentoEscolhidoObjeto = escolhaObjeto;
        orcamento = float.Parse(escolhaObjeto.GetComponentInChildren<Text>().text, CultureInfo.InvariantCulture);
        Debug.Log(orcamento);

        subtracao.text = orcamento + " €" + "\n -5 €";

        result.text = "";
        conta.SetActive(true);
    }

    public void ValidateInputFields ()
    {
        if (result.text != "")
        {
            hasChosenAnswer = true;
            writtenAnswer = result.text.Trim();
        }
        else
        {
            hasChosenAnswer = false;
            writtenAnswer = "";
        }
        manager.UpdateButtons();
    }

    public override void CheckAnswers()
    {
        result.interactable = false;
        result.GetComponent<Image>().enabled = false;
        if (ProfileManager.selectedProfile != null)
        {
            ProfileManager.selectedProfile.budget_chosen = orcamento;
            ProfileManager.selectedProfile.budget_updated = orcamento - 5f;
        }

        if (writtenAnswer == (orcamento - 5f).ToString() || writtenAnswer == (orcamento - 5f).ToString() + "€" || writtenAnswer == (orcamento - 5f).ToString() + ",00" || writtenAnswer == (orcamento - 5f).ToString() + ",00€" || writtenAnswer == (orcamento - 5f).ToString() + ",0" || writtenAnswer == (orcamento - 5f).ToString() + ",0€")
        {
            resultImg.sprite = correctIMG;
        }
        else
        {
            resultImg.sprite = incorrectIMG;
        }
        resultImg.enabled = true;

        base.CheckAnswers();
        base.CheckVictory();
    }

    public override void LowerDifficulty()
    {
        VerticalLayoutGroup verticalLayout = gameObject.GetComponentInChildren<VerticalLayoutGroup>();
        verticalLayout.enabled = false;

        if (/*hardMode == true*/difficulty == ENUM_Difficulty.Alta && gameCompleted == false)
        {
            foreach (ImageProperties element in ImagesList)
            {
                if (element.difficulty_CanDisable == true)
                {
                    element.objectChoice.transform.parent.gameObject.SetActive(false);
                }
            }
            base.LowerDifficulty();
        }
    }

}
