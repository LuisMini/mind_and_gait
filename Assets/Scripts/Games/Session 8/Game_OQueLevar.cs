﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game_OQueLevar : Game_Base
{
    public InputField inputfield1;
    public InputField inputfield2;
    public InputField inputfield3;

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
    }

    public void ValidateInputFields ()
    {
        if (inputfield1.text != "" && inputfield2.text != "" && inputfield3.text != "")
        {
            hasChosenAnswer = true;
        }
        else
        {
            hasChosenAnswer = false;
        }
        manager.UpdateButtons();
    }

    public override void CheckAnswers()
    {
        base.CheckAnswers();
        inputfield1.interactable = false;
        inputfield2.interactable = false;
        inputfield3.interactable = false;
        base.CheckVictory();
    }
}
