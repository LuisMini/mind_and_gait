﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GamePriorities : Game_Base
{
    [Header("Draggables")]
    public List<GameObject> draggables = new List<GameObject>();
    public int numEasyDraggables;

    [Header("Objectives")]
    public List<GameObject> objectives = new List<GameObject>();

    [Header("Game Results")]
    public string chosenOrder;
    public int correctPosCounter;
    public int incorrectPosCounter;

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
        stopwatch = new System.Diagnostics.Stopwatch();
    }

    public override void TriggerAction(GameObject interactiveObject, string action)
    {
        if (action == "CheckOrder")
        {
            correctPosCounter = 0;
            incorrectPosCounter = 0;
            foreach (GameObject sentence in draggables)
            {
                if (sentence.GetComponent<Drag>().IsInCorrectPosition)
                {
                    ++correctPosCounter;
                }
                else if (sentence.GetComponent<Drag>().isInIncorrectPosition)
                {
                    ++incorrectPosCounter;
                }

                if ((correctPosCounter + incorrectPosCounter) == draggables.Count)
                {
                    Debug.Log("Game Priorities: Tudo numa Posição");
                    List<GameObject> orderedObjects = new List<GameObject>();
                    orderedObjects = draggables.OrderBy(correctOrder => correctOrder.transform.position.y).ToList();

                    for (int i = 0; i < orderedObjects.Count; ++i)
                    {
                        chosenOrder += orderedObjects[i].GetComponent<Drag>().currentObjective.transform.parent.GetComponentInChildren<Text>().text;
                        if (i != orderedObjects.Count - 1)
                        {
                            chosenOrder += "\n";
                        }

                    }

                    if (!hasChosenAnswer)
                    {
                        hasChosenAnswer = true;
                        manager.UpdateButtons();
                    }
                }
                else
                {
                    if (hasChosenAnswer)
                    {
                        hasChosenAnswer = false;
                        manager.UpdateButtons();
                    }
                }
            }
        }
    }

    public override void CheckAnswers()
    {
        foreach (GameObject draggableObject in draggables)
        {
            draggableObject.GetComponent<Drag>().canDrag = false;
        }
        if (difficulty == ENUM_Difficulty.Baixa)
        {
            correctAnswers = (correctPosCounter - 3);
        }
        else
        {
            correctAnswers = correctPosCounter;
        }
        wrongAnswers = incorrectPosCounter;
        stopwatch.Stop();
        if (incorrectPosCounter > 0)
        {
            if (wrongAnswers > 0 || unselectedCorrectAnswers > 0) //se foram dadas respostas erradas, ou não selecionadas respostas correctas
            {
                canRetry = true; //dizer que pode tentar de novo
            }
        }
        base.CheckVictory();
        manager.UpdateButtons();
    }

    public override void RestartGame()
    {
        base.RestartGame();
        Debug.Log("Restart Game");
        foreach (var item in draggables)
        {
            item.GetComponent<Drag>().ResetItem();
        }
        correctPosCounter = 0;
        incorrectPosCounter = 0;
        chosenOrder = "";
        firstTry = false;
        canRetry = false;
        hasChosenAnswer = false;
        difficulty = ENUM_Difficulty.Alta;
        gameCompleted = false;
        manager.UpdateButtons();
    }

    public override void LowerDifficulty()
    {
        base.LowerDifficulty();
        for (int i = 0; i < draggables.Count; ++i)
        {
            if (i >= 5)
            {
                draggables[i].GetComponent<Drag>().IsInCorrectPosition = true;
                draggables[i].GetComponent<Drag>().correctObjective.transform.parent.gameObject.SetActive(false);
                draggables[i].SetActive(false);
            }
            else if (i == 0)
            {
                draggables[i].GetComponent<Drag>().correctObjective.transform.parent.GetComponentInChildren<Text>().text = "Preparar a roupa";
            }
            else if (i == 1)
            {
                draggables[i].GetComponent<Drag>().correctObjective.transform.parent.GetComponentInChildren<Text>().text = "Despir";
            }
            else if (i == 2)
            {
                draggables[i].GetComponent<Drag>().correctObjective.transform.parent.GetComponentInChildren<Text>().text = "Fazer a higiene pessoal";
            }
            else if (i == 3)
            {
                draggables[i].GetComponent<Drag>().correctObjective.transform.parent.GetComponentInChildren<Text>().text = "Vestir";
            }
            else if (i == 4)
            {
                draggables[i].GetComponent<Drag>().correctObjective.transform.parent.GetComponentInChildren<Text>().text = "Calçar";
            }
        }
    }
}
