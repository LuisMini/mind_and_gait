﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game_Arrived : Game_Base
{
    public Text arrivalTextComp;
    [TextArea]
    public string arrivalText;
    private string SrOuSra;

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
        if (ProfileManager.selectedProfile != null)
        {
            if (ProfileManager.selectedProfile.sex == Sexo.male)
            {
                SrOuSra = "Sr.";
            }
            else
            {
                SrOuSra = "Sra.";
            }
            arrivalTextComp.text = "Óla " + SrOuSra + " " + ProfileManager.selectedProfile.name + ". \n" + arrivalText;
        }
        else
        {
            arrivalTextComp.text = "Óla " + "Sr." + " " + "Sem Perfil Selecionado" + ". \n" + arrivalText;
        }
        Invoke("SetGameCompleted", 5);
    }

    void SetGameCompleted ()
    {
        gameCompleted = true;
        manager.UpdateButtons();
    }
}
