﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game_ChooseClothes : Game_Base
{
    public GameObject boletimMetereologico;
    public GameObject mask;
    public GameObject instructions;
    public Button buttonBoletim;
    public Game_PickCorrect gamePickCorrect;
    private bool disableUpdate;
    public List<string> selectedClothes = new List<string>();

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
        mask.SetActive(false);
        gamePickCorrect.transform.gameObject.SetActive(false);
        buttonBoletim.gameObject.SetActive(false);
        boletimMetereologico.SetActive(true);
        instructions.SetActive(true);
        Invoke("DisplayChoices", 15f);
    }

    // Update is called once per frame
    void Update()
    {
        if (!disableUpdate)
        {
            int counter = 0;
            for (int i = 0; i < gamePickCorrect.ImagesList.Length; ++i)
            {
                if (gamePickCorrect.ImagesList[i].ImagePrefab.GetComponent<Choices>().Selected)
                {
                    ++counter;
                }
            }
            if (counter > 0)
            {
                hasChosenAnswer = true;
                manager.UpdateButtons();
            }
            else
            {
                hasChosenAnswer = false;
                manager.UpdateButtons();
            }
        }
    }

    public override void CheckAnswers()
    {
        base.CheckAnswers();
        disableUpdate = true;
        for (int i = 0; i < gamePickCorrect.ImagesList.Length; ++i)
        {
            if (gamePickCorrect.ImagesList[i].ImagePrefab.GetComponent<Choices>().Selected)
            {
                selectedClothes.Add(gamePickCorrect.ImagesList[i].ImagePrefab.GetComponent<Choices>().description);
            }
        }
        gamePickCorrect.GameCompleted = true;
        base.CheckVictory();
        manager.UpdateButtons();
    }

    public override void LowerDifficulty()
    {
        base.LowerDifficulty();
        gamePickCorrect.LowerDifficulty();
    }

    public void ToggleBoletim()
    {
        if (boletimMetereologico.activeInHierarchy)
        {
            boletimMetereologico.SetActive(false);
            mask.SetActive(false);
        }
        else
        {
            boletimMetereologico.SetActive(true);
            mask.SetActive(true);
        }
    }

    private void DisplayChoices ()
    {
        gamePickCorrect.transform.gameObject.SetActive(true);
        boletimMetereologico.SetActive(false);
        buttonBoletim.gameObject.SetActive(true);
        instructions.SetActive(false);
        mask.SetActive(false);
    }
}
