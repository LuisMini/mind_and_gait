﻿using System.Collections;
using System.Collections.Generic;
using BansheeGz.BGSpline.Components;
using BansheeGz.BGSpline.Curve;
using UnityEngine;

public class Game_Trajectory : Game_Base
{
    public BZ_MouseClosestPoint scriptMouseClosestPoint;
    public BGCurve curveGameobject;
    private BGCcCursor cursor;

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();   
    }

    public override void TriggerAction(GameObject interactiveObject, string action = "none")
    {
        base.TriggerAction(interactiveObject, action);
        if (action == "TrajectoryDrag")
        {
            cursor = curveGameobject.GetComponent<BGCcCursor>();
            cursor.DistanceRatio = 1;

            scriptMouseClosestPoint.enabled = false;
            hasChosenAnswer = true;
            CheckVictory();
        }
    }
}
