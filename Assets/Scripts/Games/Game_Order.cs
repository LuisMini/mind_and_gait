﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Game_Order : Game_Base {

    public List<GameObject> correctOrder = new List<GameObject>();
    public List<GameObject> objects = new List<GameObject>();
    public List<GameObject> fillersToDelete = new List<GameObject>();
    public float minDistanceX;
    public float maxDistanceY;

    private bool XdistancesCorrect;
    private bool YdistancesCorrect;

    // Use this for initialization
    new void Start () {
        base.Start();
        stopwatch = new System.Diagnostics.Stopwatch();
	}
	
	// Update is called once per frame
	void Update () {
        if (!GameCompleted)
        {
            objects = correctOrder.OrderBy(correctOrder => correctOrder.transform.position.x).ToList();
            CheckOrder(objects[0]);
        }
    }

    private void CheckOrder(GameObject primeiro)
    {
        for (int i = 1; i < objects.Count; ++i)
        {
            if (objects[i].transform.localPosition.x >= (objects[i-1].transform.localPosition.x + minDistanceX)) //verificar distancia minima entre as casas no eixo do X (se true está correcto)
            {
                Debug.DrawLine(objects[i].transform.position, objects[i-1].transform.position, Color.blue); 
                if (i == objects.Count - 1)
                {
                    Debug.Log("X Distance Correct = True");
                    XdistancesCorrect = true;
                }
            }
            else
            {
                Debug.Log("X Distance Correct = False");
                XdistancesCorrect = false;
                return;
            }            

            if (objects[i].transform.localPosition.y > (primeiro.transform.localPosition.y - maxDistanceY) && objects[i].transform.localPosition.y < (primeiro.transform.localPosition.y + maxDistanceY)) //verificar distancias minima entre as casas no eixo do Y (se true está correcto)
            {
                Debug.DrawLine(new Vector3(objects[i].transform.position.x, objects[i].transform.position.y + 0.5f, objects[i].transform.position.z), new Vector3(primeiro.transform.position.x, primeiro.transform.position.y + 0.5f, primeiro.transform.position.z), Color.green);
                if (i == objects.Count - 1)
                {
                    Debug.Log("Y Distance Correct = True");
                    YdistancesCorrect = true;
                }
            }
            else
            {
                Debug.Log("Y Distance Correct = False");
                YdistancesCorrect = false;
                return;
            }
        }

        if (XdistancesCorrect && YdistancesCorrect)
        {
            Debug.Log("LALLALALALALA");
            for (int i = 0; i < objects.Count; ++i)
            {
                if (objects[i] != correctOrder[i])
                {
                    Debug.Log("POPOPOPOPOPOPO");
                    return;
                }
                else if(i == correctOrder.Count - 1)
                {
                    Debug.Log("VICTORY");
                    stopwatch.Stop();
                    Debug.Log(stopwatch.Elapsed);
                    base.CheckVictory();
                }
            }
        }
    }

    public override void LowerDifficulty()
    {
        foreach (var item in fillersToDelete)
        {
            item.gameObject.SetActive(false);
        }
        base.LowerDifficulty();
    }
}
