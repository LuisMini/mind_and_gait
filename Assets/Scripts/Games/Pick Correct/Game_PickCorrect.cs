﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
//using UnityEditor;

public class Game_PickCorrect : Game_Base
{
    [System.Serializable]
    public class ImageProperties
    {
        public Sprite Image;
        public bool correct;
        public bool difficulty_DontDisable;
        public string description;
        public GameObject ImagePrefab;
    }

    public ImageProperties[] ImagesList; //array that contains the images to be shown on screen and which are correct or incorrect
    public GameObject Prefab_Choice; //variable for the image container prefabs (prefab with the image component that will be displayed on screen)
    [HideInInspector]
    public int possibleCorrectAnswers; //number of correct answers    
    public int easyImageAmmount;
    public bool allowWrongAnswers = true;
    public bool noGridSetup;

    [Header("Grid Properties")]
    [SerializeField]
    public Vector2[,] Grid; //grid array
    [SerializeField]
    private int rows;
    [SerializeField]
    private int cols;
    [Range(0.0f, 50.0f)]
    public float spacingPercentage;
    private Vector2 spacing;
    private Vector2 cellSize;

    [Header("Components")]
    RectTransform parent_RectTransform;
    GridLayoutGroup gridLayoutGroup;

    private bool gameGenerated;

    private void OnEnable()
    {
        if (gameGenerated == false)
        {
            GenerateGame();
        }
    }

    new void Start()
    {
        base.Start();
        if (!noGridSetup)
        {
            SetupGridLayout();
        }
    }

    //faz alguns calculos relacionados com o jogo, depois instancia e atribui as opções aos prefabs 
    public void GenerateGame()
    {
        gameGenerated = true;
        foreach (ImageProperties element in ImagesList)
        {
            if (element.correct == true)
            {
                possibleCorrectAnswers++;
            }
        }

        //calculate cols and rows based on image ammount
        rows = (int)Mathf.Ceil(Mathf.Sqrt((float)ImagesList.Length));
        cols = rows;
        Grid = new Vector2[rows, cols];

        //instantiating the image containers (prefabs) and assigning each of them the correct image
        if (ImagesList[0].Image != null)
        {
            int imageID = 0;
            for (int row = 0; row < rows; row++)
            {
                for (int col = 0; col < cols; col++)
                {
                    if (imageID <= ImagesList.Length - 1)
                    {
                        GameObject GridElement = Instantiate(Prefab_Choice, Grid[row, col], transform.rotation, transform); //instanciates the on-screen option prefab
                        Choices GridElementScript = GridElement.GetComponent<Choices>(); //gets reference to the instantiated object' choices script
                        GridElementScript.correct = ImagesList[imageID].correct; // sets the prefab's script "correct" value to the list's value
                        GridElementScript.Image = ImagesList[imageID].Image; //sets the prefab's script "image" value to the list's image
                        GridElementScript.description = ImagesList[imageID].description;
                        ImagesList[imageID].ImagePrefab = GridElement; //ads the prefab to the array
                        imageID += 1;
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }
        else
        {
            foreach (var item in ImagesList)
            {
                Choices GridElementScript = item.ImagePrefab.GetComponentInChildren<Choices>();
                GridElementScript.correct = item.correct;
                GridElementScript.description = item.description;
            }
        }
    }

    public void SetupGridLayout()
    {
        Debug.Log("SETUP GRID LAYOUT");
        //initialization
        parent_RectTransform = GetComponent<RectTransform>();
        gridLayoutGroup = GetComponent<GridLayoutGroup>();
        Vector2 containerSize = parent_RectTransform.rect.size; //rect transform dimensions of the parent container

        //set the grid layout's constraints
        gridLayoutGroup.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
        gridLayoutGroup.constraintCount = cols;

        //calculate value of grid's spacing
        spacing = new Vector2((containerSize.x * (spacingPercentage / 100.0f)), (containerSize.y * (spacingPercentage / 100.0f)));
        cellSize = new Vector2((containerSize.x / (float)rows) - spacing.x, (containerSize.y / (float)cols) - spacing.y);

        //setting the value of grid's spacing and cellSize
        gridLayoutGroup.spacing = spacing;
        gridLayoutGroup.cellSize = cellSize;

        foreach (ImageProperties item in ImagesList)
        {
            item.ImagePrefab.GetComponent<Choices>().Initialize();
        }
    }

    public override void RestartGame()
    {
        Debug.Log("Restart Game");
        base.RestartGame();
        foreach (ImageProperties element in ImagesList) //por cada escolha possivel
        {
            element.ImagePrefab.SetActive(true);
            element.ImagePrefab.GetComponent<Choices>().Selected = false;
            element.ImagePrefab.GetComponent<Choices>().SelectionBorder.enabled = false;
        }
        firstTry = false;
        canRetry = false;
        hasChosenAnswer = false;
        chosenAnswersDescription = "";
        difficulty = ENUM_Difficulty.Alta;
        gameCompleted = false;
        manager.UpdateButtons();
    }

    public void ChoiceSelected(Choices selectedChoice)
    {
        //aceita respostas incorretas
        if (allowWrongAnswers)
        {
            hasChosenAnswer = true;

            if (possibleCorrectAnswers > 1)
            {
                //faz nada porque neste caso não preciso de remover seleções
            }
            else if (possibleCorrectAnswers == 1)
            {
                foreach (ImageProperties element in ImagesList) //por cada escolha possivel
                {
                    Choices ImagePrefabScript = element.ImagePrefab.GetComponent<Choices>();
                    if (ImagePrefabScript != selectedChoice) //se for diferente da escolha escolhida agora
                    {
                        ImagePrefabScript.Selected = false; //remover a seleção
                        ImagePrefabScript.SelectionBorder.enabled = false;
                    }
                }
            }
            manager.UpdateButtons();
        }
        //não aceita respostas incorretas
        else
        {
            int correctCounter = 0;
            int selectedCounter = 0;
            //ver se as choices corretas foram todas selecionadas
            foreach (ImageProperties element in ImagesList) //por cada escolha possivel
            {
                Choices ImagePrefabScript = element.ImagePrefab.GetComponent<Choices>();
                if (ImagePrefabScript.correct)
                {
                    ++correctCounter;
                }
                if (ImagePrefabScript.Selected)
                {
                    ++selectedCounter;
                }
            }
            if (correctCounter == selectedCounter)
            {
                hasChosenAnswer = true;
                CheckAnswers();
            }
        }

    }

    public void ChoiceDeselected(Choices deselectedChoice)
    {
        foreach (ImageProperties element in ImagesList) //por cada escolha possivel
        {
            Choices ImagePrefabScript = element.ImagePrefab.GetComponent<Choices>();
            if (ImagePrefabScript.Selected) //se estiver escolhida
            {
                hasChosenAnswer = true;
                return;
            }
            else
            {
                hasChosenAnswer = false;
            }
        }
        manager.UpdateButtons();
    }

    public override void CheckAnswers() //chamar isto quando carrega no terminei para validar as respostas
    {
        foreach (ImageProperties element in ImagesList)
        {
            Choices ImagePrefabScript = element.ImagePrefab.GetComponent<Choices>();

            if (ImagePrefabScript.Selected == true && ImagePrefabScript.correct) //resposta correcta
            {
                ++correctAnswers;
                ImagePrefabScript.SelectionBorder.color = ImagePrefabScript.colorCorrect;
            }
            else if (ImagePrefabScript.Selected == true && !ImagePrefabScript.correct) //resposta incorrecta
            {
                ++wrongAnswers;
                if (!canRetry)
                {
                    ImagePrefabScript.SelectionBorder.color = ImagePrefabScript.colorIncorrect;
                }
            }
            else if (ImagePrefabScript.Selected == false && ImagePrefabScript.correct) //resposta correcta não selecionada
            {
                ++unselectedCorrectAnswers;
            }
            
            if (ImagePrefabScript.Selected)
            {
                if (chosenAnswersDescription.Length > 0) //se a string ja conter items
                {
                    chosenAnswersDescription += (", " + ImagePrefabScript.description);
                }
                else
                {
                    chosenAnswersDescription += ImagePrefabScript.description;
                }
            }
        }

        if (wrongAnswers > 0 || unselectedCorrectAnswers > 0) //se foram dadas respostas erradas, ou não selecionadas respostas correctas
        {
            canRetry = true; //dizer que pode tentar de novo
        }
        base.CheckVictory();
        manager.UpdateButtons();
    }

    public override void LowerDifficulty()
    {
        if (gridLayoutGroup)
        {
            gridLayoutGroup.enabled = false;
        }
        Debug.Log("dificuldade");
        if (difficulty == ENUM_Difficulty.Alta && gameCompleted == false && easyImageAmmount > 0)
        {
            List<GameObject> wrongAnswersList = new List<GameObject>();

            foreach (ImageProperties element in ImagesList)
            {
                if (element.correct == false && !element.difficulty_DontDisable)
                {
                    wrongAnswersList.Add(element.ImagePrefab);
                }
            }

            int ammountToDelete = ImagesList.Length - easyImageAmmount;
            List<GameObject> objectsToDelete = new List<GameObject>();

            for (int i = 1; i <= ammountToDelete; i++)
            {
                int index = UnityEngine.Random.Range(0, wrongAnswersList.Count);
                objectsToDelete.Add(wrongAnswersList[index]);
                wrongAnswersList.RemoveAt(index);
            }

            foreach (GameObject element in objectsToDelete)
            {
                element.SetActive(false);
            }
            base.LowerDifficulty();
        }
    }

    public Vector2 Spacing
    {
        get
        {
            return spacing;
        }

        set
        {
            spacing = value;
        }
    }

    public Vector2 CellSize
    {
        get
        {
            return cellSize;
        }

        set
        {
            cellSize = value;
        }
    }

    public GridLayoutGroup GridLayoutGroup
    {
        get
        {
            return gridLayoutGroup;
        }

        set
        {
            gridLayoutGroup = value;
        }
    }
}

//[CustomEditor(typeof(Game_PickCorrect))]
//public class Game_PickCorrectEditor : Editor
//{
//    override public void OnInspectorGUI()
//    {
//        var script = target as Game_PickCorrect;

//        script.canChangeDifficulty = GUILayout.Toggle(script.canChangeDifficulty, "Can Change Difficulty:");

//        if(script.canChangeDifficulty)
//        {
//            script.EasyImageAmmount = EditorGUILayout.IntField("Easy Ammount:", script.EasyImageAmmount);
//        }
//    }
//}