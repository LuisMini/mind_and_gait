﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[System.Serializable]
public class Choices : MonoBehaviour {

    private Sprite image;
    public bool correct;
    public bool componentResizesBorder = true;
    public string description;
    private bool selected;
    private bool canClick = true;
    private Image imageComp;
    [SerializeField]
    private Game_PickCorrect gameScript;
    public EscolhaOrcamento scriptOrcamento;
    [SerializeField]
    private Image selectionBorder;
    //referencia ao script AspectLockRectTransform presente no gameobject deste script
    private AspectLockRectTransform aspectLockScripts;
    //script para a animação de shake
    private ObjectBlink animationScript;

    public Color colorChosen;
    public Color colorCorrect;
    public Color colorIncorrect;

    //initialization
    public void Start()
    {
        imageComp = GetComponent<Image>();

        animationScript = this.gameObject.AddComponent<ObjectBlink>();

        if (image != null)
        {
            imageComp.sprite = image;
        }
        Initialize();                
    }

    private void OnEnable()
    {
        if (componentResizesBorder)
        {
            ResizeBorder();
        }
    }

    public void Initialize()
    {
        Debug.Log("CHOICES - INITIALIZE");
        BoxCollider2D collider = transform.GetComponent<BoxCollider2D>();
        gameScript = transform.gameObject.GetComponentInParent<Game_PickCorrect>();
        if (gameScript != null && !gameScript.noGridSetup)
        {
            if (!transform.GetComponent<ColliderSize>())
            {
                transform.gameObject.AddComponent<ColliderSize>();
            }
            else
            {
                collider.size = gameScript.CellSize;
            }
        }
    }

    public void ResizeBorder ()
    {
        RectTransform borderRectTransform = selectionBorder.GetComponent<RectTransform>();
        borderRectTransform.sizeDelta = new Vector2((transform.GetComponent<RectTransform>().sizeDelta.x + 10), (transform.GetComponent<RectTransform>().sizeDelta.y + 10));
    }

    private void OnMouseDown()
    {
        if (!EventSystem.current.IsPointerOverGameObject()) // if no UI elements are getting the hit/hover
        {
            //se não for o jogo do orcamento
            if (!scriptOrcamento)
            {
                //se o jogo NÃO permitir seleção de wrong answers
                if (!gameScript.allowWrongAnswers)
                {
                    if (!selected && !gameScript.GameCompleted && !correct && !animationScript.animPlaying) //wrong answer, animate object, dont allow selection
                    {
                        Debug.Log("Choices - 1");
                        gameScript.wrongAnswers++;
                        animationScript.AnimateObject();
                        return;
                    }
                    else if (!selected && correct == true && !gameScript.GameCompleted) //correct answer, allow selection
                    {
                        Debug.Log("Choices - 2");
                        selected = true;
                        selectionBorder.enabled = true;
                        selectionBorder.color = colorCorrect;
                        gameScript.ChoiceSelected(this);
                        return;
                    }
                }
                //se o jogo PERMITIR seleção de wrong answers
                else
                {
                    if (!selected && !gameScript.GameCompleted) //select this choice
                    {
                        selected = true;
                        selectionBorder.enabled = true;
                        selectionBorder.color = colorChosen;
                        gameScript.ChoiceSelected(this);
                    }
                    else if (selected && !gameScript.GameCompleted) //unselect this choice
                    {
                        selected = false;
                        selectionBorder.enabled = false;
                        gameScript.ChoiceDeselected(this);
                    }
                }
            }
            //se for o jogo do orçamento
            if (scriptOrcamento)
            {
                if (scriptOrcamento && correct == true && !scriptOrcamento.GameCompleted) //correct choice
                {
                    selected = true;
                    selectionBorder.enabled = true;
                    scriptOrcamento.EscolherOrcamento(this.gameObject);
                    return;
                }
                else if (scriptOrcamento && correct == false && !scriptOrcamento.GameCompleted) //incorrect choice
                {
                    animationScript.enabled = true;
                    animationScript.AnimateObject();
                    scriptOrcamento.wrongAnswers++;
                }
            }            
        }
    }

    public Sprite Image
    {
        get
        {
            return image;
        }

        set
        {
            image = value;
        }
    }

    public bool Selected
    {
        get
        {
            return selected;
        }

        set
        {
            selected = value;
        }
    }

    public Image SelectionBorder
    {
        get
        {
            return selectionBorder;
        }

        set
        {
            selectionBorder = value;
        }
    }

    public bool CanClick
    {
        get
        {
            return canClick;
        }

        set
        {
            canClick = value;
        }
    }

}
