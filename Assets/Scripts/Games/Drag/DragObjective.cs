﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragObjective : MonoBehaviour
{
    public Drag occupyingObject;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (occupyingObject)
        {
            if (occupyingObject.gameObject == collision.gameObject)
            {
                if (occupyingObject.transform.position != this.transform.position)
                {
                    occupyingObject = null;
                }
            }
        }
    }
}
