﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class Drag : MonoBehaviour
{
    //private ObjectBlink animationScript;
    [Space(10)]
    public Game_Base gameManager;
    public string managerTriggerAction;
    public GameObject[] possibleObjectives;
    public bool multipleCorrectPositions;
    public GameObject correctObjective;
    public DragObjective currentObjective;
    public bool correctAnswer;

    [Header("Drag Objective Properties")]
    public bool shouldSnap = true;
    public bool canDrag = true;
    public bool triggerActionOnDrag;

    [Header("If object doesn't snap")]
    public float lerpTime = 1; //time it takes for the lerp to complete
    private Vector2 currentPosition;  //lerp starting position aka this object's transform.position
    private Vector2 objectivePosition; //lerp objective position aka objective object transform.position
    [SerializeField]
    private bool lerpPos; //should position Lerp be active
    private float currentLerpTime = 0; //current lerp time that changes according to += Time.DeltaTime
    private float timePercent; //value between 0 and 1 for the lerp
    [SerializeField]
    private bool isInCorrectPosition;
    public bool isInIncorrectPosition;

    [Header("Object Properties")]
    public bool resetOnMouseUp = false;
    private bool dragging = false;
    private float distance = 100; //distance for the mouse pos raycasting to screen
    private RectTransform rectTransform;
    private Vector2 startingPos;
    private Vector2 lastPos;
    private Vector3 offset;

    public bool IsInCorrectPosition
    {
        get
        {
            return isInCorrectPosition;
        }

        set
        {
            isInCorrectPosition = value;
        }
    }

    public Vector2 StartingPos
    {
        get
        {
            return startingPos;
        }

        set
        {
            startingPos = value;
        }
    }

    public bool Dragging
    {
        get
        {
            return dragging;
        }

        set
        {
            dragging = value;
        }
    }

    // Use this for initialization
    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        startingPos = rectTransform.anchoredPosition;
        lastPos = startingPos;
    }

    // Update is called once per frame
    void Update()
    {
        if (lerpPos)
        {
            if (!isInCorrectPosition || !isInIncorrectPosition)
            {
                currentLerpTime += Time.deltaTime;
                if (currentLerpTime >= lerpTime)
                {
                    currentLerpTime = lerpTime;
                }

                Debug.Log("Update - Lerp - Called by: " + transform.name);
                timePercent = currentLerpTime / lerpTime;
                transform.position = Vector2.Lerp(currentPosition, objectivePosition, timePercent);

                if ((Vector2)transform.position == objectivePosition) //se a posição que chegou for igual á posição POSSIVEL
                {
                    Debug.Log("Update - Pos Compare");
                    transform.position = objectivePosition;
                    if (multipleCorrectPositions) //se tiver varias posições possiveis
                    {
                        isInCorrectPosition = true;
                        isInIncorrectPosition = false;
                    }
                    else if ((Vector2)transform.position == (Vector2)correctObjective.transform.position) //se a posição que chegou for igual á posição CORRECTA
                    {
                        isInCorrectPosition = true;
                        isInIncorrectPosition = false;
                    }
                    else
                    {
                        isInIncorrectPosition = true;
                        isInCorrectPosition = false;
                    }
                    lerpPos = false;
                    gameManager.TriggerAction(this.gameObject, managerTriggerAction);
                }
            }
        }
    }

    private void OnMouseDrag()
    {
        Debug.Log("Mouse Drag");
        CheckScreenPos();
        if (dragging == true)
        {
            isInCorrectPosition = false;
            if (triggerActionOnDrag)
            {
                gameManager.TriggerAction(this.gameObject, managerTriggerAction);
            }
            Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
            Vector3 objPosition = Camera.main.ScreenToWorldPoint(mousePosition);
            objPosition.z = 0;
            rectTransform.position = objPosition + offset;
        }
    }

    private void OnMouseDown()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            if (canDrag == true)
            {
                if (gameManager)
                {
                    if (!gameManager.stopwatch.IsRunning)
                    {
                        gameManager.stopwatch.Start();
                    }
                }

                offset = transform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition);
                offset.z = 0;

                dragging = true;
                lerpPos = false;
                currentLerpTime = 0;
                currentObjective = null; ;
            }
        }
    }

    private void OnMouseUp()
    {
        //Debug.Log("Mouse Up");
        if (resetOnMouseUp && dragging)
        {
            ResetItem();
        }
        else
        {
            if (dragging == true)
            {
                dragging = false;
                lastPos = rectTransform.anchoredPosition;
            }
        }
    }

    private void CheckScreenPos()
    {
        Vector2 viewportPoint = Camera.main.WorldToViewportPoint(transform.position);
        if (viewportPoint.x > 0 && viewportPoint.x < 1 && viewportPoint.y > 0 && viewportPoint.y < 1)
        {
            //Debug.Log("Inside Viewport");
        }
        else
        {
            //Debug.Log("Outside Viewport");
            dragging = false;
            rectTransform.anchoredPosition = lastPos;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (!shouldSnap)
        {
            DragObjective dragObjective = collision.transform.gameObject.GetComponent<DragObjective>();
            if (!isInCorrectPosition || !isInIncorrectPosition)
            {
                foreach (GameObject possibleObj in possibleObjectives)
                {
                    if (collision.transform == possibleObj.transform && !dragging && lerpPos == false && !IsInCorrectPosition && !dragObjective.occupyingObject)
                    {
                        currentPosition = transform.position;
                        objectivePosition = collision.transform.position;
                        currentObjective = dragObjective;
                        dragObjective.occupyingObject = this;
                        lerpPos = true;
                    }
                }
            }
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (shouldSnap && correctObjective != null)
        {
            //Debug.Log("2");
            if (collision.transform == correctObjective.transform)
            {
                //Debug.Log("3");
                gameManager.TriggerAction(this.gameObject, managerTriggerAction);
            }
        }
    }

    public void ResetItem()
    {
        isInCorrectPosition = false;
        isInIncorrectPosition = false;
        currentObjective = null;
        lerpPos = false;
        canDrag = true;
        dragging = false;
        rectTransform.anchoredPosition = startingPos;
        lastPos = startingPos;
    }
}