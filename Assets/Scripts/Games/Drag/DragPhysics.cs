﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class DragPhysics : MonoBehaviour
{

    [Space(10)]
    public Game_Base gameManager;

    [Header("Drag Objective Properties")]
    public bool shouldSnap = true;
    public bool canDrag = true;
    public bool triggerActionOnDrag;

    [Header("Object Properties")]
    public bool resetOnMouseUp = false;
    private bool dragging = false;
    private float distance = 100; //distance for the mouse pos raycasting to screen
    private RectTransform rectTransform;
    private Rigidbody2D rigidBody;
    private Vector2 startingPos;
    private Vector2 lastPos;
    private Vector3 offset;

    // Start is called before the first frame update
    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        startingPos = rectTransform.anchoredPosition;
        lastPos = startingPos;
        rigidBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (dragging)
        {   
            Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
            Vector3 pos;
            pos.z = distance;
            pos = Camera.main.ScreenToWorldPoint(mousePosition);
            rigidBody.velocity = (pos - rectTransform.position) * 10;
        }
    }

    private void OnMouseDrag()
    {
        CheckScreenPos();
    }

    private void OnMouseUp()
    {
        if (dragging == true)
        {
            dragging = false;
            lastPos = rectTransform.anchoredPosition;
            rigidBody.velocity = Vector3.zero;
                rigidBody.bodyType = RigidbodyType2D.Static;
            
        }
    }

    private void OnMouseDown()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            if (canDrag == true)
            {
                if (gameManager)
                {
                    if (!gameManager.stopwatch.IsRunning)
                    {
                        gameManager.stopwatch.Start();
                    }
                }

                offset = transform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition);
                offset.z = 0;

                dragging = true;

                rigidBody.bodyType = RigidbodyType2D.Dynamic;

            }
        }
    }

    private void CheckScreenPos()
    {
        Vector2 viewportPoint = Camera.main.WorldToViewportPoint(transform.position);
        if (viewportPoint.x > 0 && viewportPoint.x < 1 && viewportPoint.y > 0 && viewportPoint.y < 1)
        {
            //Debug.Log("Inside Viewport");
        }
        else
        {
            //Debug.Log("Outside Viewport");
            dragging = false;
            rigidBody.velocity = Vector2.zero;
            rectTransform.anchoredPosition = lastPos;
            rigidBody.bodyType = RigidbodyType2D.Static;
        }
    }
}
