﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BansheeGz.BGSpline.Components;
using BansheeGz.BGSpline.Curve;

public class BZ_MouseClosestPoint : MonoBehaviour {

    [Header("Gane Manager")]
    public Game_Base gameManager;
    public string managerTriggerAction;
    [Header("Curves")]
    public GameObject curveGameobject;
    private BGCurve curve;
    private BGCcMath math;
    private BGCcCursor cursor;

    private Vector2 mousePos;    
    private BGCurveBaseMath.SectionPointInfo closestPoint;
    private Vector2 closestPointPos;
    public float maxDistance;
    public float maxJumpDistance;
    [SerializeField]
    private bool dragging = false;
    public bool canDrag = true;
    public bool onMouseUpReset = true;
    private bool objectiveReached = false;

    [Header("Content to move")]
    public GameObject mouseIndicator;
    public GameObject contentToMove;

    [Header("Animation Content")]
    public string animationAction;

    // Use this for initialization
    void Start () {
        math = curveGameobject.GetComponent<BGCcMath>();       
        cursor = curveGameobject.GetComponent<BGCcCursor>();               

        //Debug.Log(math.Math.SectionsCount);
        //for (int i = 0; i < math.Math.SectionsCount; ++i)
        //{
        //    Debug.Log(math.Math.SectionInfos[i].PointsCount);
        //}
    }

    private void OnMouseDrag()
    {
        if (dragging == true)
        {
            Vector2 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y);
            mousePos = Camera.main.ScreenToWorldPoint(mousePosition);
            CalculateClosestPoint();
        }
    }

    private void OnMouseDown()
    {
        if (dragging == false && canDrag == true)
        {
            dragging = true;
        }
    }

    private void OnMouseUp()
    {
        dragging = false;
        if (objectiveReached == false && onMouseUpReset == true)
        {
            mouseIndicator.transform.position = math.Math.SectionInfos[0].Points[0].Position;
            if(contentToMove != null)
            {
                contentToMove.transform.position = math.Math.SectionInfos[0].Points[0].Position;
            }
        }
    }

    private void CalculateClosestPoint()
    {
        var sections = math.Math.SectionInfos; //array com as sections todas
        var sectionsCount = sections.Count; //numero de sections

        //for loop para percorrer as sections
        for (int i = 0; i < sectionsCount; ++i)
        {
            var currentSection = sections[i]; //section actual
            int currentSectionInt = i;

            var points = currentSection.Points; //section's array of points
            var pointsCount = points.Count; //numero de points da section actual

            float closestDistance = 0;

            //for loop para percorrer os points
            for (int j = 0; j < pointsCount; ++j)
            {
                BGCurveBaseMath.SectionPointInfo currentPoint = points[j];
                Vector3 currentPointPos = currentPoint.Position;

                //float distanceToCurrentPoint = Vector3.Distance(math.CalcPositionByClosestPoint(mousePos), currentPointPos);
                float distanceToCurrentPoint = Vector3.Distance(mousePos, currentPointPos);

                if (j == 0)
                {
                    closestDistance = distanceToCurrentPoint;
                }

                if (distanceToCurrentPoint < closestDistance && distanceToCurrentPoint < maxDistance && (Vector3.Distance(mousePos, cursor.CalculatePosition())) < maxJumpDistance)
                {
                    closestDistance = distanceToCurrentPoint;
                    closestPointPos = currentPoint.Position;
                    closestPoint = currentPoint;

                    mouseIndicator.transform.position = math.CalcPositionByClosestPoint(closestPointPos);
                    if (contentToMove != null)
                    {
                        contentToMove.transform.position = new Vector3((math.CalcPositionByClosestPoint(closestPointPos).x), (math.CalcPositionByClosestPoint(closestPointPos).y), 0);
                    }
                    //----------------------------------------------- this works
                    //cursor.Distance = Vector3.Distance(closestPoint.Position, currentSection.Points[0].Position);
                    //Debug.Log(Vector3.Distance(closestPoint.Position, currentSection.Points[0].Position));

                    //percorer as sections para traz e adicionar á distancia
                    float prevSectionsDistance = 0;
                    for (int s = currentSectionInt -1; s >= 0; --s)
                    {
                        prevSectionsDistance += sections[s].Distance;
                    }

                    float currentSectionDistance = Vector3.Distance(closestPoint.Position, currentSection.Points[0].Position);

                    cursor.Distance = prevSectionsDistance + currentSectionDistance;

                    //cursor.Distance = Vector3.Distance(closestPoint.Position, math.Math.SectionInfos[0].Points[0].Position);
                    //Debug.Log(Vector3.Distance(closestPoint.Position, math.Math.SectionInfos[0].Points[0].Position));

                    //se este jogo for o Game_Resposta
                    if (gameManager.GetType() == typeof(Game_Resposta))
                    {
                        Game_Resposta gameScript = (Game_Resposta)gameManager;
                        gameScript.BzDistanceChanged(cursor, animationAction);
                    }
                    
                    //se chegou ao final
                    //if (closestPoint == currentSection.Points[pointsCount - 1])
                    //{
                    //    Debug.Log("OBJECTIVE REACHED");
                    //    gameManager.TriggerAction(contentToMove, managerTriggerAction);
                    //}

                    if (cursor.DistanceRatio > 0.97)
                    {
                        dragging = false;
                        Debug.Log("OBJECTIVE REACHED");
                        gameManager.TriggerAction(contentToMove, managerTriggerAction);
                    }
                }
            }
        }
    }

    // Update is called once per frame
    void Update ()
    {
        Debug.DrawLine(mousePos, closestPointPos, Color.black);
    }

}
