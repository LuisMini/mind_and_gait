﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BansheeGz.BGSpline.Components;
using BansheeGz.BGSpline.Curve;
using System;
using UnityEngine;
using UnityEngine.UI;

public class Game_Resposta : Game_Base {

    [Header("Notepad")]
    public GameObject notepad;
    public GameObject[] notepad_Objectives;

    [Header("Open Envelope")]
    public GameObject open_Envelope;
    public GameObject content;
    public GameObject curve_Content;
    public GameObject envelopeCloseAnim;
    public GameObject curve_Envelope;
    public Image envelopeToAnim;
    public Sprite[] sprites;
    private float spriteChangePercent;

    [Header("Closed Envelope")]
    public GameObject closed_Envelope;
    public GameObject selo;
    public GameObject morada;
    public GameObject mensagemReforço;
    public InputField[] inputFields;
    public Text[] legendas;

    // Use this for initialization
    new void Start () {
        base.Start();
        spriteChangePercent = 1f / sprites.Length;
    }
    
    public override void TriggerAction(GameObject interactiveObject, string action = "none")
    {
        if (action == "notepadToEnvelope")
        {
            Debug.Log("OH ZE MANEL");
            notepad.SetActive(false);
            content.SetActive(true);
            curve_Content.SetActive(true);
        }

        if (action == "ContentDrag")
        {
            curve_Content.SetActive(false);
            curve_Envelope.SetActive(true);
            open_Envelope.SetActive(false);
            envelopeCloseAnim.SetActive(true);
        }

        if (action == "FecharEnvelope")
        {
            curve_Envelope.SetActive(false);
            StartCoroutine(DelayEnvelopeLerp());
        }

        if (action == "EnvelopeClosed_Lerp")
        {
            selo.SetActive(true);
            //selo.GetComponent<Animator>().SetTrigger("Start_FadeIn");
            morada.SetActive(true);
            //morada.GetComponent<Animator>().SetTrigger("Start_FadeIn");
        }

        if (action == "Morada_Selo")
        {
            Drag seloScript = selo.GetComponent<Drag>();
            Drag moradaScript = morada.GetComponent<Drag>();

            if (seloScript.IsInCorrectPosition && moradaScript.IsInCorrectPosition)
            {
                seloScript.canDrag = false;
                moradaScript.canDrag = false;
                mensagemReforço.SetActive(true);
                base.CheckVictory();
            }
        }
    }

    public void CheckInputFields ()
    {
        int fieldsDone = 0;

        for (int i = 0; i < inputFields.Length; ++i)
        {
            if (inputFields[i].text != "")
            {
                ++fieldsDone;
            }
            if (i == inputFields.Length - 1 && fieldsDone == inputFields.Length) //se for o ultimo index
            {
                //activar o butão terminar
                hasChosenAnswer = true;
                manager.UpdateButtons();
            }
            else
            {
                hasChosenAnswer = false;
                manager.UpdateButtons();
            }
        }
    }

    public override void CheckAnswers()
    {
        //tirar os backgrounds dos input fields
        foreach (var item in inputFields)
        {
            item.interactable = false;
            item.GetComponent<Image>().enabled = false;
        }
        foreach (var item in legendas)
        {
            item.enabled = false;
        }
        //activar o can drag do objecto morada
        morada.GetComponent<Drag>().canDrag = true;
        hasChosenAnswer = false;
        manager.UpdateButtons();
    }

    private IEnumerator DelayEnvelopeLerp()
    {
        yield return new WaitForSeconds(1);
        envelopeCloseAnim.SetActive(false);
        closed_Envelope.SetActive(true);
        closed_Envelope.GetComponent<Lerp2D>().active = true;
    }

    public void BzDistanceChanged (BGCcCursor cursor, string action = "none")
    {
        if (action == "envelope_close")
        {
            if (cursor.DistanceRatio < spriteChangePercent)
            {
                envelopeToAnim.sprite = sprites[0];
            }
            else if (cursor.DistanceRatio > spriteChangePercent && cursor.DistanceRatio < spriteChangePercent * 2)
            {
                envelopeToAnim.sprite = sprites[1];
            }
            else if (cursor.DistanceRatio > spriteChangePercent * 2 && cursor.DistanceRatio < spriteChangePercent * 3)
            {
                envelopeToAnim.sprite = sprites[2];
            }
            else if (cursor.DistanceRatio > spriteChangePercent * 3 && cursor.DistanceRatio < spriteChangePercent * 4)
            {
                envelopeToAnim.sprite = sprites[3];
            }
            else if (cursor.DistanceRatio > spriteChangePercent * 4 && cursor.DistanceRatio < spriteChangePercent * 5)
            {
                envelopeToAnim.sprite = sprites[4];
            }
            else if (cursor.DistanceRatio > spriteChangePercent * 5 && cursor.DistanceRatio < spriteChangePercent * 6)
            {
                envelopeToAnim.sprite = sprites[5];
            }
            else if (cursor.DistanceRatio > spriteChangePercent * 6 && cursor.DistanceRatio < spriteChangePercent * 7)
            {
                envelopeToAnim.sprite = sprites[6];
            }
            else if (cursor.DistanceRatio > spriteChangePercent * 7 && cursor.DistanceRatio < spriteChangePercent * 8)
            {
                envelopeToAnim.sprite = sprites[7];
            }
            else if (cursor.DistanceRatio > spriteChangePercent * 8 && cursor.DistanceRatio < spriteChangePercent * 9)
            {
                envelopeToAnim.sprite = sprites[8];
            }
            else if (cursor.DistanceRatio > spriteChangePercent * 9 && cursor.DistanceRatio < spriteChangePercent * 10)
            {
                envelopeToAnim.sprite = sprites[9];
            }
            else if (cursor.DistanceRatio >= 1)
            {
                envelopeToAnim.sprite = sprites[10];
            }
        }
    }
}
