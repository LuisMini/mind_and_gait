﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;
using UnityEngine.UI;

public class Game_RespostaSentences : Game_Base {

    [Header("Sentences")]
    public GameObject[] drag_Sentences;

    [Header("Notepad")]
    public GameObject[] notepad_Objectives;

    [Header("Notepad - Drag")]
    public GameObject[] notepadDrag_Objectives;

    public List<GameObject> chosenAnswers;
    public List<GameObject> SortedList = new List<GameObject>();

    // Use this for initialization
    new void Start () {
        base.Start();
    }

    public override void CheckAnswers()
    {
        Debug.Log("Game Acabado");
        foreach (GameObject sentence in drag_Sentences)
        {
            if (sentence.GetComponent<Drag>().IsInCorrectPosition)
            {
                sentence.GetComponent<Drag>().canDrag = false; //desativar o drag depois de terminado o jogo
                chosenAnswers.Add(sentence); //guardar escolhas numa lista para organizar
                SortedList = chosenAnswers.OrderByDescending(o => o.transform.position.y).ToList(); //lista organizada
                if (sentence.GetComponent<Drag>().correctAnswer)
                {
                    ++correctAnswers;
                }
                else
                {
                    ++wrongAnswers;
                }
            }
        }
        for (int i = 0; i < notepadDrag_Objectives.Length; ++i) //copiar escolhas para o 2nd notepad
        {
            notepadDrag_Objectives[i].GetComponentInChildren<Text>().text = SortedList[i].GetComponentInChildren<Text>().text;
        }

        base.CheckVictory();
        manager.UpdateButtons();
    }

    public override void TriggerAction(GameObject interactiveObject, string action)
    {
        if (action == "CheckSentences")
        {
            Debug.Log("TRIGGER ACTION - CHECKSENTENCES");
            int counter = 0;
            foreach (GameObject sentence in drag_Sentences)
            {
                if (sentence.GetComponent<Drag>().IsInCorrectPosition)
                {
                    ++counter;
                    if (counter == 3) //se os tres slots estiverem ocupados o jogo pode terminar
                    {
                        hasChosenAnswer = true;
                    }
                    else
                    {
                        hasChosenAnswer = false;
                    }
                }
            }
            manager.UpdateButtons();
        }
    }
}
