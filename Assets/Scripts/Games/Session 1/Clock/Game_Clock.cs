﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_Clock : Game_Base {

    public GameObject pointerHours;
    public GameObject pointerMinutes;
    public GameObject drag_pointerHours;
    public GameObject drag_pointerMinutes;
    public int targetHour;
    public int targetMinutes;
    [SerializeField]
    private int currentHour;
    [SerializeField]
    private int currentMinutes;

    public int CurrentHour
    {
        get
        {
            return currentHour;
        }

        set
        {
            currentHour = value;
        }
    }

    public int CurrentMinutes
    {
        get
        {
            return currentMinutes;
        }

        set
        {
            currentMinutes = value;
        }
    }

    // Use this for initialization
    new void Start () {
        base.Start();
        //hasChosenAnswer = true;
	}

    public override void LowerDifficulty()
    {

    }

    public override void CheckAnswers()
    {
        if (targetHour == currentHour)
        {
            ++correctAnswers;
        }
        else
        {
            ++wrongAnswers;
        }
        if (targetMinutes == currentMinutes)
        {
            ++correctAnswers;
        }
        else
        {
            ++wrongAnswers;
        }
        base.CheckVictory();
        pointerHours.GetComponent<Rotate>().enabled = false;
        pointerMinutes.GetComponent<Rotate>().enabled = false;
    }
}
