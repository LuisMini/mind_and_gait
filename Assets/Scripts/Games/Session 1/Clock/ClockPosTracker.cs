﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClockPosTracker : MonoBehaviour {

#pragma warning disable CS0108 // Member hides inherited member; missing new keyword
    private BoxCollider2D collider;
#pragma warning restore CS0108 // Member hides inherited member; missing new keyword
    public int time;
    private Game_Clock gameScript;

	// Use this for initialization
	void Start () {
        collider = transform.GetComponent<BoxCollider2D>();
        collider.size = transform.GetComponent<RectTransform>().rect.size;
        collider.offset = new Vector2(0, transform.GetComponent<RectTransform>().rect.size.y / 2);
        gameScript = transform.parent.parent.parent.GetComponent<Game_Clock>();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision == gameScript.pointerHours.GetComponent<BoxCollider2D>())
        {
            gameScript.CurrentHour = time;
        }
        else if (collision == gameScript.pointerMinutes.GetComponent<BoxCollider2D>())
        {
            gameScript.CurrentMinutes = time;
        }
    }

}
