﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Globalization;
using UnityEngine.UI;
//using UnityEditor;

[System.Serializable]
public class Calendar : MonoBehaviour {

    public GameObject calendarPanel;
    public Text _yearNumText;
    public Text _monthNumText;
    public GameObject slot;
    public bool buttonsVisible = false;
    public GameObject buttonsContainer;
    public GameObject dayHeaderSlot;
    [SerializeField]
    public Vector3Int data_AMD;
    public bool correct;

    private List<GameObject> _dateItems = new List<GameObject>();
    private const int _totalDateNum = 42;
    private DateTime _dateTime;
    private static Calendar _calendarInstance;

    private void Start()
    {
        if (correct && ProfileManager.selectedProfile != null)
        {
            data_AMD.x = ProfileManager.selectedProfile.dateParty.Year;
            data_AMD.y = ProfileManager.selectedProfile.dateParty.Month;
            data_AMD.z = ProfileManager.selectedProfile.dateParty.Day;
        }
        else if (correct)
        {
            data_AMD.x = DateTime.Now.Year;
            data_AMD.y = DateTime.Now.Month;
            data_AMD.z = DateTime.Now.Day;
        }
        GenerateCalendar();
        ChangeDate();
    }

    public void DEBUGSIZES ()
    {
        Debug.Log("DEBUG SIZES!!!");
        GameObject slotsContainer = slot.transform.parent.gameObject;
        //Debug.Log(slot.transform.parent.gameObject);
        RectTransform slotsContainerTransform = slot.transform.parent.GetComponent<RectTransform>();
        Rect slotsContainerRect = slot.transform.parent.GetComponent<RectTransform>().rect;
        Debug.Log(slotsContainerTransform.name);
        //Debug.Log(RectTransformUtility.CalculateRelativeRectTransformBounds(slotsContainerTransform as RectTransform).size);
        Vector2 literalSize = RectTransformUtility.CalculateRelativeRectTransformBounds(slotsContainerTransform as RectTransform).size;
        Debug.Log("Literal Size: " + literalSize);
        //RectTransform rt = myUIGameObject.transform.GetComponent<RectTransform>();
        float w = slotsContainerTransform.sizeDelta.x * slotsContainerTransform.localScale.x;
        float h = slotsContainerTransform.sizeDelta.y * slotsContainerTransform.localScale.y;
        //Debug.Log(w + " x " + h);

        slotsContainer.GetComponent<GridLayoutGroup>().cellSize = new Vector2((literalSize.x / 7), (literalSize.y / 6));
        Debug.Log(literalSize.x / 7 + " x " + literalSize.y / 6);
    }

    void Initialize ()
    {
        _calendarInstance = this;

        RectTransform rt = slot.GetComponent<RectTransform>();
        rt.sizeDelta = dayHeaderSlot.GetComponent<RectTransform>().sizeDelta;

        Vector3 startPos = slot.transform.localPosition;
        DeleteDays();
        _dateItems.Clear();
        _dateItems.Add(slot);

        for (int i = 1; i < _totalDateNum; i++)
        {
            GameObject item = GameObject.Instantiate(slot, calendarPanel.transform) as GameObject;
            item.name = "Item" + (i + 1).ToString();
            item.transform.SetParent(slot.transform.parent);
            item.transform.localScale = Vector3.one;
            item.transform.localRotation = Quaternion.identity;

            _dateItems.Add(item);
        }

        _dateTime = DateTime.Now;
    }

    public void CreateCalendar()
    {
        DateTime firstDay = _dateTime.AddDays(-(_dateTime.Day - 1));
        int index = GetDays(firstDay.DayOfWeek);

        int date = 0;
        for (int i = 0; i < _totalDateNum; i++)
        {
            Text label = _dateItems[i].GetComponentInChildren<Text>();
            //_dateItems[i].SetActive(false);
            label.enabled = false;

            if (i >= index)
            {
                DateTime thatDay = firstDay.AddDays(date);
                if (thatDay.Month == firstDay.Month)
                {
                    _dateItems[i].GetComponentInChildren<Text>().enabled = true;
                    _dateItems[i].SetActive(true);

                    label.text = (date + 1).ToString();
                    date++;
                }
            }
        }
        _yearNumText.text = (_dateTime.Year.ToString());
        _monthNumText.text = GetMonth();
    }

    public void GenerateCalendar ()
    {        
        Initialize();
        CreateCalendar();
        GameObject slotsContainer = slot.transform.parent.gameObject;
        slotsContainer.GetComponent<GridLayout>().UpdateGrid();
        Debug.Log("Calendar Generated");
    }

    public void DeleteDays ()
    {
        int childCount = slot.transform.parent.childCount;

        for (int i = childCount-1; i > 0; --i)
        {
            GameObject child = slot.transform.parent.GetChild(i).gameObject;
            if (child != slot)
            DestroyImmediate(child);
        }
    }

    private string GetMonth ()
    {
        int mes = _dateTime.Month;

        switch (mes)
        #region 
        {
            case 1:
                return "Janeiro";
            case 2:
                return "Fevereiro";
            case 3:
                return "Março";
            case 4:
                return "Abril";
            case 5:
                return "Maio";
            case 6:
                return "Junho";
            case 7:
                return "Julho";
            case 8:
                return "Agosto";
            case 9:
                return "Setembro";
            case 10:
                return "Outubro";
            case 11:
                return "Novembro";
            case 12:
                return "Dezembro";
        }
        #endregion
        return "Erro";
    }

    int GetDays(DayOfWeek day)
    {
        switch (day)
        {
            case DayOfWeek.Monday: return 1;
            case DayOfWeek.Tuesday: return 2;
            case DayOfWeek.Wednesday: return 3;
            case DayOfWeek.Thursday: return 4;
            case DayOfWeek.Friday: return 5;
            case DayOfWeek.Saturday: return 6;
            case DayOfWeek.Sunday: return 0;
        }

        return 0;
    }

    public void ShowCalendar(Text target)
    {
        calendarPanel.SetActive(true);
    }

    public void YearPrev()
    {
        _dateTime = _dateTime.AddYears(-1);
        CreateCalendar();
    }

    public void YearNext()
    {
        _dateTime = _dateTime.AddYears(1);
        CreateCalendar();
    }

    public void MonthPrev()
    {
        _dateTime = _dateTime.AddMonths(-1);
        CreateCalendar();
    }

    public void MonthNext()
    {
        _dateTime = _dateTime.AddMonths(1);
        CreateCalendar();
    }

    public void MonthChange (int months)
    {
        _dateTime = _dateTime.AddMonths(months);
        //CreateCalendar();
    }

    public void YearChange(int years)
    {
        _dateTime = _dateTime.AddYears(years);
        //CreateCalendar();
    }

    public void ChangeDate ()
    {
        if (data_AMD.x != 0 && data_AMD.y != 0 && data_AMD.z != 0)
        {
            DateTime currentDate = DateTime.Now;
            int monthDifference = data_AMD.y - currentDate.Month;
            int yearDifference = data_AMD.x - currentDate.Year;
            MonthChange(monthDifference);
            YearChange(yearDifference);
            CreateCalendar();
            foreach (var item in _dateItems)
            {
                if (item.activeInHierarchy == true)
                {
                    Text slotDay = item.GetComponentInChildren<Text>();
                    if (slotDay.text == (data_AMD.z.ToString()))
                    {
                        item.GetComponent<Image>().color = new Color32(247,150,70,255);
                    }
                }
            }
        }
    }

}

//[CustomEditor(typeof(Calendar))]
//[System.Serializable]
//public class MyScriptEditor : Editor
//{
//    override public void OnInspectorGUI()
//    {
//        var script = target as Calendar;

//        script.calendarPanel = EditorGUILayout.ObjectField("Calendar Panel", script.calendarPanel, typeof(GameObject), true) as GameObject;
//        script._yearNumText = EditorGUILayout.ObjectField("Year Text", script._yearNumText, typeof(Text), true) as Text;
//        script._monthNumText = EditorGUILayout.ObjectField("Month Text", script._monthNumText, typeof(Text), true) as Text;
//        script.slot = EditorGUILayout.ObjectField("Slot", script.slot, typeof(GameObject), true) as GameObject;
//        script.dayHeaderSlot = EditorGUILayout.ObjectField("Day Header Slot", script.dayHeaderSlot, typeof(GameObject), true) as GameObject;
//        script.buttonsContainer = EditorGUILayout.ObjectField("Buttons Container", script.buttonsContainer, typeof(GameObject), true) as GameObject;
//        script.buttonsVisible = GUILayout.Toggle(script.buttonsVisible, "Buttons Visible?");
//        //script._ano = EditorGUILayout.IntField("Ano", script._ano);        
//        //script._mes = EditorGUILayout.IntField("Mes", script._mes);
//        //script._dia = EditorGUILayout.IntField("Dia", script._dia);
//        script.data_AMD = EditorGUILayout.Vector3IntField("Data A/M/D", script.data_AMD);

//        if (GUILayout.Button("Generate Calendar"))
//        {
//            script.GenerateCalendar();
//            script.ChangeDate();
//        }

//        if (GUILayout.Button("Delete Slots"))
//        {
//            script.DeleteDays();
//        }

//        if (script.buttonsVisible == true)
//        #region
//        {
//            script.buttonsContainer.SetActive(true);
//        }
//        else
//        {
//            script.buttonsContainer.SetActive(false);
//        }
//        #endregion


//    }
//}
