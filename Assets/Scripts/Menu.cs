﻿using System.Collections;
using System.Collections.Generic;
using System.Timers;
using System.Threading;
using System.Globalization;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Menu : MonoBehaviour {

    public Text text_date;
    public Text text_hours;
    public Text text_timer;
    private DateTime dataHora = new DateTime();

    private Manager manager;

    // Use this for initialization
    void Start () {
        dataHora = DateTime.Now;
        text_date.text = dataHora.ToString("dd-MM-yy");

        manager = GameObject.FindGameObjectWithTag("Manager").GetComponent<Manager>();
    }
	
	// Update is called once per frame
	void Update () {
        dataHora = DateTime.Now;
        text_hours.text = dataHora.ToString("HH:mm");

        //atualiza o cronometro no menu
        if(manager.menuScript) { 
            DateTime timer = new DateTime(manager.stopwatch.ElapsedTicks);
            text_timer.text = string.Format("{0:00}:{1:00}:{2:00}", timer.Hour, timer.Minute, timer.Second);
        }
        else
        {
            manager.menuScript = this;
        }
    }

    public void ReturnToProfileSelection()
    {
        ProfileManager.returnToSessionSelection = true;
        SceneManager.LoadScene("Profiles");
    }

    public void RestartSession()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void SkipActivity()
    {
        manager.SkipActivity();
    }

    public void PreviousPanel ()
    {
        manager.PreviousPanel();
    }
}
