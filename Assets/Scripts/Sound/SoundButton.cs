﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundButton : MonoBehaviour {

    [Header("Audio Properties")]
    public AudioClip soundClip;
    [Range(0, 1)]
    public float volume = 1;
    [Header("Button Sprites")]
    public Sprite spritePlaying;
    public Sprite spriteNotPlaying;
    [SerializeField]
    private SoundManager soundManager;
    private Image image;

    [HideInInspector]
    public bool isPlaying;

    public SoundManager SoundManager
    {
        get
        {
            return soundManager;
        }

        set
        {
            soundManager = value;
        }
    }

    // Use this for initialization
    void Start () {
        Initialize();
    }

    private void OnEnable()
    {
        Initialize();
    }

    private void Initialize()
    {
        soundManager = (SoundManager)FindObjectOfType(typeof(SoundManager));
        image = transform.GetChild(0).GetComponent<Image>();
    }

    //ao carregar no botão, se este butão estiver OFF fáz play ao som e mete este butão ON || se este butão estiver ON diz ao manager para parar o som e mete este butão OFF 
    public void ButtonPress ()
    {
        if (!isPlaying)
        {
            soundManager.PlaySound(soundClip, this, volume);
            TurnON();
        }
        else 
        {
            soundManager.StopPlayingAll();
            TurnOFF();
        }
        
    }

    private void ButtonState () {
        
        if (image)
        {
            if (!isPlaying)
            {
                image.sprite = spritePlaying;
            }
            else
            {
                image.sprite = spriteNotPlaying;
            }
        }
    }

    private void TurnON ()
    {
        isPlaying = true;
        if (image)
        {
            image.sprite = spritePlaying;
        }
    }

    public void TurnOFF()
    {
        isPlaying = false;
        if (image)
        {
            image.sprite = spriteNotPlaying;
        }
    }
}
