﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {
    [HideInInspector]
    public AudioSource audioSource;
    private SoundButton currentButtonPlaying;

	// Use this for initialization
	void Start () {
        audioSource = GetComponent<AudioSource>();
	}

    private void Update()
    {
        if (currentButtonPlaying != null)
        {
            if (!audioSource.isPlaying)
            {
                currentButtonPlaying.TurnOFF();
                currentButtonPlaying = null;
            }
        }
    }

    public void PlaySound (AudioClip soundToPlay, SoundButton button, float volume)
    {
        if (audioSource.isPlaying)
        {
            Debug.Log("PlaySound - Playing");
            audioSource.Stop();
            if (currentButtonPlaying != null)
            {
                currentButtonPlaying.TurnOFF();
                currentButtonPlaying = null;
            }
        }
        audioSource.volume = volume;
        audioSource.clip = soundToPlay;
        audioSource.Play();
        currentButtonPlaying = button;
    }

    public void StopPlayingAll ()
    {
        if (audioSource && audioSource.isPlaying)
        {
            audioSource.Stop();
            if (currentButtonPlaying != null)
            {
                currentButtonPlaying.TurnOFF();
                currentButtonPlaying = null;
            }
        }
    }
    
}
