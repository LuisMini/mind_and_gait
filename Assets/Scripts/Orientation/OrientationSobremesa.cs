﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrientationSobremesa : MonoBehaviour
{

    public Game_PickCorrect gamePickCorrect;

    // Start is called before the first frame update
    void Start()
    {
        gamePickCorrect = GetComponent<Game_PickCorrect>();
        gamePickCorrect.possibleCorrectAnswers = 1;

        if (ProfileManager.selectedProfile != null)
        {
            foreach (var item in gamePickCorrect.ImagesList)
            {
                if (item.ImagePrefab.GetComponent<OrientationSobremesaChoice>().sobremesa == ProfileManager.selectedProfile.dessert)
                {
                    item.correct = true;
                    item.ImagePrefab.GetComponent<Choices>().correct = true;
                }
                else
                {
                    item.correct = false;
                    item.ImagePrefab.GetComponent<Choices>().correct = false;
                }
            }
        }
        else
        {
            foreach (var item in gamePickCorrect.ImagesList) //meter todos a true porque não existe profile selected
            {
                item.ImagePrefab.GetComponent<Choices>().correct = true;
            }
        }
    }
}
