﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrientationEmblem : MonoBehaviour
{
    public GameObject emblem1;
    public GameObject emblem2;
    public GameObject emblem3;
    public GameObject emblem4;
    private GameObject activeEmblem;

    public List<Sprite> patterns = new List<Sprite>();

    // Start is called before the first frame update
    void Start()
    {
        //base.Start();
        if (ProfileManager.selectedProfile != null)
        {
            Debug.Log(ProfileManager.selectedProfile.chosenEmblem.ToDescription());
            switch (ProfileManager.selectedProfile.chosenEmblem)
            {
                case ENUM_Emblema.Emblem_1:
                    emblem1.SetActive(true);
                    activeEmblem = emblem1;
                    break;

                case ENUM_Emblema.Emblem_2:
                    emblem2.SetActive(true);
                    activeEmblem = emblem2;
                    break;

                case ENUM_Emblema.Emblem_3:
                    emblem3.SetActive(true);
                    activeEmblem = emblem3;
                    break;

                case ENUM_Emblema.Emblem_4:
                    emblem4.SetActive(true);
                    activeEmblem = emblem4;
                    break;
            }

            for (int i = 0; i < activeEmblem.GetComponent<Emblem>().emblemPieces.Count; ++i)
            {
                Debug.Log("Emblema " + activeEmblem.name + " | Peça: " + i + " | Name: " + activeEmblem.GetComponent<Emblem>().emblemPieces[i].gameObject);
                int numeroDoPadrao = ProfileManager.selectedProfile.emblemPatterns[i];
                activeEmblem.GetComponent<Emblem>().emblemPieces[i].image.sprite = patterns[numeroDoPadrao];
            }
        }
        else
        {
            emblem1.SetActive(true);
            activeEmblem = emblem1;

            for (int i = 0; i < activeEmblem.GetComponent<Emblem>().emblemPieces.Count; ++i)
            {
                Debug.Log("Emblema " + activeEmblem.name + " | Peça: " + i + " | Name: " + activeEmblem.GetComponent<Emblem>().emblemPieces[i].gameObject);
                activeEmblem.GetComponent<Emblem>().emblemPieces[i].image.sprite = patterns[0];
            }
        }
    }   
}
