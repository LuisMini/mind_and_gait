﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class OrientationOrcamento : Game_Base {

    public Text correctChoice;
    public Game_PickCorrect gamePickCorrect;

    [Header("Sound Clips")]
    public SoundButton correctSoundButton;
    public AudioClip S_13euros;
    public AudioClip S_17euros;
    public AudioClip S_22euros;
    public AudioClip S_33euros;

    private new void Start()
    {
        base.Start();
        Initialize();
    }

    private void OnEnable()
    {
        Initialize();
    }

    private void Initialize ()
    {
        gamePickCorrect = transform.GetComponentInChildren<Game_PickCorrect>();
        if (ProfileManager.selectedProfile != null)
        {
            correctChoice.text = ProfileManager.selectedProfile.budget_chosen.ToString() + " €";
            Debug.Log(ProfileManager.selectedProfile.budget_chosen.ToString());

            switch (ProfileManager.selectedProfile.budget_chosen.ToString())
            {
                case "13":
                    correctSoundButton.soundClip = S_13euros;
                    return;
                case "17":
                    correctSoundButton.soundClip = S_17euros;
                    return;
                case "22":
                    correctSoundButton.soundClip = S_22euros;
                    return;
                case "33":
                    correctSoundButton.soundClip = S_33euros;
                    return;
            }
        }
        else
        {
            correctChoice.text = "Não Existe Profile";
        }
    }
}
