﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;
using System.Globalization;

public class OrientationPanel : MonoBehaviour
{
    public bool completed;
    [Space]

    [SerializeField]
    private SoundButton soundButton;
    public Manager manager;

    [Header("Components")]
    public Text compTitle;
    public GameObject questTextEmpty;
    public GameObject questTextCompleted;
    [Space]
    public Text evento;
    public Text date;
    public Text local;
    public Text adress;
    public Text hourBegin;
    public Text hourEnd;
    public Text toTake;
    public Text budget;
    public Text dessert;
    public Text chapéu;

    [TextArea(6,15)]
    [SerializeField]
    private string textEmpty;
    [Header("Audio")]
    public List<AudioClip> soundClipsEmpty = new List<AudioClip>();
    public List<AudioClip> soundClipsCompleted = new List<AudioClip>();
    public int currentSoundIndex;
    public bool canceledSound;

    [Header("Sound Clips Orçamento")]
    public AudioClip S_13euros;
    public AudioClip S_17euros;
    public AudioClip S_22euros;
    public AudioClip S_33euros;
    [Header("Sound Clips Sobremesa")]
    public AudioClip S_BoloLaranja;
    public AudioClip S_ArrozDoce;

    private void OnEnable()
    {
        Initialize();
    }

    private void Initialize()
    {
        if (manager == null)
        {
            manager = GameObject.FindGameObjectWithTag("Manager").GetComponent<Manager>();
        }
        if (manager && compTitle)
        {
            compTitle.text = "Cartão de Orientação Nº " + ((int)manager.currentSession + 1);
        }

        if (completed)
        {
            questTextEmpty.SetActive(false);
            questTextCompleted.SetActive(true);

            //validar valor da data da festa no perfil
            if (date && ProfileManager.selectedProfile != null)
            {
                CultureInfo culture = new CultureInfo(0x0816);
                date.text = "<b>Data:</b> " + ProfileManager.selectedProfile.dateParty.ToString("dddd, dd MMMMM yyyy", culture);
            }
            else
            {
                date.text = "<b>Data:</b> No Profile Selected";
            }

            if (local)
            {
                local.text = "<b>Local:</b> Salão de Festas.";
            }
            if (adress)
            {
                adress.text = "<b>Morada para resposta ao convite:</b> Rua da Alegria Nº 17.";
            }
            if (hourBegin)
            {
                hourBegin.text = "<b>Hora de Início:</b> 19:15 horas";
            }
            if (hourEnd)
            {
                hourEnd.text = "<b>Hora de Fim:</b> 22:30 horas";
            }
            if (toTake)
            {
                toTake.text = "<b>Tenho que levar:</b> Sobremesa e dinheiro.";
            }

            //validar valor do budget no perfil
            if (budget && ProfileManager.selectedProfile != null)
            {
                budget.text = "<b>Orçamento:</b> " + ProfileManager.selectedProfile.budget_chosen.ToString() + " Euros";             
                switch (ProfileManager.selectedProfile.budget_chosen.ToString())
                {
                    case "13":
                        soundClipsCompleted[7] = S_13euros;
                        break;
                    case "17":
                        soundClipsCompleted[7] = S_17euros;
                        break;
                    case "22":
                        soundClipsCompleted[7] = S_22euros;
                        break;
                    case "33":
                        soundClipsCompleted[7] = S_33euros;
                        break;
                }
            }
            else if (budget)
            {
                budget.text = "<b>Orçamento:</b> No Profile Selected";
                soundClipsCompleted[7] = S_33euros;
            }

            //validar valor da sobremesa no perfil
            if (dessert && ProfileManager.selectedProfile != null)
            {
                dessert.text = "<b>Sobremesa:</b> " + ProfileManager.selectedProfile.dessert.ToDescription();
                switch (ProfileManager.selectedProfile.dessert)
                {
                    case ENUM_Sobremesa.ArrozDoce:
                        soundClipsCompleted[8] = S_ArrozDoce;
                        break;
                    case ENUM_Sobremesa.BoloDeLaranja:
                        soundClipsCompleted[8] = S_BoloLaranja;
                        break;
                }
            }
            else if (dessert)
            {
                dessert.text = "<b>Sobremesa:</b> No Profile Selected";
                soundClipsCompleted[8] = S_BoloLaranja;
            }

            //validar valor do chapéu no perfil
            if (chapéu && ProfileManager.selectedProfile != null)
            {
                chapéu.text = "<b>Chapéu:</b> De cor castanho personalizado com um emblema.";
            }
            else if (chapéu)
            {
                chapéu.text = "<b>Chapéu:</b> No Profile Selected";
            }
        }

        else
        {
            if (questTextEmpty)
            {
                questTextEmpty.GetComponent<Text>().text = textEmpty;
                questTextEmpty.SetActive(true);
            }
            
            if (questTextCompleted) questTextCompleted.SetActive(false);
        }
    }

    public void SoundButtonUIPressed ()
    {
        if (soundButton.isPlaying)
        {
            Debug.Log("SoundButtonUIPressed");
            StopAllCoroutines();
            soundButton.ButtonPress();
            currentSoundIndex = 0;
        }
        else
        {
            SoundButton();
        }
    }

    private void SoundButton ()
    {
        if (completed)
        {
            currentSoundIndex = 0;
            soundButton.soundClip = soundClipsCompleted[currentSoundIndex];
            soundButton.ButtonPress();
            StartCoroutine(CompletedCard_PlayNextSound());
        }
        else
        {
            currentSoundIndex = 0;
            soundButton.soundClip = soundClipsEmpty[currentSoundIndex];
            soundButton.ButtonPress();
            StartCoroutine(EmptyCard_PlayNextSound());
        }
    }

    private IEnumerator EmptyCard_PlayNextSound()
    {
        //int rounded = (int)Math.Ceiling(soundButton.soundClip.length);
        yield return new WaitForSeconds(soundButton.soundClip.length + 0.01f);

        if (soundClipsEmpty.Count > currentSoundIndex + 1) //se o index for valido
        {
            ++currentSoundIndex;
            SoundButton();
        }
        else
        {
            StopAllCoroutines();
            currentSoundIndex = 0;
        }
    }

    private IEnumerator CompletedCard_PlayNextSound()
    {
        //int rounded = (int)Math.Ceiling(soundButton.soundClip.length);
        yield return new WaitForSeconds(soundButton.soundClip.length + 0.1f);

        if (soundClipsCompleted.Count > currentSoundIndex + 1) //se o index for valido
        {
            ++currentSoundIndex;
            SoundButton();
        }
        else
        {
            StopAllCoroutines();
            currentSoundIndex = 0;
        }
    }
}