﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class SummaryScreen : Game_Base {

    [Header("Components")]
    public GameObject checkmark;
    private Slider slider;
    public Text title;

    [Header("Animations")]
    public float progress = 0;
    public float progressToAdd = 0.01f;
    public float speed = 0;
    public float progressBarDelay = 2;

    [Header("Results API")]
    public GameObject popUpWindow;
    private bool saved = false;
    private bool updateDisable;

    new void Start () {
        base.Start();
        slider = transform.GetComponentInChildren<Slider>();
        checkmark.GetComponent<Animator>().SetTrigger("Start_FadeIn");
        InvokeRepeating("ProgressBar", 2.0f, speed);
        title.text = "Concluiu com sucesso a sessão Nº " + ((int)manager.currentSession + 1);
    }

    private void Update()
    {
        if (progress >= 1 && !updateDisable)
        {
            updateDisable = true;
            CancelInvoke();
            base.CheckVictory();
            if (!saved)
            { 
                if (ProfileManager.selectedProfile != null)
                {
                    if (ProfileManager.selectedProfile.completedSessions <= (int)manager.currentSession)
                    {
                        ProfileManager.selectedProfile.completedSessions += 1;
                        ProfileManager.selectedProfile.lastSession = manager.currentSession;
                    }
                    ProfileManager.selectedProfile.totalDuration = ProfileManager.selectedProfile.totalDuration + manager.stopwatch.Elapsed;
                    ProfileManager.Save();

                    StartCoroutine(UploadSessionData());
                    StartCoroutine(ProfileManager.APIUpdatePatientProfile());
                }
                saved = true;
            }
        }
    }

    private void ProgressBar ()
    {
        progress += progressToAdd;
        slider.value = progress;
    }

    private IEnumerator UploadSessionData()
    {
        Debug.Log("UPLOAD SESSION DATA - POST");
        string json = "";
        string url = "";

        switch (manager.currentSession)
        {
            case ENUM_Sessions.Session1:
                {
                    url = "https://mind-gait.esenfc.pt/api/first-individual-activity/create";
                    ProfileManager.selectedProfile.session1.patient_id = ProfileManager.selectedProfile.patientID;
                    json = JsonUtility.ToJson(ProfileManager.selectedProfile.session1);
                    Debug.Log("UPLOAD SESSION 1 DATA - POST: json= " + json);
                    break;
                }
            case ENUM_Sessions.Session2:
                {
                    Debug.Log("UPLOAD SESSION 2 DATA - POST: json= " + json);
                    url = "https://mind-gait.esenfc.pt/api/second-individual-activity/create";
                    ProfileManager.selectedProfile.session2.patient_id = ProfileManager.selectedProfile.patientID;
                    if (ProfileManager.selectedProfile.session2.opinion_difficulty == "" || ProfileManager.selectedProfile.session2.opinion_session == "")
                    {
                        ProfileManager.selectedProfile.session2.opinion_difficulty = ENUM_Review_Difficulty.MuitoFácil.ToDescription();
                        ProfileManager.selectedProfile.session2.opinion_session = ENUM_Review_Like.GosteiMuito.ToDescription();
                    }
                    json = JsonUtility.ToJson(ProfileManager.selectedProfile.session2);
                    break;
                }
            case ENUM_Sessions.Session3:
                {
                    Debug.Log("UPLOAD SESSION 3 DATA - POST: json= " + json);
                    url = "https://mind-gait.esenfc.pt/api/third-individual-activity/create";
                    ProfileManager.selectedProfile.session3.patient_id = ProfileManager.selectedProfile.patientID;
                    if (ProfileManager.selectedProfile.session3.opinion_difficulty == "" || ProfileManager.selectedProfile.session3.opinion_session == "")
                    {
                        ProfileManager.selectedProfile.session3.opinion_difficulty = ENUM_Review_Difficulty.MuitoFácil.ToDescription();
                        ProfileManager.selectedProfile.session3.opinion_session = ENUM_Review_Like.GosteiMuito.ToDescription();
                    }
                    json = JsonUtility.ToJson(ProfileManager.selectedProfile.session3);
                    break;
                }
            case ENUM_Sessions.Session4:
                {
                    Debug.Log("UPLOAD SESSION 4 DATA - POST: json= " + json);
                    url = "https://mind-gait.esenfc.pt/api/fourth-individual-activity/create";
                    ProfileManager.selectedProfile.session4.patient_id = ProfileManager.selectedProfile.patientID;
                    if (ProfileManager.selectedProfile.session4.opinion_difficulty == "" || ProfileManager.selectedProfile.session4.opinion_session == "")
                    {
                        ProfileManager.selectedProfile.session4.opinion_difficulty = ENUM_Review_Difficulty.MuitoFácil.ToDescription();
                        ProfileManager.selectedProfile.session4.opinion_session = ENUM_Review_Like.GosteiMuito.ToDescription();
                    }
                    json = JsonUtility.ToJson(ProfileManager.selectedProfile.session4);
                    break;
                }
            case ENUM_Sessions.Session5:
                {
                    Debug.Log("UPLOAD SESSION 5 DATA - POST: json= " + json);
                    url = "https://mind-gait.esenfc.pt/api/fifth-individual-activity/create";
                    ProfileManager.selectedProfile.session5.patient_id = ProfileManager.selectedProfile.patientID;
                    if (ProfileManager.selectedProfile.session5.opinion_difficulty == "" || ProfileManager.selectedProfile.session5.opinion_session == "")
                    {
                        ProfileManager.selectedProfile.session5.opinion_difficulty = ENUM_Review_Difficulty.MuitoFácil.ToDescription();
                        ProfileManager.selectedProfile.session5.opinion_session = ENUM_Review_Like.GosteiMuito.ToDescription();
                    }
                    json = JsonUtility.ToJson(ProfileManager.selectedProfile.session5);
                    break;
                }
            case ENUM_Sessions.Session6:
                {
                    Debug.Log("UPLOAD SESSION 6 DATA - POST: json= " + json);
                    url = "https://mind-gait.esenfc.pt/api/sixth-individual-activity/create";
                    ProfileManager.selectedProfile.session6.patient_id = ProfileManager.selectedProfile.patientID;
                    if (ProfileManager.selectedProfile.session6.opinion_difficulty == "" || ProfileManager.selectedProfile.session6.opinion_session == "")
                    {
                        ProfileManager.selectedProfile.session6.opinion_difficulty = ENUM_Review_Difficulty.MuitoFácil.ToDescription();
                        ProfileManager.selectedProfile.session6.opinion_session = ENUM_Review_Like.GosteiMuito.ToDescription();
                    }
                    json = JsonUtility.ToJson(ProfileManager.selectedProfile.session6);
                    break;
                }
            case ENUM_Sessions.Session7:
                {
                    Debug.Log("UPLOAD SESSION 7 DATA - POST: json= " + json);
                    url = "https://mind-gait.esenfc.pt/api/seventh-individual-activity/create";
                    ProfileManager.selectedProfile.session7.patient_id = ProfileManager.selectedProfile.patientID;
                    if (ProfileManager.selectedProfile.session7.opinion_difficulty == "" || ProfileManager.selectedProfile.session7.opinion_session == "")
                    {
                        ProfileManager.selectedProfile.session7.opinion_difficulty = ENUM_Review_Difficulty.MuitoFácil.ToDescription();
                        ProfileManager.selectedProfile.session7.opinion_session = ENUM_Review_Like.GosteiMuito.ToDescription();
                    }
                    json = JsonUtility.ToJson(ProfileManager.selectedProfile.session7);
                    break;
                }
            case ENUM_Sessions.Session8:
                {
                    Debug.Log("UPLOAD SESSION 8 DATA - POST: json= " + json);
                    url = "https://mind-gait.esenfc.pt/api/eighth-individual-activity/create";
                    ProfileManager.selectedProfile.session8.patient_id = ProfileManager.selectedProfile.patientID;
                    if (ProfileManager.selectedProfile.session8.opinion_difficulty == "" || ProfileManager.selectedProfile.session8.opinion_session == "")
                    {
                        ProfileManager.selectedProfile.session8.opinion_difficulty = ENUM_Review_Difficulty.MuitoFácil.ToDescription();
                        ProfileManager.selectedProfile.session8.opinion_session = ENUM_Review_Like.GosteiMuito.ToDescription();
                    }
                    json = JsonUtility.ToJson(ProfileManager.selectedProfile.session8);
                    break;
                }
        }

        using (UnityWebRequest www = new UnityWebRequest(url, "POST"))
        {
            Debug.Log(json);
            byte[] bodyRaw = Encoding.UTF8.GetBytes(json);
            www.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
            www.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
            www.SetRequestHeader("Authorization", "Bearer " + ProfileManager.acessToken);
            www.SetRequestHeader("Accept", "application/json");
            www.SetRequestHeader("Content-Type", "application/json");
            yield return www.SendWebRequest();

            if (Application.internetReachability == NetworkReachability.NotReachable) //não está conectado à internet
            {
                PopUpWindow popUp = Instantiate(popUpWindow, transform.position, transform.rotation, GameObject.Find("Canvas").transform).GetComponent<PopUpWindow>();
                string errorString = "Erro: Não está conectado à internet, resultados da sessão irão ser guardados apenas localmente.";
                popUp.Initialize(errorString, this, "SucessfullPatientCreation");
            }
            else //está conectado à internet
            {
                if (www.isNetworkError || www.isHttpError) //deu erro
                {
                    Debug.Log(www.error);
                    PopUpWindow popUp = Instantiate(popUpWindow, transform.position, transform.rotation, GameObject.Find("Canvas").transform).GetComponent<PopUpWindow>();
                    popUp.Initialize("Erro: " + www.error + "\n" + "Resultados da sessão irão ser guardados apenas localmente");
                }
                else //não deu erro
                {
                    Debug.Log("Session Data Uploaded Sucessfully: " + www.downloadHandler.text);
                }
            }
        }
    }
}
