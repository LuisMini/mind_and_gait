﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameObjectSearcher {

    public static T FindComponentInChildWithTag<T>(this GameObject parent, string tag) where T : Component
    {
        Transform parentTransform = parent.transform;
        foreach (Transform child in parentTransform)
        {
            if (child.tag == tag)
            {
                return child.GetComponent<T>();
            }
        }
        return null;
    }

    public static T FindComponentInParentsWithTag<T>(this GameObject childGameobject, string tag) where T : Component
    {
        Transform childTransform = childGameobject.transform;
        while (childTransform.parent != null)
        {
            if (childTransform.parent.tag == tag)
            {
                return childTransform.parent.GetComponent<T>();
            }
            childTransform = childTransform.parent.transform;
        }
        return null; // Could not find a parent with given tag.        
    }

    public static GameObject FindChildWithTag(GameObject parent, string tag)
    {
        Transform transform = parent.transform;
        foreach (Transform child in transform)
        {
            if (child.gameObject.tag == tag)
            {
                return child.gameObject;
            }
        }
        return null;
    }

}
