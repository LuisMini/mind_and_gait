﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ValidateInput : MonoBehaviour
{
    private InputField inputField;
    public List<char> allowedChars;
    //public TouchScreenKeyboardType keyboardType = TouchScreenKeyboardType.Default;

    // Start is called before the first frame update
    void Start()
    {
        inputField = GetComponent<InputField>();
        inputField.onValidateInput += delegate (string input, int charIndex, char addedChar) { return MyValidate(addedChar); };
        //inputField.keyboardType = keyboardType;
        //inputField.characterValidation = InputField.CharacterValidation.None;
    }

    public char MyValidate(char charToValidate)
    {
        for (int i = 0; i < allowedChars.Count; ++i)
        {
            //Debug.Log(i.ToString());
            if (allowedChars[i] == charToValidate) //character is allowed
            {
                break;
            }
            else
            {
                if ((i + 1) == allowedChars.Count) //character is not allowed
                {
                    charToValidate = '\0';
                    break;
                }
            }
        }
        return charToValidate;
    }
}
