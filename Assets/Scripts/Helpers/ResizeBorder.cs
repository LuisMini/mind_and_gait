﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

[ExecuteInEditMode]
public class ResizeBorder : MonoBehaviour {

    private Image selectionBorder;
    private RectTransform borderRectTransform;    

	// Use this for initialization
	void Start () {
        selectionBorder = transform.gameObject.FindComponentInChildWithTag<Image>("SelectionBorder");
        borderRectTransform = selectionBorder.GetComponent<RectTransform>();
        Resize();
	}

    private void OnEnable()
    {
        selectionBorder = transform.gameObject.FindComponentInChildWithTag<Image>("SelectionBorder");
        borderRectTransform = selectionBorder.GetComponent<RectTransform>();
        Resize();
    }

    private void OnRectTransformDimensionsChange()
    {
        Resize();
    }

    public void Resize()
    {
        if (borderRectTransform)
        {
            borderRectTransform.sizeDelta = new Vector2((transform.GetComponent<RectTransform>().sizeDelta.x + 10), (transform.GetComponent<RectTransform>().sizeDelta.y + 10));
        }
    }
}
