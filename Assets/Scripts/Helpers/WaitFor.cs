﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class WaitFor {

    public static IEnumerator Frames(int frameCount)
    {
        if (frameCount <= 0)
        {
            throw new System.Exception("Cannot wait for less that 1 frame");
        }

        while (frameCount > 0)
        {
            frameCount--;
            yield return null;
        }
    }
    
    public static IEnumerator WaitForSeconds(int seconds)
    {
        yield return WaitForSeconds(seconds);
    }
}
