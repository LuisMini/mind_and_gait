﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lerp2D : MonoBehaviour {

    [Header("Game Manager")]
    public Game_Base gameManager;
    public string managerTriggerAction;

    [Header("Lerp")]
    public bool active; //se começar active dá trigger on Start()
    public GameObject target;
    public float lerpTime; //time it takes for the lerp to complete    
    private Vector2 targetPos; //target gameobject's transform.position (target position of the lerp)
    private Vector2 originPos; //this gameobject transform.position (starting position of the lerp)  
    private float currentLerpTime = 0;
    private float timePercent;

	// Use this for initialization
	void Start () {
        Initialize();
	}
	
	// Update is called once per frame
	void Update () {
		if (active)
        {
            Lerp();
        }
	}

    private void Initialize()
    {
        originPos = transform.position;
        targetPos = target.transform.position;
        timePercent = 0;
        currentLerpTime = 0;
    }

    public void Lerp ()
    {
        currentLerpTime += Time.deltaTime;
        if (currentLerpTime >= lerpTime)
        {
            currentLerpTime = lerpTime;
        }

        timePercent = currentLerpTime / lerpTime;
        transform.position = Vector2.Lerp(originPos, targetPos, timePercent);

        if ((Vector2)transform.position == targetPos)
        {
            transform.position = targetPos;
            active = false;
            gameManager.TriggerAction(this.gameObject, managerTriggerAction);
        }
    }

}
