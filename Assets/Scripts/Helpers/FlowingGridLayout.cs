﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEditor;
using UnityEngine.UI;

public class FlowingGridLayout : MonoBehaviour {

    [Header("Container Properties")]
    [SerializeField]
    private GameObject container;
    [SerializeField]
    private Rect container_Rect;
    [SerializeField]
    private RectTransform container_RectTransform;
    public float containerWidth;
    public float containerHeight;
    public int rows = 1;

    [SerializeField]
    private List<GameObject> items = new List<GameObject>();

    [Header("Slot Properties")]
    public float cellHeight;
    public float currentRowWidth;
    public float currentItemWidth;

    // Use this for initialization
    void Start () {
        Initialize();
    }

    private void OnEnable()
    {
        Initialize();
    }

    public void UpdateGrid ()
    {
        Initialize();
        StartCoroutine(SetupGrid());
    }

    private void Initialize ()
    {
        //Debug.Log("Initialize");
        rows = 1;
        cellHeight = 0;
        currentRowWidth = 0;
        currentItemWidth = 0;

        container = transform.gameObject;
        container_RectTransform = container.GetComponent<RectTransform>();
        container_Rect = container_RectTransform.rect;
        containerWidth = container_Rect.width;        

        items.Clear();
        foreach (Transform child in transform)
        {
            items.Add(child.gameObject);
        }        
    } 

    private IEnumerator SetupGrid ()
    {
        Debug.Log("Got to Coroutine: Setup Grid");
        yield return new WaitForEndOfFrame();

        Vector2 position = new Vector2();

        foreach (var item in items)
        {
            RectTransform itemRectTransform = item.GetComponent<RectTransform>();
            currentItemWidth = itemRectTransform.rect.width;

            //calcular se esta cell é a mais alta da grid, para ajustar o tamanho da linha
            if (itemRectTransform.rect.height > cellHeight)
            {
                cellHeight = itemRectTransform.rect.height;
            }

            // if elemento caber na linha
            if (CheckItemFits())
            {
                // meter elemento na posição certa X e Y
                position.x = currentRowWidth;
                itemRectTransform.anchoredPosition = position;
                currentRowWidth += currentItemWidth;                
            }
            // else se não couber na linha
            else
            {
                // passar para a proxima linha e voltar a checkar o elemento
                position.y -= cellHeight;
                ++rows;
                position.x = 0;
                itemRectTransform.anchoredPosition = position;
                currentRowWidth = currentItemWidth;
            }
        }

        Debug.Log("For Each End");

        containerHeight = cellHeight * rows;
        container_RectTransform.sizeDelta = new Vector2(containerWidth, (rows * cellHeight));
    }

    private bool CheckItemFits ()
    {
        if ((currentRowWidth + currentItemWidth) <= containerWidth)
        {            
            return true;
        }
        else
        {
            return false;
        }        
    }

    public void ClearGrid ()
    {
        foreach (var item in items)
        {
            if (item.GetComponent<Tag>() != null)
            {
                item.GetComponent<Tag>().DeleteTag();
            }
            else
            {
                Destroy(item);
            }
        }
        container_RectTransform.sizeDelta = new Vector2(containerWidth, 0);
    }
}

//[CustomEditor(typeof(FlowingGridLayout))]
//[System.Serializable]
//public class ScriptEditor : Editor
//{
//    override public void OnInspectorGUI()
//    {
//        var script = target as FlowingGridLayout;

//        if (GUILayout.Button("Update Grid"))
//        {
//            script.UpdateGrid();            
//        }

//        EditorGUILayout.PropertyField(serializedObject.FindProperty("items"), true);
//        EditorGUILayout.PropertyField(serializedObject.FindProperty("container"), true);
//        EditorGUILayout.PropertyField(serializedObject.FindProperty("container_Rect"), true);
//        EditorGUILayout.PropertyField(serializedObject.FindProperty("container_RectTransform"), true);

//        script.containerWidth = EditorGUILayout.FloatField("Container Width", script.containerWidth);
//        script.containerHeight = EditorGUILayout.FloatField("Container Height", script.containerHeight);
//        script.rows = EditorGUILayout.IntField("Rows", script.rows);
//        script.cellHeight = EditorGUILayout.FloatField("Cell Height", script.cellHeight);
//    }
//}
