﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ColliderSize : MonoBehaviour {

#pragma warning disable CS0108 // Member hides inherited member; missing new keyword
    private BoxCollider2D collider;
#pragma warning restore CS0108 // Member hides inherited member; missing new keyword
    private Rect rect;
    [Range(0, 100)]
    public float percentageHorizontal = 100;
    [Range(0, 100)]
    public float percentageVertical = 100;
    public bool offsetCenterHorizontal = false;
    public bool offsetCenterVertical = false;

    // Use this for initialization
    void Start () {
        UpdateCollider();
	}

    private void OnEnable()
    {
        UpdateCollider();
    }

    private void OnRectTransformDimensionsChange()
    {
        UpdateCollider();
    }

    private void OnValidate()
    {
        UpdateCollider();
    }

    private void UpdateCollider ()
    {
        collider = transform.GetComponent<BoxCollider2D>();
        rect = transform.GetComponent<RectTransform>().rect;

        if (offsetCenterHorizontal == true)
        {
            collider.offset = new Vector2((rect.size.x / 2), collider.offset.y);
        }
        if (offsetCenterVertical == true)
        {
            collider.offset = new Vector2(collider.offset.x, -(rect.size.y / 2));
        }

        collider.size = new Vector2((rect.size.x * (percentageHorizontal / 100)), (rect.size.y * (percentageVertical / 100)));
    }
}
