﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class GridLayout : MonoBehaviour {
    [SerializeField]
    private GameObject container;
    [SerializeField]
    private List<GameObject> items;
    [SerializeField]
    private Vector2 containerSize;
    [SerializeField]
    private Vector2 itemSize;

    [Header("Grid Properties")]
    [Range(0,100)]
    public float spacing;
    public int cols;
    public int rows;

    // Use this for initialization
    void Start () {
        container = transform.gameObject;

        foreach (Transform child in transform)
        {
            items.Add(child.gameObject);
        }
        
        UpdateGrid();
    }

    // Update is called once per frame
    void Update () {
		
	}

    private void OnRectTransformDimensionsChange()
    {
        UpdateGrid();
    }

    private void OnValidate()
    {
        UpdateGrid();
    }

    public void UpdateGrid ()
    {
        CalculateItemDimensions();
        SetupElements();
    }

    private void CalculateItemDimensions ()
    {

        container = transform.gameObject;
        items.Clear();
        foreach (Transform child in transform)
        {
            items.Add(child.gameObject);
        }
        containerSize = container.GetComponent<RectTransform>().rect.size;
        itemSize = new Vector2(((containerSize.x / cols) - spacing), ((containerSize.y / rows) - spacing));

    }

    private void SetupElements ()
    {
        Vector2 position = new Vector2(0 + (spacing / 2), 0 - (spacing / 2));
                
        int itemID = 0;
        for (int row = 0; row < rows; ++row)
        {
            for (int col = 0; col < cols; col++)
            {
                if (itemID < items.Count)
                {
                    RectTransform itemRectTransform = items[itemID].GetComponent<RectTransform>();
                    itemRectTransform.anchorMax = new Vector2(0, 1);
                    itemRectTransform.anchorMin = new Vector2(0, 1);
                    itemRectTransform.pivot = new Vector2(0, 1);
                    itemRectTransform.sizeDelta = itemSize;
                    itemRectTransform.anchoredPosition = position;
                    position.x += (itemSize.x + spacing);
                    ++itemID;
                }
            }
            position.y -= (itemSize.y + (spacing));
            position.x = (0 + (spacing / 2));

        }         
    }
}
