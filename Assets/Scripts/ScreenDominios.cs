﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScreenDominios : Game_Base
{
    [Header("Cognitive Domains")]
    public GameObject grid;
    public List<CognitiveDomains> cognitiveDomains = new List<CognitiveDomains>();
    private List<Text> cognitiveDomainsText = new List<Text>();
    [Header("Sessions")]
    public GameObject sessionBarsContainer;
    private List<Image> sessionBars = new List<Image>();
    public Color32 sessionComplete;
    public Color32 sessionIncomplete;
    [Header("Components")]
    private Button button;

    [System.Serializable]
    public class CognitiveDomains
    {
        public string text;
        public bool enable;
    }

    new void Start()
    {
        base.Start();
        button = GetComponentInChildren<Button>(true);
        button.onClick.AddListener(ButtonFinishedSession);
        sessionBars = new List<Image>(sessionBarsContainer.GetComponentsInChildren<Image>());
        cognitiveDomainsText = new List<Text>(grid.GetComponentsInChildren<Text>());

        int currentSessionID = (int)manager.currentSession + 1;
        for (int i = 1; i < sessionBars.Count; ++i)
        {
            if (i <= currentSessionID)
            {
                sessionBars[i].color = sessionComplete;
            }
            else
            {
                sessionBars[i].color = sessionIncomplete;
            }
        }

        for (int i = 0; i < cognitiveDomains.Count; ++i)
        {
            if (cognitiveDomains[i].enable)
            {
                cognitiveDomainsText[i].gameObject.GetComponentInChildren<Animator>().SetTrigger("Start_FadeIn");
            }
        }

        Invoke("FinishedSession", 2f);
    }

    private void FinishedSession()
    {
        button.gameObject.SetActive(true);
    }

    /// <summary>
    /// Function for the button that appears on the last session's screen
    /// </summary>
    private void ButtonFinishedSession()
    {
        ProfileManager.returnToSessionSelection = true;
        SceneManager.LoadScene("Profiles");
    }
}
